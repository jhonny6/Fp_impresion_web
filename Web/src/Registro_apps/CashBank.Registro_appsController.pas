unit CashBank.Registro_appsController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TRegistro_appsCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateRegistro_appsCmd = class(TRegistro_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadRegistro_appsCmd = class(TRegistro_appsCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateRegistro_appsCmd = class(TRegistro_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteRegistro_appsCmd = class(TRegistro_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickRegistro_appsCmd = class(TReadRegistro_appsCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Registro_appsBrowser,
  CashBank.Registro_appsModel,
  CashBank.Registro_appsView,
  FP.Main;

{ TRegistro_appsCommand }

class function TRegistro_appsCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TRegistro_appsModel;
end;

class function TRegistro_appsCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TRegistro_appsBrowser;
end;

class function TRegistro_appsCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TRegistro_apps;
end;

{ TCreateRegistro_appsCmd }

procedure TCreateRegistro_appsCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadRegistro_appsCmd }

procedure TReadRegistro_appsCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadRegistro_appsCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateRegistro_appsCmd }

procedure TUpdateRegistro_appsCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteRegistro_appsCmd }

procedure TDeleteRegistro_appsCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickRegistro_appsCmd }

procedure TPickRegistro_appsCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateRegistro_appsCmd, TReadRegistro_appsCmd, TUpdateRegistro_appsCmd, TDeleteRegistro_appsCmd, TPickRegistro_appsCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateRegistro_appsCmd, TReadRegistro_appsCmd, TUpdateRegistro_appsCmd, TDeleteRegistro_appsCmd, TPickRegistro_appsCmd]);

end.
