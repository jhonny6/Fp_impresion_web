unit CashBank.Registro_appsView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, uniCheckBox, uniDBCheckBox,
  Vcl.DBActns, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton;

type
  TRegistro_apps = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBCheckBox1: TUniDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
