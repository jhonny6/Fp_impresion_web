inherited Registro_apps: TRegistro_apps
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Registro_apps'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 260
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'id_usuario'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Usuario'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'fecha_hora'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha registro'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 86
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ip'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Ip'
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 126
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'sistema_operativo'
      DataSource = DataSource
      TabOrder = 3
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sistema operarivo'
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 166
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'token_refresh'
      DataSource = DataSource
      TabOrder = 4
      LayoutConfig.Margin = '20'
      FieldLabel = 'Token refresh'
    end
    object UniDBEdit6: TUniDBEdit
      Left = 56
      Top = 215
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'guid_dispositivo'
      DataSource = DataSource
      TabOrder = 6
      LayoutConfig.Margin = '20'
      FieldLabel = 'Guid dispositivo'
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 243
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'acceso_denegado'
      DataSource = DataSource
      Caption = ''
      TabOrder = 5
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Acceso denegado'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
end
