unit CashBank.Registro_appsModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TRegistro_appsModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TRegistro_appsModel }

class function TRegistro_appsModel.TableName: string;
begin
  Result := 'cashbank.registro_apps';
end;

end.
