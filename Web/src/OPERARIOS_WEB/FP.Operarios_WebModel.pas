unit FP.Operarios_WebModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TOperarios_WebModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Operarios_WebModel: TOperarios_WebModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TOperarios_WebModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Operarios_Web';
end;

function TOperarios_WebModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TOperarios_WebModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TOperarios_WebModel.TableName: string;
begin
  Result := 'fastence.fp0_Operarios_Web';
end;

end.
