unit FP.Operarios_WebController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TOperarios_WebCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateOperarios_WebCmd = class(TOperarios_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadOperarios_WebCmd = class(TOperarios_WebCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateOperarios_WebCmd = class(TOperarios_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteOperarios_WebCmd = class(TOperarios_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickOperarios_WebCmd = class(TReadOperarios_WebCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Operarios_WebBrowser,
  FP.Operarios_WebModel,
  FP.Operarios_WebView,
  FP.Main;

{ TProvinciasCommand }

function TOperarios_WebCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TOperarios_WebModel;
end;

function TOperarios_WebCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TOperarios_WebBrowser;
end;

function TOperarios_WebCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TOperarios_Web;
end;

{ TCreateProvinciasCmd }

procedure TCreateOperarios_WebCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateOperarios_WebCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteOperarios_WebCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickOperarios_WebCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateOperarios_WebCmd, TReadOperarios_WebCmd, TUpdateOperarios_WebCmd, TDeleteOperarios_WebCmd, TPickOperarios_WebCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateOperarios_WebCmd, TReadOperarios_WebCmd, TUpdateOperarios_WebCmd, TDeleteOperarios_WebCmd, TPickOperarios_WebCmd]);

end.
