unit FP.Zonas_PreparacionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TZonas_PreparacionModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Zonas_PreparacionModel: TZonas_PreparacionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TZonas_PreparacionModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_ZONAS_PREP_GEST';
end;

function TZonas_PreparacionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TZonas_PreparacionModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TZonas_PreparacionModel.TableName: string;
begin
  Result := 'fastence.fp0_Zonas_Preparacion';
end;

end.
