unit FP.Zonas_PreparacionController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TZonas_PreparacionCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateZonas_PreparacionCmd = class(TZonas_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadZonas_PreparacionCmd = class(TZonas_PreparacionCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateZonas_PreparacionCmd = class(TZonas_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteZonas_PreparacionCmd = class(TZonas_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickZonas_PreparacionCmd = class(TReadZonas_PreparacionCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Zonas_PreparacionBrowser,
  FP.Zonas_PreparacionModel,
  FP.Zonas_PreparacionView,
  FP.Main;

{ TProvinciasCommand }

function TZonas_PreparacionCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TZonas_PreparacionModel;
end;

function TZonas_PreparacionCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TZonas_PreparacionBrowser;
end;

function TZonas_PreparacionCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TZonas_Preparacion;
end;

{ TCreateProvinciasCmd }

procedure TCreateZonas_PreparacionCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateZonas_PreparacionCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteZonas_PreparacionCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickZonas_PreparacionCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateZonas_PreparacionCmd, TReadZonas_PreparacionCmd, TUpdateZonas_PreparacionCmd, TDeleteZonas_PreparacionCmd, TPickZonas_PreparacionCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateZonas_PreparacionCmd, TReadZonas_PreparacionCmd, TUpdateZonas_PreparacionCmd, TDeleteZonas_PreparacionCmd, TPickZonas_PreparacionCmd]);

end.
