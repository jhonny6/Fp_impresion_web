unit FP.Tipos_InventarioController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTipos_InventarioCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateTipos_InventarioCmd = class(TTipos_InventarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTipos_InventarioCmd = class(TTipos_InventarioCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTipos_InventarioCmd = class(TTipos_InventarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTipos_InventarioCmd = class(TTipos_InventarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTipos_InventarioCmd = class(TReadTipos_InventarioCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Tipos_InventarioBrowser,
  FP.Tipos_InventarioModel,
  FP.Tipos_InventarioView,
  FP.Main;

{ TProvinciasCommand }

function TTipos_InventarioCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTipos_InventarioModel;
end;

function TTipos_InventarioCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTipos_InventarioBrowser;
end;

function TTipos_InventarioCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTipos_Inventario;
end;

{ TCreateProvinciasCmd }

procedure TCreateTipos_InventarioCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateTipos_InventarioCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteTipos_InventarioCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickTipos_InventarioCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTipos_InventarioCmd, TReadTipos_InventarioCmd, TUpdateTipos_InventarioCmd, TDeleteTipos_InventarioCmd, TPickTipos_InventarioCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTipos_InventarioCmd, TReadTipos_InventarioCmd, TUpdateTipos_InventarioCmd, TDeleteTipos_InventarioCmd, TPickTipos_InventarioCmd]);

end.
