inherited Tipos_Inventario: TTipos_Inventario
  Caption = 'Tipos_Inventario'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit3: TUniDBEdit [3]
    Left = 56
    Top = 153
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 56
    Top = 79
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 56
    Top = 116
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'TIPO'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Tipo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit4: TUniDBEdit [6]
    Left = 56
    Top = 191
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ACTIVO'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Activo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
