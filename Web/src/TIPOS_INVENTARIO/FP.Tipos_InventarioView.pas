unit FP.Tipos_InventarioView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Vcl.DBActns,
  System.Actions, Vcl.ActnList, uniMainMenu, Data.DB, uniStatusBar,
  uniGUIClasses, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit;

type
  TTipos_Inventario = class(TEntityView)
    UniDBEdit3: TUniDBEdit;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Tipos_Inventario: TTipos_Inventario;

implementation

{$R *.dfm}

end.
