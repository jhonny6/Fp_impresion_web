unit FP.Tipos_InventarioModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTipos_InventarioModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Tipos_InventarioModel: TTipos_InventarioModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TTipos_InventarioModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP05_Tipos_Inventario';
end;

function TTipos_InventarioModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TTipos_InventarioModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TTipos_InventarioModel.TableName: string;
begin
  Result := 'fastence.fp05_Tipos_Inventario';
end;

end.
