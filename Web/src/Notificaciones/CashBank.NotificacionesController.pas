unit CashBank.NotificacionesController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TNotificacionesCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateNotificacionesCmd = class(TNotificacionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadNotificacionesCmd = class(TNotificacionesCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateNotificacionesCmd = class(TNotificacionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteNotificacionesCmd = class(TNotificacionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickNotificacionesCmd = class(TReadNotificacionesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.NotificacionesBrowser,
  CashBank.NotificacionesModel,
  CashBank.NotificacionesView,
  FP.Main;

{ TNotificacionesCommand }

class function TNotificacionesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TNotificacionesModel;
end;

class function TNotificacionesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TNotificacionesBrowser;
end;

class function TNotificacionesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TNotificaciones;
end;

{ TCreateNotificacionesCmd }

procedure TCreateNotificacionesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadNotificacionesCmd }

procedure TReadNotificacionesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadNotificacionesCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateNotificacionesCmd }

procedure TUpdateNotificacionesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteNotificacionesCmd }

procedure TDeleteNotificacionesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickNotificacionesCmd }

procedure TPickNotificacionesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateNotificacionesCmd, TReadNotificacionesCmd, TUpdateNotificacionesCmd, TDeleteNotificacionesCmd, TPickNotificacionesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateNotificacionesCmd, TReadNotificacionesCmd, TUpdateNotificacionesCmd, TDeleteNotificacionesCmd, TPickNotificacionesCmd]);

end.
