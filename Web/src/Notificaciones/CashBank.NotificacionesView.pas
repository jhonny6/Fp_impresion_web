unit CashBank.NotificacionesView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, uniLabel, uniButton, uniBitBtn,
  uniSpeedButton, Vcl.DBActns, uniScrollBox;

type
  TNotificaciones = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
