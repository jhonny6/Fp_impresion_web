unit CashBank.NotificacionesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TNotificacionesModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TNotificacionesModel }

class function TNotificacionesModel.TableName: string;
begin
  Result := 'cashbank.notificaciones';
end;

end.
