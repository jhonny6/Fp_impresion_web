unit FP.MultiUbicacionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TMultiUbicacionModel = class(TEntityModel)
  private
    function SQLFindAll: string;
    function SQLFindAll_with_FK: string;

    { Private declarations }
  public
    { Public declarations }
    class function TableName: string; override;
  end;

var
  MultiUbicacionModel: TMultiUbicacionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


function TMultiUbicacionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TMultiUbicacionModel.SQLFindAll_with_FK: string;
//var
//  JoinModel: TEntityModelClass;
//  JoinTableName: string;
//  JoinPKName: string;
//  JoinColumn: string;
//  FKName: string;
begin
//  JoinModel := TMonedasModel;
//  JoinPKName := JoinModel.PKName;
//  JoinTableName := JoinModel.TableName;
//  JoinColumn := 'nombre_corto as "monedas.moneda"';
//  FKName := 'id_moneda';
//  Result := Format('select %0:s.*, %1:s.%4:s from %0:s left outer join %1:s on(%0:s.%2:s = %1:s.%3:s)', [TableName, JoinTableName, FKName,
//    JoinPKName, JoinColumn]);

   Result := 'select * from '+TableName;
end;

class function TMultiUbicacionModel.TableName: string;
begin
  Result := 'fastence.fp0_multiubicacion';
end;

end.
