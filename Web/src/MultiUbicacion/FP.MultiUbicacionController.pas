unit FP.MultiUbicacionController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TMultiUbicacionCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateMultiUbicacionCmd = class(TMultiUbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadMultiUbicacionCmd = class(TMultiUbicacionCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateMultiUbicacionCmd = class(TMultiUbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteMultiUbicacionCmd = class(TMultiUbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickMultiUbicacionCmd = class(TReadMultiUbicacionCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  FP.MultiUbicacionBrowser,
  FP.MultiUbicacionModel,
  FP.MultiUbicacionView,
  FP.Main;

{ TMultiUbicacionCommand }

function TMultiUbicacionCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TMultiUbicacionModel;
end;

function TMultiUbicacionCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TMultiUbicacionBrowser;
end;

function TMultiUbicacionCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TMultiUbicacion;
end;

{ TCreateMultiUbicacionCmd }

procedure TCreateMultiUbicacionCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadMultiUbicacionCmd }

{
procedure TReadMultiUbicacionCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadMultiUbicacionCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateMultiUbicacionCmd }

procedure TUpdateMultiUbicacionCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteMultiUbicacionCmd }

procedure TDeleteMultiUbicacionCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickMultiUbicacionCmd }

procedure TPickMultiUbicacionCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateMultiUbicacionCmd, TReadMultiUbicacionCmd, TUpdateMultiUbicacionCmd, TDeleteMultiUbicacionCmd, TPickMultiUbicacionCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateMultiUbicacionCmd, TReadMultiUbicacionCmd, TUpdateMultiUbicacionCmd, TDeleteMultiUbicacionCmd, TPickMultiUbicacionCmd]);

end.
