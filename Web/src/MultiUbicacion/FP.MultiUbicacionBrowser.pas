unit FP.MultiUbicacionBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniEdit, uniLabel,
  uniDBNavigator, uniButton, uniBitBtn, uniSpeedButton, uniGUIClasses, uniPanel,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid;

type
  TMultiUbicacionBrowser = class(TEntityBrowser)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MultiUbicacionBrowser: TMultiUbicacionBrowser;

implementation

{$R *.dfm}

end.
