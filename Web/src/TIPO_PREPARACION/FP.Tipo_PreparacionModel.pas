unit FP.Tipo_PreparacionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTipo_PreparacionModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Tipo_PreparacionModel: TTipo_PreparacionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TTipo_PreparacionModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Tipo_Preparacion';
end;

function TTipo_PreparacionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TTipo_PreparacionModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TTipo_PreparacionModel.TableName: string;
begin
  Result := 'fastence.fp0_Tipo_Preparacion';
end;

end.
