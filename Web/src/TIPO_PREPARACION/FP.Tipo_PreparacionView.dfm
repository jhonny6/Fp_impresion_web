inherited Tipo_Preparacion: TTipo_Preparacion
  Caption = 'Tipo_Preparacion'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit3: TUniDBEdit [3]
    Left = 56
    Top = 162
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 56
    Top = 74
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 56
    Top = 118
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ORDEN'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Orden'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
