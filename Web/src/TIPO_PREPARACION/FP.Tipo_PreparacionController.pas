unit FP.Tipo_PreparacionController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTipo_PreparacionCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateTipo_PreparacionCmd = class(TTipo_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTipo_PreparacionCmd = class(TTipo_PreparacionCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTipo_PreparacionCmd = class(TTipo_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTipo_PreparacionCmd = class(TTipo_PreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTipo_PreparacionCmd = class(TReadTipo_PreparacionCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Tipo_PreparacionBrowser,
  FP.Tipo_PreparacionModel,
  FP.Tipo_PreparacionView,
  FP.Main;

{ TProvinciasCommand }

function TTipo_PreparacionCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTipo_PreparacionModel;
end;

function TTipo_PreparacionCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTipo_PreparacionBrowser;
end;

function TTipo_PreparacionCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTipo_Preparacion;
end;

{ TCreateProvinciasCmd }

procedure TCreateTipo_PreparacionCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateTipo_PreparacionCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteTipo_PreparacionCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickTipo_PreparacionCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTipo_PreparacionCmd, TReadTipo_PreparacionCmd, TUpdateTipo_PreparacionCmd, TDeleteTipo_PreparacionCmd, TPickTipo_PreparacionCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTipo_PreparacionCmd, TReadTipo_PreparacionCmd, TUpdateTipo_PreparacionCmd, TDeleteTipo_PreparacionCmd, TPickTipo_PreparacionCmd]);

end.
