object SearchForm: TSearchForm
  Left = 0
  Top = 0
  ClientHeight = 441
  ClientWidth = 624
  Caption = 'SearchForm'
  BorderStyle = bsDialog
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 15
  object edFiltro: TUniEdit
    Left = 0
    Top = 41
    Width = 624
    Height = 41
    Hint = ''
    Text = ''
    ParentFont = False
    Font.Height = -19
    Align = alTop
    TabOrder = 0
  end
  object UniDBGrid1: TUniDBGrid
    Left = 0
    Top = 82
    Width = 624
    Height = 359
    Hint = ''
    DataSource = DSSearch
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    Align = alClient
    TabOrder = 1
    OnDblClick = UniDBGrid1DblClick
  end
  object UniSimplePanel1: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 624
    Height = 41
    Hint = ''
    ParentColor = False
    Align = alTop
    AlignmentControl = uniAlignmentClient
    ParentAlignmentControl = False
    TabOrder = 2
    object btOk: TUniSpeedButton
      Left = 24
      Top = 3
      Width = 55
      Height = 35
      Hint = ''
      Caption = '<i class="fa fa-check fa-2x"></i>'
      ParentColor = False
      ImageIndex = 7
      TabOrder = 1
      OnClick = btOkClick
    end
    object btClose: TUniSpeedButton
      Left = 104
      Top = 3
      Width = 55
      Height = 35
      Hint = ''
      Caption = '<i class="fa fa-window-close fa-2x"></i>'
      ParentColor = False
      ImageIndex = 8
      TabOrder = 2
      OnClick = btCloseClick
    end
  end
  object DSSearch: TDataSource
    DataSet = QSearch
    Left = 416
    Top = 248
  end
  object QSearch: TUniQuery
    Left = 416
    Top = 160
  end
end
