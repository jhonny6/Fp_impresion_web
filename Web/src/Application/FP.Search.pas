unit FP.Search;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniBasicGrid, uniDBGrid, uniGUIBaseClasses, uniEdit,
  uniButton, uniBitBtn, uniSpeedButton, uniPanel,
  Data.DB, DBAccess, Uni, MemDS, FP.UniQueryHelper;

type
  TSearchForm = class(TUniForm)
    edFiltro: TUniEdit;
    UniDBGrid1: TUniDBGrid;
    UniSimplePanel1: TUniSimplePanel;
    btOk: TUniSpeedButton;
    btClose: TUniSpeedButton;
    DSSearch: TDataSource;
    QSearch: TUniQuery;
    procedure btCloseClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure UniDBGrid1DblClick(Sender: TObject);
  private
    FEditControl: TUniEdit;
    FKey: String;
    FNomDesc: String;
    { Private declarations }
  public
    property Key: String read FKey write FKey;
    property NomDesc: String read FNomDesc write FNomDesc;
    property EditControl: TUniEdit read FEditControl write FEditControl;


    procedure Buscar(pNomID, pNomDesc, pSQL: String; pEdit: TUniEdit);
  end;

function SearchForm: TSearchForm;

implementation

{$R *.dfm}

uses
  FP.MainModule, uniGUIApplication;

function SearchForm: TSearchForm;
begin
  Result := TSearchForm(UniMainModule.GetFormInstance(TSearchForm));
end;

{ TSearchForm }

procedure TSearchForm.Buscar(pNomID, pNomDesc, pSQL: String; pEdit: TUniEdit);
begin
  FKey := pNomID;
  FNomDesc := pNomDesc;
  FEditControl := pEdit;
  QSearch.Connection := UniMainModule.SQLConn;
  QSearch.Close;
  QSearch.OpenExt(pSQL);
end;

procedure TSearchForm.UniDBGrid1DblClick(Sender: TObject);
begin
  btOkClick(Sender);
end;

procedure TSearchForm.btOkClick(Sender: TObject);
begin
  FEditControl.Tag := QSearch.FieldByName(Key).AsInteger;
  FEditControl.Text := QSearch.FieldByName(NomDesc).AsString;
  ModalResult := mrOk;
end;

procedure TSearchForm.btCloseClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
