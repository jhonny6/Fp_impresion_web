inherited EntityBrowser: TEntityBrowser
  ClientHeight = 735
  ClientWidth = 1188
  OnShow = UniFormShow
  ExplicitWidth = 1188
  ExplicitHeight = 735
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBGrid1: TUniDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 122
    Width = 1182
    Height = 610
    Hint = ''
    ClientEvents.UniEvents.Strings = (
      
        'store.afterCreate=function store.afterCreate(sender)'#13#10'{'#13#10'  sende' +
        'r.setRemoteSort(false);'#13#10'}')
    TitleFont.Name = 'Calibri'
    TitleFont.Style = [fsBold]
    DataSource = DataSource
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgCheckSelect, dgAlwaysShowSelection, dgConfirmDelete, dgMultiSelect, dgAutoRefreshRow, dgRowNumbers]
    WebOptions.PageSize = 80
    WebOptions.FetchAll = True
    LoadMask.Message = 'Cargando datos...'
    BorderStyle = ubsNone
    Align = alClient
    Font.Height = -13
    Font.Name = 'Calibri'
    ParentFont = False
    TabOrder = 0
    ParentColor = False
    Color = 15395562
    Summary.GrandTotalAlign = taBottom
    Exporter.Enabled = True
    Exporter.IncludeGroups = True
    Exporter.IncludeSummary = True
    Exporter.Title = 'Exportaci'#243'n Grid'
    OnColumnSort = UniDBGrid1ColumnSort
    OnDblClick = UniDBGrid1DblClick
    OnColumnSummary = UniDBGrid1ColumnSummary
  end
  object UniPanel1: TUniPanel
    Left = 0
    Top = 0
    Width = 1188
    Height = 73
    Hint = ''
    Align = alTop
    TabOrder = 1
    Caption = ''
    object UniSpeedButton1: TUniSpeedButton
      Left = 749
      Top = 3
      Width = 100
      Height = 47
      Hint = 'Eliminar'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-minus fa-2x"></i>'
      ParentColor = False
      TabOrder = 3
      OnClick = ModelDeleteActExecute
    end
    object UniSpeedButton4: TUniSpeedButton
      Left = 431
      Top = 3
      Width = 100
      Height = 47
      Hint = 'Ver'
      Visible = False
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-eye fa-2x"></i>'
      ParentColor = False
      TabOrder = 2
      OnClick = ModelEditActExecute
    end
    object UniSpeedButton5: TUniSpeedButton
      Left = 855
      Top = 3
      Width = 100
      Height = 47
      Hint = 'Refresh'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-retweet fa-2x"></i>'
      ParentColor = False
      TabOrder = 4
      OnClick = UniSpeedButton5Click
    end
    object UniSpeedButton6: TUniSpeedButton
      Left = 643
      Top = 3
      Width = 100
      Height = 47
      Hint = 'Editar'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-pen fa-2x"></i>'
      ParentColor = False
      TabOrder = 1
      OnClick = ModelEditActExecute
    end
    object UniSpeedButton7: TUniSpeedButton
      Left = 537
      Top = 2
      Width = 100
      Height = 47
      Hint = 'A'#241'adir'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-plus fa-2x"></i>'
      ParentColor = False
      TabOrder = 0
      OnClick = ModelInsertActExecute
    end
    object UniDBNavigator1: TUniDBNavigator
      Left = 3
      Top = 3
      Width = 358
      Height = 46
      Hint = ''
      DataSource = DataSource
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      IconSet = icsFontAwesome
      TabStop = False
      TabOrder = 6
    end
    object lbSeleccionRegistro: TUniLabel
      Left = 16
      Top = 55
      Width = 109
      Height = 13
      Hint = ''
      Visible = False
      Caption = 'Seleccione el registro'
      TabOrder = 7
    end
    object UniSpeedButton2: TUniSpeedButton
      Left = 961
      Top = 2
      Width = 100
      Height = 47
      Hint = 'Refresh'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-search fa-2x"></i>'
      ParentColor = False
      TabOrder = 8
      OnClick = UniSpeedButton2Click
    end
    object UniSpeedButton3: TUniSpeedButton
      Left = 1067
      Top = 2
      Width = 100
      Height = 47
      Hint = 'Refresh'
      ShowHint = True
      ParentShowHint = False
      Caption = '<i class="fa fa-file-excel fa-2x"></i>'
      ParentColor = False
      TabOrder = 9
      OnClick = ExportExcelExecute
    end
  end
  object edBusqueda: TUniEdit
    AlignWithMargins = True
    Left = 3
    Top = 76
    Width = 1182
    Height = 40
    Hint = 'B'#250'squeda'
    ShowHint = True
    ParentShowHint = False
    Text = ''
    Align = alTop
    TabOrder = 2
    ClearButton = True
    OnKeyPress = edBusquedaKeyPress
  end
  object UniActionList1: TUniActionList
    Left = 256
    Top = 88
    object DatasetEdit: TDataSetEdit
      Category = 'Edition'
      Caption = '&Edit'
      Hint = 'Edit'
      ImageIndex = 0
      DataSource = DataSource
    end
    object DatasetDelete: TDataSetDelete
      Category = 'Edition'
      Caption = '&Delete'
      Hint = 'Delete'
      ImageIndex = 1
      DataSource = DataSource
    end
    object DatasetInsert: TDataSetInsert
      Category = 'Edition'
      Caption = '&Insert'
      Hint = 'Insert'
      ImageIndex = 2
      DataSource = DataSource
    end
    object DatasetFirst: TDataSetFirst
      Category = 'Navigation'
      ImageIndex = 3
      DataSource = DataSource
    end
    object DatasetPrior: TDataSetPrior
      Category = 'Navigation'
      DataSource = DataSource
    end
    object DatasetNext: TDataSetNext
      Category = 'Navigation'
      DataSource = DataSource
    end
    object DatasetLast: TDataSetLast
      Category = 'Navigation'
      DataSource = DataSource
    end
    object DatasetRefresh: TDataSetRefresh
      Category = 'Navigation'
      Caption = '<i class="fa fa-retweet fa-2x"></i>'
      Hint = 'Refresh'
      DataSource = DataSource
    end
    object ModelEditAct: TAction
      Category = 'Edition'
      OnExecute = ModelEditActExecute
      OnUpdate = ModelEditActUpdate
    end
    object ModelInsertAct: TAction
      Category = 'Edition'
      OnExecute = ModelInsertActExecute
    end
    object ModelDeleteAct: TAction
      Category = 'Edition'
      OnExecute = ModelDeleteActExecute
      OnUpdate = ModelDeleteActUpdate
    end
    object ExportExcel: TAction
      Category = 'Edition'
      Caption = 'Export Excel'
      OnExecute = ExportExcelExecute
    end
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    Left = 368
    Top = 288
  end
  object UniGridExcelExporter1: TUniGridExcelExporter
    FileExtention = 'xlsx'
    MimeType = 
      'application/vnd.openxmlformats-officedocument.spreadsheetml.shee' +
      't'
    CharSet = 'UTF-8'
    Left = 864
    Top = 288
  end
end
