object EntityModel: TEntityModel
  OnCreate = DataModuleCreate
  Height = 300
  Width = 651
  PixelsPerInch = 96
  object Data: TUniQuery
    AfterOpen = DataAfterOpen
    AfterInsert = DataAfterInsert
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    BeforeDelete = DataBeforeDelete
    AfterDelete = DataAfterDelete
    OnCalcFields = DataCalcFields
    Left = 71
    Top = 46
  end
end
