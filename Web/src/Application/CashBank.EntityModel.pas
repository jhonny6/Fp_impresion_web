unit CashBank.EntityModel;

interface

uses
  System.SysUtils, System.Classes, DBAccess, Uni,
  Data.DB, uniGUIDialogs, MemDS, FP.UniQueryHelper;

type

  TEntityModelClass = class of TEntityModel;

  TEntityModel = class;

  TEntityInfo = record
  public type
    TSubject = (esCreate, esUpdate, esDelete, esPick);
  public
    Subject: TSubject;
    Entity: TEntityModel;
    PK: Variant;
  end;

  TEntityModel = class(TDataModule)
    Data: TUniQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataAfterInsert(DataSet: TDataSet);
    procedure DataAfterOpen(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure DataAfterDelete(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataCalcFields(DataSet: TDataSet);

  private
    CopyPKValue: Variant;
    { Private declarations }
  protected
    function SQLFindAll: string; virtual;
    function SQLFindByPK: string;
    function SQLNewEntity: string;
    procedure DoOnChangeField(Sender: TField);
  public
    class function PackageName: string; virtual; abstract;
    class function TableName: string; virtual; abstract;
    class function PKName: string; virtual;
    function PKValue: Variant;
    function FindAll: TDataSet;
    function FindByPK(const PK: Variant): TDataSet;
    function NewEntity: TDataSet;
    function CargaSqlUpdate(AProcedure: String): String;
    { Public declarations }
  end;

var
  EntityModel: TEntityModel;

implementation

uses System.Variants,
  System.StrUtils, FP.MainModule;

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}

procedure TEntityModel.DataAfterDelete(DataSet: TDataSet);
begin
  // NotifyChanged(esDelete, CopyPKValue);
end;

procedure TEntityModel.DataAfterInsert(DataSet: TDataSet);
begin
  // NotifyChanged(esUpdate, PKValue);
  // Reglas de negocio tipo encadenar procesos:
  // Ej: Notificar modificacion
end;

procedure TEntityModel.DataAfterOpen(DataSet: TDataSet);
// const
// NoVisibles: array [1 .. 4] of string = ('id', 'date_add', 'date_upd',
// 'lockid');
// InKey: array [1 .. 2] of string = ('id', 'lockid');
// var
// Field: TField;
begin
  // Si no creamos los TFields en dise�o, al abrir los crea el Dataset.
  // Otra opci�n es crear los TFields por c�digo (ver TFieldDefs)
  // Aqui podemos configurarlos.
  // Aprovecharemos convenios usados en el dise�o de nuestra BD
  // for Field in Data.Fields do
  // begin
  // Field.ProviderFlags := [pfInupdate];
  //
  // if MatchText(Field.FieldName, NoVisibles) then
  // Field.Visible := False;
  //
  // // los campos FK ocultos
  // if Field.FieldName.StartsWith('id', True) then
  // Field.Visible := False;
  //
  // // por convenio haremos que los campos de join contengan "." en el nombre.
  // // Los marcaremos como no actualizables
  // if Field.FieldName.Contains('.') then
  // begin
  // Field.ProviderFlags := []; // no enviaremos a la BD
  // Field.ReadOnly := False; // Permitir que podamos editar en memoria
  // end;
  //
  // // Configuramos los campos que se usan en el where para el Updatemode = WhereKeysOnly;
  // // usamos la PK + el lockid para bloqueo optimista
  // // el update_at es TDatetime es decir un float y debido a errores de redondeo
  // // no es adecuado para implementar el bloqueo optimista
  // if MatchText(Field.FieldName, InKey) then
  // Field.ProviderFlags := Field.ProviderFlags + [pfInKey];
  // Field.OnChange := DoOnChangeField;
  // end;

end;

Function TEntityModel.CargaSqlUpdate(AProcedure: String): String;
Var
  stpProcedure: TUniStoredProc;
  i: integer;
  TXSQLUPDATE: String;
begin
  stpProcedure := TUniStoredProc.Create(Self);
  try
    try
      with stpProcedure do
      begin
        Connection := UniMainModule.SQLConn;
        StoredProcName := AProcedure;
        Prepare;
        TXSQLUPDATE := ' declare  BEGIN ' + AProcedure + ' ( ';
        for i := 0 to stpProcedure.ParamCount - 1 do
        begin
          if i <> 0 then
            TXSQLUPDATE := TXSQLUPDATE + ',';

          TXSQLUPDATE := TXSQLUPDATE + ':' + copy(Params[i].Name, 2, 50);
        end;

        TXSQLUPDATE := TXSQLUPDATE + '); END; ';
      end;
    except
      on E: Exception do
        ShowMessageN(E.ToString);
    end;
  finally
    freeandnil(stpProcedure);
  end;
  Result := TXSQLUPDATE;
end;

procedure TEntityModel.DataAfterPost(DataSet: TDataSet);
begin
  // NotifyChanged(esUpdate, PKValue);
end;

procedure TEntityModel.DataBeforeDelete(DataSet: TDataSet);
begin
  CopyPKValue := PKValue;
end;

procedure TEntityModel.DataBeforePost(DataSet: TDataSet);
var
  Field: TField;
begin
  // FindField devuelve nil si el campo no existe. FieldByName espera que exista: levanta exception si el campo no existe
  // Estamos centralizando esta reglas para todas las tablas, usamos FindField por si en alguna no quisieramos estos campos

  if DataSet.State = dsInsert then
  begin // logica que dependa del estado
    Field := Data.FindField('date_add');
    if Assigned(Field) then
      Field.AsDateTime := Now;
    Field := Data.FindField('lockid');
    if Assigned(Field) then
      Field.AsLargeInt := 0;
  end
  else
  begin
    Field := Data.FindField('lockid');
    if Assigned(Field) then
      Field.AsLargeInt := 1 + Field.AsLargeInt;
  end;

  Field := Data.FindField('date_upd');
  if Assigned(Field) then
    Field.AsDateTime := Now;

  // Este evento tambien es adecueado para Validaciones: haciendo raise impediriamos la actualizacion

end;

procedure TEntityModel.DataCalcFields(DataSet: TDataSet);
begin
  //
end;

procedure TEntityModel.DoOnChangeField(Sender: TField);
begin

end;

procedure TEntityModel.DataModuleCreate(Sender: TObject);
var
  vPaquete: String;
begin
  Data.Connection := UniMainModule.SQLConn;
  Data.UpdatingTable := TableName;
//  Data.FetchOptions.Items := Data.FetchOptions.Items - [fiMeta];
//  Data.UpdateOptions.UpdateTableName := TableName;
//  Data.UpdateOptions.LockMode := lmNone;
//  Data.UpdateOptions.UpdateMode := upWhereKeyOnly;
//  Data.UpdateOptions.UpdateChangedFields := True;
//  Data.UpdateOptions.RefreshMode := rmOnDemand;

//  UpdateSQL.Connection := UniMainModule.SQLConn;

  Data.SQLInsert.Clear;
  Data.SQLUpdate.Clear;
  Data.SQLDelete.Clear;

  vPaquete := PackageName;

  if vPaquete.Contains('@') then
    vPaquete := vPaquete.Replace('@', '.')
  else
    vPaquete := '.pkg_mantenimientos.GV_' + vPaquete;

  Data.SQLInsert.Text := CargaSqlUpdate(SCHEMA + vPaquete + '_IU');
  Data.SQLUpdate.Text := Data.SQLInsert.Text;
  Data.SQLDelete.Text := 'DECLARE BEGIN  ' + SCHEMA + vPaquete +
    '_D (:ID); END;';

  // UpdateSQL.InsertSQL.text := CargaSqlUpdate(SCHEMA + '.pkg_mantenimientos.GV_FP0_PAISES_IU');
  // UpdateSQL.ModifySQL.text := CargaSqlUpdate(SCHEMA + '.pkg_mantenimientos.GV_FP0_PAISES_IU');
  // UpdateSQL.DeleteSQL.text := 'DECLARE BEGIN  ' + SCHEMA + '.pkg_mantenimientos.GV_FP0_PAISES_D (:ID); END;';


  // UpdateSQL.

//  Data.UpdateObject := UpdateSQL;
end;

class function TEntityModel.PKName: string;
begin
  Result := 'id';
end;

function TEntityModel.SQLFindAll: string;
begin
  Result := Format('select * from %s', [TableName]);
end;

function TEntityModel.SQLFindByPK: string;
begin
  Result := SQLFindAll + Format(' where %0:s.%1:s = :%1:s',
    [TableName, PKName]);
end;

function TEntityModel.SQLNewEntity: string;
begin
  // usamos una consulta que no traiga nada => truquillo para obtener los TFields correctos pero ningun registro :)
  Result := SQLFindAll + ' where 1 = 0';
end;

function TEntityModel.PKValue: Variant;
begin
  Result := Data[PKName];
end;


function TEntityModel.FindAll: TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLFindAll);
end;

function TEntityModel.FindByPK(const PK: Variant): TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLFindByPK, [PK], []);
  { ^ es lo mismo que:
    Data.SQL.Text := SQLFindByPK;
    Data.ParamByName('ID').Value := PK;
    Data.Open;
  }
end;

function TEntityModel.NewEntity: TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLNewEntity);
end;

end.
