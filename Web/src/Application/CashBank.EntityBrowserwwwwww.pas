unit CashBank.EntityBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.FrameBase, uniGUIBaseClasses,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu,
  uniGUIClasses, uniBasicGrid, uniDBGrid, Data.DB,
  uniButton, uniBitBtn, uniPanel, CashBank.EntityModel,
  uniSpeedButton, uniDBNavigator, uniLabel, uniEdit, uFiltro,
  Generics.Collections, DBAccess, Uni, FP.GridHelper;

type

  TEntityBrowserClass = class of TEntityBrowser;

  TEntityBrowser = class(TfrmBase)
    UniDBGrid1: TUniDBGrid;
    UniActionList1: TUniActionList;
    DatasetEdit: TDataSetEdit;
    DatasetDelete: TDataSetDelete;
    DatasetInsert: TDataSetInsert;
    DatasetFirst: TDataSetFirst;
    DatasetPrior: TDataSetPrior;
    DatasetNext: TDataSetNext;
    DatasetLast: TDataSetLast;
    DatasetRefresh: TDataSetRefresh;
    DataSource: TDataSource;
    UniPanel1: TUniPanel;
    ModelEditAct: TAction;
    ModelInsertAct: TAction;
    ModelDeleteAct: TAction;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton6: TUniSpeedButton;
    UniSpeedButton7: TUniSpeedButton;
    UniDBNavigator1: TUniDBNavigator;
    lbSeleccionRegistro: TUniLabel;
    edBusqueda: TUniEdit;
    UniSpeedButton2: TUniSpeedButton;
    procedure ModelEditActExecute(Sender: TObject);
    procedure ModelDeleteActExecute(Sender: TObject);
    procedure ModelDeleteActUpdate(Sender: TObject);
    procedure ModelEditActUpdate(Sender: TObject);
    procedure ModelInsertActExecute(Sender: TObject);
    procedure ModelPickActExecute(Sender: TObject);
    procedure ModelPickActUpdate(Sender: TObject);
    procedure UniDBGrid1DblClick(Sender: TObject);
    procedure UniSpeedButton5Click(Sender: TObject);
    procedure edBusquedaKeyPress(Sender: TObject; var Key: Char);
    procedure UniDBGrid1ColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure UniSpeedButton2Click(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
  private
    FPickMode: Boolean;
    FSLQBase: String;
    procedure SetPickMode(const Value: Boolean);
    procedure Filtro(DataSet: TDataSet; var Accept: Boolean);
  protected
    procedure CreateEntity;
    procedure DeleteEntity;
    procedure PickEntity;
    procedure UpdateEntity;
    function EntityName: string;
    { Private declarations }
  public
    { Public declarations }
    procedure FiltrarYEjecutar(pCondicion: String);

    property SQLBase: String read FSLQBase;

    property PickMode: Boolean read FPickMode write SetPickMode;
    function Model: TEntityModel;
    procedure refresh;
  end;

implementation

{$R *.dfm}

uses CashBank.AppController, uniguitypes;

function TEntityBrowser.EntityName: string;
begin
  Result := self.ClassName;
  // Remove 'T' prefix
  Result := Result.Substring(1);
  // Remove 'Browser' sufix
  Result := Result.Substring(0, Length(Result) - Length('Browser'));
end;

function TEntityBrowser.Model: TEntityModel;
begin
  Result := DataSource.DataSet.Owner as TEntityModel;
end;

procedure TEntityBrowser.SetPickMode(const Value: Boolean);
begin
  FPickMode := Value;
  lbSeleccionRegistro.Visible := not Value;
  BorderStyle := uniguitypes.bsSingle;
end;

procedure TEntityBrowser.CreateEntity;
begin
  TAppCommand['Create' + EntityName].Execute;
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowser.UniDBGrid1ColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  inherited;
  // if Column.FieldName = 'ID' then
  // Column.AuxValue := Column.AuxValue + 1;
end;

procedure TEntityBrowser.UniDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if PickMode then
  begin
    modalresult := mrOK;
    close;
  end;
end;

procedure TEntityBrowser.UniFormShow(Sender: TObject);
begin
  inherited;
  FSLQBase := TUniQuery(DataSource.DataSet).SQL.Text;
end;

procedure TEntityBrowser.UniSpeedButton2Click(Sender: TObject);
var
  vFiltro: TFiltroFrm;
begin
  inherited;
  vFiltro := FiltroFrm;
  vFiltro.Show;
  vFiltro.DataSet := DataSource.DataSet;

  vFiltro.OnAplicaFiltro := (
    procedure(pFiltros: TList<TConector>)
    var
      vDT: TUniQuery;
    begin
      vDT := TUniQuery(DataSource.DataSet);

      vDT.close;
      vDT.SQL.Clear;
      try
        vDT.Open(SQLBase + ' where ' + vFiltro.Condiciones);
      finally
        edBusqueda.Text := TUniQuery(DataSource.DataSet).SQL.Text;
      end;
    end);

  // vFiltro.ShowModal;
end;

procedure TEntityBrowser.UniSpeedButton5Click(Sender: TObject);
begin
  inherited;
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowser.UpdateEntity;
begin
  TAppCommand['Update' + EntityName].Execute(Model.PKValue);
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowser.DataSourceStateChange(Sender: TObject);
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
    UniDBGrid1.SetGridConfig(self.Name);
end;

procedure TEntityBrowser.DeleteEntity;
var
  vI: Integer;
  vBook: TBookmark;
begin
  DataSource.DataSet.DisableControls;
  try
    for vI := 0 to UniDBGrid1.SelectedRows.Count - 1 do
    begin
      vBook := UniDBGrid1.SelectedRows.Items[vI];
      DataSource.DataSet.GotoBookmark(vBook);

      TAppCommand['Delete' + EntityName].Execute(Model.PKValue);

      DataSource.DataSet.FreeBookmark(vBook);
    end;
    DataSource.DataSet.refresh;
  finally
    DataSource.DataSet.EnableControls;
  end;
end;

procedure TEntityBrowser.edBusquedaKeyPress(Sender: TObject; var Key: Char);
var
  vDS: TDataSet;
begin
  vDS := DataSource.DataSet;
  if Ord(Key) = 13 then
  begin
    if Trim(edBusqueda.Text) = '' then
      vDS.Filtered := False
    else
    begin
      vDS.Filtered := False;
      vDS.OnFilterRecord := Filtro;
      vDS.Filtered := True;
    end;
  end;
end;

procedure TEntityBrowser.FiltrarYEjecutar(pCondicion: String);
begin
  Model.Data.SQL.Clear;
  Model.Data.SQL.Text := FSLQBase;

  Model.Data.SQL.Text := Model.Data.SQL.Text.Replace('where 1=0', '');

  if pCondicion <> EmptyStr then
    if Model.Data.SQL.Text.Contains('where') then
      Model.Data.SQL.Text := Model.Data.SQL.Text + pCondicion
    else
      Model.Data.SQL.Text := Model.Data.SQL.Text + ' where 1=1 ' + pCondicion;

  Model.Data.close;
  Model.Data.Open;
end;

procedure TEntityBrowser.Filtro(DataSet: TDataSet; var Accept: Boolean);
var
  vI: Integer;
begin
  for vI := 0 to DataSet.Fields.Count - 1 do
  begin
    Accept := DataSet.Fields[vI].AsString.Contains(edBusqueda.Text);

    if Accept then
      break;
  end;
end;

procedure TEntityBrowser.PickEntity;
begin
end;

procedure TEntityBrowser.refresh;
begin
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowser.ModelDeleteActExecute(Sender: TObject);
begin
  DeleteEntity;
end;

procedure TEntityBrowser.ModelInsertActExecute(Sender: TObject);
begin
  CreateEntity;
end;

procedure TEntityBrowser.ModelPickActExecute(Sender: TObject);
begin
  PickEntity;
end;

procedure TEntityBrowser.ModelDeleteActUpdate(Sender: TObject);
begin
  ModelDeleteAct.Enabled := Model.PKValue <> Null;
end;

procedure TEntityBrowser.ModelEditActExecute(Sender: TObject);
begin
  inherited;
  UpdateEntity;
end;

procedure TEntityBrowser.ModelEditActUpdate(Sender: TObject);
begin
  ModelEditAct.Enabled := Model.PKValue <> Null;
end;

procedure TEntityBrowser.ModelPickActUpdate(Sender: TObject);
begin
  // ModelPickAct.Visible := PickMode;
end;

end.
