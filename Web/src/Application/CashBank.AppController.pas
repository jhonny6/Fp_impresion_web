unit CashBank.AppController;

interface

uses uniGUIClasses, uniGUIForm, CashBank.EntityView, CashBank.EntityBrowser,
  CashBank.EntityModel;

type
  TCommandArg = Variant;
  TCommandArgs = array of TCommandArg;

  TAppCommandClass = class of TAppCommand;

  TAppCommand = class(TObject)
  strict private
    class function GetCommands(const CommandName: string): TAppCommandClass; static;
  private
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
    function EntityModelClass: TEntityModelClass; virtual; abstract;
    function EntityBrowserClass: TEntityBrowserClass;virtual; abstract;
    function EntityViewClass: TEntityViewClass;virtual; abstract;

  public
    class procedure Execute; overload;
    class procedure Execute(Arg: TCommandArg); overload;
    class procedure Execute(Args: TCommandArgs); overload;
    class procedure Execute(Args: TUniControl); overload;
    class procedure Execute(Args: TEntityView; PickMode:boolean); overload;
    procedure Proccess(Args: TCommandArgs); overload; virtual; abstract;
    procedure Proccess(Args: TUniControl); overload; virtual;
    procedure Proccess(Args: TEntityView; pickView:boolean); overload; virtual;

    // API para gestionar un registro global de comandos disponibles por nombre.
    // La invocacion por nombre permite reducir el acoplamiento entre partes
    // de la aplicacion de forma sencilla
    class procedure RegisterCmd(CommandClass: TAppCommandClass); overload;
    class procedure RegisterCmd(CommandClasses: TArray<TAppCommandClass>); overload;
    class procedure UnegisterCmd(CommandClass: TAppCommandClass); overload;
    class procedure UnegisterCmd(CommandClasses: TArray<TAppCommandClass>); overload;
    class property Commands[const ClassName: string]: TAppCommandClass read GetCommands; default;
  end;

implementation

uses System.SysUtils,
  System.Variants,
  System.Generics.Collections, uniGUIApplication, System.Classes;

var
  FCommands: TDictionary<string, TAppCommandClass>;

  { TEntityCommand }

class procedure TAppCommand.Execute;
begin
  Execute([]);
end;

class procedure TAppCommand.Execute(Arg: TCommandArg);
begin
  Execute([Arg]);
end;

class procedure TAppCommand.Execute(Args: TCommandArgs);
var
  Command: TAppCommand;
begin
  Command := Self.Create;
  Command.Proccess(Args);
  Command.Free;
end;

class procedure TAppCommand.Execute(Args: TUniControl);
var
  Command: TAppCommand;
begin
  Command := Self.Create;
  Command.Proccess(Args);
  Command.Free;

end;

class procedure TAppCommand.Execute(Args: TEntityView; PickMode:boolean);
var
  Command: TAppCommand;
begin
  Command := Self.Create;
  Command.Proccess(Args, PickMode);
  Command.Free;
end;


class function TAppCommand.GetCommands(const CommandName: string): TAppCommandClass;
begin
  Result := FCommands[Format('T%sCmd', [CommandName])];
end;

procedure TAppCommand.Proccess(Args: TEntityView; pickView: boolean);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.PickMode:= pickView;
  View.ShowModal(procedure(Sender: TComponent; Result:Integer)
  var
     EC: TEntityInfo;
    begin
      if result=1 then
      begin
        EC.Subject := esPick;
        EC.Entity :=(Sender as  TEntityBrowser).Model;
        EC.PK := EC.Entity.PKValue;
        Args.setEntityInfo(EC);
      end;
      //args.refresh;
    end);


end;

procedure TAppCommand.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

class procedure TAppCommand.RegisterCmd(CommandClasses: TArray<TAppCommandClass>);
var
  CommandClass: TAppCommandClass;
begin
  for CommandClass in CommandClasses do
    RegisterCmd(CommandClass);
end;

class procedure TAppCommand.UnegisterCmd(CommandClasses: TArray<TAppCommandClass>);
var
  CommandClass: TAppCommandClass;
begin
  for CommandClass in CommandClasses do
    UnegisterCmd(CommandClass);
end;

class procedure TAppCommand.RegisterCmd(CommandClass: TAppCommandClass);
begin
  FCommands.AddOrSetValue(CommandClass.ClassName, CommandClass);
end;

class procedure TAppCommand.UnegisterCmd(CommandClass: TAppCommandClass);
begin
  FCommands.Remove(CommandClass.ClassName);
end;

initialization

FCommands := TDictionary<string, TAppCommandClass>.Create;

finalization

FCommands.Free;

end.
