unit CashBank.EntityView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, CashBank.EntityModel, uniStatusBar,
  uniPanel, uniGUIBaseClasses, System.Actions, Vcl.ActnList, uniMainMenu,
  uniLabel, uniButton, uniBitBtn, uniSpeedButton, Vcl.DBActns, uniScrollBox;

type
  TEntityViewClass = class of TEntityView;

  TEntityView = class(TUniForm)
    DataSource: TDataSource;
    UniSimplePanel1: TUniSimplePanel;    
    UniContainerPanel1: TUniScrollBox;	
    StatusBar: TUniStatusBar;
    UniActionList1: TUniActionList;
    StatusBarAct: TAction;
    DatasetPost: TDataSetPost;
    DatasetCancel: TDataSetCancel;
    DatasetRefresh: TDataSetRefresh;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton2: TUniSpeedButton;
    UniSpeedButton3: TUniSpeedButton;
    
    procedure StatusBarActUpdate(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
  private
    procedure SetUpDbControl(Control: TComponent);
  protected
    class function EntityName: string; static;

    { Private declarations }
  public
    { Public declarations }
    function Model: TEntityModel;
    procedure setEntityInfo(const Info: TEntityInfo); virtual;
  end;

implementation

uses
  uniGUIApplication, System.TypInfo;

{$R *.dfm}


class function TEntityView.EntityName: string;
begin
  Result := ClassName;
  // Remove 'T' prefix
  Result := Result.Substring(1);
  // Remove 'View' sufix
  Result := Result.Substring(0, Length(Result) - Length('View'));
end;


function TEntityView.Model: TEntityModel;
begin
  Result := DataSource.DataSet.Owner as TEntityModel;
end;


procedure TEntityView.setEntityInfo(const Info: TEntityInfo);
begin
//
end;

procedure TEntityView.StatusBarActUpdate(Sender: TObject);
var field : TField;
begin
  field:=Model.Data.FindField('date_add');
  if assigned(field) then
     StatusBar.Panels[0].Text := Format('Created: %s', [field.AsString]);
  field:=Model.Data.FindField('date_upd');
  if assigned(field) then
  StatusBar.Panels[1].Text := Format('Updated: %s', [field.AsString]);
end;

procedure TEntityView.SetUpDbControl(Control: TComponent);
begin
if not IsPublishedProp(Control, 'DataSource') then Exit;
var propiedad:String := 'DataSource';
SetObjectProp(Control, propiedad, self.Datasource);
end;

procedure TEntityView.UniFormShow(Sender: TObject);
begin
  StatusBarActUpdate(self);
  for var i := 0 to (Self.ComponentCount - 1) do begin
     SetUpDbControl(Components[i]);
end;

end;

end.
