unit FP.StockBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniEdit, uniLabel,
  uniDBNavigator, uniButton, uniBitBtn, uniSpeedButton, uniGUIClasses, uniPanel,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid, uniTabControl, uniCheckBox,
  uniPageControl, DBAccess, Uni,
  uniDateTimePicker, uniDBEdit, MemDS, uniGridExporters;

type
  TStockBrowser = class(TEntityBrowser)
    pnlFiltroBuscar: TUniPanel;
    UniPanel2: TUniPanel;
    UniLabel1: TUniLabel;
    UniPanel3: TUniPanel;
    UniLabel4: TUniLabel;
    UniCheckBox7: TUniCheckBox;
    UniCheckBox8: TUniCheckBox;
    UniLabel5: TUniLabel;
    UniCheckBox9: TUniCheckBox;
    UniCheckBox10: TUniCheckBox;
    UniLabel6: TUniLabel;
    UniCheckBox11: TUniCheckBox;
    UniCheckBox12: TUniCheckBox;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel7: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniEdit1: TUniEdit;
    UniEdit2: TUniEdit;
    UniEdit3: TUniEdit;
    UniEdit4: TUniEdit;
    UniEdit5: TUniEdit;
    UniEdit6: TUniEdit;
    UniPageControl1: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniTabSheet2: TUniTabSheet;
    DBBusqueda: TUniDBGrid;
    QDetalle: TUniQuery;
    UniPageControl2: TUniPageControl;
    UniTabSheet3: TUniTabSheet;
    UniDBGrid2: TUniDBGrid;
    UniTabSheet4: TUniTabSheet;
    UniTabSheet5: TUniTabSheet;
    UniDBGrid3: TUniDBGrid;
    UniDBGrid4: TUniDBGrid;
    UniPanel4: TUniPanel;
    UniDateTimePicker1: TUniDateTimePicker;
    UniDateTimePicker2: TUniDateTimePicker;
    UniDateTimePicker3: TUniDateTimePicker;
    UniLabel10: TUniLabel;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniLabel13: TUniLabel;
    UniLabel14: TUniLabel;
    DSDetalle: TDataSource;
    QOperativa: TUniQuery;
    DSOPerativa: TDataSource;
    QStock: TUniQuery;
    DSStock: TDataSource;
    DBETipoPedido: TUniDBEdit;
    DBENumero: TUniDBEdit;
    DBEAno: TUniDBEdit;
    DBETipoPedido2: TUniDBEdit;
    procedure UniSpeedButton2Click(Sender: TObject);
    procedure ModelEditActExecute(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure UniPageControl2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StockBrowser: TStockBrowser;

implementation

{$R *.dfm}

procedure TStockBrowser.ModelEditActExecute(Sender: TObject);
begin
  QDetalle.Connection := TUniQuery(DataSource.DataSet).Connection;
  QDetalle.Close;
  QDetalle.Open;

  // inherited;
  UniPageControl1.ActivePage := UniTabSheet2;
end;

procedure TStockBrowser.UniFormShow(Sender: TObject);
begin
  inherited;
  UniPageControl1.ActivePage := UniTabSheet1;
  UniPageControl2.ActivePage := UniTabSheet3;
end;

procedure TStockBrowser.UniPageControl2Change(Sender: TObject);
begin
  inherited;
  QOperativa.Connection := TUniQuery(DataSource.DataSet).Connection;
  QOperativa.Close;
  QOperativa.sql.Clear;
  QOperativa.sql.Add
    (' Select * from fastence.gv_fp03_pedidos_gestdet_l  where id_pedidos_detalle='
    + QDetalle.FieldByName('ID').AsString);
  QOperativa.sql.Add(' order by fecha_hora desc ');

  if QDetalle.FieldByName('ID').AsString > '0' then
    QOperativa.Open;

  QStock.Connection := TUniQuery(DataSource.DataSet).Connection;
  QStock.Close;
  QStock.sql.Clear;
  QStock.sql.Add
    (' Select * from fastence.gv_fp03_pedidos_gestdet_Stock  where id_articulo='
    + QDetalle.FieldByName('ID_articulo_preparar').AsString);
  QStock.sql.Add(' order by lote_fecha Asc ');

  if QDetalle.FieldByName('ID').AsString > '0' then
    QStock.Open;
end;

procedure TStockBrowser.UniSpeedButton2Click(Sender: TObject);
var
  vQ: TUniQuery;
begin
  //inherited;

  vQ := DataSource.DataSet as TUniQuery;
  vQ.Close;
  DBBusqueda.DataSource := DataSource;
  vQ.sql.Clear;
  vQ.sql.Add('select a.* from fastence.gv_fp03_salidas_ped_gestcab  a');
  vQ.Open;

  DBETipoPedido.DataField := 'TIPO_PEDIDO';
  DBETipoPedido2.DataField := DBETipoPedido.DataField;
  DBENumero.DataField := 'NUMERO';
  DBEAno.DataField := 'ANO';
end;

end.
