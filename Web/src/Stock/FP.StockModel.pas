unit FP.StockModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TStockModel = class(TEntityModel)
  private
    { Private declarations }
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  public
    { Public declarations }
    class function TableName: string; override;
    class function PackageName: string; override;
  end;
var
  StockModel: TStockModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

class function TStockModel.PackageName: string;
begin
  Result := 'FP0_ESQUEMAS';
end;

function TStockModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TStockModel.SQLFindAll_with_FK: string;
//var
//  JoinModel: TEntityModelClass;
//  JoinTableName: string;
//  JoinPKName: string;
//  JoinColumn: string;
//  FKName: string;
begin
//  JoinModel := TMonedasModel;
//  JoinPKName := JoinModel.PKName;
//  JoinTableName := JoinModel.TableName;
//  JoinColumn := 'nombre_corto as "monedas.moneda"';
//  FKName := 'id_moneda';
//  Result := Format('select %0:s.*, %1:s.%4:s from %0:s left outer join %1:s on(%0:s.%2:s = %1:s.%3:s)', [TableName, JoinTableName, FKName,
//    JoinPKName, JoinColumn]);

   Result := 'select * from '+TableName;

end;

class function TStockModel.TableName: string;
begin
  Result := 'fastence.GV_FP0_MULTIUBICACION_GEST_INF';
end;

end.
