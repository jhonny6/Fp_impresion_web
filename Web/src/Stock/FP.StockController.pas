unit FP.StockController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TStockCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateStockCmd = class(TStockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadStockCmd = class(TStockCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateStockCmd = class(TStockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteStockCmd = class(TStockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickStockCmd = class(TReadStockCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  FP.StockBrowser,
  FP.StockModel,
  FP.StockView,
  FP.Main;

{ TStockCommand }

function TStockCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TStockModel;
end;

function TStockCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TStockBrowser;
end;

function TStockCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TStock;
end;

{ TCreateStockCmd }

procedure TCreateStockCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadStockCmd }

{
procedure TReadStockCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadStockCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateStockCmd }

procedure TUpdateStockCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteStockCmd }

procedure TDeleteStockCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickStockCmd }

procedure TPickStockCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateStockCmd, TReadStockCmd, TUpdateStockCmd, TDeleteStockCmd, TPickStockCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateStockCmd, TReadStockCmd, TUpdateStockCmd, TDeleteStockCmd, TPickStockCmd]);

end.
