inherited StockBrowser: TStockBrowser
  Caption = 'StockBrowser'
  ExplicitWidth = 640
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniDBGrid1: TUniDBGrid
    Visible = False
  end
  inherited UniPanel1: TUniPanel
    inherited UniSpeedButton1: TUniSpeedButton
      Top = 2
      Visible = False
      ExplicitTop = 2
    end
    inherited UniSpeedButton4: TUniSpeedButton
      Top = 2
      ExplicitTop = 2
    end
    inherited UniSpeedButton5: TUniSpeedButton
      Visible = False
    end
    inherited UniSpeedButton6: TUniSpeedButton
      Top = 2
      ExplicitTop = 2
    end
    inherited UniSpeedButton7: TUniSpeedButton
      Visible = False
    end
    inherited UniDBNavigator1: TUniDBNavigator
      Visible = False
    end
  end
  inherited edBusqueda: TUniEdit
    Visible = False
  end
  object UniPageControl1: TUniPageControl [3]
    Left = 0
    Top = 119
    Width = 1188
    Height = 616
    Hint = ''
    ActivePage = UniTabSheet1
    TabBarVisible = False
    Align = alClient
    TabOrder = 3
    object UniTabSheet1: TUniTabSheet
      Hint = ''
      Caption = 'Buscar'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object pnlFiltroBuscar: TUniPanel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1174
        Height = 133
        Hint = ''
        AutoScroll = True
        Align = alTop
        TabOrder = 0
        Caption = ''
        ScrollHeight = 133
        ScrollWidth = 1174
        object UniPanel2: TUniPanel
          Left = 257
          Top = 1
          Width = 256
          Height = 131
          Hint = ''
          Align = alLeft
          TabOrder = 1
          Caption = ''
          object UniLabel1: TUniLabel
            Left = 48
            Top = 5
            Width = 62
            Height = 13
            Hint = ''
            Caption = 'Ubicaciones'
            TabOrder = 1
          end
          object UniLabel2: TUniLabel
            Left = 48
            Top = 24
            Width = 33
            Height = 13
            Hint = ''
            Caption = 'Pasillo'
            TabOrder = 2
          end
          object UniLabel3: TUniLabel
            Left = 104
            Top = 24
            Width = 46
            Height = 13
            Hint = ''
            Caption = 'Columna'
            TabOrder = 3
          end
          object UniLabel7: TUniLabel
            Left = 160
            Top = 24
            Width = 6
            Height = 13
            Hint = ''
            Caption = 'E'
            TabOrder = 4
          end
          object UniLabel8: TUniLabel
            Left = 10
            Top = 41
            Width = 32
            Height = 13
            Hint = ''
            Caption = 'Desde'
            TabOrder = 5
          end
          object UniLabel9: TUniLabel
            Left = 10
            Top = 66
            Width = 29
            Height = 13
            Hint = ''
            Caption = 'Hasta'
            TabOrder = 6
          end
          object UniEdit1: TUniEdit
            Left = 48
            Top = 40
            Width = 50
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 7
          end
          object UniEdit2: TUniEdit
            Left = 104
            Top = 40
            Width = 50
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 8
          end
          object UniEdit3: TUniEdit
            Left = 160
            Top = 40
            Width = 43
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 9
          end
          object UniEdit4: TUniEdit
            Left = 48
            Top = 64
            Width = 50
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 10
          end
          object UniEdit5: TUniEdit
            Left = 104
            Top = 64
            Width = 50
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 11
          end
          object UniEdit6: TUniEdit
            Left = 160
            Top = 64
            Width = 43
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 12
          end
        end
        object UniPanel3: TUniPanel
          Left = 1
          Top = 1
          Width = 256
          Height = 131
          Hint = ''
          Align = alLeft
          TabOrder = 2
          Caption = ''
          object UniLabel4: TUniLabel
            Left = 16
            Top = 8
            Width = 114
            Height = 13
            Hint = ''
            Caption = 'Mercancia Disponible:'
            TabOrder = 1
          end
          object UniCheckBox7: TUniCheckBox
            Left = 33
            Top = 30
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 2
          end
          object UniCheckBox8: TUniCheckBox
            Left = 128
            Top = 30
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 3
          end
          object UniLabel5: TUniLabel
            Left = 16
            Top = 49
            Width = 121
            Height = 13
            Hint = ''
            Caption = 'Mercancia por Encargo:'
            TabOrder = 4
          end
          object UniCheckBox9: TUniCheckBox
            Left = 33
            Top = 70
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 5
          end
          object UniCheckBox10: TUniCheckBox
            Left = 128
            Top = 68
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 6
          end
          object UniLabel6: TUniLabel
            Left = 16
            Top = 89
            Width = 127
            Height = 13
            Hint = ''
            Caption = 'Expediciones Preparadas'
            TabOrder = 7
          end
          object UniCheckBox11: TUniCheckBox
            Left = 33
            Top = 108
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 8
          end
          object UniCheckBox12: TUniCheckBox
            Left = 128
            Top = 108
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 9
          end
        end
      end
      object DBBusqueda: TUniDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 142
        Width = 1174
        Height = 443
        Hint = ''
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow]
        LoadMask.Message = 'Loading data...'
        Align = alClient
        TabOrder = 1
      end
    end
    object UniTabSheet2: TUniTabSheet
      Hint = ''
      Caption = 'Detalle'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object UniPageControl2: TUniPageControl
        Left = 0
        Top = 164
        Width = 1180
        Height = 422
        Hint = ''
        ActivePage = UniTabSheet3
        Align = alClient
        TabOrder = 0
        OnChange = UniPageControl2Change
        ExplicitHeight = 424
        object UniTabSheet3: TUniTabSheet
          Hint = ''
          Caption = 'Detalle'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 128
          object UniDBGrid2: TUniDBGrid
            Left = 0
            Top = 0
            Width = 1172
            Height = 396
            Hint = ''
            DataSource = DSDetalle
            LoadMask.Message = 'Loading data...'
            Align = alClient
            TabOrder = 0
          end
        end
        object UniTabSheet4: TUniTabSheet
          Hint = ''
          Caption = 'Operativa'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 128
          object UniDBGrid3: TUniDBGrid
            Left = 0
            Top = 0
            Width = 1172
            Height = 396
            Hint = ''
            DataSource = DSOPerativa
            LoadMask.Message = 'Loading data...'
            Align = alClient
            TabOrder = 0
          end
        end
        object UniTabSheet5: TUniTabSheet
          Hint = ''
          Caption = 'Stock'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 256
          ExplicitHeight = 128
          object UniDBGrid4: TUniDBGrid
            Left = 0
            Top = 0
            Width = 1172
            Height = 396
            Hint = ''
            DataSource = DSStock
            LoadMask.Message = 'Loading data...'
            Align = alClient
            TabOrder = 0
          end
        end
      end
      object UniPanel4: TUniPanel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1174
        Height = 158
        Hint = ''
        Align = alTop
        TabOrder = 1
        Caption = ''
        object UniDateTimePicker1: TUniDateTimePicker
          Left = 80
          Top = 31
          Width = 120
          Hint = ''
          DateTime = 44663.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
        end
        object UniDateTimePicker2: TUniDateTimePicker
          Left = 80
          Top = 57
          Width = 120
          Hint = ''
          DateTime = 44663.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 2
        end
        object UniDateTimePicker3: TUniDateTimePicker
          Left = 80
          Top = 85
          Width = 120
          Hint = ''
          DateTime = 44663.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 3
        end
        object UniLabel10: TUniLabel
          Left = 3
          Top = 7
          Width = 41
          Height = 13
          Hint = ''
          Caption = 'N'#250'mero'
          TabOrder = 4
        end
        object UniLabel11: TUniLabel
          Left = 3
          Top = 34
          Width = 30
          Height = 13
          Hint = ''
          Caption = 'Fecha'
          TabOrder = 5
        end
        object UniLabel12: TUniLabel
          Left = 3
          Top = 62
          Width = 42
          Height = 13
          Hint = ''
          Caption = 'F. Cierre'
          TabOrder = 6
        end
        object UniLabel13: TUniLabel
          Left = 3
          Top = 88
          Width = 43
          Height = 13
          Hint = ''
          Caption = 'F. Salida'
          TabOrder = 7
        end
        object UniLabel14: TUniLabel
          Left = 3
          Top = 115
          Width = 62
          Height = 13
          Hint = ''
          Caption = 'Tipo Pedido'
          TabOrder = 8
        end
        object DBETipoPedido: TUniDBEdit
          Left = 80
          Top = 3
          Width = 55
          Height = 22
          Hint = ''
          DataSource = DataSource
          TabOrder = 9
        end
        object DBENumero: TUniDBEdit
          Left = 137
          Top = 3
          Width = 121
          Height = 22
          Hint = ''
          DataSource = DataSource
          TabOrder = 10
        end
        object DBEAno: TUniDBEdit
          Left = 264
          Top = 3
          Width = 121
          Height = 22
          Hint = ''
          DataSource = DataSource
          TabOrder = 11
        end
        object DBETipoPedido2: TUniDBEdit
          Left = 80
          Top = 110
          Width = 55
          Height = 22
          Hint = ''
          DataSource = DataSource
          TabOrder = 12
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 632
    Top = 144
  end
  object QDetalle: TUniQuery
    SQL.Strings = (
      'SELECT * from'
      'fastence.gv_fp03_salidas_ped_gestdet')
    Left = 584
    Top = 352
  end
  object DSDetalle: TDataSource
    DataSet = QDetalle
    Left = 584
    Top = 416
  end
  object QOperativa: TUniQuery
    SQL.Strings = (
      'SELECT * from'
      'fastence.gv_fp03_salidas_ped_gestdet')
    Left = 752
    Top = 416
  end
  object DSOPerativa: TDataSource
    DataSet = QOperativa
    Left = 752
    Top = 480
  end
  object QStock: TUniQuery
    SQL.Strings = (
      'SELECT * from'
      'fastence.gv_fp03_salidas_ped_gestdet')
    Left = 864
    Top = 416
  end
  object DSStock: TDataSource
    DataSet = QStock
    Left = 864
    Top = 480
  end
end
