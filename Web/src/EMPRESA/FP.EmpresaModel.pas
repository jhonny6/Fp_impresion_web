unit FP.EmpresaModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TEmpresaModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  EmpresaModel: TEmpresaModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TEmpresaModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Empresas_GEST';
end;

function TEmpresaModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TEmpresaModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TEmpresaModel.TableName: string;
begin
  Result := 'fastence.fp0_Empresas';
end;

end.
