unit FP.EmpresaController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TEmpresaCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateEmpresaCmd = class(TEmpresaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadEmpresaCmd = class(TEmpresaCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateEmpresaCmd = class(TEmpresaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteEmpresaCmd = class(TEmpresaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickEmpresaCmd = class(TReadEmpresaCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.EmpresaBrowser,
  FP.EmpresaModel,
  FP.EmpresaView,
  FP.Main;

{ TProvinciasCommand }

function TEmpresaCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TEmpresaModel;
end;

function TEmpresaCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TEmpresaBrowser;
end;

function TEmpresaCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TEmpresa;
end;

{ TCreateProvinciasCmd }

procedure TCreateEmpresaCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateEmpresaCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteEmpresaCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickEmpresaCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateEmpresaCmd, TReadEmpresaCmd, TUpdateEmpresaCmd, TDeleteEmpresaCmd, TPickEmpresaCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateEmpresaCmd, TReadEmpresaCmd, TUpdateEmpresaCmd, TDeleteEmpresaCmd, TPickEmpresaCmd]);

end.
