inherited Empresa: TEmpresa
  Caption = 'Empresa'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ExplicitTop = 44
    ScrollHeight = 133
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 38
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 111
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'NOMBRE_EMPRESA'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre empresa'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 74
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CODIGO'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  inherited DataSource: TDataSource
    Left = 324
    Top = 134
  end
end
