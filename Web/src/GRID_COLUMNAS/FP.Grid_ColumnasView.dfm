inherited Grid_Columnas: TGrid_Columnas
  Caption = 'Grid_Columnas'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ExplicitTop = 44
    ScrollHeight = 311
    ScrollWidth = 364
    object UniDBEdit3: TUniDBEdit
      Left = 64
      Top = 76
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CAMPO'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Campo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 64
      Top = 111
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'TITULO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'T'#237'tulo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit5: TUniDBEdit
      Left = 64
      Top = 146
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ALINEACION'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Alineaci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit6: TUniDBEdit
      Left = 64
      Top = 182
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ANCHO'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Ancho'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit7: TUniDBEdit
      Left = 64
      Top = 218
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'NOMBREVISTA'
      DataSource = DataSource
      TabOrder = 4
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre vista'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit8: TUniDBEdit
      Left = 64
      Top = 253
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'USUARIO'
      DataSource = DataSource
      TabOrder = 5
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Usuario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit9: TUniDBEdit
      Left = 64
      Top = 289
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'GRUPO_USUARIO'
      DataSource = DataSource
      TabOrder = 6
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Grupo usuarios'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  object UniDBEdit1: TUniDBEdit [3]
    Left = 65
    Top = 83
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'NOMBREGRID'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Nombre grid'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [4]
    Left = 65
    Top = 47
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
