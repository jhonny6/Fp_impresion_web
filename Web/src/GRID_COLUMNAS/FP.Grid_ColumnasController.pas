unit FP.Grid_ColumnasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGrid_ColumnasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGrid_ColumnasCmd = class(TGrid_ColumnasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGrid_ColumnasCmd = class(TGrid_ColumnasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGrid_ColumnasCmd = class(TGrid_ColumnasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGrid_ColumnasCmd = class(TGrid_ColumnasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGrid_ColumnasCmd = class(TReadGrid_ColumnasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Grid_ColumnasBrowser,
  FP.Grid_ColumnasModel,
  FP.Grid_ColumnasView,
  FP.Main;

{ TProvinciasCommand }

function TGrid_ColumnasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGrid_ColumnasModel;
end;

function TGrid_ColumnasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGrid_ColumnasBrowser;
end;

function TGrid_ColumnasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGrid_Columnas;
end;

{ TCreateProvinciasCmd }

procedure TCreateGrid_ColumnasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateGrid_ColumnasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteGrid_ColumnasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickGrid_ColumnasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGrid_ColumnasCmd, TReadGrid_ColumnasCmd, TUpdateGrid_ColumnasCmd, TDeleteGrid_ColumnasCmd, TPickGrid_ColumnasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGrid_ColumnasCmd, TReadGrid_ColumnasCmd, TUpdateGrid_ColumnasCmd, TDeleteGrid_ColumnasCmd, TPickGrid_ColumnasCmd]);

end.
