unit FP.Grid_ColumnasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TGrid_ColumnasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Grid_ColumnasModel: TGrid_ColumnasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TGrid_ColumnasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Grid_Columnas';
end;

function TGrid_ColumnasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TGrid_ColumnasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TGrid_ColumnasModel.TableName: string;
begin
  Result := 'fastence.fp0_Grid_Columnas';
end;

end.
