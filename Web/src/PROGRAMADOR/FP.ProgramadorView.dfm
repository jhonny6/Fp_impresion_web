inherited Programador: TProgramador
  Caption = 'Programador'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ExplicitLeft = -8
  end
  object UniDBEdit3: TUniDBEdit [3]
    Left = 0
    Top = 112
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 0
    Top = 56
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 0
    Top = 84
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'TIPO'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Tipo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit4: TUniDBEdit [6]
    Left = 0
    Top = 140
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'HORA'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Hora'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit5: TUniDBEdit [7]
    Left = 0
    Top = 168
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ACTIVO'
    DataSource = DataSource
    TabOrder = 7
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Activo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit6: TUniDBEdit [8]
    Left = 0
    Top = 196
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ARCHIVO'
    DataSource = DataSource
    TabOrder = 8
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Archivo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit7: TUniDBEdit [9]
    Left = 0
    Top = 224
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DESTINATARIOS_EMAIL'
    DataSource = DataSource
    TabOrder = 9
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Email'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit8: TUniDBEdit [10]
    Left = 0
    Top = 252
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'PROC_PREENVIO'
    DataSource = DataSource
    TabOrder = 10
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Proc. de preenvio'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit9: TUniDBEdit [11]
    Left = 0
    Top = 280
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'PROC_POSENVIO'
    DataSource = DataSource
    TabOrder = 11
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Proc de posenvio'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit10: TUniDBEdit [12]
    Left = 0
    Top = 308
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CONDICION'
    DataSource = DataSource
    TabOrder = 12
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Condici'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit11: TUniDBEdit [13]
    Left = 0
    Top = 336
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'SELECTIVIDAD'
    DataSource = DataSource
    TabOrder = 13
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Selectividad'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit12: TUniDBEdit [14]
    Left = 0
    Top = 364
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_LUNES'
    DataSource = DataSource
    TabOrder = 14
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Lunes'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit13: TUniDBEdit [15]
    Left = 287
    Top = 55
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_MARTES'
    DataSource = DataSource
    TabOrder = 15
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Martes'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit14: TUniDBEdit [16]
    Left = 287
    Top = 84
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_MIERCOLES'
    DataSource = DataSource
    TabOrder = 16
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Mi'#233'rcoles'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit15: TUniDBEdit [17]
    Left = 287
    Top = 112
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_JUEVES'
    DataSource = DataSource
    TabOrder = 17
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Jueves'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit16: TUniDBEdit [18]
    Left = 287
    Top = 140
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_VIERNES'
    DataSource = DataSource
    TabOrder = 18
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Viernes'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit17: TUniDBEdit [19]
    Left = 287
    Top = 168
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_SABADO'
    DataSource = DataSource
    TabOrder = 19
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'S'#225'bado'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit18: TUniDBEdit [20]
    Left = 287
    Top = 196
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DESTINATARIOS_EMAIL'
    DataSource = DataSource
    TabOrder = 20
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Destinatario email'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit19: TUniDBEdit [21]
    Left = 287
    Top = 224
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CODEMPRESA'
    DataSource = DataSource
    TabOrder = 21
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Cod. Empresa'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit20: TUniDBEdit [22]
    Left = 287
    Top = 252
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIA_DOMINGO'
    DataSource = DataSource
    TabOrder = 22
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Domingo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit21: TUniDBEdit [23]
    Left = 287
    Top = 280
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ULTIMA_EJECUCION'
    DataSource = DataSource
    TabOrder = 23
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = #218'ltima ejecuci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit22: TUniDBEdit [24]
    Left = 287
    Top = 308
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'REPETIRCADA_MINUTOS'
    DataSource = DataSource
    TabOrder = 24
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Repetir cada '
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit23: TUniDBEdit [25]
    Left = 287
    Top = 335
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'SQLADICIONAL'
    DataSource = DataSource
    TabOrder = 25
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Sql adicional'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
