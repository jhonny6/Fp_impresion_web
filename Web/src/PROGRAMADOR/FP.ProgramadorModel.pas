unit FP.ProgramadorModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TProgramadorModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ProgramadorModel: TProgramadorModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TProgramadorModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Programador_GEST';
end;

function TProgramadorModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TProgramadorModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TProgramadorModel.TableName: string;
begin
  Result := 'fastence.fp0_Programador';
end;

end.
