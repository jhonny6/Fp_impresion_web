unit FP.ProgramadorController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TProgramadorCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateProgramadorCmd = class(TProgramadorCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadProgramadorCmd = class(TProgramadorCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateProgramadorCmd = class(TProgramadorCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteProgramadorCmd = class(TProgramadorCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickProgramadorCmd = class(TReadProgramadorCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.ProgramadorBrowser,
  FP.ProgramadorModel,
  FP.ProgramadorView,
  FP.Main;

{ TProvinciasCommand }

function TProgramadorCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TProgramadorModel;
end;

function TProgramadorCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TProgramadorBrowser;
end;

function TProgramadorCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TProgramador;
end;

{ TCreateProvinciasCmd }

procedure TCreateProgramadorCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateProgramadorCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteProgramadorCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickProgramadorCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateProgramadorCmd, TReadProgramadorCmd, TUpdateProgramadorCmd, TDeleteProgramadorCmd, TPickProgramadorCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateProgramadorCmd, TReadProgramadorCmd, TUpdateProgramadorCmd, TDeleteProgramadorCmd, TPickProgramadorCmd]);

end.
