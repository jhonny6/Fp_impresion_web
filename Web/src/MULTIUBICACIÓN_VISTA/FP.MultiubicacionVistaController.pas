unit FP.MultiubicacionVistaController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TMultiubicacionVistaCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateMultiubicacionVistaCmd = class(TMultiubicacionVistaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadMultiubicacionVistaCmd = class(TMultiubicacionVistaCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateMultiubicacionVistaCmd = class(TMultiubicacionVistaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteMultiubicacionVistaCmd = class(TMultiubicacionVistaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickMultiubicacionVistaCmd = class(TReadMultiubicacionVistaCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.MultiubicacionVistaBrowser,
  FP.MultiubicacionVistaModel,
  FP.MultiubicacionVistaView,
  FP.Main;

{ TProvinciasCommand }

function TMultiubicacionVistaCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TMultiubicacionVistaModel;
end;

function TMultiubicacionVistaCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TMultiubicacionVistaBrowser;
end;

function TMultiubicacionVistaCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TMultiubicacionVista;
end;

{ TCreateProvinciasCmd }

procedure TCreateMultiubicacionVistaCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateMultiubicacionVistaCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteMultiubicacionVistaCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickMultiubicacionVistaCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateMultiubicacionVistaCmd, TReadMultiubicacionVistaCmd, TUpdateMultiubicacionVistaCmd, TDeleteMultiubicacionVistaCmd, TPickMultiubicacionVistaCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateMultiubicacionVistaCmd, TReadMultiubicacionVistaCmd, TUpdateMultiubicacionVistaCmd, TDeleteMultiubicacionVistaCmd, TPickMultiubicacionVistaCmd]);

end.
