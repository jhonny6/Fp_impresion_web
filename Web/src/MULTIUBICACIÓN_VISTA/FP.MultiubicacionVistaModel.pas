unit FP.MultiubicacionVistaModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TMultiubicacionVistaModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string; override;
  end;

var
  MultiubicacionVistaModel: TMultiubicacionVistaModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TMultiubicacionVistaModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_PROVINCIAS_GEST';
end;

function TMultiubicacionVistaModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TMultiubicacionVistaModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TMultiubicacionVistaModel.TableName: string;
begin
  Result := 'fastence.fp0_provincias';
end;

end.
