unit FP.Gestion_SalidasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGestion_SalidasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGestion_SalidasCmd = class(TGestion_SalidasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGestion_SalidasCmd = class(TGestion_SalidasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGestion_SalidasCmd = class(TGestion_SalidasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGestion_SalidasCmd = class(TGestion_SalidasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGestion_SalidasCmd = class(TReadGestion_SalidasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Gestion_SalidasBrowser,
  FP.Gestion_SalidasModel,
  FP.Gestion_SalidasView,
  FP.Main;

{ TProvinciasCommand }

function TGestion_SalidasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGestion_SalidasModel;
end;

function TGestion_SalidasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGestion_SalidasBrowser;
end;

function TGestion_SalidasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGestion_Salidas;
end;

{ TCreateProvinciasCmd }

procedure TCreateGestion_SalidasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateGestion_SalidasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteGestion_SalidasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickGestion_SalidasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGestion_SalidasCmd, TReadGestion_SalidasCmd, TUpdateGestion_SalidasCmd, TDeleteGestion_SalidasCmd, TPickGestion_SalidasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGestion_SalidasCmd, TReadGestion_SalidasCmd, TUpdateGestion_SalidasCmd, TDeleteGestion_SalidasCmd, TPickGestion_SalidasCmd]);

end.
