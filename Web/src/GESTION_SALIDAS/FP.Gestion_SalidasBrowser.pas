unit FP.Gestion_SalidasBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniEdit, uniLabel,
  uniDBNavigator, uniButton, uniBitBtn, uniSpeedButton, uniGUIClasses, uniPanel,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid, uniGUIAbstractClasses,
  uniSplitter;

type
  TGestion_SalidasBrowser = class(TEntityBrowser)
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
  public
    { Public declarations }
  end;

var
  Gestion_SalidasBrowser: TGestion_SalidasBrowser;

implementation

{$R *.dfm}

procedure TGestion_SalidasBrowser.DataSourceDataChange(Sender: TObject;
  Field: TField);
var
  vCol, vColAc: TUniBaseDBGridColumn;
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
  begin
    if (UniDBGrid1.Columns.Count > 0) then
    begin
      // *** Codigo para la lista
      vCol := UniDBGrid1.Columns.ColumnFromFieldName('VALORACION');

      if Assigned(vCol) then
      begin
        vCol.PickList.Clear;
        vCol.PickList.Add('Bueno');
        vCol.PickList.Add('Regular');
        vCol.PickList.Add('Malo');

        vCol.Width := 100;
      end;

      // *** Codigo para las acciones
      vColAc := UniDBGrid1.ColumnFromCaption('Acciones');

       if not Assigned(vColAc) then
      begin
        vColAc := UniDBGrid1.Columns.Add;
        vColAc.ActionColumn.Enabled := True;
        vColAc.ActionColumn.Buttons.Add;

        vColAc.Title.Caption := 'Acciones';
        vColAc.ActionColumn.Buttons[0].ButtonId := 0;
        vColAc.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
        vColAc.ActionColumn.Buttons[0].IconCls := 'action';
        vColAc.Visible := True;
        vColAc.Width := 75;
      end;

      // vCol.ActionColumn.Buttons.Add;
      // vCol.ActionColumn.Buttons[1].ButtonId := 1;
      // vCol.ActionColumn.Buttons[1].Hint := 'Lupa Grid 2';
      // vCol.ActionColumn.Buttons[1].IconCls := 'action';

    end;
  end;
end;

end.
