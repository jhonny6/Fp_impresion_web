unit FP.ComboHelper;

interface

uses uniComboBox, FP.MainModule, DBAccess, Uni, Data.DB,
  uniGUIAbstractClasses, System.Classes, System.SysUtils, FP.UniQueryHelper;

type
  TUniComboBoxHelper = class helper for TUniComboBox
  private

  public
    procedure CargaComboSQL(const pSQL: string; pClear: Boolean = True);
  end;

implementation

{ TUniComboBoxHelper }

procedure TUniComboBoxHelper.CargaComboSQL(const pSQL: string;
  pClear: Boolean = True);
var
  vQ: TUniQuery;
begin
  if pClear then
    Self.Items.Clear;

  vQ := TUniQuery.Create(nil);
  try
    vQ.Connection := UniMainModule.SQLConn;
    vQ.OpenExt(pSQL);

    vQ.First;
    while not vQ.Eof do
    begin
      Self.Items.Add(vQ.Fields[0].AsString);
      vQ.Next;
    end;
  finally
    FreeAndNil(vQ);
  end;
end;

end.
