unit FP.ImageHelper;

interface

uses uniImage, uniEdit, Fp.Search, System.SysUtils ;

type
  TUniImageBoxHelper = class helper for TUniImage
  private

  public
    procedure Buscar(pNomID, pNomDesc, pSQL: String; pEdit: TUniEdit);
  end;


implementation

{ TUniImageBoxHelper }

procedure TUniImageBoxHelper.Buscar(pNomID, pNomDesc, pSQL: String;
  pEdit: TUniEdit);
var
  vSearchForm: TSearchForm;
begin
  vSearchForm := SearchForm;
  vSearchForm.Buscar(pNomID, pNomDesc, pSQL, pEdit);
  vSearchForm.ShowModalN;
//  FreeAndNil(vSearchForm);
end;

end.
