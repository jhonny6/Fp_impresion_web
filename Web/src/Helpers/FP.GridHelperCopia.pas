unit FP.GridHelper;

interface

uses uniDBGrid, FP.MainModule, DBAccess, Uni, Data.DB,
  uniGUIAbstractClasses, System.Classes;

type
  TUniDBGridHelper = class helper for TUniDBGrid
  public
    function ColumnFromCaption(pCaption: String): TUniBaseDBGridColumn;
    procedure SetGridConfig(const pNombreForm: string);
  end;

implementation

{ TUniDBGridHelper }

function TUniDBGridHelper.ColumnFromCaption(
  pCaption: String): TUniBaseDBGridColumn;
var
  vI: Integer;
begin
  Result := nil;
  for vI := 0 to Self.Columns.Count -1 do
    if Self.Columns[vI].Title.Caption = pCaption then
    begin
      Result := Self.Columns[vI];
      break;
    end;
end;

procedure TUniDBGridHelper.SetGridConfig(const pNombreForm: string);
var
  vI: Integer;
  vQ: TUniQuery;
  vCol: TUniBaseDBGridColumn;
begin
  vQ := TUniQuery.Create(nil);
  try
    vQ.Connection := UniMainModule.SQLConn;
    vQ.Open('select campo, titulo, alineacion, ancho from ' + SCHEMA +
      '.fp0_grid_columnas where nombregrid=:nombregrid',
      [pNombreForm + '.' + Self.Name], [TFieldType.ftString]);

    vQ.DisableControls;
    vQ.First;
    while not vQ.Eof do
    begin
      vCol := Self.Columns.ColumnFromFieldName(vQ.FieldByName('campo')
        .AsString);

      if Assigned(vCol) then
      begin
        vCol.Title.Caption := vQ.FieldByName('titulo').AsString;

        vCol.Title.Alignment := TAlignment(vQ.FieldByName('alineacion')
          .AsInteger);
        vCol.Alignment := vCol.Title.Alignment;

        vCol.Width := vQ.FieldByName('ancho').AsInteger;
      end;
      vQ.Next;
    end;

    if vQ.RecordCount > 0 then
      for vI := 0 to Self.Columns.Count - 1 do
        Self.Columns[vI].Visible :=
          vQ.Locate('campo', Self.Columns[vI].FieldName, []);

  finally
    vQ.Free;
  end;
end;

end.
