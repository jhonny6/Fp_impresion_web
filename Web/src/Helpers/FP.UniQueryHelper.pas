unit FP.UniQueryHelper;

interface

uses DBAccess, Uni, Data.DB;

type
  TUniQueryHelper = class helper for TUniQuery
  private

  public
    procedure OpenExt(const ASQL: String); overload;
    procedure OpenExt(const ASQL: String; const AParams: array of Variant;
  const ATypes: array of TFieldType); overload;
  end;


implementation

{ TUniQueryHelper }

procedure TUniQueryHelper.OpenExt(const ASQL: String;
  const AParams: array of Variant; const ATypes: array of TFieldType);
var
  i: Integer;
begin
  Close;

  SQL.Text := ASQL;

//  if ASQL <> '' then
//    Command.SetCommandText(ASQL,
//      not (Command.CommandKind in [skStoredProc, skStoredProcNoCrs, skStoredProcWithCrs]) and
//      ResourceOptions.ParamCreate);
//  if Command.CommandKind in [skStoredProc, skStoredProcNoCrs, skStoredProcWithCrs] then
//    Prepare;

  for i := Low(AParams) to High(AParams) do
    Params[i].Value := AParams[i];

  for i := Low(ATypes) to High(ATypes) do
    Params[i].DataType := ATypes[i];


  Prepare;
  Open;
end;

procedure TUniQueryHelper.OpenExt(const ASQL: String);
begin
  OpenExt(ASQL, [], []);
end;

end.
