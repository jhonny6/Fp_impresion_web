unit CashBank.Notificaciones_appsView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, Vcl.DBActns, uniScrollBox,
  uniButton, uniBitBtn, uniSpeedButton;

type
  TNotificaciones_apps = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
