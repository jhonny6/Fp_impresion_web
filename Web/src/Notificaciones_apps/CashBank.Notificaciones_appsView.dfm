inherited Notificaciones_apps: TNotificaciones_apps
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Notificaciones_apps'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 68
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'id_notificacion'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Notificaci'#243'n'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'id_registro_app'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Registro app'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
end
