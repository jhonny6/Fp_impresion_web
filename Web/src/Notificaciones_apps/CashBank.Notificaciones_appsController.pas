unit CashBank.Notificaciones_appsController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TNotificaciones_appsCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateNotificaciones_appsCmd = class(TNotificaciones_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadNotificaciones_appsCmd = class(TNotificaciones_appsCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateNotificaciones_appsCmd = class(TNotificaciones_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteNotificaciones_appsCmd = class(TNotificaciones_appsCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickNotificaciones_appsCmd = class(TReadNotificaciones_appsCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Notificaciones_appsBrowser,
  CashBank.Notificaciones_appsModel,
  CashBank.Notificaciones_appsView,
  FP.Main;

{ TNotificaciones_appsCommand }

class function TNotificaciones_appsCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TNotificaciones_appsModel;
end;

class function TNotificaciones_appsCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TNotificaciones_appsBrowser;
end;

class function TNotificaciones_appsCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TNotificaciones_apps;
end;

{ TCreateNotificaciones_appsCmd }

procedure TCreateNotificaciones_appsCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadNotificaciones_appsCmd }

procedure TReadNotificaciones_appsCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadNotificaciones_appsCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateNotificaciones_appsCmd }

procedure TUpdateNotificaciones_appsCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteNotificaciones_appsCmd }

procedure TDeleteNotificaciones_appsCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickNotificaciones_appsCmd }

procedure TPickNotificaciones_appsCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateNotificaciones_appsCmd, TReadNotificaciones_appsCmd, TUpdateNotificaciones_appsCmd, TDeleteNotificaciones_appsCmd, TPickNotificaciones_appsCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateNotificaciones_appsCmd, TReadNotificaciones_appsCmd, TUpdateNotificaciones_appsCmd, TDeleteNotificaciones_appsCmd, TPickNotificaciones_appsCmd]);

end.
