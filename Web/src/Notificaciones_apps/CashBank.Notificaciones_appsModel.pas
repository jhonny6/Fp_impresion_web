unit CashBank.Notificaciones_appsModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TNotificaciones_appsModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TNotificaciones_appsModel }

class function TNotificaciones_appsModel.TableName: string;
begin
  Result := 'cashbank.notificaciones_apps';
end;

end.
