inherited Indicadores_Tipos: TIndicadores_Tipos
  Caption = 'Indicadores_Tipos'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit3: TUniDBEdit [3]
    Left = 56
    Top = 159
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION_INDICADOR'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 56
    Top = 79
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 56
    Top = 119
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'TIPO_INDICADOR'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Tipo indicador'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 110
  end
end
