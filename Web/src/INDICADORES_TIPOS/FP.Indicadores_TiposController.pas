unit FP.Indicadores_TiposController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TIndicadores_TiposCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateIndicadores_TiposCmd = class(TIndicadores_TiposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadIndicadores_TiposCmd = class(TIndicadores_TiposCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateIndicadores_TiposCmd = class(TIndicadores_TiposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteIndicadores_TiposCmd = class(TIndicadores_TiposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickIndicadores_TiposCmd = class(TReadIndicadores_TiposCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Indicadores_TiposBrowser,
  FP.Indicadores_TiposModel,
  FP.Indicadores_TiposView,
  FP.Main;

{ TProvinciasCommand }

function TIndicadores_TiposCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TIndicadores_TiposModel;
end;

function TIndicadores_TiposCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TIndicadores_TiposBrowser;
end;

function TIndicadores_TiposCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TIndicadores_Tipos;
end;

{ TCreateProvinciasCmd }

procedure TCreateIndicadores_TiposCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateIndicadores_TiposCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteIndicadores_TiposCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickIndicadores_TiposCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateIndicadores_TiposCmd, TReadIndicadores_TiposCmd, TUpdateIndicadores_TiposCmd, TDeleteIndicadores_TiposCmd, TPickIndicadores_TiposCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateIndicadores_TiposCmd, TReadIndicadores_TiposCmd, TUpdateIndicadores_TiposCmd, TDeleteIndicadores_TiposCmd, TPickIndicadores_TiposCmd]);

end.
