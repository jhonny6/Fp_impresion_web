unit FP.Indicadores_TiposModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TIndicadores_TiposModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Indicadores_TiposModel: TIndicadores_TiposModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TIndicadores_TiposModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Indicadores_Tipos';
end;

function TIndicadores_TiposModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TIndicadores_TiposModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TIndicadores_TiposModel.TableName: string;
begin
  Result := 'fastence.fp0_Indicadores_Tipos';
end;

end.
