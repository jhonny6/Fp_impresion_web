inherited Tipos_mensaje: TTipos_mensaje
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Tipos_mensaje'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 164
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'nombre'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre'
    end
    object UniDBMemo1: TUniDBMemo
      Left = 56
      Top = 43
      Width = 300
      Height = 89
      Hint = ''
      Margins.Left = 20
      DataField = 'observaciones'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Observaciones'
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 147
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'es_critico'
      DataSource = DataSource
      Caption = ''
      TabOrder = 1
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Cr'#237'tico'#13#10
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
end
