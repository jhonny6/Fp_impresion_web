unit CashBank.Tipos_mensajeController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTipos_mensajeCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateTipos_mensajeCmd = class(TTipos_mensajeCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTipos_mensajeCmd = class(TTipos_mensajeCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTipos_mensajeCmd = class(TTipos_mensajeCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTipos_mensajeCmd = class(TTipos_mensajeCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTipos_mensajeCmd = class(TReadTipos_mensajeCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Tipos_mensajeBrowser,
  CashBank.Tipos_mensajeModel,
  CashBank.Tipos_mensajeView,
  FP.Main;

{ TTipos_mensajeCommand }

class function TTipos_mensajeCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTipos_mensajeModel;
end;

class function TTipos_mensajeCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTipos_mensajeBrowser;
end;

class function TTipos_mensajeCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTipos_mensaje;
end;

{ TCreateTipos_mensajeCmd }

procedure TCreateTipos_mensajeCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadTipos_mensajeCmd }

procedure TReadTipos_mensajeCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadTipos_mensajeCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateTipos_mensajeCmd }

procedure TUpdateTipos_mensajeCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteTipos_mensajeCmd }

procedure TDeleteTipos_mensajeCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickTipos_mensajeCmd }

procedure TPickTipos_mensajeCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTipos_mensajeCmd, TReadTipos_mensajeCmd, TUpdateTipos_mensajeCmd, TDeleteTipos_mensajeCmd, TPickTipos_mensajeCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTipos_mensajeCmd, TReadTipos_mensajeCmd, TUpdateTipos_mensajeCmd, TDeleteTipos_mensajeCmd, TPickTipos_mensajeCmd]);

end.
