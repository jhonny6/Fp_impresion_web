unit CashBank.Tipos_mensajeModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTipos_mensajeModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TTipos_mensajeModel }

class function TTipos_mensajeModel.TableName: string;
begin
  Result := 'cashbank.tipos_mensaje';
end;

end.
