inherited Paises: TPaises
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Paises'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 116
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 5
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'pais'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre Pais'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'codigo'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo Pais'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 94
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
  inherited UniActionList1: TUniActionList
    object SearchMonedaAct: TAction
      Caption = '<i class="fa fa-search"></i>'
      OnExecute = SearchMonedaActExecute
    end
  end
end
