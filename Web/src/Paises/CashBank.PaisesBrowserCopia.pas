unit CashBank.PaisesBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  uniGUIBaseClasses, uniImageList, Vcl.DBActns, System.Actions, Vcl.ActnList,
  uniMainMenu, uniGUIClasses, uniBasicGrid, uniDBGrid, System.ImageList,
  Vcl.ImgList, uniButton, uniBitBtn, uniPanel, uniSpeedButton, uniDBNavigator,
  uniLabel, uniEdit, UniComboBox;

type
  TPaisesBrowser = class(TEntityBrowser)
    procedure DataSourceStateChange(Sender: TObject);
    procedure UniDBGrid1ColumnActionClick(Column: TUniDBGridColumn;
      ButtonId: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TPaisesBrowser.DataSourceStateChange(Sender: TObject);

begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
  begin
//    if UniDBGrid1.Columns.Count >= 2 then
//    begin
//      UniDBGrid1.Columns.Add;
//      UniDBGrid1.Columns[2].Title.Caption := 'Acciones';
//
//      UniDBGrid1.Columns[2].ActionColumn.Enabled := True;
//      UniDBGrid1.Columns[2].ActionColumn.Buttons.Add;
//      UniDBGrid1.Columns[2].ActionColumn.Buttons[0].ButtonId := 0;
//      UniDBGrid1.Columns[2].ActionColumn.Buttons[0].Hint := 'Lupa Grid';
//      UniDBGrid1.Columns[2].ActionColumn.Buttons[0].IconCls := 'action';
//      UniDBGrid1.Columns[2].Width := 100;
//
//      UniDBGrid1.Columns[1].PickList.Add('Bueno');
//      UniDBGrid1.Columns[1].PickList.Add('Regular');
//      UniDBGrid1.Columns[1].PickList.Add('Malo');
//    end;
  end;
end;

procedure TPaisesBrowser.UniDBGrid1ColumnActionClick(Column: TUniDBGridColumn;
  ButtonId: Integer);
begin
  inherited;
  ShowMessage('Click ' + DataSource.DataSet.FieldByName('ID').AsString);
end;

end.
