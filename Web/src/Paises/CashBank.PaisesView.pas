unit CashBank.PaisesView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, Vcl.DBActns, uniButton, uniBitBtn,
  uniSpeedButton, CashBank.EntityModel, uniScrollBox;

type
  TPaises = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    SearchMonedaAct: TAction;
    UniDBEdit3: TUniDBEdit;
    procedure SearchMonedaActExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure setEntityInfo(const Info: TEntityInfo); override;
  end;

implementation

{$R *.dfm}

uses CashBank.AppController, CashBank.MonedasModel;

procedure TPaises.SearchMonedaActExecute(Sender: TObject);
begin
  inherited;
//  TAppCommand['ReadMonedas'].Execute(Self, true);
end;

procedure TPaises.setEntityInfo(const Info: TEntityInfo);
begin
  inherited;
   if Info.Subject <> esPick then
    Exit;

//  if (Info.Entity is TMonedasModel) then begin
//    if not(Model.Data.State in dsEditModes) then
//      Model.Data.Edit;
//    Model.Data['id_moneda'] := Info.PK;
//
//    Monedas_NameCtl.Text := Info.Entity.Data['nombre_corto'];
//  end;



//  if (Info.Entity is TProductModel) then begin
//    if not(ItemsSource.DataSet.State in dsEditModes) then
//      ItemsSource.DataSet.Edit;
//    ItemsSource.DataSet['product_id'] := Info.Entity.Data['id'];
//    ItemsSource.DataSet['price'] := Info.Entity.Data['price'];
//    ItemsSource.DataSet['product.code'] := Info.Entity.Data['code'];
//  end;
end;

end.
