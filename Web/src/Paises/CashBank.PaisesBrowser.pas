unit CashBank.PaisesBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  uniGUIBaseClasses, uniImageList, Vcl.DBActns, System.Actions, Vcl.ActnList,
  uniMainMenu, uniGUIClasses, uniBasicGrid, uniDBGrid, System.ImageList,
  Vcl.ImgList, uniButton, uniBitBtn, uniPanel, uniSpeedButton, uniDBNavigator,
  uniLabel, uniEdit, UniComboBox, uniGUIAbstractClasses, uniGridExporters;

type
  TPaisesBrowser = class(TEntityBrowser)
    procedure DataSourceStateChange(Sender: TObject);
    procedure UniDBGrid1ColumnActionClick(Column: TUniDBGridColumn;
      ButtonId: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TPaisesBrowser.DataSourceStateChange(Sender: TObject);
var
  vCol, vColAc: TUniBaseDBGridColumn;
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
  begin
    if (UniDBGrid1.Columns.Count > 0) then
    begin
      // *** Codigo para la lista
      vCol := UniDBGrid1.Columns.ColumnFromFieldName('PROVINCIA');

      if Assigned(vCol) then
      begin
        vCol.PickList.Clear;
        vCol.PickList.Add('Bueno');
        vCol.PickList.Add('Regular');
        vCol.PickList.Add('Malo');
        vCol.Width := 100;
      end;

      // *** Codigo para las acciones
      vColAc := UniDBGrid1.ColumnFromCaption('Acciones');

      if not Assigned(vColAc) then
      begin
        vColAc := UniDBGrid1.Columns.Add;
        vColAc.ActionColumn.Enabled := True;
        vColAc.ActionColumn.Buttons.Add;
      end;

      vColAc.Title.Caption := 'Acciones';
      vColAc.ActionColumn.Buttons[0].ButtonId := 0;
      vColAc.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
      vColAc.ActionColumn.Buttons[0].IconCls := 'action';
      vColAc.Visible := True;

      // vCol.ActionColumn.Buttons.Add;
      // vCol.ActionColumn.Buttons[1].ButtonId := 1;
      // vCol.ActionColumn.Buttons[1].Hint := 'Lupa Grid 2';
      // vCol.ActionColumn.Buttons[1].IconCls := 'action';
//      vCol.Width := 100;
    end;
  end;
end;

// var
// vCol: TUniBaseDBGridColumn;
// begin
// inherited;
// if (DataSource.DataSet.State in [dsBrowse]) then
// begin
// // *** Codigo para la lista
// vCol := UniDBGrid1.Columns.ColumnFromFieldName('PAIS');
//
// vCol.PickList.Add('Bueno');
// vCol.PickList.Add('Regular');
// vCol.PickList.Add('Malo');
//
// // *** Codigo para las acciones
// vCol := UniDBGrid1.Columns.Add;
// vCol.Title.Caption := 'Acciones';
// vCol.ActionColumn.Enabled := True;
// vCol.ActionColumn.Buttons.Add;
// vCol.ActionColumn.Buttons[0].ButtonId := 0;
// vCol.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
// vCol.ActionColumn.Buttons[0].IconCls := 'action';
//
// vCol.ActionColumn.Buttons.Add;
// vCol.ActionColumn.Buttons[1].ButtonId := 1;
// vCol.ActionColumn.Buttons[1].Hint := 'Lupa Grid 2';
// vCol.ActionColumn.Buttons[1].IconCls := 'action';
// vCol.Width := 100;
// end;
// end;

procedure TPaisesBrowser.UniDBGrid1ColumnActionClick(Column: TUniDBGridColumn;
  ButtonId: Integer);
begin
  inherited;
  case ButtonId of
    0:
      ShowMessage('Click en la acci�n ' + ButtonId.ToString);
    1:
      ShowMessage('Click en la acci�n n�mero ' + ButtonId.ToString);
  end;
end;

end.
