unit CashBank.PaisesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TPaisesModel = class(TEntityModel)
  private
    { Private declarations }
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  public
    { Public declarations }
    class function TableName: string; override;
    class function PackageName: string; override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses CashBank.MonedasModel;

{$R *.dfm}

{ TPaisesModel }


class function TPaisesModel.PackageName: string;
begin
  Result := 'FP0_PAISES';
  //Result := '@pkg_mantenimientos.GV_FP0_PAISES';
end;

function TPaisesModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TPaisesModel.SQLFindAll_with_FK: string;
//var
//  JoinModel: TEntityModelClass;
//  JoinTableName: string;
//  JoinPKName: string;
//  JoinColumn: string;
//  FKName: string;
begin
//  JoinModel := TMonedasModel;
//  JoinPKName := JoinModel.PKName;
//  JoinTableName := JoinModel.TableName;
//  JoinColumn := 'nombre_corto as "monedas.moneda"';
//  FKName := 'id_moneda';
//  Result := Format('select %0:s.*, %1:s.%4:s from %0:s left outer join %1:s on(%0:s.%2:s = %1:s.%3:s)', [TableName, JoinTableName, FKName,
//    JoinPKName, JoinColumn]);

   Result := 'select * from '+TableName;

end;

class function TPaisesModel.TableName: string;
begin
  Result := 'fastence.fp0_paises';
end;

end.
