unit CashBank.PaisesController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TPaisesCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreatePaisesCmd = class(TPaisesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadPaisesCmd = class(TPaisesCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdatePaisesCmd = class(TPaisesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeletePaisesCmd = class(TPaisesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickPaisesCmd = class(TReadPaisesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.PaisesBrowser,
  CashBank.PaisesModel,
  CashBank.PaisesView,
  FP.Main;

{ TPaisesCommand }

function TPaisesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TPaisesModel;
end;

function TPaisesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TPaisesBrowser;
end;

function TPaisesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TPaises;
end;

{ TCreatePaisesCmd }

procedure TCreatePaisesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadPaisesCmd }

{
procedure TReadPaisesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadPaisesCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdatePaisesCmd }

procedure TUpdatePaisesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeletePaisesCmd }

procedure TDeletePaisesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickPaisesCmd }

procedure TPickPaisesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreatePaisesCmd, TReadPaisesCmd, TUpdatePaisesCmd, TDeletePaisesCmd, TPickPaisesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreatePaisesCmd, TReadPaisesCmd, TUpdatePaisesCmd, TDeletePaisesCmd, TPickPaisesCmd]);

end.
