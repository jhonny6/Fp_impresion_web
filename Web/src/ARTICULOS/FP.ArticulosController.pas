unit FP.ArticulosController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TArticulosCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateArticulosCmd = class(TArticulosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadArticulosCmd = class(TArticulosCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateArticulosCmd = class(TArticulosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteArticulosCmd = class(TArticulosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickArticulosCmd = class(TReadArticulosCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.ArticulosBrowser,
  FP.ArticulosModel,
  FP.ArticulosView,
  FP.Main;

{ TProvinciasCommand }

function TArticulosCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TArticulosModel;
end;

function TArticulosCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TArticulosBrowser;
end;

function TArticulosCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TArticulos;
end;

{ TCreateProvinciasCmd }

procedure TCreateArticulosCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateArticulosCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteArticulosCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickArticulosCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateArticulosCmd, TReadArticulosCmd, TUpdateArticulosCmd, TDeleteArticulosCmd, TPickArticulosCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateArticulosCmd, TReadArticulosCmd, TUpdateArticulosCmd, TDeleteArticulosCmd, TPickArticulosCmd]);

end.
