unit FP.ArticulosModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TArticulosModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ArticulosModel: TArticulosModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TArticulosModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Articulos_GEST';
end;

function TArticulosModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TArticulosModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName + ' where rownum<=10 '   // where 1=0 ';
end;

class function TArticulosModel.TableName: string;
begin
  Result := 'fastence.fp0_Articulos_ext';
end;

end.
