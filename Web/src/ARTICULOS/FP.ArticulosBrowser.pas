unit FP.ArticulosBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniEdit, uniLabel,
  uniDBNavigator, uniButton, uniBitBtn, uniSpeedButton, uniGUIClasses, uniPanel,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid, uniGUIAbstractClasses,
  uniSplitter, uniCheckBox, uniGridExporters;

type
  TArticulosBrowser = class(TEntityBrowser)
    pnlFiltroBuscar: TUniPanel;
    UniPanel3: TUniPanel;
    UniLabel4: TUniLabel;
    UniCheckBox7: TUniCheckBox;
    UniCheckBox8: TUniCheckBox;
    UniLabel5: TUniLabel;
    UniCheckBox9: TUniCheckBox;
    UniCheckBox10: TUniCheckBox;
    UniLabel6: TUniLabel;
    UniCheckBox11: TUniCheckBox;
    UniCheckBox12: TUniCheckBox;
    UniPanel6: TUniPanel;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    edNumProveedor: TUniEdit;
    UniPanel4: TUniPanel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel18: TUniLabel;
    Edesdearticulo: TUniEdit;
    Ehastaarticulo: TUniEdit;
    UniPanel2: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel7: TUniLabel;
    edNumFamilia: TUniEdit;
    UniPanel5: TUniPanel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    edNumDescripcion: TUniEdit;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure UniSpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ArticulosBrowser: TArticulosBrowser;

implementation

{$R *.dfm}

procedure TArticulosBrowser.DataSourceDataChange(Sender: TObject;
  Field: TField);
var
  vCol, vColAc: TUniBaseDBGridColumn;
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
  begin
    if (UniDBGrid1.Columns.Count > 0) then
    begin
      // *** Codigo para la lista
      vCol := UniDBGrid1.Columns.ColumnFromFieldName('VALORACION');

      if Assigned(vCol) then
      begin
        vCol.PickList.Clear;
        vCol.PickList.Add('Bueno');
        vCol.PickList.Add('Regular');
        vCol.PickList.Add('Malo');

        vCol.Width := 100;
      end;

      // *** Codigo para las acciones
      vColAc := UniDBGrid1.ColumnFromCaption('Acciones');

      if not Assigned(vColAc) then
      begin
        vColAc := UniDBGrid1.Columns.Add;
        vColAc.ActionColumn.Enabled := True;
        vColAc.ActionColumn.Buttons.Add;

        vColAc.Title.Caption := 'Acciones';
        vColAc.ActionColumn.Buttons[0].ButtonId := 0;
        vColAc.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
        vColAc.ActionColumn.Buttons[0].IconCls := 'action';
        vColAc.Visible := True;
        vColAc.Width := 75;
      end;

      // vCol.ActionColumn.Buttons.Add;
      // vCol.ActionColumn.Buttons[1].ButtonId := 1;
      // vCol.ActionColumn.Buttons[1].Hint := 'Lupa Grid 2';
      // vCol.ActionColumn.Buttons[1].IconCls := 'action';
    end;
  end;
end;

procedure TArticulosBrowser.UniSpeedButton2Click(Sender: TObject);
//  inherited;
var
  vCondicion: String;
begin
  // inherited;
  vCondicion := EmptyStr;
  // if DesdePasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo>=' + quotedstr(DesdePasillo.text);
  //
  // if HastaPasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo<=' + quotedstr(HastaPasillo.text);

  if edNumfamilia.text <> '' then
  begin
    vCondicion := vCondicion + ' and familia = ' + string(edNumfamilia.text).QuotedString;
  end;

  if edNumDescripcion.text <> '' then
  begin
    vCondicion := vCondicion + ' and descripcion2 like ''% ' + edNumDescripcion.text + ' %'' ';
  end;

  if Edesdearticulo.text <> '' then
  begin
    vCondicion := vCondicion + ' and articulo>=' + Edesdearticulo.text;
  end;

  if Ehastaarticulo.text <> '' then
  begin
    vCondicion := vCondicion + ' and articulo<=' + Ehastaarticulo.text;
  end;

  if edNumProveedor.Text <> '' then
  begin
    vCondicion := vCondicion + ' and proveedor = ' + edNumProveedor.Text;
  end;

  FiltrarYEjecutar(vCondicion);
end;

end.
