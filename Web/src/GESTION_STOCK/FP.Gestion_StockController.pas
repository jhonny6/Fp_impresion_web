unit FP.Gestion_StockController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGestion_StockCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGestion_StockCmd = class(TGestion_StockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGestion_StockCmd = class(TGestion_StockCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGestion_StockCmd = class(TGestion_StockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGestion_StockCmd = class(TGestion_StockCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGestion_StockCmd = class(TReadGestion_StockCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Gestion_StockBrowser,
  FP.Gestion_StockModel,
  FP.Gestion_StockView,
  FP.Main;

{ TGestion_StockCommand }

function TGestion_StockCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGestion_StockModel;
end;

function TGestion_StockCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGestion_StockBrowser;
end;

function TGestion_StockCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGestion_Stock;
end;

{ TCreateGestion_StockCmd }

procedure TCreateGestion_StockCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadGestion_StockCmd }

{
procedure TReadGestion_StockCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadGestion_StockCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateGestion_StockCmd }

procedure TUpdateGestion_StockCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteGestion_StockCmd }

procedure TDeleteGestion_StockCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickGestion_StockCmd }

procedure TPickGestion_StockCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGestion_StockCmd, TReadGestion_StockCmd, TUpdateGestion_StockCmd, TDeleteGestion_StockCmd, TPickGestion_StockCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGestion_StockCmd, TReadGestion_StockCmd, TUpdateGestion_StockCmd, TDeleteGestion_StockCmd, TPickGestion_StockCmd]);

end.
