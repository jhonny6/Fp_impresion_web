unit FP.Gestion_StockModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TGestion_StockModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Gestion_StockModel: TGestion_StockModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TGestion_StockModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := '@PKG99_GRID_GESTION.GV_FP0_MULTIUBICACION_GEST';
end;

function TGestion_StockModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TGestion_StockModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TGestion_StockModel.TableName: string;
begin
  Result := 'fastprocess.gv_fp0_multiubicacion_gest';
end;

end.
