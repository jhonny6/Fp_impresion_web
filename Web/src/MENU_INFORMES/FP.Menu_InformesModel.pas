unit FP.Menu_InformesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TMenu_InformesModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Menu_InformesModel: TMenu_InformesModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TMenu_InformesModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'AppMenu_Informes';
end;

function TMenu_InformesModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TMenu_InformesModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TMenu_InformesModel.TableName: string;
begin
  Result := 'fastence.AppMenu_Informes';
end;

end.
