inherited Menu_Informes: TMenu_Informes
  Caption = 'Menu_Informes'
  ExplicitTop = -91
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 392
    ScrollWidth = 538
    object UniDBEdit1: TUniDBEdit
      Left = 0
      Top = 13
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 0
      Top = 52
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'APL_CODIGO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'APL C'#243'digo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 0
      Top = 92
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'GRP_GRUPO'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'GRP Grupo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 0
      Top = 131
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'USU_USUARIO'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Usuario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit5: TUniDBEdit
      Left = -1
      Top = 171
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_ORDEN'
      DataSource = DataSource
      TabOrder = 4
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Orden'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit6: TUniDBEdit
      Left = 0
      Top = 210
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_OPCION'
      DataSource = DataSource
      TabOrder = 5
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Opci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit7: TUniDBEdit
      Left = 0
      Top = 250
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_TITULO'
      DataSource = DataSource
      TabOrder = 6
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'T'#237'tulo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit8: TUniDBEdit
      Left = 0
      Top = 290
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_FUNCION'
      DataSource = DataSource
      TabOrder = 7
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Funci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit9: TUniDBEdit
      Left = 0
      Top = 330
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_DLL'
      DataSource = DataSource
      TabOrder = 8
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'DLL'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit10: TUniDBEdit
      Left = 0
      Top = 370
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'ESTADO'
      DataSource = DataSource
      TabOrder = 9
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Estado'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit11: TUniDBEdit
      Left = 268
      Top = 13
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'USUARIO_EXCLUIDOS'
      DataSource = DataSource
      TabOrder = 10
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Usuarios excluidos'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit12: TUniDBEdit
      Left = 268
      Top = 52
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_IMAGEN'
      DataSource = DataSource
      TabOrder = 11
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Imagen men'#250
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit13: TUniDBEdit
      Left = 268
      Top = 92
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_TITULO_AUXILIAR'
      DataSource = DataSource
      TabOrder = 12
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'T'#237'tulo auxiliar'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit14: TUniDBEdit
      Left = 268
      Top = 131
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_FONT_SIZE'
      DataSource = DataSource
      TabOrder = 13
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Tama'#241'o fuente'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit15: TUniDBEdit
      Left = 268
      Top = 171
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_IMAGE_WIDTH'
      DataSource = DataSource
      TabOrder = 14
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Ancho imagen'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit16: TUniDBEdit
      Left = 268
      Top = 210
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_IMAGE_HEIGTH'
      DataSource = DataSource
      TabOrder = 15
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Altura imagen'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit17: TUniDBEdit
      Left = 268
      Top = 250
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_OPTION_WIDTH'
      DataSource = DataSource
      TabOrder = 16
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Opci'#243'n ancho'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit18: TUniDBEdit
      Left = 268
      Top = 290
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_OPTION_HEIGTH'
      DataSource = DataSource
      TabOrder = 17
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Opci'#243'n altura'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit19: TUniDBEdit
      Left = 268
      Top = 330
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_TITULO_TOP'
      DataSource = DataSource
      TabOrder = 18
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'T'#237'tulo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit20: TUniDBEdit
      Left = 268
      Top = 370
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MNU_TITULO_AUXILIAR_TOP'
      DataSource = DataSource
      TabOrder = 19
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'T'#237'tulo auxiliar'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
