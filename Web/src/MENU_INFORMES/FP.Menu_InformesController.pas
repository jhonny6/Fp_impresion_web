unit FP.Menu_InformesController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TMenu_InformesCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateMenu_InformesCmd = class(TMenu_InformesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadMenu_InformesCmd = class(TMenu_InformesCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateMenu_InformesCmd = class(TMenu_InformesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteMenu_InformesCmd = class(TMenu_InformesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickMenu_InformesCmd = class(TReadMenu_InformesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Menu_InformesBrowser,
  FP.Menu_InformesModel,
  FP.Menu_InformesView,
  FP.Main;

{ TProvinciasCommand }

function TMenu_InformesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TMenu_InformesModel;
end;

function TMenu_InformesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TMenu_InformesBrowser;
end;

function TMenu_InformesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TMenu_Informes;
end;

{ TCreateProvinciasCmd }

procedure TCreateMenu_InformesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateMenu_InformesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteMenu_InformesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickMenu_InformesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateMenu_InformesCmd, TReadMenu_InformesCmd, TUpdateMenu_InformesCmd, TDeleteMenu_InformesCmd, TPickMenu_InformesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateMenu_InformesCmd, TReadMenu_InformesCmd, TUpdateMenu_InformesCmd, TDeleteMenu_InformesCmd, TPickMenu_InformesCmd]);

end.
