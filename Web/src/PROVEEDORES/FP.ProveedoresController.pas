unit FP.ProveedoresController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TProveedoresCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateProveedoresCmd = class(TProveedoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadProveedoresCmd = class(TProveedoresCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateProveedoresCmd = class(TProveedoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteProveedoresCmd = class(TProveedoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickProveedoresCmd = class(TReadProveedoresCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.ProveedoresBrowser,
  FP.ProveedoresModel,
  FP.ProveedoresView,
  FP.Main;

{ TProvinciasCommand }

function TProveedoresCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TProveedoresModel;
end;

function TProveedoresCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TProveedoresBrowser;
end;

function TProveedoresCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TProveedores;
end;

{ TCreateProvinciasCmd }

procedure TCreateProveedoresCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateProveedoresCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteProveedoresCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickProveedoresCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateProveedoresCmd, TReadProveedoresCmd, TUpdateProveedoresCmd, TDeleteProveedoresCmd, TPickProveedoresCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateProveedoresCmd, TReadProveedoresCmd, TUpdateProveedoresCmd, TDeleteProveedoresCmd, TPickProveedoresCmd]);

end.
