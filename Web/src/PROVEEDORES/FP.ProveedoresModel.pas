unit FP.ProveedoresModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TProveedoresModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ProveedoresModel: TProveedoresModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TProveedoresModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Proveedores_GEST';
end;

function TProveedoresModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TProveedoresModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TProveedoresModel.TableName: string;
begin
  Result := 'fastence.fp0_Proveedores';
end;

end.
