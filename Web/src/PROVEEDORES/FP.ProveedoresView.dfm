inherited Proveedores: TProveedores
  Caption = 'Proveedores'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit1: TUniDBEdit [3]
    Left = 0
    Top = 61
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [4]
    Left = 0
    Top = 97
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CODIGO'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'C'#243'digo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit3: TUniDBEdit [5]
    Left = 0
    Top = 133
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit4: TUniDBEdit [6]
    Left = 0
    Top = 173
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CODEMPRESA'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Cod. Empresa'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit5: TUniDBEdit [7]
    Left = 0
    Top = 213
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CIF'
    DataSource = DataSource
    TabOrder = 7
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'CIF'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit6: TUniDBEdit [8]
    Left = 0
    Top = 253
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'NOMBRE_COMERCIAL'
    DataSource = DataSource
    TabOrder = 8
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Nombre comercial'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit7: TUniDBEdit [9]
    Left = 269
    Top = 61
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DIRECCION'
    DataSource = DataSource
    TabOrder = 9
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Direcci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit8: TUniDBEdit [10]
    Left = 269
    Top = 97
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'POBLACION'
    DataSource = DataSource
    TabOrder = 10
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Poblaci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit9: TUniDBEdit [11]
    Left = 269
    Top = 133
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CODE128'
    DataSource = DataSource
    TabOrder = 11
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'C'#243'digo 128'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit10: TUniDBEdit [12]
    Left = 269
    Top = 173
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'PROVINCIA'
    DataSource = DataSource
    TabOrder = 12
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Provincia'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit11: TUniDBEdit [13]
    Left = 269
    Top = 213
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CPOSTAL'
    DataSource = DataSource
    TabOrder = 13
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'C'#243'digo postal'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit12: TUniDBEdit [14]
    Left = 269
    Top = 253
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'EMAIL'
    DataSource = DataSource
    TabOrder = 14
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Email'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
