unit FP.PaisController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TPaisCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreatePaisCmd = class(TPaisCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadPaisCmd = class(TPaisCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdatePaisCmd = class(TPaisCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeletePaisCmd = class(TPaisCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickPaisCmd = class(TReadPaisCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.PaisBrowser,
  FP.PaisModel,
  FP.PaisView,
  FP.Main;

{ TProvinciasCommand }

function TPaisCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TPaisModel;
end;

function TPaisCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TPaisBrowser;
end;

function TPaisCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TPais;
end;

{ TCreateProvinciasCmd }

procedure TCreatePaisCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdatePaisCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeletePaisCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickPaisCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreatePaisCmd, TReadPaisCmd, TUpdatePaisCmd, TDeletePaisCmd, TPickPaisCmd]);

finalization

TAppCommand.UnegisterCmd([TCreatePaisCmd, TReadPaisCmd, TUpdatePaisCmd, TDeletePaisCmd, TPickPaisCmd]);

end.
