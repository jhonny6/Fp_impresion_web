unit FP.PaisModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TPaisModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  PaisModel: TPaisModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TPaisModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Paises';
end;

function TPaisModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TPaisModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TPaisModel.TableName: string;
begin
  Result := 'fastence.fp0_Paises';
end;

end.
