unit FP.EtiquetasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TEtiquetasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  EtiquetasModel: TEtiquetasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TEtiquetasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Etiquetas_GEST';
end;

function TEtiquetasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TEtiquetasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TEtiquetasModel.TableName: string;
begin
  Result := 'fastence.fp0_Etiquetas';
end;

end.
