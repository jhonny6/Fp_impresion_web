unit FP.EtiquetasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TEtiquetasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateEtiquetasCmd = class(TEtiquetasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadEtiquetasCmd = class(TEtiquetasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateEtiquetasCmd = class(TEtiquetasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteEtiquetasCmd = class(TEtiquetasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickEtiquetasCmd = class(TReadEtiquetasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.EtiquetasBrowser,
  FP.EtiquetasModel,
  FP.EtiquetasView,
  FP.Main;

{ TProvinciasCommand }

function TEtiquetasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TEtiquetasModel;
end;

function TEtiquetasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TEtiquetasBrowser;
end;

function TEtiquetasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TEtiquetas;
end;

{ TCreateProvinciasCmd }

procedure TCreateEtiquetasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateEtiquetasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteEtiquetasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickEtiquetasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateEtiquetasCmd, TReadEtiquetasCmd, TUpdateEtiquetasCmd, TDeleteEtiquetasCmd, TPickEtiquetasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateEtiquetasCmd, TReadEtiquetasCmd, TUpdateEtiquetasCmd, TDeleteEtiquetasCmd, TPickEtiquetasCmd]);

end.
