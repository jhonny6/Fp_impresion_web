unit FP.DestinosModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TDestinosModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  DestinosModel: TDestinosModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TDestinosModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Destinos_GEST';
end;

function TDestinosModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TDestinosModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TDestinosModel.TableName: string;
begin
  Result := 'fastence.fp0_Destinos';
end;

end.
