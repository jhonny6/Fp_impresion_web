unit FP.DestinosController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TDestinosCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateDestinosCmd = class(TDestinosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadDestinosCmd = class(TDestinosCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateDestinosCmd = class(TDestinosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteDestinosCmd = class(TDestinosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickDestinosCmd = class(TReadDestinosCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.DestinosBrowser,
  FP.DestinosModel,
  FP.DestinosView,
  FP.Main;

{ TProvinciasCommand }

function TDestinosCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TDestinosModel;
end;

function TDestinosCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TDestinosBrowser;
end;

function TDestinosCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TDestinos;
end;

{ TCreateProvinciasCmd }

procedure TCreateDestinosCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateDestinosCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteDestinosCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickDestinosCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateDestinosCmd, TReadDestinosCmd, TUpdateDestinosCmd, TDeleteDestinosCmd, TPickDestinosCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateDestinosCmd, TReadDestinosCmd, TUpdateDestinosCmd, TDeleteDestinosCmd, TPickDestinosCmd]);

end.
