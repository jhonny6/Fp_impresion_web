unit FP.Tipo_Codigo_BarrasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTipo_Codigo_BarrasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Tipo_Codigo_BarrasModel: TTipo_Codigo_BarrasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TTipo_Codigo_BarrasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Tipo_Codigo_Barras';
end;

function TTipo_Codigo_BarrasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TTipo_Codigo_BarrasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TTipo_Codigo_BarrasModel.TableName: string;
begin
  Result := 'fastence.fp0_Tipos_Codigos_Barras';
end;

end.
