inherited Tipo_Codigo_Barras: TTipo_Codigo_Barras
  Caption = 'Tipo_Codigo_Barras'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit3: TUniDBEdit [3]
    Left = 56
    Top = 168
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'FUNCION_VALIDACION'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Fun. Validaci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 56
    Top = 66
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 56
    Top = 117
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'CODIGO'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'C'#243'digo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
