unit FP.Tipo_Codigo_BarrasView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Vcl.DBActns,
  System.Actions, Vcl.ActnList, uniMainMenu, Data.DB, uniStatusBar,
  uniGUIClasses, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit;

type
  TTipo_Codigo_Barras = class(TEntityView)
    UniDBEdit3: TUniDBEdit;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Tipo_Codigo_Barras: TTipo_Codigo_Barras;

implementation

{$R *.dfm}

end.
