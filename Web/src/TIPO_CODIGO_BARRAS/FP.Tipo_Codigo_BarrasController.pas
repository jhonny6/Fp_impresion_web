unit FP.Tipo_Codigo_BarrasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTipo_Codigo_BarrasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateTipo_Codigo_BarrasCmd = class(TTipo_Codigo_BarrasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTipo_Codigo_BarrasCmd = class(TTipo_Codigo_BarrasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTipo_Codigo_BarrasCmd = class(TTipo_Codigo_BarrasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTipo_Codigo_BarrasCmd = class(TTipo_Codigo_BarrasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTipo_Codigo_BarrasCmd = class(TReadTipo_Codigo_BarrasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Tipo_Codigo_BarrasBrowser,
  FP.Tipo_Codigo_BarrasModel,
  FP.Tipo_Codigo_BarrasView,
  FP.Main;

{ TProvinciasCommand }

function TTipo_Codigo_BarrasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTipo_Codigo_BarrasModel;
end;

function TTipo_Codigo_BarrasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTipo_Codigo_BarrasBrowser;
end;

function TTipo_Codigo_BarrasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTipo_Codigo_Barras;
end;

{ TCreateProvinciasCmd }

procedure TCreateTipo_Codigo_BarrasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateTipo_Codigo_BarrasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteTipo_Codigo_BarrasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickTipo_Codigo_BarrasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTipo_Codigo_BarrasCmd, TReadTipo_Codigo_BarrasCmd, TUpdateTipo_Codigo_BarrasCmd, TDeleteTipo_Codigo_BarrasCmd, TPickTipo_Codigo_BarrasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTipo_Codigo_BarrasCmd, TReadTipo_Codigo_BarrasCmd, TUpdateTipo_Codigo_BarrasCmd, TDeleteTipo_Codigo_BarrasCmd, TPickTipo_Codigo_BarrasCmd]);

end.
