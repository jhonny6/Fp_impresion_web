unit FP.Regulariza_CausasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TRegulariza_CausasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Regulariza_CausasModel: TRegulariza_CausasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TRegulariza_CausasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Regulariza_Causas';
end;

function TRegulariza_CausasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TRegulariza_CausasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TRegulariza_CausasModel.TableName: string;
begin
  Result := 'fastence.fp0_Regulariza_Causas';
end;

end.
