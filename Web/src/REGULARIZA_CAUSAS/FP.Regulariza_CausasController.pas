unit FP.Regulariza_CausasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TRegulariza_CausasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateRegulariza_CausasCmd = class(TRegulariza_CausasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadRegulariza_CausasCmd = class(TRegulariza_CausasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateRegulariza_CausasCmd = class(TRegulariza_CausasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteRegulariza_CausasCmd = class(TRegulariza_CausasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickRegulariza_CausasCmd = class(TReadRegulariza_CausasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Regulariza_CausasBrowser,
  FP.Regulariza_CausasModel,
  FP.Regulariza_CausasView,
  FP.Main;

{ TProvinciasCommand }

function TRegulariza_CausasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TRegulariza_CausasModel;
end;

function TRegulariza_CausasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TRegulariza_CausasBrowser;
end;

function TRegulariza_CausasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TRegulariza_Causas;
end;

{ TCreateProvinciasCmd }

procedure TCreateRegulariza_CausasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateRegulariza_CausasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteRegulariza_CausasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickRegulariza_CausasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateRegulariza_CausasCmd, TReadRegulariza_CausasCmd, TUpdateRegulariza_CausasCmd, TDeleteRegulariza_CausasCmd, TPickRegulariza_CausasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateRegulariza_CausasCmd, TReadRegulariza_CausasCmd, TUpdateRegulariza_CausasCmd, TDeleteRegulariza_CausasCmd, TPickRegulariza_CausasCmd]);

end.
