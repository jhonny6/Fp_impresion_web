inherited Regulariza_Causas: TRegulariza_Causas
  Caption = 'Regulariza_Causas'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 148
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 70
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 126
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'REGULARIZA_CAUSA'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Causa regulaci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
