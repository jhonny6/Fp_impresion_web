unit FP.Mapa_almacenModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TMapa_almacenModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;
  end;

var
  Mapa_almacenModel: TMapa_almacenModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TMapa_almacenModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'GV_FP0_Mapa_almacen_GEST';
end;

function TMapa_almacenModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TMapa_almacenModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TMapa_almacenModel.TableName: string;
begin
  Result := 'fastence.gv_fp0_Mapa_almacen';
end;

end.
