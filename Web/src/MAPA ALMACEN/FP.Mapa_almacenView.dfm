inherited Mapa_almacen: TMapa_almacen
  Caption = 'Mapa_almacen'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 133
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 70
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CODIGO_PROVINCIA'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo Provincia'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 111
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'PROVINCIA'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Provincia'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  inherited DataSource: TDataSource
    Left = 324
    Top = 134
  end
  inherited UniActionList1: TUniActionList
    Left = 224
  end
end
