unit FP.Mapa_almacenController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TMapa_almacenCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateMapa_almacenCmd = class(TMapa_almacenCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadMapa_almacenCmd = class(TMapa_almacenCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateMapa_almacenCmd = class(TMapa_almacenCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteMapa_almacenCmd = class(TMapa_almacenCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickMapa_almacenCmd = class(TReadMapa_almacenCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Mapa_almacenBrowser,
  FP.Mapa_almacenModel,
  FP.Mapa_almacenView,
  FP.Main;

{ TProvinciasCommand }

function TMapa_almacenCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TMapa_almacenModel;
end;

function TMapa_almacenCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TMapa_almacenBrowser;
end;

function TMapa_almacenCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TMapa_almacen;
end;

{ TCreateProvinciasCmd }

procedure TCreateMapa_almacenCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateMapa_almacenCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteMapa_almacenCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickMapa_almacenCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateMapa_almacenCmd, TReadMapa_almacenCmd, TUpdateMapa_almacenCmd, TDeleteMapa_almacenCmd, TPickMapa_almacenCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateMapa_almacenCmd, TReadMapa_almacenCmd, TUpdateMapa_almacenCmd, TDeleteMapa_almacenCmd, TPickMapa_almacenCmd]);

end.
