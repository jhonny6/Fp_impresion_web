unit FP.Mapa_almacenBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniEdit, uniLabel,
  uniDBNavigator, uniButton, uniBitBtn, uniSpeedButton, uniGUIClasses, uniPanel,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid, uniSplitter, uniCheckBox,
  uniDateTimePicker, uniGridExporters, uniGUIAbstractClasses;

type
  TMapa_almacenBrowser = class(TEntityBrowser)
    pnlFiltroBuscar: TUniPanel;
    UniPanel3: TUniPanel;
    UniLabel4: TUniLabel;
    UniCheckBox7: TUniCheckBox;
    UniCheckBox8: TUniCheckBox;
    UniLabel5: TUniLabel;
    UniCheckBox9: TUniCheckBox;
    UniCheckBox10: TUniCheckBox;
    UniLabel6: TUniLabel;
    UniCheckBox11: TUniCheckBox;
    UniCheckBox12: TUniCheckBox;
    UniPanel5: TUniPanel;
    UniLabel7: TUniLabel;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    Edesdepasillo: TUniEdit;
    Ehastapasillo: TUniEdit;
    UniPanel6: TUniPanel;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    edNumUbicacion: TUniEdit;
    UniPanel8: TUniPanel;
    UniLabel20: TUniLabel;
    UniLabel21: TUniLabel;
    UniLabel22: TUniLabel;
    Edesdecolumna: TUniEdit;
    Ehastacolumna: TUniEdit;
    UniPanel4: TUniPanel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel18: TUniLabel;
    Edesdealtura: TUniEdit;
    Ehastaaltura: TUniEdit;
    procedure UniSpeedButton2Click(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Mapa_almacenBrowser: TMapa_almacenBrowser;

implementation

{$R *.dfm}

procedure TMapa_almacenBrowser.DataSourceDataChange(Sender: TObject;
  Field: TField);
  var
  vCol, vColAc: TUniBaseDBGridColumn;
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
  begin
    if (UniDBGrid1.Columns.Count > 0) then
    begin
      // *** Codigo para la lista
      vCol := UniDBGrid1.Columns.ColumnFromFieldName('VALORACION');

      if Assigned(vCol) then
      begin
        vCol.PickList.Clear;
        vCol.PickList.Add('Bueno');
        vCol.PickList.Add('Regular');
        vCol.PickList.Add('Malo');

        vCol.Width := 100;
      end;
      // *** Codigo para las acciones
      vColAc := UniDBGrid1.ColumnFromCaption('Acciones');

      if not Assigned(vColAc) then
      begin
        vColAc := UniDBGrid1.Columns.Add;
        vColAc.ActionColumn.Enabled := True;
        vColAc.ActionColumn.Buttons.Add;

        vColAc.Title.Caption := 'Acciones';
        vColAc.ActionColumn.Buttons[0].ButtonId := 0;
        vColAc.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
        vColAc.ActionColumn.Buttons[0].IconCls := 'action';
        vColAc.Visible := True;
        vColAc.Width := 75;
      end;
    end;
  end;
end;

procedure TMapa_almacenBrowser.UniSpeedButton2Click(Sender: TObject);
//  inherited;
var
  vCondicion: String;
begin
  // inherited;
  vCondicion := EmptyStr;
  // if DesdePasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo>=' + quotedstr(DesdePasillo.text);
  //
  // if HastaPasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo<=' + quotedstr(HastaPasillo.text);

  if Edesdepasillo.text <> '' then
  begin
    vCondicion := vCondicion + ' and pasillo = ' + Edesdepasillo.text;
  end;

  if Edesdecolumna.text <> '' then
  begin
    vCondicion := vCondicion + ' and columna>=' + Edesdecolumna.text;
  end;

  if Ehastacolumna.text <> '' then
  begin
    vCondicion := vCondicion + ' and columna<=' + Ehastacolumna.text;
  end;

  if Edesdealtura.text <> '' then
  begin
    vCondicion := vCondicion + ' and altura>=' + Edesdealtura.text;
  end;

  if Ehastaaltura.text <> '' then
  begin
    vCondicion := vCondicion + ' and altura<=' + Ehastaaltura.text;
  end;

  if edNumUbicacion.Text <> '' then
  begin
    vCondicion := vCondicion + ' and ubicacion = ' + edNumUbicacion.Text;
  end;

  FiltrarYEjecutar(vCondicion);
end;

end.
