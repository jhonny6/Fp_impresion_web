inherited Mapa_almacenBrowser: TMapa_almacenBrowser
  Caption = 'Mapa_almacenBrowser'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniDBGrid1: TUniDBGrid
    Top = 398
    Height = 334
  end
  inherited edBusqueda: TUniEdit
    Top = 352
    ExplicitTop = 352
  end
  object pnlFiltroBuscar: TUniPanel [3]
    AlignWithMargins = True
    Left = 3
    Top = 76
    Width = 1182
    Height = 270
    Hint = ''
    AutoScroll = True
    Align = alTop
    TabOrder = 3
    Caption = ''
    ScrollHeight = 270
    ScrollWidth = 1182
    object UniPanel3: TUniPanel
      Left = 2
      Top = -6
      Width = 256
      Height = 140
      Hint = ''
      TabOrder = 1
      Caption = ''
      object UniLabel4: TUniLabel
        Left = 16
        Top = 8
        Width = 114
        Height = 13
        Hint = ''
        Caption = 'Mercancia Disponible:'
        TabOrder = 1
      end
      object UniCheckBox7: TUniCheckBox
        Left = 32
        Top = 27
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'S'#237
        TabOrder = 2
      end
      object UniCheckBox8: TUniCheckBox
        Left = 128
        Top = 30
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'No'
        TabOrder = 3
      end
      object UniLabel5: TUniLabel
        Left = 16
        Top = 49
        Width = 121
        Height = 13
        Hint = ''
        Caption = 'Mercancia por Encargo:'
        TabOrder = 4
      end
      object UniCheckBox9: TUniCheckBox
        Left = 33
        Top = 68
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'S'#237
        TabOrder = 5
      end
      object UniCheckBox10: TUniCheckBox
        Left = 127
        Top = 68
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'No'
        TabOrder = 6
      end
      object UniLabel6: TUniLabel
        Left = 16
        Top = 89
        Width = 127
        Height = 13
        Hint = ''
        Caption = 'Expediciones Preparadas'
        TabOrder = 7
      end
      object UniCheckBox11: TUniCheckBox
        Left = 33
        Top = 108
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'S'#237
        TabOrder = 8
      end
      object UniCheckBox12: TUniCheckBox
        Left = 128
        Top = 108
        Width = 97
        Height = 17
        Hint = ''
        Caption = 'No'
        TabOrder = 9
      end
    end
    object UniPanel5: TUniPanel
      Left = 502
      Top = 3
      Width = 257
      Height = 136
      Hint = ''
      TabOrder = 2
      Caption = ''
      object UniLabel7: TUniLabel
        Left = 80
        Top = 3
        Width = 33
        Height = 13
        Hint = ''
        Caption = 'Pasillo'
        TabOrder = 1
      end
      object UniLabel14: TUniLabel
        Left = 10
        Top = 39
        Width = 68
        Height = 13
        Hint = ''
        Caption = 'Desde Pasillo'
        TabOrder = 2
      end
      object UniLabel15: TUniLabel
        Left = 10
        Top = 66
        Width = 65
        Height = 13
        Hint = ''
        Caption = 'Hasta Pasillo'
        TabOrder = 3
      end
      object Edesdepasillo: TUniEdit
        Left = 140
        Top = 35
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 4
      end
      object Ehastapasillo: TUniEdit
        Left = 140
        Top = 62
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 5
      end
    end
    object UniPanel6: TUniPanel
      Left = 258
      Top = -3
      Width = 245
      Height = 137
      Hint = ''
      TabOrder = 3
      Caption = ''
      object UniLabel16: TUniLabel
        Left = 80
        Top = 3
        Width = 105
        Height = 13
        Hint = ''
        Caption = 'B'#250'squeda Ubicaci'#243'n'
        TabOrder = 1
      end
      object UniLabel17: TUniLabel
        Left = 10
        Top = 39
        Width = 51
        Height = 13
        Hint = ''
        Caption = 'Ubicaci'#243'n'
        TabOrder = 2
      end
      object edNumUbicacion: TUniEdit
        Left = 140
        Top = 37
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 3
      end
    end
    object UniPanel8: TUniPanel
      Left = 765
      Top = 3
      Width = 257
      Height = 136
      Hint = ''
      TabOrder = 4
      Caption = ''
      object UniLabel20: TUniLabel
        Left = 80
        Top = 3
        Width = 46
        Height = 13
        Hint = ''
        Caption = 'Columna'
        TabOrder = 1
      end
      object UniLabel21: TUniLabel
        Left = 10
        Top = 39
        Width = 81
        Height = 13
        Hint = ''
        Caption = 'Desde Columna'
        TabOrder = 2
      end
      object UniLabel22: TUniLabel
        Left = 10
        Top = 66
        Width = 78
        Height = 13
        Hint = ''
        Caption = 'Hasta Columna'
        TabOrder = 3
      end
      object Edesdecolumna: TUniEdit
        Left = 140
        Top = 35
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 4
      end
      object Ehastacolumna: TUniEdit
        Left = 140
        Top = 62
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 5
      end
    end
    object UniPanel4: TUniPanel
      Left = 3
      Top = 134
      Width = 257
      Height = 136
      Hint = ''
      TabOrder = 5
      Caption = ''
      object UniLabel2: TUniLabel
        Left = 80
        Top = 3
        Width = 31
        Height = 13
        Hint = ''
        Caption = 'Altura'
        TabOrder = 1
      end
      object UniLabel3: TUniLabel
        Left = 10
        Top = 39
        Width = 66
        Height = 13
        Hint = ''
        Caption = 'Desde Altura'
        TabOrder = 2
      end
      object UniLabel18: TUniLabel
        Left = 10
        Top = 66
        Width = 63
        Height = 13
        Hint = ''
        Caption = 'Hasta Altura'
        TabOrder = 3
      end
      object Edesdealtura: TUniEdit
        Left = 140
        Top = 35
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 4
      end
      object Ehastaaltura: TUniEdit
        Left = 140
        Top = 62
        Width = 85
        Height = 21
        Hint = ''
        Text = ''
        TabOrder = 5
      end
    end
  end
  inherited DataSource: TDataSource
    OnDataChange = DataSourceDataChange
  end
end
