unit CashBank.Cajon_diarioModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TCajon_diarioModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TCajon_diarioModel }

class function TCajon_diarioModel.TableName: string;
begin
  Result := 'cashbank.cajon_diario';
end;

end.
