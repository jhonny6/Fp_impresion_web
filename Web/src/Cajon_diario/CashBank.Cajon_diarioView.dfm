inherited Cajon_Diario: TCajon_Diario
  ClientHeight = 607
  ClientWidth = 729
  Caption = 'Cajon_Diario'
  ExplicitWidth = 745
  ExplicitHeight = 646
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 544
    ExplicitWidth = 729
    ExplicitHeight = 544
    ScrollHeight = 352
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'fecha_hora'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'saldo_dispensadores'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Saldo dispensadores'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 86
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'saldo_cofres'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Saldo cofres'
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 126
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'sensor_puerta_valor'
      DataSource = DataSource
      TabOrder = 3
      LayoutConfig.Margin = '20'
      FieldLabel = 'Valor sensor puerta'
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 166
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'sensor_cofre_valor'
      DataSource = DataSource
      TabOrder = 4
      LayoutConfig.Margin = '20'
      FieldLabel = 'Valor sensor cofre'#13#10
    end
    object UniDBEdit6: TUniDBEdit
      Left = 56
      Top = 206
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'sensor_temperatura_valor'
      DataSource = DataSource
      TabOrder = 5
      LayoutConfig.Margin = '20'
      FieldLabel = 'Valor sensor temperatura'#13#10#13#10
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 243
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'sensor_placa'
      DataSource = DataSource
      Caption = ''
      TabOrder = 6
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sensor placa'
    end
    object UniDBCheckBox2: TUniDBCheckBox
      Left = 56
      Top = 266
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'sensor_puerta'
      DataSource = DataSource
      Caption = ''
      TabOrder = 7
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sensor puerta'#13#10
    end
    object UniDBCheckBox3: TUniDBCheckBox
      Left = 56
      Top = 289
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'sensor_cofre'
      DataSource = DataSource
      Caption = ''
      TabOrder = 8
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sensor cofre'
    end
    object UniDBCheckBox4: TUniDBCheckBox
      Left = 56
      Top = 312
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'sensor_temperatura'
      DataSource = DataSource
      Caption = ''
      TabOrder = 10
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sensor temperatura'
    end
    object UniDBCheckBox5: TUniDBCheckBox
      Left = 56
      Top = 335
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'sensor_sismico'
      DataSource = DataSource
      Caption = ''
      TabOrder = 9
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Sensor s'#237'smico'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 585
    Width = 729
    ExplicitTop = 585
    ExplicitWidth = 729
  end
end
