unit CashBank.Cajon_diarioController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TCajon_diarioCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateCajon_diarioCmd = class(TCajon_diarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadCajon_diarioCmd = class(TCajon_diarioCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateCajon_diarioCmd = class(TCajon_diarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteCajon_diarioCmd = class(TCajon_diarioCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickCajon_diarioCmd = class(TReadCajon_diarioCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Cajon_diarioBrowser,
  CashBank.Cajon_diarioModel,
  CashBank.Cajon_diarioView,
  FP.Main;

{ TCajon_diarioCommand }

class function TCajon_diarioCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TCajon_diarioModel;
end;

class function TCajon_diarioCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TCajon_diarioBrowser;
end;

class function TCajon_diarioCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TCajon_diario;
end;

{ TCreateCajon_diarioCmd }

procedure TCreateCajon_diarioCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadCajon_diarioCmd }

procedure TReadCajon_diarioCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadCajon_diarioCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateCajon_diarioCmd }

procedure TUpdateCajon_diarioCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteCajon_diarioCmd }

procedure TDeleteCajon_diarioCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickCajon_diarioCmd }

procedure TPickCajon_diarioCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateCajon_diarioCmd, TReadCajon_diarioCmd, TUpdateCajon_diarioCmd, TDeleteCajon_diarioCmd, TPickCajon_diarioCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateCajon_diarioCmd, TReadCajon_diarioCmd, TUpdateCajon_diarioCmd, TDeleteCajon_diarioCmd, TPickCajon_diarioCmd]);

end.
