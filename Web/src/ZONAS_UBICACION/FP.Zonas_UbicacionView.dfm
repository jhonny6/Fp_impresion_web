inherited Zonas_Ubicacion: TZonas_Ubicacion
  Caption = 'Zonas_Ubicacion'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ExplicitTop = 44
    ScrollHeight = 140
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 30
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 118
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'DESCRIPCION'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Descripci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 74
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ORDEN'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Orden'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
