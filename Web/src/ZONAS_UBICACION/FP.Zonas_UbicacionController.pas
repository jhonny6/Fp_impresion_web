unit FP.Zonas_UbicacionController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TZonas_UbicacionCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateZonas_UbicacionCmd = class(TZonas_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadZonas_UbicacionCmd = class(TZonas_UbicacionCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateZonas_UbicacionCmd = class(TZonas_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteZonas_UbicacionCmd = class(TZonas_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickZonas_UbicacionCmd = class(TReadZonas_UbicacionCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Zonas_UbicacionBrowser,
  FP.Zonas_UbicacionModel,
  FP.Zonas_UbicacionView,
  FP.Main;

{ TProvinciasCommand }

function TZonas_UbicacionCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TZonas_UbicacionModel;
end;

function TZonas_UbicacionCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TZonas_UbicacionBrowser;
end;

function TZonas_UbicacionCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TZonas_Ubicacion;
end;

{ TCreateProvinciasCmd }

procedure TCreateZonas_UbicacionCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateZonas_UbicacionCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteZonas_UbicacionCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickZonas_UbicacionCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateZonas_UbicacionCmd, TReadZonas_UbicacionCmd, TUpdateZonas_UbicacionCmd, TDeleteZonas_UbicacionCmd, TPickZonas_UbicacionCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateZonas_UbicacionCmd, TReadZonas_UbicacionCmd, TUpdateZonas_UbicacionCmd, TDeleteZonas_UbicacionCmd, TPickZonas_UbicacionCmd]);

end.
