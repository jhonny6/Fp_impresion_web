unit FP.Zonas_UbicacionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TZonas_UbicacionModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Zonas_UbicacionModel: TZonas_UbicacionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TZonas_UbicacionModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Zonas_Ubicacion_GEST';
end;

function TZonas_UbicacionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TZonas_UbicacionModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TZonas_UbicacionModel.TableName: string;
begin
  Result := 'fastence.fp0_zonas_ubicacion';
end;

end.
