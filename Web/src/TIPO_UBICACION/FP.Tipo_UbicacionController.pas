unit FP.Tipo_UbicacionController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTipo_UbicacionCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateTipo_UbicacionCmd = class(TTipo_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTipo_UbicacionCmd = class(TTipo_UbicacionCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTipo_UbicacionCmd = class(TTipo_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTipo_UbicacionCmd = class(TTipo_UbicacionCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTipo_UbicacionCmd = class(TReadTipo_UbicacionCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Tipo_UbicacionBrowser,
  FP.Tipo_UbicacionModel,
  FP.Tipo_UbicacionView,
  FP.Main;

{ TProvinciasCommand }

function TTipo_UbicacionCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTipo_UbicacionModel;
end;

function TTipo_UbicacionCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTipo_UbicacionBrowser;
end;

function TTipo_UbicacionCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTipo_Ubicacion;
end;

{ TCreateProvinciasCmd }

procedure TCreateTipo_UbicacionCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateTipo_UbicacionCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteTipo_UbicacionCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickTipo_UbicacionCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTipo_UbicacionCmd, TReadTipo_UbicacionCmd, TUpdateTipo_UbicacionCmd, TDeleteTipo_UbicacionCmd, TPickTipo_UbicacionCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTipo_UbicacionCmd, TReadTipo_UbicacionCmd, TUpdateTipo_UbicacionCmd, TDeleteTipo_UbicacionCmd, TPickTipo_UbicacionCmd]);

end.
