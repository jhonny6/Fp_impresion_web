inherited Tipo_Ubicacion: TTipo_Ubicacion
  Caption = 'Tipo_Ubicacion'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 143
    ScrollWidth = 356
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 81
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'tipoubi'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Tipo ubicaci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 37
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 121
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'DESCRIPCION'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Descripci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
