unit FP.Tipo_UbicacionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTipo_UbicacionModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Tipo_UbicacionModel: TTipo_UbicacionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TTipo_UbicacionModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Tipo_Ubicacion_GEST';
end;

function TTipo_UbicacionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TTipo_UbicacionModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TTipo_UbicacionModel.TableName: string;
begin
  Result := 'fastence.fp0_tipo_ubicacion';
end;

end.
