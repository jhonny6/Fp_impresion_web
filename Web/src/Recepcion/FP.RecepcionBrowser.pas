unit FP.RecepcionBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowserMD, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniBasicGrid,
  uniDBGrid, uniPageControl, uniGUIClasses, uniEdit, uniLabel, uniDBNavigator,
  uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses, uniPanel,
  uniCheckBox, DBAccess, Uni, uniDBEdit, uniDateTimePicker,
  uniDBDateTimePicker, uniGridExporters;

type
  TRecepcionBrowser = class(TEntityBrowserMD)
    UniPanel2: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    Edesdenumero: TUniEdit;
    Ehastanumero: TUniEdit;
    UniPanel3: TUniPanel;
    UniLabel4: TUniLabel;
    UniCheckBox7: TUniCheckBox;
    UniCheckBox8: TUniCheckBox;
    UniLabel5: TUniLabel;
    UniCheckBox9: TUniCheckBox;
    UniCheckBox10: TUniCheckBox;
    UniLabel6: TUniLabel;
    UniCheckBox11: TUniCheckBox;
    UniCheckBox12: TUniCheckBox;
    DBETipoPedido: TUniDBEdit;
    UniLabel10: TUniLabel;
    DBENumero: TUniDBEdit;
    DBEAno: TUniDBEdit;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniLabel13: TUniLabel;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    procedure UniSpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RecepcionBrowser: TRecepcionBrowser;

implementation

{$R *.dfm}

procedure TRecepcionBrowser.UniSpeedButton2Click(Sender: TObject);
var
  vCondicion: String;
begin
  // inherited;
  vCondicion := EmptyStr;
  // if DesdePasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo>=' + quotedstr(DesdePasillo.text);
  //
  // if HastaPasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo<=' + quotedstr(HastaPasillo.text);

  if Edesdenumero.text <> '' then
  begin
    vCondicion := vCondicion + ' and numero>=' + Edesdenumero.text;
  end;

  if Ehastanumero.text <> '' then
  begin
    vCondicion := vCondicion + ' and numero<=' + Ehastanumero.text;
  end;

  FiltrarYEjecutar(vCondicion);
end;

end.
