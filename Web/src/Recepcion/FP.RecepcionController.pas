unit FP.RecepcionController;

interface

uses CashBank.AppControllerMD,
  CashBank.EntityBrowserMD,
  CashBank.EntityViewMD,
  CashBank.EntityModelMD, uniGUIClasses;

type

  TRecepcionCommand = class(TAppCommandMD)
  public
    function EntityModelClass: TEntityModelClassMD; override;
    function EntityBrowserClass: TEntityBrowserClassMD; override;
    function EntityViewClass: TEntityViewClassMD; override;
  end;

  TCreateRecepcionCmd = class(TRecepcionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TReadRecepcionCmd = class(TRecepcionCommand)
  protected
    // View: TEntityBrowser;
    // Model: TEntityModel;
  public
    // procedure Proccess(Args: TCommandArgs); overload;override;
    // procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateRecepcionCmd = class(TRecepcionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TDeleteRecepcionCmd = class(TRecepcionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TPickRecepcionCmd = class(TReadRecepcionCmd)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.RecepcionBrowser,
  FP.RecepcionModel,
  FP.RecepcionView,
  FP.Main;

{ TRecepcionCommand }

function TRecepcionCommand.EntityModelClass: TEntityModelClassMD;
begin
  Result := TRecepcionModel;
end;

function TRecepcionCommand.EntityBrowserClass: TEntityBrowserClassMD;
begin
  Result := TRecepcionBrowser;
end;

function TRecepcionCommand.EntityViewClass: TEntityViewClassMD;
begin
  Result := TRecepcion;
end;

{ TCreateRecepcionCmd }

procedure TCreateRecepcionCmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadRecepcionCmd }

{
  procedure TReadRecepcionCmd.Proccess(Args: TCommandArgs);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
}

(*
  procedure TReadRecepcionCmd.Proccess(Args: TUniControl);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
*)
{ TUpdateRecepcionCmd }

procedure TUpdateRecepcionCmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteRecepcionCmd }

procedure TDeleteRecepcionCmd.Proccess(Args: TCommandArgsMD);
var
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  // if not ConfirmDlg('Desea borrar este registro?') then
  // Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickRecepcionCmd }

procedure TPickRecepcionCmd.Proccess(Args: TCommandArgsMD);
begin
  inherited;
  // View.PickMode := True;
end;

initialization

TAppCommandMD.RegisterCmd([TCreateRecepcionCmd, TReadRecepcionCmd, TUpdateRecepcionCmd,
  TDeleteRecepcionCmd, TPickRecepcionCmd]);

finalization

TAppCommandMD.UnegisterCmd([TCreateRecepcionCmd, TReadRecepcionCmd, TUpdateRecepcionCmd,
  TDeleteRecepcionCmd, TPickRecepcionCmd]);

end.
