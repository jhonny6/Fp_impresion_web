unit FP.RecepcionModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModelMD,
  DBAccess, Uni, Data.DB, MemDS;

type
  TRecepcionModel = class(TEntityModelMD)
  private
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  public
    class function TableName: string; override;
    class function PackageNameEnc: string; override;
    class function PackageNameDet: string; override;
  end;

var
  RecepcionModel: TRecepcionModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}

class function TRecepcionModel.PackageNameDet: string;
begin
  Result := '@pkg01_grid_gestion.GV_FP01_PEDIDOSC_GESTDET';
end;

class function TRecepcionModel.PackageNameEnc: string;
begin
  Result := '@pkg01_grid_gestion.GV_FP01_PEDIDOSC_GESTCAB';
end;

function TRecepcionModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TRecepcionModel.SQLFindAll_with_FK: string;
// var
// JoinModel: TEntityModelClass;
// JoinTableName: string;
// JoinPKName: string;
// JoinColumn: string;
// FKName: string;
begin
  // JoinModel := TMonedasModel;
  // JoinPKName := JoinModel.PKName;
  // JoinTableName := JoinModel.TableName;
  // JoinColumn := 'nombre_corto as "monedas.moneda"';
  // FKName := 'id_moneda';
  // Result := Format('select %0:s.*, %1:s.%4:s from %0:s left outer join %1:s on(%0:s.%2:s = %1:s.%3:s)', [TableName, JoinTableName, FKName,
  // JoinPKName, JoinColumn]);

  Result := 'select * from ' + TableName + ' WHERE 1 = 0'//' OFFSET 0 ROWS FETCH NEXT 200 ROWS ONLY';

end;

class function TRecepcionModel.TableName: string;
begin
  Result := 'fastence.gv_fp01_pedidosc_gestcab';
end;

end.
