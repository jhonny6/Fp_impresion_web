unit CashBank.CajonesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TCajonesModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TCajonesModel }

class function TCajonesModel.TableName: string;
begin
  Result := 'cashbank.cajones';
end;

end.
