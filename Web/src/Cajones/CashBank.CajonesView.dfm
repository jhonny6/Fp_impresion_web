inherited Cajones: TCajones
  ClientHeight = 802
  ClientWidth = 729
  Caption = 'Cajones'
  ExplicitWidth = 745
  ExplicitHeight = 841
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 739
    ExplicitWidth = 729
    ExplicitHeight = 739
    ScrollHeight = 656
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'nombre'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'numero_serie'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'N'#250'mero de serie'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 86
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'direccion'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Direcci'#243'n'
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 126
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'localidad'
      DataSource = DataSource
      TabOrder = 3
      LayoutConfig.Margin = '20'
      FieldLabel = 'Localidad'
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 166
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'provincia'
      DataSource = DataSource
      TabOrder = 4
      LayoutConfig.Margin = '20'
      FieldLabel = 'Provincia'
    end
    object UniDBEdit8: TUniDBEdit
      Left = 56
      Top = 318
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'email'
      DataSource = DataSource
      TabOrder = 6
      LayoutConfig.Margin = '20'
      FieldLabel = 'Email'
    end
    object UniDBEdit9: TUniDBEdit
      Left = 56
      Top = 358
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'ip'
      DataSource = DataSource
      TabOrder = 7
      LayoutConfig.Margin = '20'
      FieldLabel = 'Ip'
    end
    object UniDBEdit10: TUniDBEdit
      Left = 56
      Top = 398
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'puerto'
      DataSource = DataSource
      TabOrder = 8
      LayoutConfig.Margin = '20'
      FieldLabel = 'Puerto'
    end
    object UniDBEdit11: TUniDBEdit
      Left = 56
      Top = 438
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'fecha_hora_ultimo_monitoreo'
      DataSource = DataSource
      TabOrder = 9
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha '#250'ltmo monitoreo'
    end
    object UniDBEdit12: TUniDBEdit
      Left = 56
      Top = 478
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'estado_maquina'
      DataSource = DataSource
      TabOrder = 10
      LayoutConfig.Margin = '20'
      FieldLabel = 'Estado m'#225'quina'
    end
    object UniDBEdit13: TUniDBEdit
      Left = 56
      Top = 518
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'id_pais'
      DataSource = DataSource
      TabOrder = 11
      LayoutConfig.Margin = '20'
      FieldLabel = 'Pais'
    end
    object UniDBEdit14: TUniDBEdit
      Left = 56
      Top = 558
      Width = 200
      Height = 22
      Hint = ''
      DataField = 'id_codigo_postal'
      DataSource = DataSource
      TabOrder = 12
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo postal'
    end
    object UniDBEdit15: TUniDBEdit
      Left = 56
      Top = 598
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'id_empresa'
      DataSource = DataSource
      TabOrder = 14
      LayoutConfig.Margin = '20'
      FieldLabel = 'Empresa'
    end
    object UniDBMemo1: TUniDBMemo
      Left = 56
      Top = 206
      Width = 300
      Height = 89
      Hint = ''
      Margins.Left = 20
      DataField = 'observaciones'
      DataSource = DataSource
      TabOrder = 5
      LayoutConfig.Margin = '20'
      FieldLabel = 'Observaciones'
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 639
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'activo'
      DataSource = DataSource
      Caption = ''
      TabOrder = 13
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Administrador'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 780
    Width = 729
    ExplicitTop = 780
    ExplicitWidth = 729
  end
end
