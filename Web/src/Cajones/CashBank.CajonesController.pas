unit CashBank.CajonesController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TCajonesCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateCajonesCmd = class(TCajonesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadCajonesCmd = class(TCajonesCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateCajonesCmd = class(TCajonesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteCajonesCmd = class(TCajonesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickCajonesCmd = class(TReadCajonesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.CajonesBrowser,
  CashBank.CajonesModel,
  CashBank.CajonesView,
  FP.Main;

{ TCajonesCommand }

class function TCajonesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TCajonesModel;
end;

class function TCajonesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TCajonesBrowser;
end;

class function TCajonesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TCajones;
end;

{ TCreateCajonesCmd }

procedure TCreateCajonesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadCajonesCmd }

procedure TReadCajonesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadCajonesCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateCajonesCmd }

procedure TUpdateCajonesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteCajonesCmd }

procedure TDeleteCajonesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickCajonesCmd }

procedure TPickCajonesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateCajonesCmd, TReadCajonesCmd, TUpdateCajonesCmd, TDeleteCajonesCmd, TPickCajonesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateCajonesCmd, TReadCajonesCmd, TUpdateCajonesCmd, TDeleteCajonesCmd, TPickCajonesCmd]);

end.
