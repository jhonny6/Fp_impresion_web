unit FP.Etiquetas_ControlModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TEtiquetas_ControlModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Etiquetas_ControlModel: TEtiquetas_ControlModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TEtiquetas_ControlModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Etiquetas_Control';
end;

function TEtiquetas_ControlModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TEtiquetas_ControlModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TEtiquetas_ControlModel.TableName: string;
begin
  Result := 'fastence.fp0_etiquetas_control';
end;

end.
