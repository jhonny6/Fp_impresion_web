inherited Etiquetas_Control: TEtiquetas_Control
  Caption = 'Etiquetas_Control'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 165
    ScrollWidth = 356
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 104
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'TEXTO'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Texto'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 65
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'TIPO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Tipo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 143
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CODIGO_BARRAS'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo barras'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 25
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  inherited DataSource: TDataSource
    Left = 380
    Top = 118
  end
end
