unit FP.Etiquetas_ControlController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TEtiquetas_ControlCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateEtiquetas_ControlCmd = class(TEtiquetas_ControlCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadEtiquetas_ControlCmd = class(TEtiquetas_ControlCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateEtiquetas_ControlCmd = class(TEtiquetas_ControlCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteEtiquetas_ControlCmd = class(TEtiquetas_ControlCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickEtiquetas_ControlCmd = class(TReadEtiquetas_ControlCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Etiquetas_ControlBrowser,
  FP.Etiquetas_ControlModel,
  FP.Etiquetas_ControlView,
  FP.Main;

{ TProvinciasCommand }

function TEtiquetas_ControlCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TEtiquetas_ControlModel;
end;

function TEtiquetas_ControlCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TEtiquetas_ControlBrowser;
end;

function TEtiquetas_ControlCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TEtiquetas_Control;
end;

{ TCreateProvinciasCmd }

procedure TCreateEtiquetas_ControlCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateEtiquetas_ControlCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteEtiquetas_ControlCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickEtiquetas_ControlCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateEtiquetas_ControlCmd, TReadEtiquetas_ControlCmd, TUpdateEtiquetas_ControlCmd, TDeleteEtiquetas_ControlCmd, TPickEtiquetas_ControlCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateEtiquetas_ControlCmd, TReadEtiquetas_ControlCmd, TUpdateEtiquetas_ControlCmd, TDeleteEtiquetas_ControlCmd, TPickEtiquetas_ControlCmd]);

end.
