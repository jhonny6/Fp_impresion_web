object MainForm: TMainForm
  Left = 0
  Top = 0
  Width = 1133
  Height = 724
  Caption = 'MainForm'
  OldCreateOrder = True
  OnClose = UniFormClose
  KeyPreview = True
  AutoScroll = True
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnCreate = UniFormCreate
  OnScreenResize = UniFormScreenResize
  PixelsPerInch = 96
  TextHeight = 15
  object sboxMain: TUniScrollBox
    Left = 0
    Top = 0
    Width = 1117
    Height = 685
    Hint = ''
    Align = alClient
    TabOrder = 0
    ScrollWidth = 305
    object paBackGround: TUniContainerPanel
      Left = 305
      Top = 0
      Width = 810
      Height = 683
      Hint = ''
      ParentColor = False
      Color = 13273092
      Align = alClient
      TabOrder = 0
      ExplicitLeft = 260
      ExplicitWidth = 855
      object pgGeneral: TUniPageControl
        Left = 0
        Top = 44
        Width = 810
        Height = 639
        Hint = ''
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ActivePage = tabDashBoard
        Align = alClient
        TabOrder = 1
        ScreenMask.WaitData = True
        ScreenMask.Target = sboxMain
        ExplicitWidth = 855
        object tabDashBoard: TUniTabSheet
          Hint = ''
          Caption = '<i class="fa fa-home fa-1x"></i>'
          ExplicitWidth = 847
          object paGeral: TUniContainerPanel
            Left = 0
            Top = 0
            Width = 802
            Height = 611
            Hint = ''
            ParentColor = False
            Color = clWhite
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 847
            object sboxGridBlock: TUniContainerPanel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 796
              Height = 605
              Hint = ''
              BodyRTL = False
              ParentColor = False
              Color = 15395562
              Align = alClient
              AlignmentControl = uniAlignmentClient
              ParentAlignmentControl = False
              AutoScroll = True
              TabOrder = 1
              LayoutAttribs.Columns = 4
              LayoutConfig.IgnorePosition = False
              ExplicitWidth = 841
              ScrollHeight = 1216
              ScrollWidth = 803
              object rcBlock150: TUniContainerPanel
                Tag = 4
                Left = 74
                Top = 1201
                Width = 729
                Height = 15
                Hint = '[[cols:sm-8 md-8 lg-8 xl-8 | round:all]]'
                ParentColor = False
                TabOrder = 0
              end
            end
          end
        end
      end
      object paLayoutTopo: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 810
        Height = 44
        Hint = ''
        ParentColor = False
        Color = 13273092
        Align = alTop
        TabOrder = 2
        ExplicitWidth = 855
        DesignSize = (
          810
          44)
        object btnMenu: TUniLabel
          Left = 7
          Top = 0
          Width = 74
          Height = 45
          Cursor = crHandPoint
          Hint = ''
          Margins.Left = 0
          Margins.Top = 10
          CreateOrder = 1
          TextConversion = txtHTML
          AutoSize = False
          Caption = '<i class="fa fa-bars"></i>'
          ParentFont = False
          Font.Color = 15724527
          Font.Height = -37
          TabOrder = 1
          OnClick = btnMenuClick
        end
        object labbtnExit: TUniLabel
          AlignWithMargins = True
          Left = 748
          Top = 13
          Width = 30
          Height = 28
          Cursor = crHandPoint
          Hint = '[['#13#10'ico:fa-power-off |'#13#10'cls:rc-hover-zoom-1-2'#13#10']]'
          Margins.Left = 0
          Margins.Top = 13
          Margins.Right = 10
          CreateOrder = 1
          Alignment = taCenter
          TextConversion = txtHTML
          AutoSize = False
          Caption = '<i class="rc-hover-zoom-1-2 fa fa-power-off fa-2x"></i>'
          Anchors = [akTop, akRight]
          ParentFont = False
          Font.Color = 15724527
          Font.Height = -16
          TabOrder = 2
          OnClick = labbtnExitClick
          ExplicitLeft = 793
        end
        object labCompanyName: TUniLabel
          Left = 60
          Top = -5
          Width = 190
          Height = 44
          Hint = '[[hide:mobile-v ]]'
          Margins.Top = 14
          AutoSize = False
          Caption = 'FASTPROCESS'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 15724527
          Font.Height = -45
          Font.Name = 'Consolas'
          TabOrder = 3
        end
      end
    end
    object paLayoutMainMenu: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 305
      Height = 683
      Hint = ''
      Margins.Top = 10
      ParentColor = False
      Color = 13273092
      Align = alLeft
      AutoScroll = True
      TabOrder = 1
      ScrollHeight = 718
      ScrollWidth = 305
      object paLayoutMenuCfgVersion: TUniContainerPanel
        AlignWithMargins = True
        Left = 0
        Top = 43
        Width = 304
        Height = 640
        Hint = ''
        Margins.Left = 0
        Margins.Top = 10
        Margins.Right = 0
        Margins.Bottom = 0
        ParentColor = False
        Color = 15315739
        Align = alClient
        TabOrder = 1
        Layout = 'vbox'
        LayoutConfig.Flex = 1
        LayoutConfig.Width = '100%'
        LayoutConfig.DockWhenAligned = False
        ExplicitWidth = 260
      end
      object paLayoutLogo: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 304
        Height = 12
        Hint = ''
        ParentColor = False
        Color = 13273092
        Align = alTop
        TabOrder = 2
        ExplicitWidth = 260
      end
      object paLayoutBottom: TUniContainerPanel
        Left = 0
        Top = 683
        Width = 304
        Height = 35
        Hint = ''
        ParentColor = False
        Color = 13273092
        Align = alBottom
        TabOrder = 3
        ExplicitWidth = 261
        object labAppVersion: TUniLabel
          Left = 98
          Top = 10
          Width = 152
          Height = 16
          Hint = '[['#13#10'label-link:https://https://cashbank.cash/'#13#10']]'
          Alignment = taCenter
          TextConversion = txtHTML
          AutoSize = False
          Caption = '1.0.0.343'
          ParentFont = False
          Font.Color = 33023
          Font.Name = 'Calibri'
          TabOrder = 1
        end
      end
      object UniTreeMainMenu: TUniTreeMenu
        Left = 0
        Top = 33
        Width = 304
        Height = 650
        Hint = ''
        Margins.Top = 8
        Align = alClient
        ClientEvents.UniEvents.Strings = (
          
            'treeMenu.afterCreate=function treeMenu.afterCreate(sender)'#13#10'{'#13#10' ' +
            ' sender.el.setStyle('#39'overflow-y'#39', '#39'auto'#39');'#13#10'}')
        Items.FontData = {0100000000}
        ScreenMask.WaitData = True
        ScreenMask.Target = Owner
        Color = 13273092
        SingleExpand = True
        ExpanderOnly = False
        OnClick = UniTreeMainMenuClick
        ExplicitLeft = -2
        ExplicitTop = 37
        ExplicitWidth = 260
      end
      object UniContainerPanel1: TUniContainerPanel
        AlignWithMargins = True
        Left = 0
        Top = 22
        Width = 304
        Height = 11
        Hint = ''
        Margins.Left = 0
        Margins.Top = 10
        Margins.Right = 0
        Margins.Bottom = 0
        ParentColor = False
        Color = 13273092
        Align = alTop
        TabOrder = 5
        Layout = 'vbox'
        LayoutConfig.Flex = 1
        LayoutConfig.Width = '100%'
        LayoutConfig.DockWhenAligned = False
        ExplicitWidth = 260
      end
    end
  end
end
