object UniMainModule: TUniMainModule
  OnCreate = UniGUIMainModuleCreate
  OnDestroy = UniGUIMainModuleDestroy
  BackButtonAction = bbaWarnUser
  Theme = 'triton.modified'
  TouchTheme = 'material'
  MonitoredKeys.Enabled = True
  MonitoredKeys.KeyEnableAll = True
  MonitoredKeys.Keys = <>
  EnableSynchronousOperations = True
  ConstrainForms = True
  Height = 342
  Width = 521
  PixelsPerInch = 96
  object SQLConn: TUniConnection
    ProviderName = 'Oracle'
    Username = 'fastence'
    Server = 'fastprocess21.sytes.net'
    LoginPrompt = False
    Left = 73
    Top = 39
    EncryptedPassword = '99FF9EFF8CFF8BFF9AFF91FF9CFF9AFF'
  end
  object QAuxiliar: TUniQuery
    Connection = SQLConn
    Left = 184
    Top = 72
  end
  object UniSQLMonitor1: TUniSQLMonitor
    Left = 336
    Top = 168
  end
end
