inherited Agencias: TAgencias
  Caption = 'Agencias'
  ExplicitTop = -14
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ExplicitTop = 44
    ScrollHeight = 325
    ScrollWidth = 355
    object UniDBEdit1: TUniDBEdit
      Left = 55
      Top = 5
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit7: TUniDBEdit
      Left = 55
      Top = 229
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CONTADOR_MAXIMO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Contador m'#225'ximo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit8: TUniDBEdit
      Left = 55
      Top = 265
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'NUMERO_CLIENTE_ASIGNADO'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'N'#250'mero cliente'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit9: TUniDBEdit
      Left = 55
      Top = 303
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'OBSERVACIONES'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Observaciones'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  object UniDBEdit3: TUniDBEdit [3]
    Left = 56
    Top = 121
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'NOMBRE_AGENCIA'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Nombre agencia'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [4]
    Left = 56
    Top = 83
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'AGENCIA'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Agencia'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit4: TUniDBEdit [5]
    Left = 56
    Top = 157
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'CODEMPRESA'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Cod. empresa'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit5: TUniDBEdit [6]
    Left = 56
    Top = 191
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'CONVERSION_M3'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Conversion'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit6: TUniDBEdit [7]
    Left = 56
    Top = 231
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'CONTADOR_ACTUAL'
    DataSource = DataSource
    TabOrder = 7
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Contador actual'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
