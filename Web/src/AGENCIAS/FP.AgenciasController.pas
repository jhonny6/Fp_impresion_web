unit FP.AgenciasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TAgenciasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateAgenciasCmd = class(TAgenciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadAgenciasCmd = class(TAgenciasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateAgenciasCmd = class(TAgenciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteAgenciasCmd = class(TAgenciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickAgenciasCmd = class(TReadAgenciasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.AgenciasBrowser,
  FP.AgenciasModel,
  FP.AgenciasView,
  FP.Main;

{ TProvinciasCommand }

function TAgenciasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TAgenciasModel;
end;

function TAgenciasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TAgenciasBrowser;
end;

function TAgenciasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TAgencias;
end;

{ TCreateProvinciasCmd }

procedure TCreateAgenciasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateAgenciasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteAgenciasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickAgenciasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateAgenciasCmd, TReadAgenciasCmd, TUpdateAgenciasCmd, TDeleteAgenciasCmd, TPickAgenciasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateAgenciasCmd, TReadAgenciasCmd, TUpdateAgenciasCmd, TDeleteAgenciasCmd, TPickAgenciasCmd]);

end.
