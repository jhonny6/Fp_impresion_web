unit FP.AgenciasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TAgenciasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  AgenciasModel: TAgenciasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TAgenciasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Agencias_GEST';
end;

function TAgenciasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TAgenciasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TAgenciasModel.TableName: string;
begin
  Result := 'fastence.fp0_Agencias';
end;

end.
