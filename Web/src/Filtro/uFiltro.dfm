object FiltroFrm: TFiltroFrm
  Left = 0
  Top = 0
  ClientHeight = 441
  ClientWidth = 471
  Caption = 'Filtro Avanzado'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object sbPpal: TUniScrollBox
    Left = 0
    Top = 0
    Width = 471
    Height = 384
    Hint = ''
    Align = alClient
    TabOrder = 0
    object pnlNivel: TUniContainerPanel
      AlignWithMargins = True
      Left = 10
      Top = 5
      Width = 449
      Height = 29
      Hint = ''
      Margins.Left = 10
      Margins.Top = 5
      Margins.Right = 10
      Visible = False
      ParentColor = False
      Align = alTop
      TabOrder = 0
      object cbCampo: TUniComboBox
        Left = 3
        Top = 3
        Width = 145
        Hint = ''
        Text = 'cbCampo'
        TabOrder = 1
        IconItems = <>
      end
      object cbFiltro: TUniComboBox
        Left = 151
        Top = 3
        Width = 145
        Hint = ''
        Text = 'UniComboBox1'
        TabOrder = 2
        IconItems = <>
      end
      object edValor: TUniEdit
        Left = 298
        Top = 3
        Width = 121
        Hint = ''
        Text = ''
        TabOrder = 3
      end
      object btEliminar: TUniLabel
        Left = 424
        Top = 7
        Width = 19
        Height = 16
        Hint = ''
        Alignment = taCenter
        AutoSize = False
        Caption = 'X'
        TabOrder = 4
      end
    end
  end
  object pnlButtom: TUniContainerPanel
    Left = 0
    Top = 384
    Width = 471
    Height = 57
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 1
    object btAplicar: TUniButton
      Left = 3
      Top = 29
      Width = 110
      Height = 25
      Hint = ''
      Caption = 'Aplicar'
      TabOrder = 1
      OnClick = OnClickAplicaFiltro
    end
    object btAddCondicion: TUniButton
      Left = 3
      Top = 3
      Width = 110
      Height = 25
      Hint = ''
      Caption = 'Agregar Condici'#243'n'
      TabOrder = 2
      OnClick = btAddCondicionClick
    end
  end
end
