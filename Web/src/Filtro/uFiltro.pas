unit uFiltro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniPanel, uniEdit, uniMultiItem,
  uniComboBox, uniButton, uniLabel, Generics.Collections, uniScrollBox, Data.DB,
  uniBitBtn, uniSpeedButton, Dialogs;

type
  TTipoConector = (tcNone, tcAnd, tcOr);
  TTipoOperacion = (toEsIgual, toNoEsIgual, toEsMayor, toEsMayorOIgual,
    toEsMenor, toEsMenorOIgual, toEsNulo, toNoEsNulo, toEmpiezaCon,
    toNoEmpiezaCon, toFinalizaCon, toNoFinalizaCon, toContiene, toNoContiene);

type
  PRecColumna = ^TRecColumna;

  TRecColumna = record
    Columna: String;
  end;

  TFiltroNivel = class(TUniContainerPanel)
  private
    // FpnlContenido: TGridPanelLayout;
    FValor: String;
    FColumna: TRecColumna;
    FedValor: TUniEdit;
    FpnlContenido: TUniPanel;
    FOperacion: TTipoOperacion;
    FcbColumna, FcbOperacion: TUniComboBox;
    FbtBotonAgrega, FbtBotonElimina: TUniSpeedButton;
    FNombreColumnas: TDictionary<Integer, String>;
    function CreaLabel(pTexto: String): TUniLabel;
    function CreaComboBox: TUniComboBox;
    function CreaEdit: TUniEdit;
    function CreaBoton: TUniSpeedButton;
    procedure AjustaControl(pControl: TUniControl);
    procedure SetOperacion(pOperacion: TTipoOperacion);

    // Eventos
    procedure OnClickBotonAgrega(Sender: TObject);
    procedure OnClickBotonElimina(Sender: TObject);
    procedure OnCambiaComboOperacion(Sender: TObject);
    procedure OnCambiaValor(Sender: TObject);
    procedure OnCambiaColumna(Sender: TObject);
    procedure SetColumna(const Value: String);
    procedure SetValor(const Value: String);
    function GetColumna: PRecColumna;
  public
    OnBotonAgrega, OnBotonElimina: TProc;

    property NombreColumnas: TDictionary<Integer, String> read FNombreColumnas
      write FNombreColumnas;

    property edValor: TUniEdit read FedValor write FedValor;
    property cbOperacion: TUniComboBox read FcbOperacion write FcbOperacion;
    property cbColumna: TUniComboBox read FcbColumna write FcbColumna;

    property Columna: PRecColumna read GetColumna;
    property Operacion: TTipoOperacion read FOperacion write SetOperacion;
    property Valor: String read FValor write SetValor;

    procedure Redimensionar;
    procedure AgregaColumnas(pColumnas: TDictionary<String, String>);

    procedure Inicializa;
    constructor Create(pOwner: TComponent); override;
  end;

  TConector = class(TUniComboBox)
  private
    FNivel: TFiltroNivel;
    FTipoConector: TTipoConector;
    procedure OnChangeConector(Sender: TObject);
    procedure SetTipoConector(pValue: TTipoConector);
  public
    property TipoConector: TTipoConector read FTipoConector
      write SetTipoConector;
    property Nivel: TFiltroNivel read FNivel write FNivel;

    constructor Create(pComponente: TComponent; pNivel: TFiltroNivel);
  end;

  // type
  // TAplicaFiltroEvent = procedure(pFiltros: TList<TConector>) of object;

type
  TFiltroFrm = class(TUniForm)
    pnlNivel: TUniContainerPanel;
    cbCampo: TUniComboBox;
    cbFiltro: TUniComboBox;
    edValor: TUniEdit;
    sbPpal: TUniScrollBox;
    btAplicar: TUniButton;
    pnlButtom: TUniContainerPanel;
    btAddCondicion: TUniButton;
    btEliminar: TUniLabel;
    procedure UniFormCreate(Sender: TObject);
    procedure btAddCondicionClick(Sender: TObject);
    procedure OnClickAplicaFiltro(Sender: TObject);
  private
    FNumCreaNiveles: Integer;
    FCamposOmitidos: TStrings;
    FTitulo: String;
    FScroll: TUniScrollBox;
    FbtCerrar: TUniButton;
    FFiltros: TList<TConector>;
    FCampos: TDictionary<String, String>;
    FDataSet: TDataSet;
    FAplicaFiltroEvent: TProc<TList<TConector>>;
    FCondiciones: String;
    FOperadoresLogicos: TList<string>;
    FOnGuardaFiltro: TProc<TList<TConector>>;

    function ClonarPnlNivel(pOwner: TComponent): TFiltroNivel;

    procedure SetCamposOmitidos(pValue: TStrings);
    procedure SetTitulo(pValue: String);
    procedure SetCampos(pValue: TDictionary<String, String>);
    procedure SetDataSet(pDataSet: TDataSet);

    procedure OnClickCerrar(Sender: TObject);
    procedure OnClickGuardaFiltro(Sender: TObject);
    procedure OnClickReiniciaFiltro(Sender: TObject);
    procedure OnCambiaCampo(Sender: TObject; const Item: String;
      Action: TCollectionNotification);
  protected
    FpnlPie: TUniPanel;
    FpnlTitulo: TUniPanel;
    FlbTitulo: TUniLabel;
  public
    btAplicaFiltro, btReiniciaFiltro, btGuardarFiltro: TUniButton;

    property Filtros: TList<TConector> read FFiltros write FFiltros;
    property Campos: TDictionary<String, String> read FCampos write SetCampos;
    property Condiciones: String read FCondiciones;

    procedure AgregaNivel;

    procedure Inicializar(AOwner: TObject);
  published
    property Titulo: String read FTitulo write SetTitulo;
    property DataSet: TDataSet read FDataSet write SetDataSet;
    property CamposOmitidos: TStrings read FCamposOmitidos
      write SetCamposOmitidos stored True;

    property OnAplicaFiltro: TProc < TList < TConector >>
      read FAplicaFiltroEvent write FAplicaFiltroEvent;
    property OnGuardaFiltro: TProc < TList < TConector >> read FOnGuardaFiltro
      write FOnGuardaFiltro;
  end;

function FiltroFrm: TFiltroFrm;

implementation

{$R *.dfm}

uses
  FP.MainModule, uniGUIApplication;

function FiltroFrm: TFiltroFrm;
begin
  Result := TFiltroFrm(UniMainModule.GetFormInstance(TFiltroFrm));
end;

procedure TFiltroFrm.AgregaNivel;
var
  vFiltroNivel: TFiltroNivel;
  vConector: TConector;
begin
  vFiltroNivel := ClonarPnlNivel(Self);

  vFiltroNivel.Inicializa;
  vFiltroNivel.Parent := sbPpal;

  vConector := TConector.Create(Self, vFiltroNivel);
  if FFiltros.Count > 0 then
    vConector.TipoConector := tcAnd;
  // else
  // vFiltroNivel.FbtBotonElimina.Visible := False;

  // vConector.Parent := sbPpal;
  // //vConector.Position.Y := vFiltroNivel.Position.Y - vFiltroNivel.Height;
  // //vConector.Margins := TBounds.Create(TRectF.Create(5, 5,
  // //vConector.Width * 0.9, 5));

  FFiltros.Add(vConector);

  // // /// /  vFiltroNivel.OnBotonElimina := (
  // // /// /    procedure
  // // /// /    begin
  // // /// /      FFiltros.Remove(vConector);
  // // /// /      vConector.Free;
  // // /// /      vFiltroNivel.Free;
  // // /// /    end);
  //
  vFiltroNivel.AgregaColumnas(FCampos);

  if vFiltroNivel.FcbColumna.Items.Count > 0 then
  begin
    vFiltroNivel.FcbColumna.ItemIndex := 0;
    vFiltroNivel.FcbColumna.Text := vFiltroNivel.FcbColumna.Items[0];
    vFiltroNivel.FcbColumna.OnChange(vFiltroNivel.FcbColumna);
  end;
end;

procedure TFiltroFrm.btAddCondicionClick(Sender: TObject);
begin
  AgregaNivel;
end;

function TFiltroFrm.ClonarPnlNivel(pOwner: TComponent): TFiltroNivel;
var
  vedValor: TUniEdit;
  vbtEliminar: TUniLabel;
  MemStream: TMemoryStream;
  vPnlNivel: TFiltroNivel;
  vcbCampo, vcbFiltro: TUniComboBox;
begin
  Result := nil;
  Inc(FNumCreaNiveles);

  vPnlNivel := TFiltroNivel.Create(pOwner);

  MemStream := TMemoryStream.Create;
  try
    MemStream.WriteComponent(pnlNivel);
    pnlNivel.Name := pnlNivel.Name + FNumCreaNiveles.ToString;
    MemStream.Position := 0;
    MemStream.ReadComponent(vPnlNivel);

  finally
    FreeAndNil(MemStream);
  end;

  vcbCampo := TUniComboBox.Create(pOwner);
  MemStream := TMemoryStream.Create;
  try
    MemStream.WriteComponent(cbCampo);
    cbCampo.Name := cbCampo.Name + FNumCreaNiveles.ToString;

    MemStream.Position := 0;
    MemStream.ReadComponent(vcbCampo);
    vcbCampo.Parent := vPnlNivel;
  finally
    FreeAndNil(MemStream);
  end;

  vcbFiltro := TUniComboBox.Create(pOwner);
  MemStream := TMemoryStream.Create;
  try
    MemStream.WriteComponent(cbFiltro);
    cbFiltro.Name := cbFiltro.Name + FNumCreaNiveles.ToString;

    MemStream.Position := 0;
    MemStream.ReadComponent(vcbFiltro);
    vcbFiltro.Parent := vPnlNivel;
  finally
    FreeAndNil(MemStream);
  end;

  vedValor := TUniEdit.Create(pOwner);
  MemStream := TMemoryStream.Create;
  try
    MemStream.WriteComponent(edValor);
    edValor.Name := edValor.Name + FNumCreaNiveles.ToString;

    MemStream.Position := 0;
    MemStream.ReadComponent(vedValor);
    vedValor.Parent := vPnlNivel;
  finally
    FreeAndNil(MemStream);
  end;

  // vbtEliminar := TUniLabel.Create(pOwner);
  // MemStream := TMemoryStream.Create;
  // try
  // MemStream.WriteComponent(btEliminar);
  // btEliminar.Name := btEliminar.Name + FNumCreaNiveles.ToString;
  //
  // MemStream.Position := 0;
  // MemStream.ReadComponent(vbtEliminar);
  // vbtEliminar.Parent := vPnlNivel;
  // finally
  // FreeAndNil(MemStream);
  // end;

  Result := TFiltroNivel(vPnlNivel);

  Result.cbColumna := vcbCampo;
  Result.cbOperacion := vcbFiltro;
  Result.edValor := vedValor;
  // Result.FbtBotonElimina := vbtEliminar;
  Result.Visible := True;

end;

procedure TFiltroFrm.Inicializar(AOwner: TObject);
begin
  // inherited Create(AOwner);
  FNumCreaNiveles := 0;
  FCamposOmitidos := TStringList.Create;
  FFiltros := TList<TConector>.Create;
  FCampos := TDictionary<String, String>.Create;
  FCampos.OnKeyNotify := OnCambiaCampo;
  FOperadoresLogicos := TList<string>.Create;

  FOperadoresLogicos.AddRange(['=''%s''', '<>''%s''', '>''%s''', '>=''%s''',
    '<''%s''', '<=''%s''', '%s is null', '%s is not null', ' like ''%s%%''',
    ' not like ''%s%%''', ' like (''%%%s'')', ' not like (''%%%s'')',
    ' like ''%%%s%%''', ' not like ''%%%s%%''']);

  // Height := 350;
  // Width := 570;

  // begin
  // FpnlTitulo := TUniPanel.Create(Self);
  // FpnlTitulo.Align := alTop;
  // FpnlTitulo.Height := 40;
  // FpnlTitulo.Parent := Self;
  //
  // FlbTitulo := TUniLabel.Create(FpnlTitulo);
  // FlbTitulo.Align := alClient;
  // FlbTitulo.Parent := FpnlTitulo;
  // FlbTitulo.Text := 'B�SQUEDA AVANZADA';
  //
  // FbtCerrar := TUniButton.Create(FpnlTitulo);
  // FbtCerrar.Align := alRight;
  // FbtCerrar.Width := 40;
  // FbtCerrar.Parent := FpnlTitulo;
  // FbtCerrar.OnClick := OnClickCerrar;
  //
  // FScroll := TUniScrollBox.Create(Self);
  // FScroll.Align := alClient;
  // FScroll.Parent := Self;
  //
  // FpnlPie := TUniPanel.Create(Self);
  // FpnlPie.Align := alBottom;
  // FpnlPie.Height := 40;
  // FpnlPie.Parent := Self;
  //
  // btReiniciaFiltro := TUniButton.Create(FpnlPie);
  // btReiniciaFiltro.Align := alRight;
  // btReiniciaFiltro.Width := 130;
  // btReiniciaFiltro.Caption := 'Reiniciar';
  // btReiniciaFiltro.Parent := FpnlPie;
  // btReiniciaFiltro.OnClick := OnClickReiniciaFiltro;
  //
  // btAplicaFiltro := TUniButton.Create(FpnlPie);
  // btAplicaFiltro.Align := alRight;
  // btAplicaFiltro.Width := 130;
  // btAplicaFiltro.Caption := 'Aplicar';
  // btAplicaFiltro.Parent := FpnlPie;
  // btAplicaFiltro.OnClick := OnClickAplicaFiltro;
  //
  // btGuardarFiltro := TUniButton.Create(FpnlPie);
  // btGuardarFiltro.Align := alRight;
  // btGuardarFiltro.Width := 130;
  // btGuardarFiltro.Caption := 'Guardar';
  // btGuardarFiltro.Parent := FpnlPie;
  // btGuardarFiltro.OnClick := OnClickGuardaFiltro;
  //
  // AgregaNivel;
  // end;
end;

procedure TFiltroFrm.OnCambiaCampo(Sender: TObject; const Item: String;
  Action: TCollectionNotification);
begin
  if Action = TCollectionNotification.cnAdded then
    SetCampos(FCampos);
end;

procedure TFiltroFrm.OnClickAplicaFiltro(Sender: TObject);
var
  vConector, vNombreCampo, vOperador: String;
  vFiltro: TConector;
begin
  FCondiciones := EmptyStr;
  for vFiltro in FFiltros do
  begin
    vNombreCampo := vFiltro.Nivel.Columna.Columna;

    if vNombreCampo = EmptyStr then
    begin
      vFiltro.Nivel.FcbColumna.SetFocus;
      raise Exception.Create('Filtro no v�lido. Seleccione un campo');
    end;

    vNombreCampo := 'lower( cast(' + vFiltro.Nivel.Columna.Columna + ' as VARCHAR2(1024)) )';

    vConector := EmptyStr;
    case vFiltro.TipoConector of
      tcAnd:
        vConector := ' and ';
      tcOr:
        vConector := ' or ';
    end;

    vOperador := EmptyStr;
    vOperador := FOperadoresLogicos.Items[Integer(vFiltro.Nivel.FOperacion)];

    if vFiltro.Nivel.Operacion in [toEsNulo, toNoEsNulo] then
      FCondiciones := FCondiciones + vConector + '(' +
        Format(vOperador, [vFiltro.Nivel.Columna.Columna]) + ')'
    else
      FCondiciones := FCondiciones + vConector + '(' + vNombreCampo +
        Format(vOperador, [vFiltro.Nivel.Valor.ToLower]) + ')';
  end;

  if Assigned(FAplicaFiltroEvent) then
    FAplicaFiltroEvent(FFiltros);
end;

procedure TFiltroFrm.OnClickGuardaFiltro(Sender: TObject);
var
  vConector, vNombreCampo, vOperador: String;
  vFiltro: TConector;
begin
  FCondiciones := EmptyStr;
  for vFiltro in FFiltros do
  begin
    vNombreCampo := vFiltro.Nivel.Columna.Columna;

    if vNombreCampo = EmptyStr then
    begin
      vFiltro.Nivel.FcbColumna.SetFocus;
      raise Exception.Create('Filtro no v�lido. Seleccione un campo');
    end;

    vConector := EmptyStr;
    case vFiltro.TipoConector of
      tcAnd:
        vConector := ' and ';
      tcOr:
        vConector := ' or ';
    end;

    vOperador := EmptyStr;
    vOperador := FOperadoresLogicos.Items[Integer(vFiltro.Nivel.FOperacion)];

    if vFiltro.Nivel.Operacion in [toEsNulo, toNoEsNulo] then
      FCondiciones := FCondiciones + vConector + '(' +
        Format(vOperador, [vFiltro.Nivel.Columna]) + ')'
    else
      FCondiciones := FCondiciones + vConector + '(' + vNombreCampo +
        Format(vOperador, [vFiltro.Nivel.Valor]) + ')';
  end;

  if Assigned(FOnGuardaFiltro) then
    FOnGuardaFiltro(FFiltros);
end;

procedure TFiltroFrm.OnClickCerrar(Sender: TObject);
begin
  Visible := False;
end;

procedure TFiltroFrm.OnClickReiniciaFiltro(Sender: TObject);
var
  vI: Integer;
begin
  for vI := Filtros.Count - 1 downto 1 do
  begin
    FFiltros.Items[vI].Nivel.Free;
    FFiltros.Items[vI].Free;
    FFiltros.Delete(vI);
  end;

  FFiltros.Items[0].Nivel.Columna.Columna := '';
  FFiltros.Items[0].Nivel.Valor := '';
end;

procedure TFiltroFrm.SetCampos(pValue: TDictionary<String, String>);
var
  vConector: TConector;
begin
  FCampos := pValue;

  for vConector in FFiltros do
    vConector.Nivel.AgregaColumnas(FCampos);
end;

procedure TFiltroFrm.SetCamposOmitidos(pValue: TStrings);
begin
  CamposOmitidos.Assign(pValue);
end;

procedure TFiltroFrm.SetDataSet(pDataSet: TDataSet);
var
  vI: Integer;
begin
  FDataSet := pDataSet;
  FCampos.Clear;

  if Assigned(pDataSet) then
    for vI := 0 to pDataSet.FieldCount - 1 do
      if ((pDataSet.Fields[vI].Visible) and
        (FCamposOmitidos.IndexOf(pDataSet.Fields[vI].FieldName) = -1)) then
        FCampos.Add(pDataSet.Fields[vI].FieldName,
          pDataSet.Fields[vI].DisplayName);

end;

procedure TFiltroFrm.SetTitulo(pValue: String);
begin
  FTitulo := pValue;
  FlbTitulo.Text := pValue;
end;

procedure TFiltroFrm.UniFormCreate(Sender: TObject);
begin
  Inicializar(Sender);
end;

{ TFiltroNivel }

procedure TFiltroNivel.AgregaColumnas(pColumnas: TDictionary<String, String>);
var
  vI: Integer;
  vClave: String;
begin
  vI := 0;
  if not Assigned(FNombreColumnas) then
    FNombreColumnas := TDictionary<Integer, String>.Create;

  FNombreColumnas.Clear;
  FcbColumna.Items.Clear;

  for vClave in pColumnas.Keys do
  begin
    FNombreColumnas.Add(vI, vClave);
    FcbColumna.Items.Add(pColumnas.Items[vClave]);
    Inc(vI);
  end;

  // FcbColumna.DropDown;
end;

procedure TFiltroNivel.AjustaControl(pControl: TUniControl);
begin
  pControl.Align := alBottom;
  pControl.AlignWithMargins := True;
  pControl.Margins.Bottom := 5;
  pControl.Margins.Left := 8;
  pControl.Margins.Right := 8;
  pControl.Margins.Top := 8;
  // pControl.Margins := TBounds.Create(TRectF.Create(5, 8, 8, 8));
  pControl.Parent := FpnlContenido;
end;

function TFiltroNivel.CreaBoton: TUniSpeedButton;
begin
  Result := TUniSpeedButton.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaComboBox: TUniComboBox;
begin
  Result := TUniComboBox.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaEdit: TUniEdit;
begin
  Result := TUniEdit.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaLabel(pTexto: String): TUniLabel;
begin
  Result := TUniLabel.Create(FpnlContenido);
  Result.Text := pTexto;
  AjustaControl(Result);
end;

constructor TFiltroNivel.Create(pOwner: TComponent);
begin
  inherited Create(pOwner);

  // Align := alTop;
  // Height := 80;
  // // Margins := TBounds.Create(TRectF.Create(5, 5, 5, 5));
  //
  // FpnlContenido := TUniPanel.Create(Self);
  // FpnlContenido.Align := alClient;
  // // FpnlContenido.ColumnCollection.Clear;
  // // FpnlContenido.ColumnCollection.AddRange(5);
  // FpnlContenido.Layout := 'table';
  // FpnlContenido.LayoutAttribs.Columns := 5;
  //
  // FpnlContenido.Parent := Self;
  //
  // Redimensionar;
  // Redimensionar;
  // Redimensionar;
  //
  // CreaLabel('Columna');
  // CreaLabel('Operaci�n');
  // CreaLabel('Valor');
  // CreaLabel(EmptyStr);
  // CreaLabel(EmptyStr);
  //
  // FcbColumna := CreaComboBox;
  // FcbColumna.OnChange := OnCambiaColumna;
  //
  // FcbOperacion := CreaComboBox;
  //
  // FcbOperacion.Items.AddStrings(['Es igual', 'No es igual', 'Es mayor',
  // 'Es mayor o igual', 'Es menor', 'Es menor o igual', 'Es nulo', 'No es nulo',
  // 'Empieza con', 'No empieza con', 'Finaliza con', 'No finaliza con',
  // 'Contiene', 'No contiene']);
  //
  // SetOperacion(toContiene);
  // FcbOperacion.ItemIndex := 12;
  // FcbOperacion.OnChange := OnCambiaComboOperacion;
  //
  // FedValor := CreaEdit;
  // FedValor.OnChange := OnCambiaValor;
  //
  // FbtBotonAgrega := CreaBoton;
  // FbtBotonAgrega.Caption := '<i class="fa fa-add fa-2x"></i>';
  // // FbtBotonAgrega.StyleLookup := 'additembutton';
  // FbtBotonAgrega.OnClick := OnClickBotonAgrega;
  //
  // FbtBotonElimina := CreaBoton;
  // FbtBotonElimina.Caption := '<i class="fa fa-remove fa-2x"></i>';
  // // FbtBotonElimina.StyleLookup := 'deleteitembutton';
  // FbtBotonElimina.OnClick := OnClickBotonElimina;
end;

function TFiltroNivel.GetColumna: PRecColumna;
begin
  Result := @FColumna;
end;

procedure TFiltroNivel.Inicializa;
begin
  cbColumna.OnChange := OnCambiaColumna;

  FcbOperacion.Items.Clear;
  FcbOperacion.Items.AddStrings(['Es igual', 'No es igual', 'Es mayor',
    'Es mayor o igual', 'Es menor', 'Es menor o igual', 'Es nulo', 'No es nulo',
    'Empieza con', 'No empieza con', 'Finaliza con', 'No finaliza con',
    'Contiene', 'No contiene']);

  SetOperacion(toContiene);
  FcbOperacion.ItemIndex := 12;
  FcbOperacion.Text := FcbOperacion.Items[12];
  FcbOperacion.OnChange := OnCambiaComboOperacion;

  FedValor.OnChange := OnCambiaValor;

  // FbtBotonAgrega.Caption := '<i class="fa fa-add fa-2x"></i>';
  // // FbtBotonAgrega.StyleLookup := 'additembutton';
  // FbtBotonAgrega.OnClick := OnClickBotonAgrega;

  // FbtBotonElimina.Caption := '<i class="fa fa-remove fa-2x"></i>';
  // FbtBotonElimina.StyleLookup := 'deleteitembutton';
  // FbtBotonElimina.OnClick := OnClickBotonElimina;
end;

procedure TFiltroNivel.OnCambiaColumna(Sender: TObject);
var
  // vColumna: String;
  // vCombo: ^TUniCombobox;

  vCombo: TUniComboBox absolute Sender;
begin
  if NombreColumnas.Count > 0 then
    Columna.Columna := NombreColumnas[FcbColumna.ItemIndex]
  else
    Columna.Columna := vCombo.Items[vCombo.ItemIndex];

  // FColumna := vCombo.ClassName; //FcbColumna.Text

  // vColumna := vCombo.Items[vCombo.ItemIndex];

  // vCombo := @cbColumna;

  //Columna.Columna := vCombo.Items[vCombo.ItemIndex];
  // Columna := vColumna;

  // ShowMessage(vCombo.ClassName);

  // FcbOperacion.DropDown;
end;

procedure TFiltroNivel.OnCambiaValor(Sender: TObject);
begin
  FValor := FedValor.Text;
end;

procedure TFiltroNivel.OnClickBotonAgrega(Sender: TObject);
begin
  if Assigned(OnBotonAgrega) then
    OnBotonAgrega;
end;

procedure TFiltroNivel.OnClickBotonElimina(Sender: TObject);
begin
  if Assigned(OnBotonElimina) then
    OnBotonElimina;
end;

procedure TFiltroNivel.OnCambiaComboOperacion(Sender: TObject);
begin
  FOperacion := TTipoOperacion(FcbOperacion.ItemIndex);

  FedValor.Enabled := (not(FOperacion in [toEsNulo, toNoEsNulo]));

  if not FedValor.Enabled then
    FedValor.Text := EmptyStr;

  FedValor.SetFocus;
end;

procedure TFiltroNivel.Redimensionar;
begin
  // FpnlContenido.ColumnCollection.Items[0].Value := 30;
  // FpnlContenido.ColumnCollection.Items[1].Value := 30;
  // FpnlContenido.ColumnCollection.Items[2].Value := 30;
  // FpnlContenido.ColumnCollection.Items[3].Value := 5;
  // FpnlContenido.ColumnCollection.Items[4].Value := 5;
end;

procedure TFiltroNivel.SetColumna(const Value: String);
var
  vLista: TStrings;
begin
  Columna.Columna := Value;

  vLista := TStringList.Create;
  try
    if Columna.Columna = EmptyStr then
    begin
      vLista.Assign(FcbColumna.Items);
      FcbColumna.Items.Clear;
      FcbColumna.Items.AddStrings(vLista);
    end
    else
      FcbColumna.ItemIndex := FcbColumna.Items.IndexOf(Value);
  finally
    vLista.Free;
  end;
end;

procedure TFiltroNivel.SetOperacion(pOperacion: TTipoOperacion);
begin
  if FOperacion <> pOperacion then
    FOperacion := pOperacion;

  if (FcbOperacion.ItemIndex <> Integer(pOperacion)) then
    FcbOperacion.ItemIndex := Integer(pOperacion);
end;

procedure TFiltroNivel.SetValor(const Value: String);
begin
  FValor := Value;

  // if FValor = EmptyStr then
  FedValor.Text := Value;
end;

{ TColumnCollectionHelper }

// procedure TColumnCollectionHelper.AddRange(pCantidad: Integer);
// var
// vI: Integer;
// begin
// for vI := 1 to pCantidad do
// Add;
// end;

// procedure TColumnCollectionHelper.PorcentajeAuto;
// var
// vI: Integer;
// vPorc: Double;
// begin
// if Count > 0 then
// vPorc := 100 / Count
// else
// vPorc := 100;
//
// for vI := 0 to Count - 1 do
// Items[vI].Value := vPorc;
// end;

constructor TConector.Create(pComponente: TComponent; pNivel: TFiltroNivel);
begin
  inherited Create(pComponente);
  Align := alTop;
  FNivel := pNivel;

  Items.AddStrings(['Y', 'O']);
  SetTipoConector(tcNone);

  OnChange := OnChangeConector;
end;

procedure TConector.OnChangeConector(Sender: TObject);
begin
  FTipoConector := TTipoConector(ItemIndex + 1);
end;

procedure TConector.SetTipoConector(pValue: TTipoConector);
begin
  FTipoConector := pValue;

  ItemIndex := Integer(pValue) - 1;

  Visible := not(FTipoConector = tcNone);
end;

end.
