unit CashBank.EmpresasView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, uniCheckBox, uniDBCheckBox,
  uniMemo, uniDBMemo, Vcl.DBActns, uniScrollBox, uniButton, uniBitBtn,
  uniSpeedButton;

type
  TEmpresas = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit12: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBEdit10: TUniDBEdit;
    UniDBEdit11: TUniDBEdit;
    UniDBMemo2: TUniDBMemo;
    UniDBCheckBox1: TUniDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
