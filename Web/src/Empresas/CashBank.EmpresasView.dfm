inherited Empresas: TEmpresas
  ClientHeight = 593
  ClientWidth = 729
  Caption = 'Empresas'
  ExplicitWidth = 745
  ExplicitHeight = 632
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 530
    ExplicitWidth = 729
    ExplicitHeight = 530
    ScrollHeight = 497
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      AlignWithMargins = True
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      Margins.Left = 20
      Margins.Bottom = 20
      DataField = 'nombre_comercial'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre comercial'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 500
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'direccion'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Direcci'#243'n'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 86
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'localidad'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Localidad'
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 126
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'provincia'
      DataSource = DataSource
      TabOrder = 3
      LayoutConfig.Margin = '20'
      FieldLabel = 'Provincia'
    end
    object UniDBEdit12: TUniDBEdit
      Left = 56
      Top = 406
      Width = 200
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'nif'
      DataSource = DataSource
      TabOrder = 10
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nif'
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 170
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'web'
      DataSource = DataSource
      TabOrder = 4
      LayoutConfig.Margin = '20'
      FieldLabel = 'Web'
    end
    object UniDBEdit6: TUniDBEdit
      Left = 56
      Top = 211
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'email'
      DataSource = DataSource
      TabOrder = 5
      LayoutConfig.Margin = '20'
      FieldLabel = 'Email'
    end
    object UniDBEdit10: TUniDBEdit
      Left = 56
      Top = 366
      Width = 200
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'id_pais'
      DataSource = DataSource
      TabOrder = 7
      LayoutConfig.Margin = '20'
      FieldLabel = 'Pais'
    end
    object UniDBEdit11: TUniDBEdit
      Left = 56
      Top = 444
      Width = 200
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'id_codigo_postal'
      DataSource = DataSource
      TabOrder = 8
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo postal'
    end
    object UniDBMemo2: TUniDBMemo
      Left = 56
      Top = 255
      Width = 300
      Height = 89
      Hint = ''
      Margins.Left = 20
      DataField = 'observaciones'
      DataSource = DataSource
      TabOrder = 6
      LayoutConfig.Margin = '20'
      FieldLabel = 'Observaciones'
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 480
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'activo'
      DataSource = DataSource
      Caption = ''
      TabOrder = 9
      ParentColor = False
      Color = clBtnFace
      FieldLabel = 'Activo'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 571
    Width = 729
    ExplicitTop = 571
    ExplicitWidth = 729
  end
end
