unit CashBank.EmpresasController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TEmpresasCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateEmpresasCmd = class(TEmpresasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadEmpresasCmd = class(TEmpresasCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateEmpresasCmd = class(TEmpresasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteEmpresasCmd = class(TEmpresasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickEmpresasCmd = class(TReadEmpresasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.EmpresasBrowser,
  CashBank.EmpresasModel,
  CashBank.EmpresasView,
  FP.Main;

{ TEmpresasCommand }

class function TEmpresasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TEmpresasModel;
end;

class function TEmpresasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TEmpresasBrowser;
end;

class function TEmpresasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TEmpresas;
end;

{ TCreateEmpresasCmd }

procedure TCreateEmpresasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadEmpresasCmd }

procedure TReadEmpresasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadEmpresasCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateEmpresasCmd }

procedure TUpdateEmpresasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteEmpresasCmd }

procedure TDeleteEmpresasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickEmpresasCmd }

procedure TPickEmpresasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateEmpresasCmd, TReadEmpresasCmd, TUpdateEmpresasCmd, TDeleteEmpresasCmd, TPickEmpresasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateEmpresasCmd, TReadEmpresasCmd, TUpdateEmpresasCmd, TDeleteEmpresasCmd, TPickEmpresasCmd]);

end.
