inherited Operarios_Grupos: TOperarios_Grupos
  Caption = 'Operarios_Grupos'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 132
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 30
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 70
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID_GRUPO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Grupo ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 110
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID_OPERARIO'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Operario ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
