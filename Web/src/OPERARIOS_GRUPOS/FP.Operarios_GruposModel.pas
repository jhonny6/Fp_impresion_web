unit FP.Operarios_GruposModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TOperarios_GruposModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Operarios_GruposModel: TOperarios_GruposModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TOperarios_GruposModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Operarios_Grup';
end;

function TOperarios_GruposModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TOperarios_GruposModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TOperarios_GruposModel.TableName: string;
begin
  Result := 'fastence.fp0_Operarios_Grupos';
end;

end.
