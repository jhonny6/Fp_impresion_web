unit FP.Operarios_GruposController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TOperarios_GruposCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateOperarios_GruposCmd = class(TOperarios_GruposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadOperarios_GruposCmd = class(TOperarios_GruposCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateOperarios_GruposCmd = class(TOperarios_GruposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteOperarios_GruposCmd = class(TOperarios_GruposCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickOperarios_GruposCmd = class(TReadOperarios_GruposCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Operarios_GruposBrowser,
  FP.Operarios_GruposModel,
  FP.Operarios_GruposView,
  FP.Main;

{ TProvinciasCommand }

function TOperarios_GruposCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TOperarios_GruposModel;
end;

function TOperarios_GruposCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TOperarios_GruposBrowser;
end;

function TOperarios_GruposCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TOperarios_Grupos;
end;

{ TCreateProvinciasCmd }

procedure TCreateOperarios_GruposCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateOperarios_GruposCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteOperarios_GruposCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickOperarios_GruposCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateOperarios_GruposCmd, TReadOperarios_GruposCmd, TUpdateOperarios_GruposCmd, TDeleteOperarios_GruposCmd, TPickOperarios_GruposCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateOperarios_GruposCmd, TReadOperarios_GruposCmd, TUpdateOperarios_GruposCmd, TDeleteOperarios_GruposCmd, TPickOperarios_GruposCmd]);

end.
