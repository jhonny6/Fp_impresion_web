unit FP.IndicadoresModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TIndicadoresModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  IndicadoresModel: TIndicadoresModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TIndicadoresModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Indicadores_GEST';
end;

function TIndicadoresModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TIndicadoresModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TIndicadoresModel.TableName: string;
begin
  Result := 'fastence.fp0_Indicadores';
end;

end.
