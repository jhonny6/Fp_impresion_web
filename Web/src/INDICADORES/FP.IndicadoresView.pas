unit FP.IndicadoresView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Vcl.DBActns,
  System.Actions, Vcl.ActnList, uniMainMenu, Data.DB, uniStatusBar,
  uniGUIClasses, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit;

type
  TIndicadores = class(TEntityView)
    UniDBEdit2: TUniDBEdit;
    UniDBEdit1: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit6: TUniDBEdit;
    UniDBEdit7: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniDBEdit9: TUniDBEdit;
    UniDBEdit10: TUniDBEdit;
    UniDBEdit11: TUniDBEdit;
    UniDBEdit12: TUniDBEdit;
    UniDBEdit13: TUniDBEdit;
    UniDBEdit14: TUniDBEdit;
    UniDBEdit15: TUniDBEdit;
    UniDBEdit16: TUniDBEdit;
    UniDBEdit17: TUniDBEdit;
    UniDBEdit18: TUniDBEdit;
    UniDBEdit19: TUniDBEdit;
    UniDBEdit20: TUniDBEdit;
    UniDBEdit21: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Indicadores: TIndicadores;

implementation

{$R *.dfm}

end.
