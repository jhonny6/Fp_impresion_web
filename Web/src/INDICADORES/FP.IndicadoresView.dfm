inherited Indicadores: TIndicadores
  Caption = 'Indicadores'
  ExplicitTop = -69
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 428
    ScrollWidth = 538
    object UniDBEdit2: TUniDBEdit
      Left = 0
      Top = 52
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'CODIGO'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'digo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit1: TUniDBEdit
      Left = 0
      Top = 13
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 0
      Top = 92
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'INDICADOR'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Indicador'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 0
      Top = 131
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'DESCRIPCION_LARGA'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Descripci'#243'n larga'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit5: TUniDBEdit
      Left = -1
      Top = 171
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'DIARIO'
      DataSource = DataSource
      TabOrder = 4
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Diario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit6: TUniDBEdit
      Left = 0
      Top = 210
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'SEMANAL'
      DataSource = DataSource
      TabOrder = 5
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Semanal'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit7: TUniDBEdit
      Left = 0
      Top = 250
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MENSUAL'
      DataSource = DataSource
      TabOrder = 6
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Mensual'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit8: TUniDBEdit
      Left = 0
      Top = 290
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'ULTIMA_ACTUALIZACION_PIVOT'
      DataSource = DataSource
      TabOrder = 7
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = #218'ltima actualizaci'#243'n'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit9: TUniDBEdit
      Left = 0
      Top = 330
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'FECHA_CALCULO'
      DataSource = DataSource
      TabOrder = 8
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha c'#225'lculo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit10: TUniDBEdit
      Left = 0
      Top = 370
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'PIVOT_TABLE'
      DataSource = DataSource
      TabOrder = 9
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Tabla pivot'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit11: TUniDBEdit
      Left = 268
      Top = 13
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'OBJETIVO_DIARIO'
      DataSource = DataSource
      TabOrder = 10
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Objetivo diario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit12: TUniDBEdit
      Left = 268
      Top = 52
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'OBJETIVO_SEMANAL'
      DataSource = DataSource
      TabOrder = 11
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Objetivo semanal'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit13: TUniDBEdit
      Left = 268
      Top = 92
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'OBJETIVO_MENSUAL'
      DataSource = DataSource
      TabOrder = 12
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Objetivo mensual'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit14: TUniDBEdit
      Left = 268
      Top = 131
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'COLOR_DIARIO'
      DataSource = DataSource
      TabOrder = 13
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Color diario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit15: TUniDBEdit
      Left = 268
      Top = 171
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'COLOR_SEMANAL'
      DataSource = DataSource
      TabOrder = 14
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Color semanal'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit16: TUniDBEdit
      Left = 268
      Top = 210
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'COLOR_MENSUAL'
      DataSource = DataSource
      TabOrder = 15
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Color mensual'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit17: TUniDBEdit
      Left = 268
      Top = 250
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'STATUS'
      DataSource = DataSource
      TabOrder = 16
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Status'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit18: TUniDBEdit
      Left = 268
      Top = 290
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MINIMO_DIARIO'
      DataSource = DataSource
      TabOrder = 17
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'M'#237'nimo diario'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit19: TUniDBEdit
      Left = 268
      Top = 330
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MINIMO_SEMANAL'
      DataSource = DataSource
      TabOrder = 18
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'M'#237'nimo semanal'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit20: TUniDBEdit
      Left = 268
      Top = 370
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'MINIMO_MENSUAL'
      DataSource = DataSource
      TabOrder = 19
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'M'#237'nimo mensual'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit21: TUniDBEdit
      Left = 268
      Top = 406
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'TIPO_INDICADOR'
      DataSource = DataSource
      TabOrder = 20
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Tipo de indicador'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
