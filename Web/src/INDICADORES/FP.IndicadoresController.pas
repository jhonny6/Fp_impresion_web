unit FP.IndicadoresController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TIndicadoresCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateIndicadoresCmd = class(TIndicadoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadIndicadoresCmd = class(TIndicadoresCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateIndicadoresCmd = class(TIndicadoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteIndicadoresCmd = class(TIndicadoresCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickIndicadoresCmd = class(TReadIndicadoresCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.IndicadoresBrowser,
  FP.IndicadoresModel,
  FP.IndicadoresView,
  FP.Main;

{ TProvinciasCommand }

function TIndicadoresCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TIndicadoresModel;
end;

function TIndicadoresCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TIndicadoresBrowser;
end;

function TIndicadoresCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TIndicadores;
end;

{ TCreateProvinciasCmd }

procedure TCreateIndicadoresCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateIndicadoresCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteIndicadoresCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickIndicadoresCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateIndicadoresCmd, TReadIndicadoresCmd, TUpdateIndicadoresCmd, TDeleteIndicadoresCmd, TPickIndicadoresCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateIndicadoresCmd, TReadIndicadoresCmd, TUpdateIndicadoresCmd, TDeleteIndicadoresCmd, TPickIndicadoresCmd]);

end.
