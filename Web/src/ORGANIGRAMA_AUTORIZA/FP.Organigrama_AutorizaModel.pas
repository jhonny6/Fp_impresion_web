unit FP.Organigrama_AutorizaModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TOrganigrama_AutorizaModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Organigrama_AutorizaModel: TOrganigrama_AutorizaModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TOrganigrama_AutorizaModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Organigrama_Autoriza';
end;

function TOrganigrama_AutorizaModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TOrganigrama_AutorizaModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TOrganigrama_AutorizaModel.TableName: string;
begin
  Result := 'fastence.fp0_Organigrama_Autoriza';
end;

end.
