unit FP.Organigrama_AutorizaController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TOrganigrama_AutorizaCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateOrganigrama_AutorizaCmd = class(TOrganigrama_AutorizaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadOrganigrama_AutorizaCmd = class(TOrganigrama_AutorizaCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateOrganigrama_AutorizaCmd = class(TOrganigrama_AutorizaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteOrganigrama_AutorizaCmd = class(TOrganigrama_AutorizaCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickOrganigrama_AutorizaCmd = class(TReadOrganigrama_AutorizaCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Organigrama_AutorizaBrowser,
  FP.Organigrama_AutorizaModel,
  FP.Organigrama_AutorizaView,
  FP.Main;

{ TProvinciasCommand }

function TOrganigrama_AutorizaCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TOrganigrama_AutorizaModel;
end;

function TOrganigrama_AutorizaCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TOrganigrama_AutorizaBrowser;
end;

function TOrganigrama_AutorizaCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TOrganigrama_Autoriza;
end;

{ TCreateProvinciasCmd }

procedure TCreateOrganigrama_AutorizaCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateOrganigrama_AutorizaCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteOrganigrama_AutorizaCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickOrganigrama_AutorizaCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateOrganigrama_AutorizaCmd, TReadOrganigrama_AutorizaCmd, TUpdateOrganigrama_AutorizaCmd, TDeleteOrganigrama_AutorizaCmd, TPickOrganigrama_AutorizaCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateOrganigrama_AutorizaCmd, TReadOrganigrama_AutorizaCmd, TUpdateOrganigrama_AutorizaCmd, TDeleteOrganigrama_AutorizaCmd, TPickOrganigrama_AutorizaCmd]);

end.
