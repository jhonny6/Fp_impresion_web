inherited Organigrama_Autoriza: TOrganigrama_Autoriza
  Caption = 'Organigrama_Autoriza'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 193
    ScrollWidth = 539
    object UniDBEdit9: TUniDBEdit
      Left = 269
      Top = 93
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'NOMBRE_EMPLEADO'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre empleado'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit10: TUniDBEdit
      Left = 269
      Top = 131
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'NOMBRE_DEPARTAMENTO'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre departamento'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit11: TUniDBEdit
      Left = 269
      Top = 171
      Width = 270
      Height = 22
      Hint = ''
      DataField = 'NOMBRE_AREA'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre '#225'rea'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  object UniDBEdit4: TUniDBEdit [3]
    Left = 0
    Top = 173
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'AREA'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = #193'rea'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit1: TUniDBEdit [4]
    Left = 0
    Top = 61
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [5]
    Left = 0
    Top = 98
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'TIPO'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Tipo'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit3: TUniDBEdit [6]
    Left = 0
    Top = 135
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'DEPARTAMENTO'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Departamento'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit5: TUniDBEdit [7]
    Left = 0
    Top = 213
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'CODIGO_EMPLEADO'
    DataSource = DataSource
    TabOrder = 7
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'C'#243'digo empleado'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit6: TUniDBEdit [8]
    Left = 0
    Top = 253
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'EMAIL'
    DataSource = DataSource
    TabOrder = 8
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Email'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit7: TUniDBEdit [9]
    Left = 270
    Top = 61
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'EMAIL_CC'
    DataSource = DataSource
    TabOrder = 9
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Email CC'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit8: TUniDBEdit [10]
    Left = 270
    Top = 98
    Width = 270
    Height = 22
    Hint = ''
    DataField = 'EMAIL_BCC'
    DataSource = DataSource
    TabOrder = 10
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Email BCC'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
