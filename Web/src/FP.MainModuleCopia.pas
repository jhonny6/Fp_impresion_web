unit FP.MainModule;

interface

uses
  uniGUIMainModule, SysUtils, Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, DBAccess, Uni,
  FireDAC.Phys.PG, FireDAC.Phys.PGDef, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Phys.Oracle,
  FireDAC.Phys.OracleDef, System.IniFiles, Generics.Collections,
  FireDAC.Moni.Base, FireDAC.Moni.RemoteClient;

type
  TCloseObserver = class
  private
    FOnCierra: TList<TProc>;

  public
    property OnCierra: TList<TProc> read FOnCierra write FOnCierra;

    constructor Create;
    destructor Destroy; override;
  end;

type
  TUniMainModule = class(TUniGUIMainModule)
    SQLConn: TUniConnection;
    QAuxiliar: TUniQuery;
    FDMoniRemoteClientLink1: TFDMoniRemoteClientLink;
    procedure UniGUIMainModuleDestroy(Sender: TObject);
    procedure UniGUIMainModuleCreate(Sender: TObject);
  private
    FOnCloseObserver: TCloseObserver;
    { Private declarations }
  public
    { Public declarations }
    pk: variant;
    function GetMenu: TUniQuery;
    function GetSubMenu(pIDPadre: Integer): TUniQuery;
    function processLogin(pUser, pPasswd: string;
      var pEsquemas: String): Integer;

    property OnCloseObserver: TCloseObserver read FOnCloseObserver
      write FOnCloseObserver;
  end;

var
  CONFIG_DATABASE_HOST_IP: String = '192.168.0.201';
  CONFIG_DATABASE_NAME: String = 'XE';
  CONFIG_DATABASE_USERNAME: String = 'system';
  CONFIG_DATABASE_PASSWORD: String = '20fast14';
  CONFIG_DATABASE_PORT: String = '5432';
  // PGVENDORHOME='E:\Proyectos\02-CashBank\Web\out\Win32\Debug\bin\pgsql\';
  SCHEMA: String = 'FASTENCE';
  // PGLIBPQ = 'libpq.dll';

function UniMainModule: TUniMainModule;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIApplication, FP.ServerModule;

function UniMainModule: TUniMainModule;
begin
  Result := TUniMainModule(UniApplication.UniMainModule)
end;

procedure TUniMainModule.UniGUIMainModuleCreate(Sender: TObject);
var
  vIniFile: TIniFile;
begin
  SQLConn.Connected := False;
  vIniFile := TIniFile.Create(ExtractFilePath(Paramstr(0)) +
    'FP_impresion_web.ini');
  try
    CONFIG_DATABASE_HOST_IP := vIniFile.ReadString('CONFIGURACION',
      'CONFIG_DATABASE_HOST_IP', CONFIG_DATABASE_HOST_IP);

    CONFIG_DATABASE_NAME := vIniFile.ReadString('CONFIGURACION', 'CRSERVER',
      CONFIG_DATABASE_NAME);

    CONFIG_DATABASE_USERNAME := vIniFile.ReadString('CONFIGURACION', 'USER',
      CONFIG_DATABASE_USERNAME);

    CONFIG_DATABASE_PASSWORD := vIniFile.ReadString('CONFIGURACION', 'PASSWORD',
      CONFIG_DATABASE_PASSWORD);

    SQLConn.Params.Clear;
    // SQLConn.Params.Add('Server=' + CONFIG_DATABASE_HOST_IP);
    SQLConn.Params.Add('DataBase=' + CONFIG_DATABASE_NAME);
    SQLConn.Params.Add('User_Name=' + CONFIG_DATABASE_USERNAME);
    SQLConn.Params.Add('Password=' + CONFIG_DATABASE_PASSWORD);
    SQLConn.Params.Add('DriverID=Ora');
    SQLConn.Params.Add('CharacterSet=utf8');
    SQLConn.Params.Add('ApplicationName=FP_IMPRESION_WEB');
    SQLConn.Params.Add('MonitorBy=Remote');
  finally
    vIniFile.Free;
  end;

  OnCloseObserver := TCloseObserver.Create;
end;

procedure TUniMainModule.UniGUIMainModuleDestroy(Sender: TObject);
var
  vProc: TProc;
begin
  if OnCloseObserver.OnCierra.Count > 0 then
    for vProc in OnCloseObserver.OnCierra do
      vProc;

  OnCloseObserver.Free;
end;

function TUniMainModule.processLogin(pUser, pPasswd: string;
  var pEsquemas: String): Integer;
var
  vQ: TUniQuery;
  bRemember, bLoginOk: Boolean;
  cPassword: string;
begin
  pUser := Trim(lowerCase(pUser));
  pPasswd := Trim(pPasswd);

  vQ := TUniQuery.Create(nil);
  try
    vQ.Connection := SQLConn;
    vQ.Open('select id, esquemas from ' + SCHEMA +
      '.fp0_operarios_web where codigo = :cod and password = :pass',
      [pUser, pPasswd], [TFieldType.ftString, TFieldType.ftString]);
    if vQ.IsEmpty and (1 = 2) then
      Result := -1
    else
    begin
      pEsquemas := 'fastence'; //vQ.FieldByName('esquemas').AsString;
      Result := 1; //vQ.FieldByName('id').AsInteger;
    end;

    // if pUser = pPasswd then
    // Result := 1
    // else
    // Result := -1;
  finally
    vQ.Free;
  end;
end;

function TUniMainModule.GetMenu: TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  Result.Connection := SQLConn;
  Result.Open
    ('select id, id_padre, orden, clase, descripcion, menuicon, tabicon, nombreimagen from '
    + SCHEMA +
    '.appmenu_web where clase NOT LIKE ''MNU_%'' or clase IS NULL order by id, id_padre, orden');
end;

function TUniMainModule.GetSubMenu(pIDPadre: Integer): TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  Result.Connection := SQLConn;
  Result.Open
    ('select id, id_padre, orden, clase, descripcion, menuicon, tabicon, nombreimagen from '
    + SCHEMA + '.appmenu_web where ID_PADRE = :ID and ID <> :ID', [pIDPadre],
    [TFieldType.ftInteger]);
end;

{ TCloseObserver }

constructor TCloseObserver.Create;
begin
  FOnCierra := TList<TProc>.Create;
end;

destructor TCloseObserver.Destroy;
begin
  OnCierra.Free;
end;

initialization

RegisterMainModuleClass(TUniMainModule);

end.
