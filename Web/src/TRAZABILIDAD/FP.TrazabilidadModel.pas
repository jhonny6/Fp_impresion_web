unit FP.TrazabilidadModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TTrazabilidadModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  TrazabilidadModel: TTrazabilidadModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TTrazabilidadModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := '@PKG99_GRID_GESTION.GV_FP0_MULTIUBICACION_GEST';
end;

function TTrazabilidadModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TTrazabilidadModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TTrazabilidadModel.TableName: string;
begin
  Result := 'fastprocess.gv_fp0_multiubicacion_gest';
end;

end.
