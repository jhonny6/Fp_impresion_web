unit FP.TrazabilidadController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TTrazabilidadCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateTrazabilidadCmd = class(TTrazabilidadCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadTrazabilidadCmd = class(TTrazabilidadCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateTrazabilidadCmd = class(TTrazabilidadCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteTrazabilidadCmd = class(TTrazabilidadCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickTrazabilidadCmd = class(TReadTrazabilidadCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.TrazabilidadBrowser,
  FP.TrazabilidadModel,
  FP.TrazabilidadView,
  FP.Main;

{ TTrazabilidadCommand }

function TTrazabilidadCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TTrazabilidadModel;
end;

function TTrazabilidadCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TTrazabilidadBrowser;
end;

function TTrazabilidadCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TTrazabilidad;
end;

{ TCreateTrazabilidadCmd }

procedure TCreateTrazabilidadCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadTrazabilidadCmd }

{
procedure TReadTrazabilidadCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadTrazabilidadCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateTrazabilidadCmd }

procedure TUpdateTrazabilidadCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteTrazabilidadCmd }

procedure TDeleteTrazabilidadCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickTrazabilidadCmd }

procedure TPickTrazabilidadCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateTrazabilidadCmd, TReadTrazabilidadCmd, TUpdateTrazabilidadCmd, TDeleteTrazabilidadCmd, TPickTrazabilidadCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateTrazabilidadCmd, TReadTrazabilidadCmd, TUpdateTrazabilidadCmd, TDeleteTrazabilidadCmd, TPickTrazabilidadCmd]);

end.
