unit CashBank.EntityBrowserMD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.FrameBase, uniGUIBaseClasses,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu,
  uniGUIClasses, uniBasicGrid, uniDBGrid, Data.DB,
  uniButton, uniBitBtn, uniPanel, CashBank.EntityModelMD,
  uniSpeedButton, uniDBNavigator, uniLabel, uniEdit, uFiltro,
  Generics.Collections, DBAccess, Uni, FP.GridHelper, uniPageControl,
  uniGridExporters, FP.UniQueryHelper;

type

  TEntityBrowserClassMD = class of TEntityBrowserMD;

  TEntityBrowserMD = class(TfrmBase)
    ExportExcel: TUniActionList;
    DatasetEdit: TDataSetEdit;
    DatasetDelete: TDataSetDelete;
    DatasetInsert: TDataSetInsert;
    DatasetFirst: TDataSetFirst;
    DatasetPrior: TDataSetPrior;
    DatasetNext: TDataSetNext;
    DatasetLast: TDataSetLast;
    DatasetRefresh: TDataSetRefresh;
    DataSource: TDataSource;
    UniPanel1: TUniPanel;
    ModelEditAct: TAction;
    ModelInsertAct: TAction;
    ModelDeleteAct: TAction;
    UniSpeedButton1: TUniSpeedButton;
    UniSpeedButton4: TUniSpeedButton;
    UniSpeedButton5: TUniSpeedButton;
    UniSpeedButton6: TUniSpeedButton;
    UniSpeedButton7: TUniSpeedButton;
    UniDBNavigator1: TUniDBNavigator;
    lbSeleccionRegistro: TUniLabel;
    edBusqueda: TUniEdit;
    UniSpeedButton2: TUniSpeedButton;
    pcPpal: TUniPageControl;
    tsBusqueda: TUniTabSheet;
    tsDetalle: TUniTabSheet;
    DBGBusqueda: TUniDBGrid;
    pnlFiltroBuscar: TUniPanel;
    pnlCabecera: TUniPanel;
    pcSubDetalle: TUniPageControl;
    tsSubDetalle: TUniTabSheet;
    DSDetalle: TDataSource;
    DBGDetalle: TUniDBGrid;
    DSEnc: TDataSource;
    UniSpeedButton3: TUniSpeedButton;
    UniGridExcelExporter1: TUniGridExcelExporter;
    procedure ModelEditActExecute(Sender: TObject);
    procedure ModelDeleteActExecute(Sender: TObject);
    procedure ModelDeleteActUpdate(Sender: TObject);
    procedure ModelEditActUpdate(Sender: TObject);
    procedure ModelInsertActExecute(Sender: TObject);
    procedure ModelPickActExecute(Sender: TObject);
    procedure ModelPickActUpdate(Sender: TObject);
    procedure DBGBusquedaDblClick(Sender: TObject);
    procedure UniSpeedButton5Click(Sender: TObject);
    procedure edBusquedaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGBusquedaColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure UniSpeedButton2Click(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure DSDetalleStateChange(Sender: TObject);
    procedure DBGBusquedaColumnSort(Column: TUniDBGridColumn;
      Direction: Boolean);
    procedure ExportExcelExecute(Action: TBasicAction; var Handled: Boolean);
  private
    FPickMode: Boolean;
    FSLQBase: String;
    procedure SetPickMode(const Value: Boolean);
    procedure Filtro(DataSet: TDataSet; var Accept: Boolean);
    procedure SortColumn(const FieldName: string; Dir: Boolean);
  protected
    procedure CreateEntity;
    procedure DeleteEntity;
    procedure PickEntity;
    procedure UpdateEntity;
    function EntityName: string;
    { Private declarations }
  public
    { Public declarations }
    property SQLBase: String read FSLQBase;

    property PickMode: Boolean read FPickMode write SetPickMode;
    function Model: TEntityModelMD;
    procedure refresh;

    procedure FiltrarYEjecutar(pCondicion: String);
  end;

implementation

{$R *.dfm}

uses CashBank.AppControllerMD, uniguitypes;

function TEntityBrowserMD.EntityName: string;
begin
  Result := self.ClassName;
  // Remove 'T' prefix
  Result := Result.Substring(1);
  // Remove 'Browser' sufix
  Result := Result.Substring(0, Length(Result) - Length('Browser'));
end;

procedure TEntityBrowserMD.ExportExcelExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  DBGBusqueda.Exporter.Title := Self.Caption;
  DBGBusqueda.Exporter.Exporter := UniGridExcelExporter1;
  DBGBusqueda.Exporter.IncludeGroups := True;
  DBGBusqueda.Exporter.IncludeSummary := True;
  DBGBusqueda.Exporter.ExportGrid;
end;

function TEntityBrowserMD.Model: TEntityModelMD;
begin
  Result := DataSource.DataSet.Owner as TEntityModelMD;
end;

procedure TEntityBrowserMD.SetPickMode(const Value: Boolean);
begin
  FPickMode := Value;
  lbSeleccionRegistro.Visible := not Value;
  BorderStyle := uniguitypes.bsSingle;
end;

procedure TEntityBrowserMD.CreateEntity;
begin
  TAppCommandMD['Create' + EntityName].Execute;
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowserMD.DBGBusquedaColumnSort(Column: TUniDBGridColumn;
  Direction: Boolean);
begin
  inherited;
  SortColumn(Column.FieldName, Direction);
end;

procedure TEntityBrowserMD.DBGBusquedaColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  inherited;
  // if Column.FieldName = 'ID' then
  // Column.AuxValue := Column.AuxValue + 1;
end;

procedure TEntityBrowserMD.DBGBusquedaDblClick(Sender: TObject);
begin
  inherited;
  if PickMode then
  begin
    modalresult := mrOK;
    close;
  end;
end;

procedure TEntityBrowserMD.UniFormShow(Sender: TObject);
begin
  inherited;
  FSLQBase := TUniQuery(DataSource.DataSet).SQL.Text;
  pcPpal.ActivePage := tsBusqueda;
end;

procedure TEntityBrowserMD.UniSpeedButton2Click(Sender: TObject);
var
  vFiltro: TFiltroFrm;
begin
  inherited;
  vFiltro := FiltroFrm;
  vFiltro.Show;
  vFiltro.DataSet := DataSource.DataSet;

  vFiltro.OnAplicaFiltro := (
    procedure(pFiltros: TList<TConector>)
    var
      vDT: TUniQuery;
    begin
      vDT := TUniQuery(DataSource.DataSet);

      vDT.Close;
      vDT.SQl.Clear;
      try
        vDT.OpenExt(SQLBase + ' where ' + vFiltro.Condiciones);
      finally
        edBusqueda.Text := TUniQuery(DataSource.DataSet).SQL.Text;
      end;
    end);

  // vFiltro.ShowModal;
end;

procedure TEntityBrowserMD.UniSpeedButton5Click(Sender: TObject);
begin
  inherited;
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowserMD.UpdateEntity;
begin
  TAppCommandMD['Update' + EntityName].Execute(Model.PKValue);
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowserMD.SortColumn(const FieldName: string; Dir: Boolean);
begin
//  if Dir then
//    TUniQuery(DataSource.DataSet).IndexName := FieldName+'_index_asc'
//  else
//    TUniQuery(DataSource.DataSet).IndexName := FieldName+'_index_des';
end;


procedure TEntityBrowserMD.DataSourceStateChange(Sender: TObject);
var
  I: Integer;
  IndexnameAsc: string;
  IndexnameDes: string;
begin
//  inherited;
//  if (DataSource.DataSet.State in [dsBrowse]) then
//    DBGBusqueda.SetGridConfig(Self.Name);
//
//  TUniQuery(DataSource.DataSet).IndexDefs.ClearAndResetID;
//
//  for I := 0 to DataSource.DataSet.FieldCount - 1 do
//  begin
//    DBGBusqueda.Columns.Items[I].Sortable := True;
//
//    IndexnameAsc := DataSource.DataSet.Fields[I].FieldName + '_index_asc';
//    IndexnameDes := DataSource.DataSet.Fields[I].FieldName + '_index_des';
//    TUniQuery(DataSource.DataSet).IndexDefs.Add(IndexnameAsc,
//      DataSource.DataSet.Fields[I].FieldName, []);
//    TUniQuery(DataSource.DataSet).IndexDefs.Add(IndexnameDes,
//      DataSource.DataSet.Fields[I].FieldName, [ixDescending]);
//  end;
end;

procedure TEntityBrowserMD.DeleteEntity;
var
  vI: Integer;
  vBook: TBookmark;
begin
  DataSource.DataSet.DisableControls;
  try
    for vI := 0 to DBGBusqueda.SelectedRows.Count - 1 do
    begin
      vBook := DBGBusqueda.SelectedRows.Items[vI];
      DataSource.DataSet.GotoBookmark(vBook);

      TAppCommandMD['Delete' + EntityName].Execute(Model.PKValue);

      DataSource.DataSet.FreeBookmark(vBook);
    end;
    DataSource.DataSet.refresh;
  finally
    DataSource.DataSet.EnableControls;
  end;
end;

procedure TEntityBrowserMD.DSDetalleStateChange(Sender: TObject);
begin
  inherited;
  if (DataSource.DataSet.State in [dsBrowse]) then
    DBGDetalle.SetGridConfig(Self.Name);
end;

procedure TEntityBrowserMD.edBusquedaKeyPress(Sender: TObject; var Key: Char);
var
  vDS: TDataSet;
begin
  vDS := DataSource.DataSet;
  if Ord(Key) = 13 then
  begin
    if Trim(edBusqueda.Text) = '' then
      vDS.Filtered := False
    else
    begin
      vDS.Filtered := False;
      vDS.OnFilterRecord := Filtro;
      vDS.Filtered := True;
    end;
  end;
end;

procedure TEntityBrowserMD.FiltrarYEjecutar(pCondicion: String);
begin
  Model.Data.SQL.Clear;
  Model.Data.SQL.Text := Model.SQLFindAll;

  Model.Data.SQL.Text :=  Model.Data.SQL.Text.Replace('where 1=0', '');

  if pCondicion <> EmptyStr then
    Model.Data.SQL.Text := Model.Data.SQL.Text + ' where 1=1 ' + pCondicion;

  Model.Data.Close;
  Model.Data.Open;
end;

procedure TEntityBrowserMD.Filtro(DataSet: TDataSet; var Accept: Boolean);
var
  vI: Integer;
begin
  for vI := 0 to DataSet.Fields.Count - 1 do
  begin
    Accept := DataSet.Fields[vI].AsString.Contains(edBusqueda.Text);

    if Accept then
      break;
  end;
end;

procedure TEntityBrowserMD.PickEntity;
begin
end;

procedure TEntityBrowserMD.refresh;
begin
  DataSource.DataSet.refresh;
end;

procedure TEntityBrowserMD.ModelDeleteActExecute(Sender: TObject);
begin
  DeleteEntity;
end;

procedure TEntityBrowserMD.ModelInsertActExecute(Sender: TObject);
begin
  CreateEntity;
end;

procedure TEntityBrowserMD.ModelPickActExecute(Sender: TObject);
begin
  PickEntity;
end;

procedure TEntityBrowserMD.ModelDeleteActUpdate(Sender: TObject);
begin
  ModelDeleteAct.Enabled := Model.PKValue <> Null;
end;

procedure TEntityBrowserMD.ModelEditActExecute(Sender: TObject);
begin
  inherited;
  //UpdateEntity;
  if pcPPal.ActivePage = tsDetalle then
    pcPPal.ActivePage := tsBusqueda
  else
    pcPPal.ActivePage := tsDetalle;
end;

procedure TEntityBrowserMD.ModelEditActUpdate(Sender: TObject);
begin
  ModelEditAct.Enabled := Model.PKValue <> Null;
end;

procedure TEntityBrowserMD.ModelPickActUpdate(Sender: TObject);
begin
  // ModelPickAct.Visible := PickMode;
end;

end.
