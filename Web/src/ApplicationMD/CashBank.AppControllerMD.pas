unit CashBank.AppControllerMD;

interface

uses uniGUIClasses, uniGUIForm, CashBank.EntityViewMD, CashBank.EntityBrowserMD,
  CashBank.EntityModelMD, Data.DB, Uni;

type
  TCommandArgMD = Variant;
  TCommandArgsMD = array of TCommandArgMD;

  TAppCommandClassMD = class of TAppCommandMD;

  TAppCommandMD = class(TObject)
  strict private
    class function GetCommands(const CommandName: string)
      : TAppCommandClassMD; static;
  private
  protected
    View: TEntityBrowserMD;
    Model: TEntityModelMD;
    function EntityModelClass: TEntityModelClassMD; virtual; abstract;
    function EntityBrowserClass: TEntityBrowserClassMD; virtual; abstract;
    function EntityViewClass: TEntityViewClassMD; virtual; abstract;

    procedure DataSourceParerByName(pView: TEntityBrowserMD;
      pModel: TEntityModelMD);

  public
    class procedure Execute; overload;
    class procedure Execute(Arg: TCommandArgMD); overload;
    class procedure Execute(Args: TCommandArgsMD); overload;
    class procedure Execute(Args: TUniControl); overload;
    class procedure Execute(Args: TEntityViewMD; PickMode: boolean); overload;
    procedure Proccess(Args: TCommandArgsMD); overload; virtual; abstract;
    procedure Proccess(Args: TUniControl); overload; virtual;
    procedure Proccess(Args: TEntityViewMD; pickView: boolean);
      overload; virtual;

    // API para gestionar un registro global de comandos disponibles por nombre.
    // La invocacion por nombre permite reducir el acoplamiento entre partes
    // de la aplicacion de forma sencilla
    class procedure RegisterCmd(CommandClass: TAppCommandClassMD); overload;
    class procedure RegisterCmd(CommandClasses
      : TArray<TAppCommandClassMD>); overload;
    class procedure UnegisterCmd(CommandClass: TAppCommandClassMD); overload;
    class procedure UnegisterCmd(CommandClasses
      : TArray<TAppCommandClassMD>); overload;
    class property Commands[const ClassName: string]: TAppCommandClassMD
      read GetCommands; default;
  end;

implementation

uses System.SysUtils,
  System.Variants,
  System.Generics.Collections, uniGUIApplication, System.Classes;

var
  FCommands: TDictionary<string, TAppCommandClassMD>;

  { TEntityCommand }

class procedure TAppCommandMD.Execute;
begin
  Execute([]);
end;

class procedure TAppCommandMD.Execute(Arg: TCommandArgMD);
begin
  Execute([Arg]);
end;

class procedure TAppCommandMD.Execute(Args: TCommandArgsMD);
var
  Command: TAppCommandMD;
begin
  Command := Self.Create;
  Command.Proccess(Args);
  Command.Free;
end;

class procedure TAppCommandMD.Execute(Args: TUniControl);
var
  Command: TAppCommandMD;
begin
  Command := Self.Create;
  Command.Proccess(Args);
  Command.Free;

end;

procedure TAppCommandMD.DataSourceParerByName(pView: TEntityBrowserMD;
  pModel: TEntityModelMD);
var
  I: Integer;
  vDS: TDataSource;
  vDataSet: TDataSet;
begin
  for I := 0 to View.ComponentCount - 1 do
  begin
    if pView.Components[I] is TDataSource then
    begin
      vDS := TDataSource(pView.Components[I]);
      if string(pView.Components[I].Name).ToUpper.StartsWith('DS') then
      begin
        vDataSet :=
          TDataSet(pModel.FindComponent(string(pView.Components[I].Name)
          .Replace('DS', 'Q')));

        vDataSet.Close;
        vDataSet.Open;

        if ((vDS.Name <> 'DSDetalle') and (vDS.Name <> 'DSEnc')) then
        begin
          TUniQuery(vDataSet).MasterSource := pView.DSDetalle;
        end;

        if Assigned(vDataSet) then
          vDS.DataSet := vDataSet;
      end;
    end;
  end;
end;

class procedure TAppCommandMD.Execute(Args: TEntityViewMD; PickMode: boolean);
var
  Command: TAppCommandMD;
begin
  Command := Self.Create;
  Command.Proccess(Args, PickMode);
  Command.Free;
end;

class function TAppCommandMD.GetCommands(const CommandName: string)
  : TAppCommandClassMD;
begin
  Result := FCommands[Format('T%sCmd', [CommandName])];
end;

procedure TAppCommandMD.Proccess(Args: TEntityViewMD; pickView: boolean);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);

  View.DataSource.DataSet := Model.FindAll;

  View.PickMode := pickView;
  View.ShowModal(
    procedure(Sender: TComponent; Result: Integer)
    var
      EC: TEntityInfoMD;
    begin
      if Result = 1 then
      begin
        EC.Subject := esPick;
        EC.Entity := (Sender as TEntityBrowserMD).Model;
        EC.PK := EC.Entity.PKValue;
        Args.setEntityInfo(EC);
      end;
      // args.refresh;
    end);

end;

procedure TAppCommandMD.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent := Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;

  DataSourceParerByName(View, Model);


  Model.QEnc.MasterSource := View.DataSource;
  Model.QDetalle.MasterSource := View.DataSource;
  // Model.QDetalle.MasterFields := 'ID';
  View.Show;
end;

class procedure TAppCommandMD.RegisterCmd(CommandClasses
  : TArray<TAppCommandClassMD>);
var
  CommandClass: TAppCommandClassMD;
begin
  for CommandClass in CommandClasses do
    RegisterCmd(CommandClass);
end;

class procedure TAppCommandMD.UnegisterCmd(CommandClasses
  : TArray<TAppCommandClassMD>);
var
  CommandClass: TAppCommandClassMD;
begin
  for CommandClass in CommandClasses do
    UnegisterCmd(CommandClass);
end;

class procedure TAppCommandMD.RegisterCmd(CommandClass: TAppCommandClassMD);
begin
  FCommands.AddOrSetValue(CommandClass.ClassName, CommandClass);
end;

class procedure TAppCommandMD.UnegisterCmd(CommandClass: TAppCommandClassMD);
begin
  FCommands.Remove(CommandClass.ClassName);
end;

initialization

FCommands := TDictionary<string, TAppCommandClassMD>.Create;

finalization

FCommands.Free;

end.
