object EntityViewMD: TEntityViewMD
  Left = 0
  Top = 0
  ClientHeight = 683
  ClientWidth = 1093
  Caption = 'EntityView'
  OnShow = UniFormShow
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 15
  object UniSimplePanel1: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 1093
    Height = 41
    Hint = ''
    ParentColor = False
    Align = alTop
    AlignmentControl = uniAlignmentClient
    ParentAlignmentControl = False
    TabOrder = 0
    ExplicitWidth = 635
    object UniSpeedButton1: TUniSpeedButton
      Left = 24
      Top = 3
      Width = 55
      Height = 35
      Action = DatasetPost
      ParentColor = False
      ImageIndex = 7
      TabOrder = 1
    end
    object UniSpeedButton2: TUniSpeedButton
      Left = 104
      Top = 3
      Width = 55
      Height = 35
      Action = DatasetCancel
      ParentColor = False
      ImageIndex = 8
      TabOrder = 2
    end
    object UniSpeedButton3: TUniSpeedButton
      Left = 176
      Top = 3
      Width = 55
      Height = 35
      Action = DatasetRefresh
      ParentColor = False
      ImageIndex = 9
      TabOrder = 3
    end
  end
  object UniContainerPanel1: TUniScrollBox
    Left = 0
    Top = 41
    Width = 1093
    Height = 620
    Hint = ''
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 635
    ExplicitHeight = 418
  end
  object StatusBar: TUniStatusBar
    Left = 0
    Top = 661
    Width = 1093
    Hint = ''
    Panels = <
      item
        Text = 'Created'
        Width = 200
      end
      item
        Text = 'Updated'
        Width = 200
      end>
    SizeGrip = False
    Align = alBottom
    ParentColor = False
    Color = clWindow
    ExplicitTop = 459
    ExplicitWidth = 635
  end
  object DataSource: TDataSource
    Left = 260
    Top = 126
  end
  object UniActionList1: TUniActionList
    Left = 208
    Top = 233
    object StatusBarAct: TAction
      Category = 'Edition'
      OnUpdate = StatusBarActUpdate
    end
    object DatasetPost: TDataSetPost
      Category = 'Dataset'
      Caption = '<i class="fa fa-save fa-2x"></i>'
      Hint = 'Post'
      ImageIndex = 7
    end
    object DatasetCancel: TDataSetCancel
      Category = 'Dataset'
      Caption = '<i class="fa fa-times fa-2x"></i>'
      Hint = 'Cancel'
      ImageIndex = 8
    end
    object DatasetRefresh: TDataSetRefresh
      Category = 'Dataset'
      Caption = '<i class="fa fa-retweet fa-2x"></i>'
      Hint = 'Refresh'
      ImageIndex = 9
    end
  end
end
