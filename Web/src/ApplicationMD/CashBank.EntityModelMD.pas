unit CashBank.EntityModelMD;

interface

uses
  System.SysUtils, System.Classes, DBAccess, Uni,
  Data.DB, uniGUIDialogs, MemDS, Fp.UniQueryHelper;

type

  TEntityModelClassMD = class of TEntityModelMD;

  TEntityModelMD = class;

  TEntityInfoMD = record
  public type
    TSubject = (esCreate, esUpdate, esDelete, esPick);
  public
    Subject: TSubject;
    Entity: TEntityModelMD;
    PK: Variant;
  end;

  TEntityModelMD = class(TDataModule)
    Data: TUniQuery;
    QDetalle: TUniQuery;
    QEnc: TUniQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataAfterInsert(DataSet: TDataSet);
    procedure DataAfterOpen(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure DataAfterDelete(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataCalcFields(DataSet: TDataSet);

  private
    CopyPKValue: Variant;
    procedure PreparaEnc(Sender: TObject);
    procedure PreparaDet(Sender: TObject);
    procedure InitConnections;
    { Private declarations }
  protected
    function SQLFindByPK: string;
    function SQLNewEntity: string;
    procedure DoOnChangeField(Sender: TField);
  public
    function SQLFindAll: string; virtual;
    class function PackageNameEnc: string; virtual; abstract;
    class function PackageNameDet: string; virtual; abstract;
    class function TableName: string; virtual; abstract;
    class function PKName: string; virtual;
    function PKValue: Variant;
    function FindAll: TDataSet;
    function FindByPK(const PK: Variant): TDataSet;
    function NewEntity: TDataSet;
    function CargaSqlUpdate(AProcedure: String): String;
    { Public declarations }
  end;

var
  EntityModelMD: TEntityModelMD;

implementation

uses System.Variants,
  System.StrUtils, Fp.MainModule;

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}

procedure TEntityModelMD.InitConnections;
var
  I: Integer;
begin
  for I := 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[I] is TUniQuery then
      TUniQuery(Self.Components[I]).Connection := UniMainModule.SQLConn;
  end;
end;

procedure TEntityModelMD.DataAfterDelete(DataSet: TDataSet);
begin
  // NotifyChanged(esDelete, CopyPKValue);
end;

procedure TEntityModelMD.DataAfterInsert(DataSet: TDataSet);
begin
  // NotifyChanged(esUpdate, PKValue);
  // Reglas de negocio tipo encadenar procesos:
  // Ej: Notificar modificacion
end;

procedure TEntityModelMD.DataAfterOpen(DataSet: TDataSet);
// const
// NoVisibles: array [1 .. 4] of string = ('id', 'date_add', 'date_upd',
// 'lockid');
// InKey: array [1 .. 2] of string = ('id', 'lockid');
// var
// Field: TField;
begin
  // Si no creamos los TFields en dise�o, al abrir los crea el Dataset.
  // Otra opci�n es crear los TFields por c�digo (ver TFieldDefs)
  // Aqui podemos configurarlos.
  // Aprovecharemos convenios usados en el dise�o de nuestra BD
  // for Field in Data.Fields do
  // begin
  // Field.ProviderFlags := [pfInupdate];
  //
  // if MatchText(Field.FieldName, NoVisibles) then
  // Field.Visible := False;
  //
  // // los campos FK ocultos
  // if Field.FieldName.StartsWith('id', True) then
  // Field.Visible := False;
  //
  // // por convenio haremos que los campos de join contengan "." en el nombre.
  // // Los marcaremos como no actualizables
  // if Field.FieldName.Contains('.') then
  // begin
  // Field.ProviderFlags := []; // no enviaremos a la BD
  // Field.ReadOnly := False; // Permitir que podamos editar en memoria
  // end;
  //
  // // Configuramos los campos que se usan en el where para el Updatemode = WhereKeysOnly;
  // // usamos la PK + el lockid para bloqueo optimista
  // // el update_at es TDatetime es decir un float y debido a errores de redondeo
  // // no es adecuado para implementar el bloqueo optimista
  // if MatchText(Field.FieldName, InKey) then
  // Field.ProviderFlags := Field.ProviderFlags + [pfInKey];
  // Field.OnChange := DoOnChangeField;
  // end;

end;

Function TEntityModelMD.CargaSqlUpdate(AProcedure: String): String;
Var
  stpProcedure: TUniStoredProc;
  I: Integer;
  TXSQLUPDATE: String;
begin
  stpProcedure := TUniStoredProc.Create(Self);
  try
    try
      with stpProcedure do
      begin
        Connection := UniMainModule.SQLConn;
        StoredProcName := AProcedure;
        Prepare;
        TXSQLUPDATE := ' declare  BEGIN ' + AProcedure + ' ( ';
        for I := 0 to stpProcedure.ParamCount - 1 do
        begin
          if I <> 0 then
            TXSQLUPDATE := TXSQLUPDATE + ',';

          TXSQLUPDATE := TXSQLUPDATE + ':' + copy(Params[I].Name, 2, 50);
        end;

        TXSQLUPDATE := TXSQLUPDATE + '); END; ';
      end;
    except
      on E: Exception do
        ShowMessageN(E.ToString);
    end;
  finally
    freeandnil(stpProcedure);
  end;
  Result := TXSQLUPDATE;
end;

procedure TEntityModelMD.DataAfterPost(DataSet: TDataSet);
begin
  // NotifyChanged(esUpdate, PKValue);
end;

procedure TEntityModelMD.DataBeforeDelete(DataSet: TDataSet);
begin
  CopyPKValue := PKValue;
end;

procedure TEntityModelMD.DataBeforePost(DataSet: TDataSet);
var
  Field: TField;
begin
  // FindField devuelve nil si el campo no existe. FieldByName espera que exista: levanta exception si el campo no existe
  // Estamos centralizando esta reglas para todas las tablas, usamos FindField por si en alguna no quisieramos estos campos

  if DataSet.State = dsInsert then
  begin // logica que dependa del estado
    Field := Data.FindField('date_add');
    if Assigned(Field) then
      Field.AsDateTime := Now;
    Field := Data.FindField('lockid');
    if Assigned(Field) then
      Field.AsLargeInt := 0;
  end
  else
  begin
    Field := Data.FindField('lockid');
    if Assigned(Field) then
      Field.AsLargeInt := 1 + Field.AsLargeInt;
  end;

  Field := Data.FindField('date_upd');
  if Assigned(Field) then
    Field.AsDateTime := Now;

  // Este evento tambien es adecueado para Validaciones: haciendo raise impediriamos la actualizacion

end;

procedure TEntityModelMD.DataCalcFields(DataSet: TDataSet);
begin
  //
end;

procedure TEntityModelMD.DoOnChangeField(Sender: TField);
begin

end;

procedure TEntityModelMD.DataModuleCreate(Sender: TObject);
begin
  InitConnections;
  PreparaEnc(Sender);
  PreparaDet(Sender);
end;

procedure TEntityModelMD.PreparaEnc(Sender: TObject);
var
  vPaquete: String;
begin
  QEnc.Connection := UniMainModule.SQLConn;
  // QEnc.UpdatingTable := TableName;
  // QEnc.FetchOptions.Items := QEnc.FetchOptions.Items - [fiMeta];
  // QEnc.UpdateOptions.UpdateTableName := TableName;
  // QEnc.UpdateOptions.LockMode := lmNone;
  // QEnc.UpdateOptions.UpdateMode := upWhereKeyOnly;
  // QEnc.UpdateOptions.UpdateChangedFields := True;
  // QEnc.UpdateOptions.RefreshMode := rmOnDemand;
  //
  // UpdateSQLEnc.Connection := UniMainModule.SQLConn;

  QEnc.SQLInsert.Clear;
  QEnc.SQLUpdate.Clear;
  QEnc.SQLDelete.Clear;

  vPaquete := PackageNameEnc;

  if vPaquete.Contains('@') then
    vPaquete := vPaquete.Replace('@', '.')
  else
    vPaquete := '.pkg_mantenimientos.GV_' + vPaquete;

  QEnc.SQLInsert.Text := CargaSqlUpdate(SCHEMA + vPaquete + '_IU');
  QEnc.SQLUpdate.Text := QEnc.SQLInsert.Text;
  QEnc.SQLDelete.Text := 'DECLARE BEGIN  ' + SCHEMA + vPaquete +
    '_D (:ID); END;';

//  QEnc.UpdateObject := UpdateSQLEnc;
end;

procedure TEntityModelMD.PreparaDet(Sender: TObject);
var
  vPaquete: String;
begin
  QDetalle.Connection := UniMainModule.SQLConn;
  //QDetalle.UpdatingTable := TableName;
  // QDetalle.FetchOptions.Items := QDetalle.FetchOptions.Items - [fiMeta];
  // QDetalle.UpdateOptions.UpdateTableName := TableName;
  // QDetalle.UpdateOptions.LockMode := lmNone;
  // QDetalle.UpdateOptions.UpdateMode := upWhereKeyOnly;
  // QDetalle.UpdateOptions.UpdateChangedFields := True;
  // QDetalle.UpdateOptions.RefreshMode := rmOnDemand;
  //
  // UpdateSQL.Connection := UniMainModule.SQLConn;

  QDetalle.SQLInsert.Clear;
  QDetalle.SQLUpdate.Clear;
  QDetalle.SQLDelete.Clear;

  vPaquete := PackageNameDet;

  if vPaquete.Contains('@') then
    vPaquete := vPaquete.Replace('@', '.')
  else
    vPaquete := '.pkg_mantenimientos.GV_' + vPaquete;

  QDetalle.SQLInsert.Text := CargaSqlUpdate(SCHEMA + vPaquete + '_IU');
  QDetalle.SQLUpdate.Text := QDetalle.SQLInsert.Text;
  QDetalle.SQLDelete.Text := 'DECLARE BEGIN  ' + SCHEMA + vPaquete +
    '_D (:ID); END;';

  // QDetalle.UpdateObject := UpdateSQL;
end;

class function TEntityModelMD.PKName: string;
begin
  Result := 'id';
end;

function TEntityModelMD.SQLFindAll: string;
begin
  Result := Format('select * from %s', [TableName]);
end;

function TEntityModelMD.SQLFindByPK: string;
begin
  Result := SQLFindAll + Format(' where %0:s.%1:s = :%1:s',
    [TableName, PKName]);
end;

function TEntityModelMD.SQLNewEntity: string;
begin
  // usamos una consulta que no traiga nada => truquillo para obtener los TFields correctos pero ningun registro :)
  Result := SQLFindAll + ' where 1 = 0';
end;

function TEntityModelMD.PKValue: Variant;
begin
  Result := Data[PKName];
end;

function TEntityModelMD.FindAll: TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLFindAll);
  QEnc.SQL.Text := Data.SQL.Text;
end;

function TEntityModelMD.FindByPK(const PK: Variant): TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLFindByPK, [PK], []);
  { ^ es lo mismo que:
    Data.SQL.Text := SQLFindByPK;
    Data.ParamByName('ID').Value := PK;
    Data.Open;
  }
end;

function TEntityModelMD.NewEntity: TDataSet;
begin
  Result := Data;
  Data.OpenExt(SQLNewEntity);
end;

end.
