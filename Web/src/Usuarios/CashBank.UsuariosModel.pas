unit CashBank.UsuariosModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TUsuariosModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TUsuariosModel }

class function TUsuariosModel.TableName: string;
begin
  Result := 'cashbank.usuarios';
end;

end.
