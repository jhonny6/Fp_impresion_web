unit CashBank.UsuariosController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TUsuariosCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateUsuariosCmd = class(TUsuariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadUsuariosCmd = class(TUsuariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
    procedure Proccess(Args: TEntityView; pickView:boolean); overload;override;
  end;

  TUpdateUsuariosCmd = class(TUsuariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteUsuariosCmd = class(TUsuariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickUsuariosCmd = class(TReadUsuariosCmd)
  public
    procedure Proccess(Args: TEntityView; pickView:boolean); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.UsuariosBrowser,
  CashBank.UsuariosModel,
  CashBank.UsuariosView,
  FP.Main, System.Classes;

{ TUsuariosCommand }

function TUsuariosCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TUsuariosModel;
end;

function TUsuariosCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TUsuariosBrowser;
end;

function TUsuariosCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TUsuarios;
end;

{ TCreateUsuariosCmd }

procedure TCreateUsuariosCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadUsuariosCmd }

procedure TReadUsuariosCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

procedure TReadUsuariosCmd.Proccess(Args: TEntityView; pickView:boolean);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.PickMode:= pickView;
  View.ShowModal(procedure(Sender: TComponent; Result:Integer)
  var
     EC: TEntityInfo;
    begin
      if result=1 then
      begin
        EC.Subject := esPick;
        EC.Entity :=(Sender as  EntityBrowserClass).Model;
        EC.PK := EC.Entity.PKValue;
        Args.setEntityInfo(EC);
      end;
      //args.refresh;
    end);
end;


{ TUpdateUsuariosCmd }

procedure TUpdateUsuariosCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteUsuariosCmd }

procedure TDeleteUsuariosCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickUsuariosCmd }

procedure TPickUsuariosCmd.Proccess(Args: TEntityView; pickView:boolean);
begin
  inherited;
//  View.PickMode := True;
end;
initialization

TAppCommand.RegisterCmd([TCreateUsuariosCmd, TReadUsuariosCmd, TUpdateUsuariosCmd, TDeleteUsuariosCmd, TPickUsuariosCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateUsuariosCmd, TReadUsuariosCmd, TUpdateUsuariosCmd, TDeleteUsuariosCmd, TPickUsuariosCmd]);

end.
