unit CashBank.UsuariosView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Data.DB,
  uniGUIClasses, uniEdit, uniDBEdit, uniPanel, uniGUIBaseClasses, uniStatusBar,
  System.Actions, Vcl.ActnList, uniMainMenu, uniMemo, uniDBMemo, uniCheckBox,
  uniDBCheckBox, Vcl.DBActns, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton;

type
  TUsuarios = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniDBEdit4: TUniDBEdit;
    UniDBEdit5: TUniDBEdit;
    UniDBEdit8: TUniDBEdit;
    UniDBEdit9: TUniDBEdit;
    UniDBEdit10: TUniDBEdit;
    UniDBEdit11: TUniDBEdit;
    UniDBMemo1: TUniDBMemo;
    UniDBCheckBox2: TUniDBCheckBox;
    UniDBCheckBox1: TUniDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
