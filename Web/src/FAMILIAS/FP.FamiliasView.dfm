inherited Familias: TFamilias
  Caption = 'Familias'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 332
    ScrollWidth = 356
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 70
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'FAMILIA'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Familia'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 30
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 110
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'NOMBRE_FAMILIA'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre familia'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 150
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'CODEMPRESA'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'C'#243'd. empresa'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 190
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'FECHA_LOTE_OBLIGATORIA'
      DataSource = DataSource
      TabOrder = 4
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha lote (Obl)'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit6: TUniDBEdit
      Left = 56
      Top = 230
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'NUMERO_LOTE_OBLIGATORIA'
      DataSource = DataSource
      TabOrder = 5
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'N'#250'mero lote (Obl)'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit7: TUniDBEdit
      Left = 56
      Top = 270
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'TEXTO_NUMERO_LOTE'
      DataSource = DataSource
      TabOrder = 6
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'N'#250'mero lote'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit8: TUniDBEdit
      Left = 56
      Top = 310
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'TEXTO_FECHA_LOTE'
      DataSource = DataSource
      TabOrder = 7
      InputType = 'text'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Fecha lote'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
end
