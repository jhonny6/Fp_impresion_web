unit FP.FamiliasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TFamiliasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateFamiliasCmd = class(TFamiliasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadFamiliasCmd = class(TFamiliasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateFamiliasCmd = class(TFamiliasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteFamiliasCmd = class(TFamiliasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickFamiliasCmd = class(TReadFamiliasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.FamiliasBrowser,
  FP.FamiliasModel,
  FP.FamiliasView,
  FP.Main;

{ TProvinciasCommand }

function TFamiliasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TFamiliasModel;
end;

function TFamiliasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TFamiliasBrowser;
end;

function TFamiliasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TFamilias;
end;

{ TCreateProvinciasCmd }

procedure TCreateFamiliasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateFamiliasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteFamiliasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickFamiliasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateFamiliasCmd, TReadFamiliasCmd, TUpdateFamiliasCmd, TDeleteFamiliasCmd, TPickFamiliasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateFamiliasCmd, TReadFamiliasCmd, TUpdateFamiliasCmd, TDeleteFamiliasCmd, TPickFamiliasCmd]);

end.
