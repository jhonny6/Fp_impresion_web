unit FP.FamiliasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TFamiliasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  FamiliasModel: TFamiliasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TFamiliasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Familias_GEST';
end;

function TFamiliasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TFamiliasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TFamiliasModel.TableName: string;
begin
  Result := 'fastence.fp0_Familias';
end;

end.
