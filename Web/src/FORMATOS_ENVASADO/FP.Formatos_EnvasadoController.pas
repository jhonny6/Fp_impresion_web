unit FP.Formatos_EnvasadoController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TFormatos_EnvasadoCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateFormatos_EnvasadoCmd = class(TFormatos_EnvasadoCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadFormatos_EnvasadoCmd = class(TFormatos_EnvasadoCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateFormatos_EnvasadoCmd = class(TFormatos_EnvasadoCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteFormatos_EnvasadoCmd = class(TFormatos_EnvasadoCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickFormatos_EnvasadoCmd = class(TReadFormatos_EnvasadoCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Formatos_EnvasadoBrowser,
  FP.Formatos_EnvasadoModel,
  FP.Formatos_EnvasadoView,
  FP.Main;

{ TProvinciasCommand }

function TFormatos_EnvasadoCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TFormatos_EnvasadoModel;
end;

function TFormatos_EnvasadoCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TFormatos_EnvasadoBrowser;
end;

function TFormatos_EnvasadoCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TFormatos_Envasado;
end;

{ TCreateProvinciasCmd }

procedure TCreateFormatos_EnvasadoCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateFormatos_EnvasadoCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteFormatos_EnvasadoCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickFormatos_EnvasadoCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateFormatos_EnvasadoCmd, TReadFormatos_EnvasadoCmd, TUpdateFormatos_EnvasadoCmd, TDeleteFormatos_EnvasadoCmd, TPickFormatos_EnvasadoCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateFormatos_EnvasadoCmd, TReadFormatos_EnvasadoCmd, TUpdateFormatos_EnvasadoCmd, TDeleteFormatos_EnvasadoCmd, TPickFormatos_EnvasadoCmd]);

end.
