inherited Formatos_Envasado: TFormatos_Envasado
  Caption = 'Formatos_Envasado'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 245
    ScrollWidth = 356
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 107
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'LARGO'
      DataSource = DataSource
      TabOrder = 0
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Largo'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 31
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 1
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'ID'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 69
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'FORMATO_ENVASADO'
      DataSource = DataSource
      TabOrder = 2
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Envasado'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 146
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ALTO'
      DataSource = DataSource
      TabOrder = 3
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Alto'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 184
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'ANCHO'
      DataSource = DataSource
      TabOrder = 4
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Ancho'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
    object UniDBEdit6: TUniDBEdit
      Left = 56
      Top = 223
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'VOLUMEN'
      DataSource = DataSource
      TabOrder = 5
      InputType = 'number'
      LayoutConfig.Margin = '20'
      FieldLabel = 'Volumen'
      FieldLabelWidth = 125
      FieldLabelAlign = laRight
    end
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 166
  end
  inherited UniActionList1: TUniActionList
    Left = 376
    Top = 281
  end
end
