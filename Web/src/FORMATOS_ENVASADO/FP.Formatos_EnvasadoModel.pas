unit FP.Formatos_EnvasadoModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TFormatos_EnvasadoModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Formatos_EnvasadoModel: TFormatos_EnvasadoModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TFormatos_EnvasadoModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Formatos_Env_GEST';
end;

function TFormatos_EnvasadoModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TFormatos_EnvasadoModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TFormatos_EnvasadoModel.TableName: string;
begin
  Result := 'fastence.fp0_Formatos_Envasado';
end;

end.
