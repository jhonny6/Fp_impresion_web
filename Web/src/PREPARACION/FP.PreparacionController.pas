unit FP.PreparacionController;

interface

uses CashBank.AppControllerMD,
  CashBank.EntityBrowserMD,
  CashBank.EntityViewMD,
  CashBank.EntityModelMD, uniGUIClasses;

type

  TPreparacionCommand = class(TAppCommandMD)
  public
    function EntityModelClass: TEntityModelClassMD; override;
    function EntityBrowserClass: TEntityBrowserClassMD; override;
    function EntityViewClass: TEntityViewClassMD; override;
  end;

  TCreatePreparacionCmd = class(TPreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TReadPreparacionCmd = class(TPreparacionCommand)
  protected
    // View: TEntityBrowser;
    // Model: TEntityModel;
  public
    // procedure Proccess(Args: TCommandArgs); overload;override;
    // procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdatePreparacionCmd = class(TPreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TDeletePreparacionCmd = class(TPreparacionCommand)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TPickPreparacionCmd = class(TReadPreparacionCmd)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.PreparacionBrowser,
  FP.PreparacionModel,
  FP.PreparacionView,
  FP.Main;

{ TPreparacionCommand }

function TPreparacionCommand.EntityModelClass: TEntityModelClassMD;
begin
  Result := TPreparacionModel;
end;

function TPreparacionCommand.EntityBrowserClass: TEntityBrowserClassMD;
begin
  Result := TPreparacionBrowser;
end;

function TPreparacionCommand.EntityViewClass: TEntityViewClassMD;
begin
  Result := TPreparacion;
end;

{ TCreatePreparacionCmd }

procedure TCreatePreparacionCmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadPreparacionCmd }

{
  procedure TReadPreparacionCmd.Proccess(Args: TCommandArgs);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
}

(*
  procedure TReadPreparacionCmd.Proccess(Args: TUniControl);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
*)
{ TUpdatePreparacionCmd }

procedure TUpdatePreparacionCmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeletePreparacionCmd }

procedure TDeletePreparacionCmd.Proccess(Args: TCommandArgsMD);
var
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  // if not ConfirmDlg('Desea borrar este registro?') then
  // Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickPreparacionCmd }

procedure TPickPreparacionCmd.Proccess(Args: TCommandArgsMD);
begin
  inherited;
  // View.PickMode := True;
end;

initialization

TAppCommandMD.RegisterCmd([TCreatePreparacionCmd, TReadPreparacionCmd, TUpdatePreparacionCmd,
  TDeletePreparacionCmd, TPickPreparacionCmd]);

finalization

TAppCommandMD.UnegisterCmd([TCreatePreparacionCmd, TReadPreparacionCmd, TUpdatePreparacionCmd,
  TDeletePreparacionCmd, TPickPreparacionCmd]);

end.
