unit FP.ProvinciasView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Vcl.DBActns,
  System.Actions, Vcl.ActnList, uniMainMenu, Data.DB, uniStatusBar,
  uniGUIClasses, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit, uniMultiItem, uniComboBox,
  FP.ComboHelper;

type
  TProvincias = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
    UniComboBox1: TUniComboBox;
    UniButton1: TUniButton;
    procedure UniButton1Click(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Provincias: TProvincias;

implementation

{$R *.dfm}

procedure TProvincias.UniButton1Click(Sender: TObject);
begin
  inherited;
  UniComboBox1.CargaComboSQL('select provincia from FP0_PROVINCIAS order by provincia')
end;

procedure TProvincias.UniFormShow(Sender: TObject);
begin
  inherited;
  UniComboBox1.CargaComboSQL('select provincia from FP0_PROVINCIAS order by provincia')
end;

end.
