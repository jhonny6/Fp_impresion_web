unit FP.ProvinciasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TProvinciasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateProvinciasCmd = class(TProvinciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadProvinciasCmd = class(TProvinciasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateProvinciasCmd = class(TProvinciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteProvinciasCmd = class(TProvinciasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickProvinciasCmd = class(TReadProvinciasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.ProvinciasBrowser,
  FP.ProvinciasModel,
  FP.ProvinciasView,
  FP.Main;

{ TProvinciasCommand }

function TProvinciasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TProvinciasModel;
end;

function TProvinciasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TProvinciasBrowser;
end;

function TProvinciasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TProvincias;
end;

{ TCreateProvinciasCmd }

procedure TCreateProvinciasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateProvinciasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteProvinciasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateProvinciasCmd, TReadProvinciasCmd, TUpdateProvinciasCmd, TDeleteProvinciasCmd, TPickProvinciasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateProvinciasCmd, TReadProvinciasCmd, TUpdateProvinciasCmd, TDeleteProvinciasCmd, TPickProvinciasCmd]);

end.
