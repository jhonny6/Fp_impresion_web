unit FP.ProvinciasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TProvinciasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ProvinciasModel: TProvinciasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TProvinciasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Provincias_GEST';
end;

function TProvinciasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TProvinciasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TProvinciasModel.TableName: string;
begin
  Result := 'fastence.fp0_Provincias';
end;

end.
