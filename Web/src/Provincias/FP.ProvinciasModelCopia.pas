unit FP.ProvinciasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  DBAccess, Uni, Data.DB, FireDAC.Comp.DataSet, FireDAC.Moni.Base,
  FireDAC.Moni.RemoteClient;

type
  TGestion_EntradasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ProvinciasModel: TProvinciasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TProvinciasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_PROVINCIAS_GEST';
end;

function TProvinciasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TProvinciasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TProvinciasModel.TableName: string;
begin
  Result := 'fastence.fp0_provincias';
end;

end.
