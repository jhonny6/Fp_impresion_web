inherited Provincias: TProvincias
  Caption = 'Provincias'
  PixelsPerInch = 96
  TextHeight = 15
  inherited UniContainerPanel1: TUniScrollBox
    ScrollHeight = 241
    ScrollWidth = 297
    object UniDBEdit1: TUniDBEdit
      Left = 37
      Top = 56
      Width = 121
      Height = 22
      Hint = ''
      DataField = 'CODIGO_PROVINCIA'
      DataSource = DataSource
      TabOrder = 0
    end
    object UniDBEdit2: TUniDBEdit
      Left = 37
      Top = 104
      Width = 121
      Height = 22
      Hint = ''
      DataField = 'ID'
      DataSource = DataSource
      TabOrder = 1
    end
    object UniDBEdit3: TUniDBEdit
      Left = 37
      Top = 160
      Width = 121
      Height = 22
      Hint = ''
      DataField = 'PROVINCIA'
      DataSource = DataSource
      TabOrder = 2
    end
    object UniComboBox1: TUniComboBox
      Left = 37
      Top = 216
      Width = 145
      Hint = ''
      Text = 'UniComboBox1'
      TabOrder = 3
      IconItems = <>
    end
    object UniButton1: TUniButton
      Left = 208
      Top = 216
      Width = 89
      Height = 25
      Hint = ''
      Caption = 'Cargar Combo'
      TabOrder = 4
      OnClick = UniButton1Click
    end
  end
  inherited UniActionList1: TUniActionList
    Left = 368
    Top = 209
  end
end
