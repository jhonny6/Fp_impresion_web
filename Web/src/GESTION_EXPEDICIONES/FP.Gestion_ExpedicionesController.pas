unit FP.Gestion_ExpedicionesController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGestion_ExpedicionesCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGestion_ExpedicionesCmd = class(TGestion_ExpedicionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGestion_ExpedicionesCmd = class(TGestion_ExpedicionesCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGestion_ExpedicionesCmd = class(TGestion_ExpedicionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGestion_ExpedicionesCmd = class(TGestion_ExpedicionesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGestion_ExpedicionesCmd = class(TReadGestion_ExpedicionesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Gestion_ExpedicionesBrowser,
  FP.Gestion_ExpedicionesModel,
  FP.Gestion_ExpedicionesView,
  FP.Main;

{ TProvinciasCommand }

function TGestion_ExpedicionesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGestion_ExpedicionesModel;
end;

function TGestion_ExpedicionesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGestion_ExpedicionesBrowser;
end;

function TGestion_ExpedicionesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGestion_Expediciones;
end;

{ TCreateGestion_ExpedicionesCmd }

procedure TCreateGestion_ExpedicionesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadGestion_ExpedicionesCmd }

{
procedure TReadGestion_ExpedicionesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadGestion_ExpedicionesCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateGestion_ExpedicionesCmd }

procedure TUpdateGestion_ExpedicionesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteGestion_ExpedicionesCmd }

procedure TDeleteGestion_ExpedicionesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickGestion_ExpedicionesCmd }

procedure TPickGestion_ExpedicionesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGestion_ExpedicionesCmd, TReadGestion_ExpedicionesCmd, TUpdateGestion_ExpedicionesCmd, TDeleteGestion_ExpedicionesCmd, TPickGestion_ExpedicionesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGestion_ExpedicionesCmd, TReadGestion_ExpedicionesCmd, TUpdateGestion_ExpedicionesCmd, TDeleteGestion_ExpedicionesCmd, TPickGestion_ExpedicionesCmd]);

end.
