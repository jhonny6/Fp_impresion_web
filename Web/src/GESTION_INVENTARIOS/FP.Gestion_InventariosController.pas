unit FP.Gestion_InventariosController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGestion_InventariosCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGestion_InventariosCmd = class(TGestion_InventariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGestion_InventariosCmd = class(TGestion_InventariosCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGestion_InventariosCmd = class(TGestion_InventariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGestion_InventariosCmd = class(TGestion_InventariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGestion_InventariosCmd = class(TReadGestion_InventariosCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Gestion_InventariosBrowser,
  FP.Gestion_InventariosModel,
  FP.Gestion_InventariosView,
  FP.Main;

{ TProvinciasCommand }

function TGestion_InventariosCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGestion_InventariosModel;
end;

function TGestion_InventariosCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGestion_InventariosBrowser;
end;

function TGestion_InventariosCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGestion_Inventarios;
end;

{ TCreateProvinciasCmd }

procedure TCreateGestion_InventariosCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateGestion_InventariosCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteGestion_InventariosCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickGestion_InventariosCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGestion_InventariosCmd, TReadGestion_InventariosCmd, TUpdateGestion_InventariosCmd, TDeleteGestion_InventariosCmd, TPickGestion_InventariosCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGestion_InventariosCmd, TReadGestion_InventariosCmd, TUpdateGestion_InventariosCmd, TDeleteGestion_InventariosCmd, TPickGestion_InventariosCmd]);

end.
