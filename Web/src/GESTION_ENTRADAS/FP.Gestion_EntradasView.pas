unit FP.Gestion_EntradasView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityView, Vcl.DBActns,
  System.Actions, Vcl.ActnList, uniMainMenu, Data.DB, uniStatusBar,
  uniGUIClasses, uniScrollBox, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit;

type
  TGestion_Entradas = class(TEntityView)
    UniDBEdit1: TUniDBEdit;
    UniDBEdit2: TUniDBEdit;
    UniDBEdit3: TUniDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Gestion_Entradas: TGestion_Entradas;

implementation

{$R *.dfm}

end.
