unit FP.Gestion_EntradasController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TGestion_EntradasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateGestion_EntradasCmd = class(TGestion_EntradasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadGestion_EntradasCmd = class(TGestion_EntradasCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateGestion_EntradasCmd = class(TGestion_EntradasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteGestion_EntradasCmd = class(TGestion_EntradasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickGestion_EntradasCmd = class(TReadGestion_EntradasCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Gestion_EntradasBrowser,
  FP.Gestion_EntradasModel,
  FP.Gestion_EntradasView,
  FP.Main;

{ TProvinciasCommand }

function TGestion_EntradasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TGestion_EntradasModel;
end;

function TGestion_EntradasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TGestion_EntradasBrowser;
end;

function TGestion_EntradasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TGestion_Entradas;
end;

{ TCreateProvinciasCmd }

procedure TCreateGestion_EntradasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateGestion_EntradasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteGestion_EntradasCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickGestion_EntradasCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateGestion_EntradasCmd, TReadGestion_EntradasCmd, TUpdateGestion_EntradasCmd, TDeleteGestion_EntradasCmd, TPickGestion_EntradasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateGestion_EntradasCmd, TReadGestion_EntradasCmd, TUpdateGestion_EntradasCmd, TDeleteGestion_EntradasCmd, TPickGestion_EntradasCmd]);

end.
