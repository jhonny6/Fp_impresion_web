unit FP.Gestion_EntradasModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TGestion_EntradasModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  Gestion_EntradasModel: TGestion_EntradasModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TGestion_EntradasModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := '@PKG99_GRID_GESTION.gv_fp0_multiubicacion_gest';
end;

function TGestion_EntradasModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TGestion_EntradasModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TGestion_EntradasModel.TableName: string;
begin
  Result := 'fastence.gv_fp0_multiubicacion_gest';
end;

end.
