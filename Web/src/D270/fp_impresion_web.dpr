{$define UNIGUI_VCL} // Comment out this line to turn this project into an ISAPI module


{$ifndef UNIGUI_VCL}
library
{$else}
program
{$endif}
  fp_impresion_web;

uses
  uniGUIISAPI,
  Forms,
  FP.ServerModule in '..\FP.ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  FP.MainModule in '..\FP.MainModule.pas' {UniMainModule: TUniGUIMainModule},
  FP.Main in '..\FP.Main.pas' {MainForm: TUniForm},
  FP.Login in '..\FP.Login.pas' {UniLoginForm1: TUniLoginForm},
  CashBank.FrameBase in '..\Application\CashBank.FrameBase.pas' {frmBase: TUniFrame},
  CashBank.EntityBrowser in '..\Application\CashBank.EntityBrowser.pas' {EntityBrowser: TUniFrame},
  CashBank.EntityModel in '..\Application\CashBank.EntityModel.pas' {EntityModel: TDataModule},
  CashBank.AppController in '..\Application\CashBank.AppController.pas',
  CashBank.UsuariosController in '..\Usuarios\CashBank.UsuariosController.pas',
  CashBank.UsuariosModel in '..\Usuarios\CashBank.UsuariosModel.pas' {UsuariosModel: TDataModule},
  CashBank.EntityView in '..\Application\CashBank.EntityView.pas' {EntityView: TUniForm},
  CashBank.UsuariosView in '..\Usuarios\CashBank.UsuariosView.pas' {Usuarios: TUniForm},
  CashBank.UsuariosBrowser in '..\Usuarios\CashBank.UsuariosBrowser.pas' {UsuariosBrowser: TUniFrame},
  CashBank.MonedasBrowser in '..\Monedas\CashBank.MonedasBrowser.pas' {MonedasBrowser: TUniFrame},
  CashBank.MonedasController in '..\Monedas\CashBank.MonedasController.pas',
  CashBank.MonedasModel in '..\Monedas\CashBank.MonedasModel.pas' {MonedasModel: TDataModule},
  CashBank.MonedasView in '..\Monedas\CashBank.MonedasView.pas' {Monedas: TUniForm},
  CashBank.PaisesBrowser in '..\Paises\CashBank.PaisesBrowser.pas' {PaisesBrowser: TUniFrame},
  CashBank.PaisesController in '..\Paises\CashBank.PaisesController.pas',
  CashBank.PaisesModel in '..\Paises\CashBank.PaisesModel.pas' {PaisesModel: TDataModule},
  CashBank.PaisesView in '..\Paises\CashBank.PaisesView.pas' {Paises: TUniForm},
  CashBank.Codigos_postalesBrowser in '..\Codigos_postales\CashBank.Codigos_postalesBrowser.pas' {Codigos_postalesBrowser: TUniFrame},
  CashBank.Codigos_postalesController in '..\Codigos_postales\CashBank.Codigos_postalesController.pas',
  CashBank.Codigos_postalesModel in '..\Codigos_postales\CashBank.Codigos_postalesModel.pas' {Codigos_postalesModel: TDataModule},
  CashBank.Codigos_postalesView in '..\Codigos_postales\CashBank.Codigos_postalesView.pas' {Codigos_postales: TUniForm},
  CashBank.EmpresasBrowser in '..\Empresas\CashBank.EmpresasBrowser.pas' {EmpresasBrowser: TUniFrame},
  CashBank.EmpresasController in '..\Empresas\CashBank.EmpresasController.pas',
  CashBank.EmpresasModel in '..\Empresas\CashBank.EmpresasModel.pas' {EmpresasModel: TDataModule},
  CashBank.EmpresasView in '..\Empresas\CashBank.EmpresasView.pas' {Empresas: TUniForm},
  CashBank.Usuarios_perfilesBrowser in '..\Usuarios_perfiles\CashBank.Usuarios_perfilesBrowser.pas' {Usuarios_perfilesBrowser: TUniFrame},
  CashBank.Usuarios_perfilesController in '..\Usuarios_perfiles\CashBank.Usuarios_perfilesController.pas',
  CashBank.Usuarios_perfilesModel in '..\Usuarios_perfiles\CashBank.Usuarios_perfilesModel.pas' {Usuarios_perfilesModel: TDataModule},
  CashBank.Usuarios_perfilesView in '..\Usuarios_perfiles\CashBank.Usuarios_perfilesView.pas' {Usuarios_perfiles: TUniForm},
  CashBank.CajonesBrowser in '..\Cajones\CashBank.CajonesBrowser.pas' {CajonesBrowser: TUniFrame},
  CashBank.CajonesController in '..\Cajones\CashBank.CajonesController.pas',
  CashBank.CajonesModel in '..\Cajones\CashBank.CajonesModel.pas' {CajonesModel: TDataModule},
  CashBank.CajonesView in '..\Cajones\CashBank.CajonesView.pas' {Cajones: TUniForm},
  CashBank.Tipos_mensajeBrowser in '..\Tipos_mensaje\CashBank.Tipos_mensajeBrowser.pas' {Tipos_mensajeBrowser: TUniFrame},
  CashBank.Tipos_mensajeController in '..\Tipos_mensaje\CashBank.Tipos_mensajeController.pas',
  CashBank.Tipos_mensajeModel in '..\Tipos_mensaje\CashBank.Tipos_mensajeModel.pas' {Tipos_mensajeModel: TDataModule},
  CashBank.Tipos_mensajeView in '..\Tipos_mensaje\CashBank.Tipos_mensajeView.pas' {Tipos_mensaje: TUniForm},
  CashBank.Notificaciones_appsBrowser in '..\Notificaciones_apps\CashBank.Notificaciones_appsBrowser.pas' {Notificaciones_appsBrowser: TUniFrame},
  CashBank.Notificaciones_appsController in '..\Notificaciones_apps\CashBank.Notificaciones_appsController.pas',
  CashBank.Notificaciones_appsModel in '..\Notificaciones_apps\CashBank.Notificaciones_appsModel.pas' {Notificaciones_appsModel: TDataModule},
  CashBank.Notificaciones_appsView in '..\Notificaciones_apps\CashBank.Notificaciones_appsView.pas' {Notificaciones_apps: TUniForm},
  CashBank.Registro_appsBrowser in '..\Registro_apps\CashBank.Registro_appsBrowser.pas' {Registro_appsBrowser: TUniFrame},
  CashBank.Registro_appsController in '..\Registro_apps\CashBank.Registro_appsController.pas',
  CashBank.Registro_appsModel in '..\Registro_apps\CashBank.Registro_appsModel.pas' {Registro_appsModel: TDataModule},
  CashBank.Registro_appsView in '..\Registro_apps\CashBank.Registro_appsView.pas' {Registro_apps: TUniForm},
  CashBank.Usuarios_logBrowser in '..\Usuarios_log\CashBank.Usuarios_logBrowser.pas' {Usuarios_logBrowser: TUniFrame},
  CashBank.Usuarios_logController in '..\Usuarios_log\CashBank.Usuarios_logController.pas',
  CashBank.Usuarios_logModel in '..\Usuarios_log\CashBank.Usuarios_logModel.pas' {Usuarios_logModel: TDataModule},
  CashBank.Usuarios_logView in '..\Usuarios_log\CashBank.Usuarios_logView.pas' {Usuarios_log: TUniForm},
  CashBank.Cajon_diarioBrowser in '..\Cajon_diario\CashBank.Cajon_diarioBrowser.pas' {Cajon_diarioBrowser: TUniFrame},
  CashBank.Cajon_diarioController in '..\Cajon_diario\CashBank.Cajon_diarioController.pas',
  CashBank.Cajon_diarioModel in '..\Cajon_diario\CashBank.Cajon_diarioModel.pas' {Cajon_diarioModel: TDataModule},
  CashBank.Cajon_diarioView in '..\Cajon_diario\CashBank.Cajon_diarioView.pas' {Cajon_Diario: TUniForm},
  CashBank.NotificacionesBrowser in '..\Notificaciones\CashBank.NotificacionesBrowser.pas' {NotificacionesBrowser: TUniFrame},
  CashBank.NotificacionesController in '..\Notificaciones\CashBank.NotificacionesController.pas',
  CashBank.NotificacionesModel in '..\Notificaciones\CashBank.NotificacionesModel.pas' {NotificacionesModel: TDataModule},
  CashBank.NotificacionesView in '..\Notificaciones\CashBank.NotificacionesView.pas' {Notificaciones: TUniForm},
  FP.MultiUbicacionBrowser in '..\MultiUbicacion\FP.MultiUbicacionBrowser.pas' {MultiUbicacionBrowser: TUniForm},
  FP.MultiUbicacionModel in '..\MultiUbicacion\FP.MultiUbicacionModel.pas' {MultiUbicacionModel: TDataModule},
  FP.MultiUbicacionView in '..\MultiUbicacion\FP.MultiUbicacionView.pas' {MultiUbicacion: TUniForm},
  FP.MultiUbicacionController in '..\MultiUbicacion\FP.MultiUbicacionController.pas',
  uFiltro in '..\Filtro\uFiltro.pas' {FiltroFrm: TUniForm},
  FP.EtiquetasBrowser in '..\ETIQUETAS\FP.EtiquetasBrowser.pas' {EtiquetasBrowser: TUniForm},
  FP.EtiquetasController in '..\ETIQUETAS\FP.EtiquetasController.pas',
  FP.EtiquetasView in '..\ETIQUETAS\FP.EtiquetasView.pas' {Etiquetas: TUniForm},
  FP.Formatos_EnvasadoBrowser in '..\FORMATOS_ENVASADO\FP.Formatos_EnvasadoBrowser.pas' {Formatos_EnvasadoBrowser: TUniForm},
  FP.Formatos_EnvasadoController in '..\FORMATOS_ENVASADO\FP.Formatos_EnvasadoController.pas',
  FP.Formatos_EnvasadoModel in '..\FORMATOS_ENVASADO\FP.Formatos_EnvasadoModel.pas' {Formatos_EnvasadoModel: TDataModule},
  FP.Formatos_EnvasadoView in '..\FORMATOS_ENVASADO\FP.Formatos_EnvasadoView.pas' {Formatos_Envasado: TUniForm},
  FP.Tipo_UbicacionBrowser in '..\TIPO_UBICACION\FP.Tipo_UbicacionBrowser.pas' {Tipo_UbicacionBrowser: TUniForm},
  FP.Tipo_UbicacionController in '..\TIPO_UBICACION\FP.Tipo_UbicacionController.pas',
  FP.Tipo_UbicacionModel in '..\TIPO_UBICACION\FP.Tipo_UbicacionModel.pas' {Tipo_UbicacionModel: TDataModule},
  FP.Tipo_UbicacionView in '..\TIPO_UBICACION\FP.Tipo_UbicacionView.pas' {Tipo_Ubicacion: TUniForm},
  FP.Zonas_UbicacionBrowser in '..\ZONAS_UBICACION\FP.Zonas_UbicacionBrowser.pas' {Zonas_UbicacionBrowser: TUniForm},
  FP.Zonas_UbicacionController in '..\ZONAS_UBICACION\FP.Zonas_UbicacionController.pas',
  FP.Zonas_UbicacionModel in '..\ZONAS_UBICACION\FP.Zonas_UbicacionModel.pas' {Zonas_UbicacionModel: TDataModule},
  FP.Zonas_UbicacionView in '..\ZONAS_UBICACION\FP.Zonas_UbicacionView.pas' {Zonas_Ubicacion: TUniForm},
  FP.Zonas_PreparacionController in '..\ZONAS_PREPARACION\FP.Zonas_PreparacionController.pas',
  FP.Zonas_PreparacionBrowser in '..\ZONAS_PREPARACION\FP.Zonas_PreparacionBrowser.pas' {Zonas_PreparacionBrowser: TUniForm},
  FP.Zonas_PreparacionModel in '..\ZONAS_PREPARACION\FP.Zonas_PreparacionModel.pas' {Zonas_PreparacionModel: TDataModule},
  FP.Zonas_PreparacionView in '..\ZONAS_PREPARACION\FP.Zonas_PreparacionView.pas' {Zonas_Preparacion: TUniForm},
  FP.Indicadores_TiposView in '..\INDICADORES_TIPOS\FP.Indicadores_TiposView.pas' {Indicadores_Tipos: TUniForm},
  FP.Indicadores_TiposController in '..\INDICADORES_TIPOS\FP.Indicadores_TiposController.pas',
  FP.Indicadores_TiposModel in '..\INDICADORES_TIPOS\FP.Indicadores_TiposModel.pas' {Indicadores_TiposModel: TDataModule},
  FP.Indicadores_TiposBrowser in '..\INDICADORES_TIPOS\FP.Indicadores_TiposBrowser.pas' {Indicadores_TiposBrowser: TUniForm},
  FP.Etiquetas_ControlBrowser in '..\ETIQUETAS_CONTROL\FP.Etiquetas_ControlBrowser.pas' {Etiquetas_ControlBrowser: TUniForm},
  FP.Etiquetas_ControlController in '..\ETIQUETAS_CONTROL\FP.Etiquetas_ControlController.pas',
  FP.Etiquetas_ControlModel in '..\ETIQUETAS_CONTROL\FP.Etiquetas_ControlModel.pas' {Etiquetas_ControlModel: TDataModule},
  FP.Etiquetas_ControlView in '..\ETIQUETAS_CONTROL\FP.Etiquetas_ControlView.pas' {Etiquetas_Control: TUniForm},
  FP.Tipos_InventarioBrowser in '..\TIPOS_INVENTARIO\FP.Tipos_InventarioBrowser.pas' {Tipos_InventarioBrowser: TUniForm},
  FP.Tipos_InventarioController in '..\TIPOS_INVENTARIO\FP.Tipos_InventarioController.pas',
  FP.Tipos_InventarioModel in '..\TIPOS_INVENTARIO\FP.Tipos_InventarioModel.pas' {Tipos_InventarioModel: TDataModule},
  FP.Tipos_InventarioView in '..\TIPOS_INVENTARIO\FP.Tipos_InventarioView.pas' {Tipos_Inventario: TUniForm},
  FP.Tipo_PreparacionBrowser in '..\TIPO_PREPARACION\FP.Tipo_PreparacionBrowser.pas' {Tipo_PreparacionBrowser: TUniForm},
  FP.Tipo_PreparacionController in '..\TIPO_PREPARACION\FP.Tipo_PreparacionController.pas',
  FP.Tipo_PreparacionModel in '..\TIPO_PREPARACION\FP.Tipo_PreparacionModel.pas' {Tipo_PreparacionModel: TDataModule},
  FP.Tipo_PreparacionView in '..\TIPO_PREPARACION\FP.Tipo_PreparacionView.pas' {Tipo_Preparacion: TUniForm},
  FP.Tipo_Codigo_BarrasBrowser in '..\TIPO_CODIGO_BARRAS\FP.Tipo_Codigo_BarrasBrowser.pas' {Tipo_Codigo_BarrasBrowser: TUniForm},
  FP.Tipo_Codigo_BarrasController in '..\TIPO_CODIGO_BARRAS\FP.Tipo_Codigo_BarrasController.pas',
  FP.Tipo_Codigo_BarrasModel in '..\TIPO_CODIGO_BARRAS\FP.Tipo_Codigo_BarrasModel.pas' {Tipo_Codigo_BarrasModel: TDataModule},
  FP.Tipo_Codigo_BarrasView in '..\TIPO_CODIGO_BARRAS\FP.Tipo_Codigo_BarrasView.pas' {Tipo_Codigo_Barras: TUniForm},
  FP.OperariosBrowser in '..\Operarios\FP.OperariosBrowser.pas' {OperariosBrowser: TUniForm},
  FP.OperariosController in '..\Operarios\FP.OperariosController.pas',
  FP.OperariosModel in '..\Operarios\FP.OperariosModel.pas' {OperariosModel: TDataModule},
  FP.OperariosView in '..\Operarios\FP.OperariosView.pas' {Operarios: TUniForm},
  FP.ProgramadorBrowser in '..\PROGRAMADOR\FP.ProgramadorBrowser.pas' {ProgramadorBrowser: TUniForm},
  FP.ProgramadorController in '..\PROGRAMADOR\FP.ProgramadorController.pas',
  FP.ProgramadorModel in '..\PROGRAMADOR\FP.ProgramadorModel.pas' {ProgramadorModel: TDataModule},
  FP.AgenciasBrowser in '..\AGENCIAS\FP.AgenciasBrowser.pas' {AgenciasBrowser: TUniForm},
  FP.AgenciasController in '..\AGENCIAS\FP.AgenciasController.pas',
  FP.AgenciasModel in '..\AGENCIAS\FP.AgenciasModel.pas' {AgenciasModel: TDataModule},
  FP.AgenciasView in '..\AGENCIAS\FP.AgenciasView.pas' {Agencias: TUniForm},
  FP.DestinosBrowser in '..\DESTINOS\FP.DestinosBrowser.pas' {DestinosBrowser: TUniForm},
  FP.DestinosController in '..\DESTINOS\FP.DestinosController.pas',
  FP.DestinosModel in '..\DESTINOS\FP.DestinosModel.pas' {DestinosModel: TDataModule},
  FP.DestinosView in '..\DESTINOS\FP.DestinosView.pas' {Destinos: TUniForm},
  FP.ClientesBrowser in '..\CLIENTES\FP.ClientesBrowser.pas' {ClientesBrowser: TUniForm},
  FP.ClientesModel in '..\CLIENTES\FP.ClientesModel.pas' {ClientesModel: TDataModule},
  FP.ClientesController in '..\CLIENTES\FP.ClientesController.pas',
  FP.ClientesView in '..\CLIENTES\FP.ClientesView.pas' {Clientes: TUniForm},
  FP.ProveedoresBrowser in '..\PROVEEDORES\FP.ProveedoresBrowser.pas' {ProveedoresBrowser: TUniForm},
  FP.ProveedoresController in '..\PROVEEDORES\FP.ProveedoresController.pas',
  FP.ProveedoresModel in '..\PROVEEDORES\FP.ProveedoresModel.pas' {ProveedoresModel: TDataModule},
  FP.ProveedoresView in '..\PROVEEDORES\FP.ProveedoresView.pas' {Proveedores: TUniForm},
  FP.ArticulosBrowser in '..\ARTICULOS\FP.ArticulosBrowser.pas' {ArticulosBrowser: TUniForm},
  FP.ArticulosController in '..\ARTICULOS\FP.ArticulosController.pas',
  FP.ArticulosModel in '..\ARTICULOS\FP.ArticulosModel.pas' {ArticulosModel: TDataModule},
  FP.ArticulosView in '..\ARTICULOS\FP.ArticulosView.pas' {Articulos: TUniForm},
  FP.GridHelper in '..\Helpers\FP.GridHelper.pas',
  FP.Organigrama_AutorizaBrowser in '..\ORGANIGRAMA_AUTORIZA\FP.Organigrama_AutorizaBrowser.pas' {Organigrama_AutorizaBrowser: TUniForm},
  FP.Organigrama_AutorizaController in '..\ORGANIGRAMA_AUTORIZA\FP.Organigrama_AutorizaController.pas',
  FP.Organigrama_AutorizaModel in '..\ORGANIGRAMA_AUTORIZA\FP.Organigrama_AutorizaModel.pas' {Organigrama_AutorizaModel: TDataModule},
  FP.Organigrama_AutorizaView in '..\ORGANIGRAMA_AUTORIZA\FP.Organigrama_AutorizaView.pas' {Organigrama_Autoriza: TUniForm},
  FP.IndicadoresBrowser in '..\INDICADORES\FP.IndicadoresBrowser.pas' {IndicadoresBrowser: TUniForm},
  FP.IndicadoresController in '..\INDICADORES\FP.IndicadoresController.pas',
  FP.IndicadoresModel in '..\INDICADORES\FP.IndicadoresModel.pas' {IndicadoresModel: TDataModule},
  FP.IndicadoresView in '..\INDICADORES\FP.IndicadoresView.pas' {Indicadores: TUniForm},
  FP.Menu_InformesView in '..\MENU_INFORMES\FP.Menu_InformesView.pas' {Menu_Informes: TUniForm},
  FP.Menu_InformesBrowser in '..\MENU_INFORMES\FP.Menu_InformesBrowser.pas' {Menu_InformesBrowser: TUniForm},
  FP.Menu_InformesController in '..\MENU_INFORMES\FP.Menu_InformesController.pas',
  FP.Menu_InformesModel in '..\MENU_INFORMES\FP.Menu_InformesModel.pas' {Menu_InformesModel: TDataModule},
  FP.Regulariza_CausasBrowser in '..\REGULARIZA_CAUSAS\FP.Regulariza_CausasBrowser.pas' {Regulariza_CausasBrowser: TUniForm},
  FP.Regulariza_CausasController in '..\REGULARIZA_CAUSAS\FP.Regulariza_CausasController.pas',
  FP.Regulariza_CausasModel in '..\REGULARIZA_CAUSAS\FP.Regulariza_CausasModel.pas' {Regulariza_CausasModel: TDataModule},
  FP.Regulariza_CausasView in '..\REGULARIZA_CAUSAS\FP.Regulariza_CausasView.pas' {Regulariza_Causas: TUniForm},
  FP.Operarios_GruposController in '..\OPERARIOS_GRUPOS\FP.Operarios_GruposController.pas',
  FP.Operarios_GruposModel in '..\OPERARIOS_GRUPOS\FP.Operarios_GruposModel.pas' {Operarios_GruposModel: TDataModule},
  FP.Operarios_GruposView in '..\OPERARIOS_GRUPOS\FP.Operarios_GruposView.pas' {Operarios_Grupos: TUniForm},
  FP.Operarios_GruposBrowser in '..\OPERARIOS_GRUPOS\FP.Operarios_GruposBrowser.pas' {Operarios_GruposBrowser: TUniForm},
  FP.FamiliasBrowser in '..\FAMILIAS\FP.FamiliasBrowser.pas' {FamiliasBrowser: TUniForm},
  FP.FamiliasController in '..\FAMILIAS\FP.FamiliasController.pas',
  FP.FamiliasModel in '..\FAMILIAS\FP.FamiliasModel.pas' {FamiliasModel: TDataModule},
  FP.FamiliasView in '..\FAMILIAS\FP.FamiliasView.pas' {Familias: TUniForm},
  FP.Grid_ColumnasBrowser in '..\GRID_COLUMNAS\FP.Grid_ColumnasBrowser.pas' {Grid_ColumnasBrowser: TUniForm},
  FP.Grid_ColumnasController in '..\GRID_COLUMNAS\FP.Grid_ColumnasController.pas',
  FP.Grid_ColumnasModel in '..\GRID_COLUMNAS\FP.Grid_ColumnasModel.pas' {Grid_ColumnasModel: TDataModule},
  FP.Grid_ColumnasView in '..\GRID_COLUMNAS\FP.Grid_ColumnasView.pas' {Grid_Columnas: TUniForm},
  FP.Operarios_WebBrowser in '..\OPERARIOS_WEB\FP.Operarios_WebBrowser.pas' {Operarios_WebBrowser: TUniForm},
  FP.Operarios_WebController in '..\OPERARIOS_WEB\FP.Operarios_WebController.pas',
  FP.Operarios_WebModel in '..\OPERARIOS_WEB\FP.Operarios_WebModel.pas' {Operarios_WebModel: TDataModule},
  FP.Operarios_WebView in '..\OPERARIOS_WEB\FP.Operarios_WebView.pas' {Operarios_Web: TUniForm},
  FP.AppMenu_WebBrowser in '..\APPMENU_WEB\FP.AppMenu_WebBrowser.pas' {AppMenu_WebBrowser: TUniForm},
  FP.AppMenu_WebController in '..\APPMENU_WEB\FP.AppMenu_WebController.pas',
  FP.AppMenu_WebModel in '..\APPMENU_WEB\FP.AppMenu_WebModel.pas' {AppMenu_WebModel: TDataModule},
  FP.AppMenu_WebView in '..\APPMENU_WEB\FP.AppMenu_WebView.pas' {AppMenu_Web: TUniForm},
  FP.PaisBrowser in '..\PAIS\FP.PaisBrowser.pas' {PaisBrowser: TUniForm},
  FP.PaisController in '..\PAIS\FP.PaisController.pas',
  FP.PaisModel in '..\PAIS\FP.PaisModel.pas' {PaisModel: TDataModule},
  FP.PaisView in '..\PAIS\FP.PaisView.pas' {Pais: TUniForm},
  FP.ProgramadorView in '..\PROGRAMADOR\FP.ProgramadorView.pas' {Programador: TUniForm},
  FP.EmpresaBrowser in '..\EMPRESA\FP.EmpresaBrowser.pas' {EmpresaBrowser: TUniForm},
  FP.EmpresaController in '..\EMPRESA\FP.EmpresaController.pas',
  FP.EmpresaModel in '..\EMPRESA\FP.EmpresaModel.pas' {EmpresaModel: TDataModule},
  FP.EmpresaView in '..\EMPRESA\FP.EmpresaView.pas' {Empresa: TUniForm},
  FP.Mapa_almacenBrowser in '..\MAPA ALMACEN\FP.Mapa_almacenBrowser.pas' {Mapa_almacenBrowser: TUniForm},
  FP.Mapa_almacenController in '..\MAPA ALMACEN\FP.Mapa_almacenController.pas',
  FP.Mapa_almacenModel in '..\MAPA ALMACEN\FP.Mapa_almacenModel.pas' {Mapa_almacenModel: TDataModule},
  FP.Mapa_almacenView in '..\MAPA ALMACEN\FP.Mapa_almacenView.pas' {Mapa_almacen: TUniForm},
  FP.MultiubicacionVistaController in '..\MULTIUBICACIÓN_VISTA\FP.MultiubicacionVistaController.pas',
  FP.MultiubicacionVistaModel in '..\MULTIUBICACIÓN_VISTA\FP.MultiubicacionVistaModel.pas' {MultiubicacionVistaModel: TDataModule},
  FP.MultiubicacionVistaView in '..\MULTIUBICACIÓN_VISTA\FP.MultiubicacionVistaView.pas' {MultiubicacionVista: TUniForm},
  FP.MultiubicacionVistaBrowser in '..\MULTIUBICACIÓN_VISTA\FP.MultiubicacionVistaBrowser.pas' {MultiubicacionVistaBrowser: TUniForm},
  FP.Gestion_EntradasBrowser in '..\GESTION_ENTRADAS\FP.Gestion_EntradasBrowser.pas' {Gestion_EntradasBrowser: TUniForm},
  FP.Gestion_EntradasController in '..\GESTION_ENTRADAS\FP.Gestion_EntradasController.pas',
  FP.Gestion_EntradasView in '..\GESTION_ENTRADAS\FP.Gestion_EntradasView.pas' {Gestion_Entradas: TUniForm},
  FP.Gestion_SalidasBrowser in '..\GESTION_SALIDAS\FP.Gestion_SalidasBrowser.pas' {Gestion_SalidasBrowser: TUniForm},
  FP.Gestion_SalidasController in '..\GESTION_SALIDAS\FP.Gestion_SalidasController.pas',
  FP.Gestion_SalidasView in '..\GESTION_SALIDAS\FP.Gestion_SalidasView.pas' {Gestion_Salidas: TUniForm},
  FP.Gestion_EntradasModel in '..\GESTION_ENTRADAS\FP.Gestion_EntradasModel.pas' {Gestion_EntradasModel: TDataModule},
  FP.EtiquetasModel in '..\ETIQUETAS\FP.EtiquetasModel.pas' {EtiquetasModel: TDataModule},
  FP.Gestion_SalidasModel in '..\GESTION_SALIDAS\FP.Gestion_SalidasModel.pas' {Gestion_SalidasModel: TDataModule},
  FP.Gestion_InventariosBrowser in '..\GESTION_INVENTARIOS\FP.Gestion_InventariosBrowser.pas' {Gestion_InventariosBrowser: TUniForm},
  FP.Gestion_InventariosController in '..\GESTION_INVENTARIOS\FP.Gestion_InventariosController.pas',
  FP.Gestion_InventariosModel in '..\GESTION_INVENTARIOS\FP.Gestion_InventariosModel.pas' {Gestion_InventariosModel: TDataModule},
  FP.Gestion_InventariosView in '..\GESTION_INVENTARIOS\FP.Gestion_InventariosView.pas' {Gestion_Inventarios: TUniForm},
  CashBank.AppControllerMD in '..\ApplicationMD\CashBank.AppControllerMD.pas',
  CashBank.EntityBrowserMD in '..\ApplicationMD\CashBank.EntityBrowserMD.pas' {EntityBrowserMD: TUniForm},
  CashBank.EntityModelMD in '..\ApplicationMD\CashBank.EntityModelMD.pas' {EntityModelMD: TDataModule},
  CashBank.EntityViewMD in '..\ApplicationMD\CashBank.EntityViewMD.pas' {EntityViewMD: TUniForm},
  FP.Gestion_ExpedicionesBrowser in '..\GESTION_EXPEDICIONES\FP.Gestion_ExpedicionesBrowser.pas' {Gestion_ExpedicionesBrowser: TUniForm},
  FP.Gestion_ExpedicionesController in '..\GESTION_EXPEDICIONES\FP.Gestion_ExpedicionesController.pas',
  FP.Gestion_ExpedicionesModel in '..\GESTION_EXPEDICIONES\FP.Gestion_ExpedicionesModel.pas' {Gestion_ExpedicionesModel: TDataModule},
  FP.Gestion_ExpedicionesView in '..\GESTION_EXPEDICIONES\FP.Gestion_ExpedicionesView.pas' {Gestion_Expediciones: TUniForm},
  FP.TrazabilidadBrowser in '..\TRAZABILIDAD\FP.TrazabilidadBrowser.pas' {TrazabilidadBrowser: TUniForm},
  FP.TrazabilidadController in '..\TRAZABILIDAD\FP.TrazabilidadController.pas',
  FP.TrazabilidadModel in '..\TRAZABILIDAD\FP.TrazabilidadModel.pas' {TrazabilidadModel: TDataModule},
  FP.TrazabilidadView in '..\TRAZABILIDAD\FP.TrazabilidadView.pas' {Trazabilidad: TUniForm},
  FP.PreparacionBrowser in '..\PREPARACION\FP.PreparacionBrowser.pas' {PreparacionBrowser: TUniForm},
  FP.PreparacionController in '..\PREPARACION\FP.PreparacionController.pas',
  FP.PreparacionModel in '..\PREPARACION\FP.PreparacionModel.pas' {PreparacionModel: TDataModule},
  FP.PreparacionView in '..\PREPARACION\FP.PreparacionView.pas' {Preparacion: TUniForm},
  FP.Gestion_StockBrowser in '..\GESTION_STOCK\FP.Gestion_StockBrowser.pas' {Gestion_StockBrowser: TUniForm},
  FP.Gestion_StockController in '..\GESTION_STOCK\FP.Gestion_StockController.pas',
  FP.Gestion_StockModel in '..\GESTION_STOCK\FP.Gestion_StockModel.pas' {Gestion_StockModel: TDataModule},
  FP.Gestion_StockView in '..\GESTION_STOCK\FP.Gestion_StockView.pas' {Gestion_Stock: TUniForm},
  FP.RecepcionBrowser in '..\Recepcion\FP.RecepcionBrowser.pas' {RecepcionBrowser: TUniForm},
  FP.RecepcionController in '..\Recepcion\FP.RecepcionController.pas',
  FP.RecepcionModel in '..\Recepcion\FP.RecepcionModel.pas' {RecepcionModel: TDataModule},
  FP.RecepcionView in '..\Recepcion\FP.RecepcionView.pas' {Recepcion: TUniForm},
  FP.StockBrowser in '..\Stock\FP.StockBrowser.pas' {StockBrowser: TUniForm},
  FP.StockController in '..\Stock\FP.StockController.pas',
  FP.StockModel in '..\Stock\FP.StockModel.pas' {StockModel: TDataModule},
  FP.StockView in '..\Stock\FP.StockView.pas' {Stock: TUniForm},
  FP.ProvinciasBrowser in '..\Provincias\FP.ProvinciasBrowser.pas' {ProvinciasBrowser: TUniForm},
  FP.ProvinciasController in '..\Provincias\FP.ProvinciasController.pas',
  FP.ProvinciasModel in '..\Provincias\FP.ProvinciasModel.pas' {ProvinciasModel: TDataModule},
  FP.ProvinciasView in '..\Provincias\FP.ProvinciasView.pas' {Provincias: TUniForm},
  FP.Recepcion2Browser in '..\RECEPCION2\FP.Recepcion2Browser.pas' {Recepcion2Browser: TUniForm},
  FP.Recepcion2Controller in '..\RECEPCION2\FP.Recepcion2Controller.pas',
  FP.Recepcion2Model in '..\RECEPCION2\FP.Recepcion2Model.pas' {Recepcion2Model: TDataModule},
  FP.Recepcion2View in '..\RECEPCION2\FP.Recepcion2View.pas' {Recepcion2: TUniForm},
  FP.ComboHelper in '..\Helpers\FP.ComboHelper.pas',
  FP.ImageHelper in '..\Helpers\FP.ImageHelper.pas',
  FP.Search in '..\Application\FP.Search.pas' {SearchForm: TUniForm},
  FP.UniQueryHelper in '..\Helpers\FP.UniQueryHelper.pas';

{$R *.res}

{$ifndef UNIGUI_VCL}
exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;
{$endif}

begin
{$ifdef UNIGUI_VCL}
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  TUniServerModule.Create(Application);
 { Application.CreateForm(TEtiquetasBrowser, EtiquetasBrowser);
  Application.CreateForm(TEtiquetasModel, EtiquetasModel);
  Application.CreateForm(TEtiquetas, Etiquetas);
  Application.CreateForm(TTipos_InventarioBrowser, Tipos_InventarioBrowser);
  Application.CreateForm(TTipos_InventarioModel, Tipos_InventarioModel);
  Application.CreateForm(TTipos_Inventario, Tipos_Inventario);
  Application.CreateForm(TFormatos_EnvasadoBrowser, Formatos_EnvasadoBrowser);
  Application.CreateForm(TFormatos_EnvasadoModel, Formatos_EnvasadoModel);
  Application.CreateForm(TFormatos_Envasado, Formatos_Envasado);
  Application.CreateForm(TTipo_UbicacionBrowser, Tipo_UbicacionBrowser);
  Application.CreateForm(TTipo_UbicacionModel, Tipo_UbicacionModel);
  Application.CreateForm(TTipo_Ubicacion, Tipo_Ubicacion);
  Application.CreateForm(TZonas_UbicacionBrowser, Zonas_UbicacionBrowser);
  Application.CreateForm(TZonas_UbicacionModel, Zonas_UbicacionModel);
  Application.CreateForm(TZonas_Ubicacion, Zonas_Ubicacion);
  Application.CreateForm(TZonas_PrepBrowser, Zonas_PrepBrowser);
  Application.CreateForm(TZonas_PrepModel, Zonas_PrepModel);
  Application.CreateForm(TZonas_Prep, Zonas_Prep);
  Application.CreateForm(TZonas_PreparacionBrowser, Zonas_PreparacionBrowser);
  Application.CreateForm(TZonas_PreparacionModel, Zonas_PreparacionModel);
  Application.CreateForm(TZonas_Preparacion, Zonas_Preparacion);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TEtiquetas_ControlBrowser, Etiquetas_ControlBrowser);
  Application.CreateForm(TEtiquetas_ControlModel, Etiquetas_ControlModel);
  Application.CreateForm(TEtiquetas_Control, Etiquetas_Control);
  Application.CreateForm(TTipo_InventarioBrowser, Tipo_InventarioBrowser);
  Application.CreateForm(TTipo_InventarioModel, Tipo_InventarioModel);
  Application.CreateForm(TTipo_Inventario, Tipo_Inventario);
  Application.CreateForm(TTipo_InventarioBrowser, Tipo_InventarioBrowser);
  Application.CreateForm(TTipo_InventarioModel, Tipo_InventarioModel);
  Application.CreateForm(TTipo_Inventario, Tipo_Inventario);
  Application.CreateForm(TTipos_InventarioBrowser, Tipos_InventarioBrowser);
  Application.CreateForm(TTipos_InventarioModel, Tipos_InventarioModel);
  Application.CreateForm(TTipos_Inventario, Tipos_Inventario);
  Application.CreateForm(TTipo_PreparacionBrowser, Tipo_PreparacionBrowser);
  Application.CreateForm(TTipo_PreparacionModel, Tipo_PreparacionModel);
  Application.CreateForm(TTipo_Preparacion, Tipo_Preparacion);
  Application.CreateForm(TTipo_Codigo_BarrasBrowser, Tipo_Codigo_BarrasBrowser);
  Application.CreateForm(TTipo_Codigo_BarrasModel, Tipo_Codigo_BarrasModel);
  Application.CreateForm(TTipo_Codigo_Barras, Tipo_Codigo_Barras);
  Application.CreateForm(TOperariosBrowser, OperariosBrowser);
  Application.CreateForm(TOperariosModel, OperariosModel);
  Application.CreateForm(TOperarios, Operarios);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TMenu_InformesModel, Menu_InformesModel);
  Application.CreateForm(TMenu_InformesBrowser, Menu_InformesBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TOperarios_GruposBrowser, Operarios_GruposBrowser);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);.
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProgramador, Programador);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TGestion_EntradasModel, Gestion_EntradasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TGestion_EntradasModel, Gestion_EntradasModel);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TEtiquetasModel, EtiquetasModel);
  Application.CreateForm(TEtiquetasModel, EtiquetasModel);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TEntityModelMD, EntityModelMD);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TPreparacionBrowser, PreparacionBrowser);
  Application.CreateForm(TPreparacionModel, PreparacionModel);
  Application.CreateForm(TPreparacion, Preparacion);
  Application.CreateForm(TRecepcionBrowser, RecepcionBrowser);
  Application.CreateForm(TRecepcionModel, RecepcionModel);
  Application.CreateForm(TRecepcion, Recepcion);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  Application.CreateForm(TRecepcionBrowser, RecepcionBrowser);
  Application.CreateForm(TRecepcionModel, RecepcionModel);
  Application.CreateForm(TRecepcion, Recepcion);
  Application.CreateForm(TStockBrowser, StockBrowser);
  Application.CreateForm(TStockModel, StockModel);
  Application.CreateForm(TStock, Stock);
  Application.CreateForm(TProvinciasBrowser, ProvinciasBrowser);
  Application.CreateForm(TProvinciasModel, ProvinciasModel);
  Application.CreateForm(TProvincias, Provincias);
  }
  Application.Run;
{$endif}
end.
