unit FP.MainModule;

interface

uses
  uniGUIMainModule, SysUtils, Classes, MemDS, Data.DB, DBAccess, Uni, Generics.Collections, System.IniFiles, FP.UniQueryHelper,
  OracleUniProvider, DASQLMonitor, UniSQLMonitor;

type
  TCloseObserver = class
  private
    FOnCierra: TList<TProc>;

  public
    property OnCierra: TList<TProc> read FOnCierra write FOnCierra;

    constructor Create;
    destructor Destroy; override;
  end;

type
  TUniMainModule = class(TUniGUIMainModule)
    SQLConn: TUniConnection;
    QAuxiliar: TUniQuery;
    UniSQLMonitor1: TUniSQLMonitor;
    procedure UniGUIMainModuleDestroy(Sender: TObject);
    procedure UniGUIMainModuleCreate(Sender: TObject);
  private
    FOnCloseObserver: TCloseObserver;
    { Private declarations }
  public
    { Public declarations }
    pk: variant;
    function GetMenu: TUniQuery;
    function GetSubMenu(pIDPadre: Integer): TUniQuery;
    function processLogin(pUser, pPasswd: string;
      var pEsquemas: String): Integer;

    property OnCloseObserver: TCloseObserver read FOnCloseObserver
      write FOnCloseObserver;
  end;

var
  CONFIG_DATABASE_HOST_IP: String = '192.168.0.201';
  CONFIG_DATABASE_NAME: String = 'XE';
  CONFIG_DATABASE_USERNAME: String = 'system';
  CONFIG_DATABASE_PASSWORD: String = '20fast14';
  CONFIG_DATABASE_PORT: String = '5432';
  // PGVENDORHOME='E:\Proyectos\02-CashBank\Web\out\Win32\Debug\bin\pgsql\';
  SCHEMA: String = 'FASTENCE';
  // PGLIBPQ = 'libpq.dll';

function UniMainModule: TUniMainModule;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIApplication, FP.ServerModule;

function UniMainModule: TUniMainModule;
begin
  Result := TUniMainModule(UniApplication.UniMainModule)
end;

procedure TUniMainModule.UniGUIMainModuleCreate(Sender: TObject);
var
  vIniFile: TIniFile;
begin
  SQLConn.Connected := False;
  vIniFile := TIniFile.Create(ExtractFilePath(Paramstr(0)) +
    'FP_impresion_web.ini');
  try
    CONFIG_DATABASE_HOST_IP := vIniFile.ReadString('CONFIGURACION',
      'CONFIG_DATABASE_HOST_IP', CONFIG_DATABASE_HOST_IP);

    CONFIG_DATABASE_NAME := vIniFile.ReadString('CONFIGURACION', 'CRSERVER',
      CONFIG_DATABASE_NAME);

    CONFIG_DATABASE_USERNAME := vIniFile.ReadString('CONFIGURACION', 'USER',
      CONFIG_DATABASE_USERNAME);

    CONFIG_DATABASE_PASSWORD := vIniFile.ReadString('CONFIGURACION', 'PASSWORD',
      CONFIG_DATABASE_PASSWORD);

    CONFIG_DATABASE_PORT := vIniFile.ReadString('CONFIGURACION', 'CONFIG_DATABASE_PORT',
      CONFIG_DATABASE_PORT);

    SQLConn.ProviderName := 'Oracle';
    SQLConn.Server := CONFIG_DATABASE_HOST_IP;

    SQLConn.Database := CONFIG_DATABASE_NAME;
    SQLConn.Username := CONFIG_DATABASE_USERNAME;
    SQLConn.Password := CONFIG_DATABASE_PASSWORD;
    SQLConn.Port := CONFIG_DATABASE_PORT.ToInteger;


//    SQLConn.Params.Clear;
//    SQLConn.Params.Add('DataBase=' + CONFIG_DATABASE_NAME);
//    SQLConn.Params.Add('User_Name=' + CONFIG_DATABASE_USERNAME);
//    SQLConn.Params.Add('Password=' + CONFIG_DATABASE_PASSWORD);
//    SQLConn.Params.Add('DriverID=Ora');
//    SQLConn.Params.Add('CharacterSet=utf8');
//    SQLConn.Params.Add('ApplicationName=FP_IMPRESION_WEB');
//    SQLConn.Params.Add('MonitorBy=Remote');
  finally
    vIniFile.Free;
  end;

  OnCloseObserver := TCloseObserver.Create;
end;

procedure TUniMainModule.UniGUIMainModuleDestroy(Sender: TObject);
var
  vProc: TProc;
begin
  if OnCloseObserver.OnCierra.Count > 0 then
    for vProc in OnCloseObserver.OnCierra do
      vProc;

  OnCloseObserver.Free;
end;

function TUniMainModule.processLogin(pUser, pPasswd: string;
  var pEsquemas: String): Integer;
var
  vQ: TUniQuery;
  bRemember, bLoginOk: Boolean;
  cPassword: string;
begin
  pUser := Trim(lowerCase(pUser));
  pPasswd := Trim(pPasswd);

  vQ := TUniQuery.Create(nil);
  try
    vQ.Connection := SQLConn;
    vQ.OpenExt('select id, esquemas from ' + SCHEMA +
      '.fp0_operarios_web where codigo = :cod and password = :pass',
      [pUser, pPasswd], [TFieldType.ftString, TFieldType.ftString]);
    if vQ.IsEmpty and (1 = 2) then
      Result := -1
    else
    begin
      pEsquemas := 'fastence'; //vQ.FieldByName('esquemas').AsString;
      Result := 1; //vQ.FieldByName('id').AsInteger;
    end;

    // if pUser = pPasswd then
    // Result := 1
    // else
    // Result := -1;
  finally
    vQ.Free;
  end;
end;

function TUniMainModule.GetMenu: TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  Result.Connection := SQLConn;
  Result.OpenExt
    ('select id, id_padre, orden, clase, descripcion, menuicon, tabicon, nombreimagen from '
    + SCHEMA +
    '.appmenu_web where clase NOT LIKE ''MNU_%'' or clase IS NULL order by id_padre, orden');
end;

function TUniMainModule.GetSubMenu(pIDPadre: Integer): TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  Result.Connection := SQLConn;
  Result.OpenExt
    ('select id, id_padre, orden, clase, descripcion, menuicon, tabicon, nombreimagen from '
    + SCHEMA + '.appmenu_web where ID_PADRE = :ID and ID <> :ID', [pIDPadre],
    [TFieldType.ftInteger]);
end;

{ TCloseObserver }

constructor TCloseObserver.Create;
begin
  FOnCierra := TList<TProc>.Create;
end;

destructor TCloseObserver.Destroy;
begin
  OnCierra.Free;
end;

initialization

RegisterMainModuleClass(TUniMainModule);

end.
