unit CashBank.Codigos_postalesController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TCodigos_postalesCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateCodigos_postalesCmd = class(TCodigos_postalesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadCodigos_postalesCmd = class(TCodigos_postalesCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateCodigos_postalesCmd = class(TCodigos_postalesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteCodigos_postalesCmd = class(TCodigos_postalesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickCodigos_postalesCmd = class(TReadCodigos_postalesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Codigos_postalesBrowser,
  CashBank.Codigos_postalesModel,
  CashBank.Codigos_postalesView,
  FP.Main;

{ TCodigos_postalesCommand }

class function TCodigos_postalesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TCodigos_postalesModel;
end;

class function TCodigos_postalesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TCodigos_postalesBrowser;
end;

class function TCodigos_postalesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TCodigos_postales;
end;

{ TCreateCodigos_postalesCmd }

procedure TCreateCodigos_postalesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadCodigos_postalesCmd }

procedure TReadCodigos_postalesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadCodigos_postalesCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateCodigos_postalesCmd }

procedure TUpdateCodigos_postalesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteCodigos_postalesCmd }

procedure TDeleteCodigos_postalesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickCodigos_postalesCmd }

procedure TPickCodigos_postalesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateCodigos_postalesCmd, TReadCodigos_postalesCmd, TUpdateCodigos_postalesCmd, TDeleteCodigos_postalesCmd, TPickCodigos_postalesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateCodigos_postalesCmd, TReadCodigos_postalesCmd, TUpdateCodigos_postalesCmd, TDeleteCodigos_postalesCmd, TPickCodigos_postalesCmd]);

end.
