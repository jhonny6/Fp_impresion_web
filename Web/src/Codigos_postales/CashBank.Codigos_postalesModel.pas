unit CashBank.Codigos_postalesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TCodigos_postalesModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TCodigos_postalesModel }

class function TCodigos_postalesModel.TableName: string;
begin
  Result := 'cashbank.codigos_postales';
end;

end.
