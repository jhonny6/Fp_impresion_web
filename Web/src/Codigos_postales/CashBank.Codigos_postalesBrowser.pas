unit CashBank.Codigos_postalesBrowser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowser, Data.DB,
  uniGUIBaseClasses, uniImageList, Vcl.DBActns, System.Actions, Vcl.ActnList,
  uniMainMenu, uniGUIClasses, uniBasicGrid, uniDBGrid, System.ImageList,
  Vcl.ImgList, uniButton, uniBitBtn, uniPanel, uniSpeedButton, uniEdit,
  uniLabel, uniDBNavigator;

type
  TCodigos_postalesBrowser = class(TEntityBrowser)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
