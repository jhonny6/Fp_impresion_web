inherited Recepcion2Browser: TRecepcion2Browser
  Caption = 'Recepcion2Browser'
  PixelsPerInch = 96
  TextHeight = 15
  inherited pcPpal: TUniPageControl
    ActivePage = tsDetalle
    inherited tsBusqueda: TUniTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 1174
      ExplicitHeight = 582
      inherited DBGBusqueda: TUniDBGrid
        Top = 279
        Height = 300
      end
      inherited pnlFiltroBuscar: TUniPanel
        Height = 270
        ExplicitHeight = 270
        ScrollHeight = 270
        ScrollWidth = 1168
        object UniPanel2: TUniPanel
          Left = 499
          Top = -2
          Width = 285
          Height = 136
          Hint = ''
          TabOrder = 1
          Caption = ''
          object UniLabel1: TUniLabel
            Left = 80
            Top = 3
            Width = 30
            Height = 13
            Hint = ''
            Caption = 'Fecha'
            TabOrder = 1
          end
          object UniLabel8: TUniLabel
            Left = 10
            Top = 39
            Width = 65
            Height = 13
            Hint = ''
            Caption = 'Desde Fecha'
            TabOrder = 2
          end
          object UniLabel9: TUniLabel
            Left = 10
            Top = 66
            Width = 62
            Height = 13
            Hint = ''
            Caption = 'Hasta Fecha'
            TabOrder = 3
          end
          object dtpFI: TUniDateTimePicker
            Left = 104
            Top = 32
            Width = 120
            Hint = ''
            DateTime = 44733.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 4
          end
          object dtpFF: TUniDateTimePicker
            Left = 104
            Top = 60
            Width = 120
            Hint = ''
            DateTime = 44733.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 5
          end
          object cbFecha: TUniCheckBox
            Left = 105
            Top = 112
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'Filtrar Fecha'
            TabOrder = 6
          end
        end
        object UniPanel3: TUniPanel
          Left = 2
          Top = -6
          Width = 256
          Height = 140
          Hint = ''
          TabOrder = 2
          Caption = ''
          object UniLabel4: TUniLabel
            Left = 16
            Top = 8
            Width = 114
            Height = 13
            Hint = ''
            Caption = 'Mercancia Disponible:'
            TabOrder = 1
          end
          object UniCheckBox7: TUniCheckBox
            Left = 32
            Top = 27
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 2
          end
          object UniCheckBox8: TUniCheckBox
            Left = 128
            Top = 30
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 3
          end
          object UniLabel5: TUniLabel
            Left = 16
            Top = 49
            Width = 121
            Height = 13
            Hint = ''
            Caption = 'Mercancia por Encargo:'
            TabOrder = 4
          end
          object UniCheckBox9: TUniCheckBox
            Left = 33
            Top = 68
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 5
          end
          object UniCheckBox10: TUniCheckBox
            Left = 127
            Top = 68
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 6
          end
          object UniLabel6: TUniLabel
            Left = 16
            Top = 89
            Width = 127
            Height = 13
            Hint = ''
            Caption = 'Expediciones Preparadas'
            TabOrder = 7
          end
          object UniCheckBox11: TUniCheckBox
            Left = 33
            Top = 108
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'S'#237
            TabOrder = 8
          end
          object UniCheckBox12: TUniCheckBox
            Left = 128
            Top = 108
            Width = 97
            Height = 17
            Hint = ''
            Caption = 'No'
            TabOrder = 9
          end
        end
        object UniPanel4: TUniPanel
          Left = 2
          Top = 134
          Width = 256
          Height = 136
          Hint = ''
          TabOrder = 3
          Caption = ''
          object UniLabel2: TUniLabel
            Left = 80
            Top = 3
            Width = 84
            Height = 13
            Hint = ''
            Caption = 'N'#250'mero Albar'#225'n'
            TabOrder = 1
          end
          object UniLabel3: TUniLabel
            Left = 10
            Top = 39
            Width = 41
            Height = 13
            Hint = ''
            Caption = 'N'#250'mero'
            TabOrder = 2
          end
          object edNumAlbaran: TUniEdit
            Left = 140
            Top = 35
            Width = 85
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 3
          end
        end
        object UniPanel5: TUniPanel
          Left = 780
          Top = 0
          Width = 257
          Height = 136
          Hint = ''
          TabOrder = 4
          Caption = ''
          object UniLabel7: TUniLabel
            Left = 80
            Top = 3
            Width = 64
            Height = 13
            Hint = ''
            Caption = 'Recepciones'
            TabOrder = 1
          end
          object UniLabel14: TUniLabel
            Left = 10
            Top = 39
            Width = 76
            Height = 13
            Hint = ''
            Caption = 'Desde N'#250'mero'
            TabOrder = 2
          end
          object UniLabel15: TUniLabel
            Left = 10
            Top = 66
            Width = 73
            Height = 13
            Hint = ''
            Caption = 'Hasta N'#250'mero'
            TabOrder = 3
          end
          object Edesdenumero: TUniEdit
            Left = 140
            Top = 35
            Width = 85
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 4
          end
          object Ehastanumero: TUniEdit
            Left = 140
            Top = 62
            Width = 85
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 5
          end
        end
        object UniPanel6: TUniPanel
          Left = 258
          Top = -3
          Width = 245
          Height = 137
          Hint = ''
          TabOrder = 5
          Caption = ''
          object UniLabel16: TUniLabel
            Left = 80
            Top = 3
            Width = 80
            Height = 13
            Hint = ''
            Caption = 'N'#250'mero Pedido'
            TabOrder = 1
          end
          object UniLabel17: TUniLabel
            Left = 10
            Top = 39
            Width = 41
            Height = 13
            Hint = ''
            Caption = 'N'#250'mero'
            TabOrder = 2
          end
          object edNumPedido: TUniEdit
            Left = 140
            Top = 35
            Width = 85
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 3
          end
        end
        object UniPanel7: TUniPanel
          Left = 258
          Top = 134
          Width = 245
          Height = 136
          Hint = ''
          TabOrder = 6
          Caption = ''
          object UniLabel18: TUniLabel
            Left = 80
            Top = 3
            Width = 96
            Height = 13
            Hint = ''
            Caption = 'Nombre Proveedor'
            TabOrder = 1
          end
          object UniLabel19: TUniLabel
            Left = 10
            Top = 39
            Width = 41
            Height = 13
            Hint = ''
            Caption = 'Nombre'
            TabOrder = 2
          end
          object edNombreProve: TUniEdit
            Left = 140
            Top = 35
            Width = 85
            Height = 21
            Hint = ''
            Text = ''
            TabOrder = 3
          end
        end
      end
    end
    inherited tsDetalle: TUniTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 1174
      ExplicitHeight = 582
      inherited pnlCabecera: TUniPanel
        object DBETipoPedido: TUniDBEdit
          Left = 76
          Top = 3
          Width = 55
          Height = 22
          Hint = ''
          DataField = 'TIPO_PEDIDO'
          DataSource = DSEnc
          TabOrder = 1
        end
        object UniLabel10: TUniLabel
          Left = 3
          Top = 7
          Width = 41
          Height = 13
          Hint = ''
          Caption = 'N'#250'mero'
          TabOrder = 2
        end
        object DBENumero: TUniDBEdit
          Left = 137
          Top = 3
          Width = 121
          Height = 22
          Hint = ''
          DataField = 'ID_PEDIDOC'
          DataSource = DSEnc
          TabOrder = 3
        end
        object DBEAno: TUniDBEdit
          Left = 264
          Top = 3
          Width = 121
          Height = 22
          Hint = ''
          DataField = 'ANO'
          DataSource = DSEnc
          TabOrder = 4
        end
        object UniLabel11: TUniLabel
          Left = 3
          Top = 34
          Width = 30
          Height = 13
          Hint = ''
          Caption = 'Fecha'
          TabOrder = 5
        end
        object UniLabel12: TUniLabel
          Left = 3
          Top = 62
          Width = 42
          Height = 13
          Hint = ''
          Caption = 'F. Cierre'
          TabOrder = 6
        end
        object UniLabel13: TUniLabel
          Left = 3
          Top = 88
          Width = 43
          Height = 13
          Hint = ''
          Caption = 'F. Salida'
          TabOrder = 7
        end
        object UniDBDateTimePicker1: TUniDBDateTimePicker
          Left = 76
          Top = 28
          Width = 120
          Hint = ''
          DataField = 'FECHA'
          DataSource = DSEnc
          DateTime = 44692.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 8
        end
        object UniDBDateTimePicker2: TUniDBDateTimePicker
          Left = 76
          Top = 54
          Width = 120
          Hint = ''
          DataField = 'FECHA_CIERRE'
          DataSource = DSEnc
          DateTime = 44692.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 9
        end
        object UniDBDateTimePicker3: TUniDBDateTimePicker
          Left = 76
          Top = 81
          Width = 120
          Hint = ''
          DataField = 'FECHA_LLEGADA'
          DataSource = DSEnc
          DateTime = 44692.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 10
        end
      end
      inherited pcSubDetalle: TUniPageControl
        Height = 442
        inherited tsSubDetalle: TUniTabSheet
          ExplicitLeft = 4
          ExplicitTop = 24
          ExplicitWidth = 1160
          ExplicitHeight = 414
        end
      end
    end
  end
end
