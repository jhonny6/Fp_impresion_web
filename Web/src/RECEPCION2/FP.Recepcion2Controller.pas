unit FP.Recepcion2Controller;

interface

uses CashBank.AppControllerMD,
  CashBank.EntityBrowserMD,
  CashBank.EntityViewMD,
  CashBank.EntityModelMD, uniGUIClasses;

type

  TRecepcion2Command = class(TAppCommandMD)
  public
    function EntityModelClass: TEntityModelClassMD; override;
    function EntityBrowserClass: TEntityBrowserClassMD; override;
    function EntityViewClass: TEntityViewClassMD; override;
  end;

  TCreateRecepcion2Cmd = class(TRecepcion2Command)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TReadRecepcion2Cmd = class(TRecepcion2Command)
  protected
    // View: TEntityBrowser;
    // Model: TEntityModel;
  public
    // procedure Proccess(Args: TCommandArgs); overload;override;
    // procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateRecepcion2Cmd = class(TRecepcion2Command)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TDeleteRecepcion2Cmd = class(TRecepcion2Command)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

  TPickRecepcion2Cmd = class(TReadRecepcion2Cmd)
  public
    procedure Proccess(Args: TCommandArgsMD); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.Recepcion2Browser,
  FP.Recepcion2Model,
  FP.Recepcion2View,
  FP.Main;

{ TRecepcionCommand }

function TRecepcion2Command.EntityModelClass: TEntityModelClassMD;
begin
  Result := TRecepcion2Model;
end;

function TRecepcion2Command.EntityBrowserClass: TEntityBrowserClassMD;
begin
  Result := TRecepcion2Browser;
end;

function TRecepcion2Command.EntityViewClass: TEntityViewClassMD;
begin
  Result := TRecepcion2;
end;

{ TCreateRecepcionCmd }

procedure TCreateRecepcion2Cmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadRecepcionCmd }

{
  procedure TReadRecepcionCmd.Proccess(Args: TCommandArgs);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
}

(*
  procedure TReadRecepcionCmd.Proccess(Args: TUniControl);
  begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
  end;
*)
{ TUpdateRecepcionCmd }

procedure TUpdateRecepcion2Cmd.Proccess(Args: TCommandArgsMD);
var
  View: TEntityViewMD;
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteRecepcion2Cmd }

procedure TDeleteRecepcion2Cmd.Proccess(Args: TCommandArgsMD);
var
  Model: TEntityModelMD;
  PK: variant;
begin
  PK := Args[0];
  // if not ConfirmDlg('Desea borrar este registro?') then
  // Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickRecepcion2Cmd }

procedure TPickRecepcion2Cmd.Proccess(Args: TCommandArgsMD);
begin
  inherited;
  // View.PickMode := True;
end;

initialization

TAppCommandMD.RegisterCmd([TCreateRecepcion2Cmd, TReadRecepcion2Cmd, TUpdateRecepcion2Cmd,
  TDeleteRecepcion2Cmd, TPickRecepcion2Cmd]);

finalization

TAppCommandMD.UnegisterCmd([TCreateRecepcion2Cmd, TReadRecepcion2Cmd, TUpdateRecepcion2Cmd,
  TDeleteRecepcion2Cmd, TPickRecepcion2Cmd]);

end.
