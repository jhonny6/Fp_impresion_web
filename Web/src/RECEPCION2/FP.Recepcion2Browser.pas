unit FP.Recepcion2Browser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CashBank.EntityBrowserMD, Data.DB,
  Vcl.DBActns, System.Actions, Vcl.ActnList, uniMainMenu, uniBasicGrid,
  uniDBGrid, uniPageControl, uniGUIClasses, uniEdit, uniLabel, uniDBNavigator,
  uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses, uniPanel,
  uniCheckBox, DBAccess, Uni, uniDBEdit, uniDateTimePicker,
  uniDBDateTimePicker, uniSplitter, uniGUIAbstractClasses, uniGridExporters;

type
  TRecepcion2Browser = class(TEntityBrowserMD)
    UniPanel2: TUniPanel;
    UniLabel1: TUniLabel;
    UniLabel8: TUniLabel;
    UniLabel9: TUniLabel;
    UniPanel3: TUniPanel;
    UniLabel4: TUniLabel;
    UniCheckBox7: TUniCheckBox;
    UniCheckBox8: TUniCheckBox;
    UniLabel5: TUniLabel;
    UniCheckBox9: TUniCheckBox;
    UniCheckBox10: TUniCheckBox;
    UniLabel6: TUniLabel;
    UniCheckBox11: TUniCheckBox;
    UniCheckBox12: TUniCheckBox;
    DBETipoPedido: TUniDBEdit;
    UniLabel10: TUniLabel;
    DBENumero: TUniDBEdit;
    DBEAno: TUniDBEdit;
    UniLabel11: TUniLabel;
    UniLabel12: TUniLabel;
    UniLabel13: TUniLabel;
    UniDBDateTimePicker1: TUniDBDateTimePicker;
    UniDBDateTimePicker2: TUniDBDateTimePicker;
    UniDBDateTimePicker3: TUniDBDateTimePicker;
    UniPanel4: TUniPanel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    edNumAlbaran: TUniEdit;
    UniPanel5: TUniPanel;
    UniLabel7: TUniLabel;
    UniLabel14: TUniLabel;
    UniLabel15: TUniLabel;
    Edesdenumero: TUniEdit;
    Ehastanumero: TUniEdit;
    UniPanel6: TUniPanel;
    UniLabel16: TUniLabel;
    UniLabel17: TUniLabel;
    edNumPedido: TUniEdit;
    dtpFI: TUniDateTimePicker;
    dtpFF: TUniDateTimePicker;
    cbFecha: TUniCheckBox;
    UniPanel7: TUniPanel;
    UniLabel18: TUniLabel;
    UniLabel19: TUniLabel;
    edNombreProve: TUniEdit;
    procedure UniSpeedButton2Click(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Recepcion2Browser: TRecepcion2Browser;

implementation

{$R *.dfm}

procedure TRecepcion2Browser.DataSourceStateChange(Sender: TObject);
//var
//  vColAc: TUniBaseDBGridColumn;
begin
  inherited;
//  // *** Codigo para las acciones
//      vColAc := DBGBusqueda.ColumnFromCaption('Acciones');
//
//      if not Assigned(vColAc) then
//      begin
//        vColAc := DBGBusqueda.Columns.Add;
//        vColAc.ActionColumn.Enabled := True;
//        vColAc.ActionColumn.Buttons.Add;
//
//        vColAc.Title.Caption := 'Acciones';
//        vColAc.ActionColumn.Buttons[0].ButtonId := 0;
//        vColAc.ActionColumn.Buttons[0].Hint := 'Lupa Grid';
//        vColAc.ActionColumn.Buttons[0].IconCls := 'action';
//        vColAc.Visible := True;
//        vColAc.Width := 75;
//      end;
end;

procedure TRecepcion2Browser.UniSpeedButton2Click(Sender: TObject);
var
  vCondicion: String;
begin
  // inherited;
  vCondicion := EmptyStr;
  // if DesdePasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo>=' + quotedstr(DesdePasillo.text);
  //
  // if HastaPasillo.text <> '' then
  // vCondicion := vCondicion + ' and pasillo<=' + quotedstr(HastaPasillo.text);

  if Edesdenumero.text <> '' then
  begin
    vCondicion := vCondicion + ' and numero>=' + Edesdenumero.text;
  end;

  if Ehastanumero.text <> '' then
  begin
    vCondicion := vCondicion + ' and numero<=' + Ehastanumero.text;
  end;

  if edNumPedido.Text <> '' then
  begin
    vCondicion := vCondicion + ' and numero = ' + edNumPedido.Text;
  end;

  if cbFecha.Checked then
  begin
    vCondicion := vCondicion + ' and fecha >=' + datetostr(dtpFI.DateTime).QuotedString;
  end;

  if cbFecha.Checked then
  begin
    vCondicion := vCondicion + ' and fecha <=' + datetostr(dtpFF.DateTime).QuotedString;
  end;

  if edNumAlbaran.Text <> '' then
  begin
    vCondicion := vCondicion + ' and albaran = ' + string(edNumAlbaran.text).QuotedString;
  end;

  if edNombreProve.Text <> '' then
  begin
    vCondicion := vCondicion + ' and proveedor = ' + string.UpperCase(edNombreProve.text).QuotedString;
  end;

  FiltrarYEjecutar(vCondicion);
end;

end.
