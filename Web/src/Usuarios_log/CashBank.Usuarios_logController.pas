unit CashBank.Usuarios_logController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TUsuarios_logCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateUsuarios_logCmd = class(TUsuarios_logCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadUsuarios_logCmd = class(TUsuarios_logCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateUsuarios_logCmd = class(TUsuarios_logCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteUsuarios_logCmd = class(TUsuarios_logCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickUsuarios_logCmd = class(TReadUsuarios_logCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Usuarios_logBrowser,
  CashBank.Usuarios_logModel,
  CashBank.Usuarios_logView,
  FP.Main;

{ TUsuarios_logCommand }

class function TUsuarios_logCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TUsuarios_logModel;
end;

class function TUsuarios_logCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TUsuarios_logBrowser;
end;

class function TUsuarios_logCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TUsuarios_log;
end;

{ TCreateUsuarios_logCmd }

procedure TCreateUsuarios_logCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadUsuarios_logCmd }

procedure TReadUsuarios_logCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadUsuarios_logCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateUsuarios_logCmd }

procedure TUpdateUsuarios_logCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteUsuarios_logCmd }

procedure TDeleteUsuarios_logCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickUsuarios_logCmd }

procedure TPickUsuarios_logCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateUsuarios_logCmd, TReadUsuarios_logCmd, TUpdateUsuarios_logCmd, TDeleteUsuarios_logCmd, TPickUsuarios_logCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateUsuarios_logCmd, TReadUsuarios_logCmd, TUpdateUsuarios_logCmd, TDeleteUsuarios_logCmd, TPickUsuarios_logCmd]);

end.
