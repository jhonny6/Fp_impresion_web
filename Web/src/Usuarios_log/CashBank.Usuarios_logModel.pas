unit CashBank.Usuarios_logModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel, DBAccess, Uni, Data.DB,
  MemDS;

type
  TUsuarios_logModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TUsuarios_logModel }

class function TUsuarios_logModel.TableName: string;
begin
  Result := 'cashbank.usuarios_log';
end;

end.
