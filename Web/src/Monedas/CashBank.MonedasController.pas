unit CashBank.MonedasController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel,
  uniGUIClasses;

type

  TMonedasCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateMonedasCmd = class(TMonedasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadMonedasCmd = class(TMonedasCommand)
  end;

  TUpdateMonedasCmd = class(TMonedasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteMonedasCmd = class(TMonedasCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  CashBank.MonedasBrowser,
  CashBank.MonedasModel,
  CashBank.MonedasView,
  FP.Main, System.Classes, Dialogs, uniGUIDialogs, Controls;

{ TMonedasCommand }

function TMonedasCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TMonedasModel;
end;

function TMonedasCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TMonedasBrowser;
end;

function TMonedasCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TMonedas;
end;

{ TCreateMonedasCmd }

procedure TCreateMonedasCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadMonedasCmd }


{ TUpdateMonedasCmd }

procedure TUpdateMonedasCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteMonedasCmd }

procedure TDeleteMonedasCmd.Proccess(Args: TCommandArgs);
var
  Res : Integer;
  PK: VARIANT;
begin
  pk:= Args[0];
  Res :=  MessageDlg('�Desea borrar este registro?', mtConfirmation, mbYesNo);
  if res=mrYes then
  begin
    Model := TEntityModelClass.Create(nil);
    try
      Model.FindByPK(pk).Delete;
    finally
      Model.Free;
    end;
  end;
end;



initialization

TAppCommand.RegisterCmd([TCreateMonedasCmd, TReadMonedasCmd, TUpdateMonedasCmd, TDeleteMonedasCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateMonedasCmd, TReadMonedasCmd, TUpdateMonedasCmd, TDeleteMonedasCmd]);

end.
