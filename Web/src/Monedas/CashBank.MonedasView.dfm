inherited Monedas: TMonedas
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Monedas'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 184
    ScrollWidth = 356
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'descripcion'
      DataSource = DataSource
      TabOrder = 0
      LayoutConfig.Margin = '20'
      FieldLabel = 'Descripci'#243'n'
    end
    object UniDBEdit2: TUniDBEdit
      Left = 56
      Top = 46
      Width = 300
      Height = 22
      Hint = ''
      Margins.Left = 20
      DataField = 'nombre_corto'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre corto'
    end
    object UniDBEdit3: TUniDBEdit
      Left = 56
      Top = 86
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'nombre_unidad'
      DataSource = DataSource
      TabOrder = 2
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre unidad'
    end
    object UniDBEdit4: TUniDBEdit
      Left = 56
      Top = 126
      Width = 300
      Height = 22
      Hint = ''
      DataField = 'nombre_centimos'
      DataSource = DataSource
      TabOrder = 4
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre c'#233'ntimos'
    end
    object UniDBEdit5: TUniDBEdit
      Left = 56
      Top = 162
      Width = 161
      Height = 22
      Hint = ''
      DataField = 'simbolo'
      DataSource = DataSource
      TabOrder = 3
      LayoutConfig.Margin = '20'
      FieldLabel = 'S'#237'mbolo'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
end
