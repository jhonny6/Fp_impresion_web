unit FP.Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniTreeView, uniTreeMenu,
  uniPanel, uniEdit, Vcl.Imaging.pngimage, uniImage, uniLabel, uniPageControl,
  uniGUIBaseClasses, uniScrollBox, DBAccess, Uni, Generics.Collections,
  uniMultiItem, uniButton;

type
  TItemMenu = class
  private
    FID: Integer;
    FID_PADRE: Integer;
    FOrden: Integer;
    FClase: String;
    FDescripcion: String;
    FMenuIcon: String;
    FTabIcon: String;
  public
    property ID: Integer read FID write FID;
    property ID_PADRE: Integer read FID_PADRE write FID_PADRE;
    property Orden: Integer read FOrden write FOrden;
    property Clase: String read FClase write FClase;
    property Descripcion: String read FDescripcion write FDescripcion;
    property MenuIcon: String read FMenuIcon write FMenuIcon;
    property TabIcon: String read FTabIcon write FTabIcon;

    constructor Create(pID, pID_PADRE, pOrden: Integer;
      pClase, pDescripcion, pMenuIcon, pTabIcon: String);

  end;

type
  TMainForm = class(TUniForm)
    sboxMain: TUniScrollBox;
    paBackGround: TUniContainerPanel;
    pgGeneral: TUniPageControl;
    tabDashBoard: TUniTabSheet;
    paGeral: TUniContainerPanel;
    sboxGridBlock: TUniContainerPanel;
    rcBlock150: TUniContainerPanel;
    paLayoutTopo: TUniContainerPanel;
    btnMenu: TUniLabel;
    paLayoutMainMenu: TUniContainerPanel;
    paLayoutMenuCfgVersion: TUniContainerPanel;
    paLayoutLogo: TUniContainerPanel;
    paLayoutBottom: TUniContainerPanel;
    labAppVersion: TUniLabel;
    UniTreeMainMenu: TUniTreeMenu;
    UniContainerPanel1: TUniContainerPanel;
    labbtnExit: TUniLabel;
    labCompanyName: TUniLabel;
    procedure UniFormScreenResize(Sender: TObject; AWidth, AHeight: Integer);
    procedure btnMenuClick(Sender: TObject);
    procedure UniTreeMainMenuClick(Sender: TObject);
    procedure labbtnExitClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure imgMenuClick(Sender: TObject);
  private
    FDicImgItem: TObjectDictionary<TUniImage, TItemMenu>;
    FDicMenuItem: TObjectDictionary<TUniTreeNode, TItemMenu>;
    procedure LimpiarSubMenu;
    procedure DibujarSubMenu(pID: Integer);
    procedure crearPantalla(nodeMenu: TUniControl;
pantalla, icon, descripcion: string);
    { Private declarations }
  public

  end;

const
  CNS_PLANTILLA_MENU =
    '<i class="rc-tree-align-icon fa %s"></i><span> %s</span>';

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, uniGUIApplication, FP.MainModule, CashBank.AppController,
  CashBank.AppControllerMD;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.btnMenuClick(Sender: TObject);
begin
  if paLayoutMainMenu.Width > 0 then
    paLayoutMainMenu.Width := 0
  else
    paLayoutMainMenu.Width := 305;

  paLayoutMainMenu.tag := 1;
  sboxMain.DoAlignControls;
end;

procedure TMainForm.UniFormClose(Sender: TObject; var Action: TCloseAction);
var
  vI: Integer;
begin
  if Assigned(FDicMenuItem) then
    FreeAndNil(FDicMenuItem);

  if Assigned(FDicImgItem) then
    FreeAndNil(FDicImgItem);
end;

procedure TMainForm.UniFormCreate(Sender: TObject);
var
  vQ: TUniQuery;
  vCurrenIDParentNode: Integer;
  vCurrentParentNode, vCurrentChildNode: TUniTreeNode;
begin
  UniMainModule.OnCloseObserver.OnCierra.Add(
    procedure
    var
      vAction: TCloseAction;
    begin
      UniFormClose(Self, vAction);
    end);

  FDicMenuItem := TObjectDictionary<TUniTreeNode, TItemMenu>.Create
    ([doOwnsValues]);

  // FDicImgItem := TObjectDictionary<TUniImage, TItemMenu>.Create([doOwnsValues]);

  vCurrenIDParentNode := -1;
  vQ := UniMainModule.GetMenu;
  try
    vQ.DisableControls;
    vQ.First;
    while not vQ.Eof do
    begin
      if ((vCurrenIDParentNode <> vQ.FieldByName('ID').AsInteger) and
        (vQ.FieldByName('ID_PADRE').AsInteger = vQ.FieldByName('ID').AsInteger))
      then
      begin
        vCurrenIDParentNode := vQ.FieldByName('ID').AsInteger;
        vCurrentParentNode := nil;
      end;

      vCurrentChildNode := UniTreeMainMenu.Items.Add(vCurrentParentNode,
        Format(CNS_PLANTILLA_MENU, [vQ.FieldByName('MenuIcon').AsString,
        vQ.FieldByName('Descripcion').AsString]));

      FDicMenuItem.Add(vCurrentChildNode,
        TItemMenu.Create(vQ.FieldByName('ID').AsInteger,
        vQ.FieldByName('ID_PADRE').AsInteger, vQ.FieldByName('Orden').AsInteger,
        vQ.FieldByName('Clase').AsString, vQ.FieldByName('Descripcion')
        .AsString, vQ.FieldByName('MenuIcon').AsString,
        vQ.FieldByName('TabIcon').AsString));

      if ((vQ.FieldByName('Clase').AsString.StartsWith('MNU')) and
        (not vQ.FieldByName('Clase').AsString.StartsWith('MNU_')))
      then
        UniTreeMainMenu.Items.Add(vCurrentChildNode, '');

      if vCurrentParentNode = nil then
        vCurrentParentNode := vCurrentChildNode;

      vQ.Next;
    end;
  finally
    vQ.Free;
  end;
end;

procedure TMainForm.UniFormScreenResize(Sender: TObject;
AWidth, AHeight: Integer);
begin

  Self.Height := AHeight;
  Self.Width := AWidth;
  Self.Left := (AWidth div 2) - (Self.Width div 2);
  Self.Top := (AHeight div 2) - (Self.Height div 2);

end;

procedure TMainForm.UniTreeMainMenuClick(Sender: TObject);
var
  nodeMenu: TUniTreeNode;
begin
  nodeMenu := UniTreeMainMenu.Selected;

  if nodeMenu.Count = 0 then
    crearPantalla(TUniControl(nodeMenu), FDicMenuItem.Items[nodeMenu].Clase,
      FDicMenuItem.Items[nodeMenu].TabIcon, FDicMenuItem.Items[nodeMenu].Descripcion)
  else
  begin
    if FDicMenuItem.Items[nodeMenu].Clase.StartsWith('MNU') then
    begin
      DibujarSubMenu(FDicMenuItem.Items[nodeMenu].ID);
    end;
  end;

  // case nodeMenu.ID of
  // 1:
  // crearPantalla(nodeMenu, 'MultiUbicacion', 'user');
  // 2:
  // crearPantalla(nodeMenu, 'Monedas', 'coins');
  // 3:
  // crearPantalla(nodeMenu, 'Paises', 'globe');
  // 4:
  // crearPantalla(nodeMenu, 'Codigos_postales', 'home');
  // 5:
  // crearPantalla(nodeMenu, 'Empresas', 'building');
  // 6:
  // crearPantalla(nodeMenu, 'Usuarios_perfiles', 'address-card');
  // 7:
  // crearPantalla(nodeMenu, 'Cajones', 'box');
  // 8:
  // crearPantalla(nodeMenu, 'Tipos_mensaje', 'comment');
  // 12:
  // crearPantalla(nodeMenu, 'Provincias', 'user');
  // 13:
  // crearPantalla(nodeMenu, 'Notificaciones_apps', 'envelope');
  // 14:
  // crearPantalla(nodeMenu, 'Registro_apps', 'registered');
  // 15:
  // crearPantalla(nodeMenu, 'Usuarios_log', 'book-open');
  // 16:
  // crearPantalla(nodeMenu, 'Cajon_diario', 'book');
  // 17:
  // crearPantalla(nodeMenu, 'Notificaciones', 'envelope-open-text');
  // end;
end;

procedure TMainForm.crearPantalla(nodeMenu: TUniControl;
pantalla, icon, descripcion: string);
var
  Ts: TUniTabSheet;
begin
  Ts := TUniTabSheet.Create(Self);
  Ts.PageControl := pgGeneral;
  Ts.Closable := True;
  Ts.tag := NativeInt(nodeMenu);
  pgGeneral.ActivePage := Ts;
  Ts.Align := alClient;

  Ts.Caption := '<i class="fa fa-' + icon + ' fa-1x"></i>  ' + descripcion;

  try
    TAppCommand['Read' + pantalla].Execute(Ts);
  except
    on E: exception do
      if (E.ClassType = EListError) then
        TAppCommandMD['Read' + pantalla].Execute(Ts);
  end;
end;

procedure TMainForm.DibujarSubMenu(pID: Integer);
var
  vLeft, vTop: Integer;
begin
  pgGeneral.BeginUpdate;
  LimpiarSubMenu;

  vLeft := 0;
  vTop := 0;
  var
    vQ: TUniQuery := UniMainModule.GetSubMenu(pID);
  try
    vQ.DisableControls;
    vQ.First;
    while not vQ.Eof do
    begin
      if vQ.FieldByName('nombreimagen').AsString.Trim <> EmptyStr then
      begin
        var
          vRuta: String := IncludeTrailingPathDelimiter
            (ExtractFilePath(ParamStr(0))) + 'files\' +
            vQ.FieldByName('nombreimagen').AsString;

        if FileExists(vRuta) then
        begin
          var
            vimgMenu: TUniImage := TUniImage.Create(Self);

            // vimgMenu.Align   := alClient;
          vimgMenu.Proportional := True;
          vimgMenu.Stretch := True;
          vimgMenu.Picture.LoadFromFile(vRuta);
          vimgMenu.Height := 128;
          vimgMenu.Width := 128;

          vimgMenu.Left := vLeft;
          vimgMenu.Top := vTop;

          vLeft := vLeft + vimgMenu.Width + 50;

          if ((vLeft + vimgMenu.Width + 50) >= sboxGridBlock.Width) then
          begin
            vLeft := 0;
            vTop := vTop + vimgMenu.Height + 20;
          end;

          vimgMenu.OnClick := imgMenuClick;

          vimgMenu.Parent := sboxGridBlock;

          var
            vItemMenu: TItemMenu :=
              TItemMenu.Create(vQ.FieldByName('ID').AsInteger,
              vQ.FieldByName('ID_PADRE').AsInteger, vQ.FieldByName('Orden')
              .AsInteger, vQ.FieldByName('Clase').AsString,
              vQ.FieldByName('Descripcion').AsString, vQ.FieldByName('MenuIcon')
              .AsString, vQ.FieldByName('TabIcon').AsString);

          FDicImgItem.Add(vimgMenu, vItemMenu);
        end;
      end;
      vQ.Next;
    end;
  finally
    vQ.Free;
    pgGeneral.ActivePage := tabDashBoard;
    pgGeneral.EndUpdate;
  end;
end;

procedure TMainForm.imgMenuClick(Sender: TObject);
var
  vImg: TUniImage absolute Sender;
begin
  crearPantalla(vImg, FDicImgItem[vImg].Clase.Replace('MNU_', EmptyStr),
    FDicImgItem[vImg].TabIcon, FDicImgItem[vImg].Descripcion);
end;

procedure TMainForm.labbtnExitClick(Sender: TObject);
begin
  Close;
  UniApplication.Restart;
end;

procedure TMainForm.LimpiarSubMenu;
var
  vImg: TUniImage;
begin
  if Assigned(FDicImgItem) then
  begin
    for vImg in FDicImgItem.Keys do
      vImg.Picture := nil;

    FreeAndNil(FDicImgItem);
  end;

  FDicImgItem := TObjectDictionary<TUniImage, TItemMenu>.Create([doOwnsValues]);
end;

{ TItemMenu }

constructor TItemMenu.Create(pID, pID_PADRE, pOrden: Integer;
pClase, pDescripcion, pMenuIcon, pTabIcon: String);
begin
  FID := pID;
  FID_PADRE := pID_PADRE;
  FOrden := pOrden;
  FClase := pClase;
  FDescripcion := pDescripcion;
  FMenuIcon := pMenuIcon;
  FTabIcon := pTabIcon;
end;

initialization

RegisterAppFormClass(TMainForm);

end.
