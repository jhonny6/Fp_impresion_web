inherited AppMenu_Web: TAppMenu_Web
  Caption = 'AppMenu_Web'
  PixelsPerInch = 96
  TextHeight = 15
  object UniDBEdit1: TUniDBEdit [3]
    Left = 56
    Top = 70
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ID'
    DataSource = DataSource
    TabOrder = 3
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'ID'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit2: TUniDBEdit [4]
    Left = 56
    Top = 110
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'ORDEN'
    DataSource = DataSource
    TabOrder = 4
    InputType = 'number'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Orden'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit3: TUniDBEdit [5]
    Left = 56
    Top = 150
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'CLASE'
    DataSource = DataSource
    TabOrder = 5
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Clase'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit4: TUniDBEdit [6]
    Left = 56
    Top = 190
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'DESCRIPCION'
    DataSource = DataSource
    TabOrder = 6
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Descripci'#243'n'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit5: TUniDBEdit [7]
    Left = 56
    Top = 230
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'MENUICON'
    DataSource = DataSource
    TabOrder = 7
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Icono men'#250
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit6: TUniDBEdit [8]
    Left = 56
    Top = 270
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'TABICON'
    DataSource = DataSource
    TabOrder = 8
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Icono tab'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
  object UniDBEdit7: TUniDBEdit [9]
    Left = 56
    Top = 310
    Width = 300
    Height = 22
    Hint = ''
    DataField = 'NOMBREIMAGEN'
    DataSource = DataSource
    TabOrder = 9
    InputType = 'text'
    LayoutConfig.Margin = '20'
    FieldLabel = 'Nombre imagen'
    FieldLabelWidth = 125
    FieldLabelAlign = laRight
  end
end
