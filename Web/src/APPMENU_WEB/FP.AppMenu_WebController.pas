unit FP.AppMenu_WebController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TAppMenu_WebCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateAppMenu_WebCmd = class(TAppMenu_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadAppMenu_WebCmd = class(TAppMenu_WebCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateAppMenu_WebCmd = class(TAppMenu_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteAppMenu_WebCmd = class(TAppMenu_WebCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickAppMenu_WebCmd = class(TReadAppMenu_WebCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.AppMenu_WebBrowser,
  FP.AppMenu_WebModel,
  FP.AppMenu_WebView,
  FP.Main;

{ TProvinciasCommand }

function TAppMenu_WebCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TAppMenu_WebModel;
end;

function TAppMenu_WebCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TAppMenu_WebBrowser;
end;

function TAppMenu_WebCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TAppMenu_Web;
end;

{ TCreateProvinciasCmd }

procedure TCreateAppMenu_WebCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateAppMenu_WebCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteAppMenu_WebCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickAppMenu_WebCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateAppMenu_WebCmd, TReadAppMenu_WebCmd, TUpdateAppMenu_WebCmd, TDeleteAppMenu_WebCmd, TPickAppMenu_WebCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateAppMenu_WebCmd, TReadAppMenu_WebCmd, TUpdateAppMenu_WebCmd, TDeleteAppMenu_WebCmd, TPickAppMenu_WebCmd]);

end.
