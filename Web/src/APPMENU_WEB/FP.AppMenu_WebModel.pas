unit FP.AppMenu_WebModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TAppMenu_WebModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  AppMenu_WebModel: TAppMenu_WebModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TAppMenu_WebModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'AppMenu_Web';
end;

function TAppMenu_WebModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TAppMenu_WebModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TAppMenu_WebModel.TableName: string;
begin
  Result := 'fastence.AppMenu_Web';
end;

end.
