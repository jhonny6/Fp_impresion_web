unit CashBank.Usuarios_perfilesController;
interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TUsuarios_perfilesCommand = class(TAppCommand)
  public
    class function EntityModelClass: TEntityModelClass;
    class function EntityBrowserClass: TEntityBrowserClass;
    class function EntityViewClass: TEntityViewClass;
  end;

  TCreateUsuarios_perfilesCmd = class(TUsuarios_perfilesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadUsuarios_perfilesCmd = class(TUsuarios_perfilesCommand)
  protected
    View: TEntityBrowser;
    Model: TEntityModel;
  public
    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateUsuarios_perfilesCmd = class(TUsuarios_perfilesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteUsuarios_perfilesCmd = class(TUsuarios_perfilesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickUsuarios_perfilesCmd = class(TReadUsuarios_perfilesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses     uniGUIApplication,
  FP.MainModule,
  CashBank.Usuarios_perfilesBrowser,
  CashBank.Usuarios_perfilesModel,
  CashBank.Usuarios_perfilesView,
  FP.Main;

{ TUsuarios_perfilesCommand }

class function TUsuarios_perfilesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TUsuarios_perfilesModel;
end;

class function TUsuarios_perfilesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TUsuarios_perfilesBrowser;
end;

class function TUsuarios_perfilesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TUsuarios_perfiles;
end;

{ TCreateUsuarios_perfilesCmd }

procedure TCreateUsuarios_perfilesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadUsuarios_perfilesCmd }

procedure TReadUsuarios_perfilesCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(MainForm);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;

//procedure TReadUsuarios_perfilesCmd.Proccess(Args: TUniControl);
//begin
//  View := EntityBrowserClass.Create(Args);
//  View.Parent:= Args;
//  Model := EntityModelClass.Create(View);
//  View.DataSource.DataSet := Model.FindAll;
//  View.Show;
//end;

{ TUpdateUsuarios_perfilesCmd }

procedure TUpdateUsuarios_perfilesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteUsuarios_perfilesCmd }

procedure TDeleteUsuarios_perfilesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickUsuarios_perfilesCmd }

procedure TPickUsuarios_perfilesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateUsuarios_perfilesCmd, TReadUsuarios_perfilesCmd, TUpdateUsuarios_perfilesCmd, TDeleteUsuarios_perfilesCmd, TPickUsuarios_perfilesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateUsuarios_perfilesCmd, TReadUsuarios_perfilesCmd, TUpdateUsuarios_perfilesCmd, TDeleteUsuarios_perfilesCmd, TPickUsuarios_perfilesCmd]);

end.
