unit CashBank.Usuarios_perfilesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TUsuarios_perfilesModel = class(TEntityModel)
  private
    { Private declarations }
  public
    { Public declarations }
    class function TableName: string;override;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TUsuarios_perfilesModel }

class function TUsuarios_perfilesModel.TableName: string;
begin
  Result := 'cashbank.usuarios_perfiles';
end;

end.
