inherited Usuarios_perfiles: TUsuarios_perfiles
  ClientHeight = 459
  ClientWidth = 729
  Caption = 'Usuarios_perfiles'
  ExplicitWidth = 745
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited UniSimplePanel1: TUniSimplePanel
    Width = 729
    ExplicitWidth = 729
  end
  inherited UniContainerPanel1: TUniScrollBox
    Width = 729
    Height = 396
    ExplicitWidth = 729
    ExplicitHeight = 396
    ScrollHeight = 60
    ScrollWidth = 556
    object UniDBEdit1: TUniDBEdit
      Left = 56
      Top = 6
      Width = 500
      Height = 22
      Hint = ''
      DataField = 'nombre'
      DataSource = DataSource
      TabOrder = 1
      LayoutConfig.Margin = '20'
      FieldLabel = 'Nombre'
    end
    object UniDBCheckBox1: TUniDBCheckBox
      Left = 56
      Top = 43
      Width = 97
      Height = 17
      Hint = ''
      Margins.Left = 20
      DataField = 'es_administrador'
      DataSource = DataSource
      Caption = ''
      TabOrder = 0
      ParentColor = False
      Color = clBtnFace
      LayoutConfig.Margin = '20'
      FieldLabel = 'Administrador'
    end
  end
  inherited StatusBar: TUniStatusBar
    Top = 437
    Width = 729
    ExplicitTop = 437
    ExplicitWidth = 729
  end
end
