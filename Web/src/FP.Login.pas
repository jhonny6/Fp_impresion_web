unit FP.Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniHTMLFrame, uniImage,
  uniBasicGrid, uniDBGrid, uniLabel, uniEdit, uniCheckBox, uniButton, uniBitBtn,
  uniPanel, uniGUIBaseClasses, Vcl.Imaging.pngimage, uniMultiItem, uniComboBox,
  uniDBComboBox;

type
  TUniLoginForm1 = class(TUniLoginForm)
    paBackground: TUniContainerPanel;
    paBackgroundLogin: TUniPanel;
    btnLog_In: TUniBitBtn;
    edPassword: TUniEdit;
    edUserName: TUniEdit;
    imgLogo: TUniImage;
    cbEsquemas: TUniComboBox;
    procedure UniLoginFormScreenResize(Sender: TObject;
      AWidth, AHeight: Integer);
    procedure btnLog_InClick(Sender: TObject);
    procedure cbEsquemasSelect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function UniLoginForm1: TUniLoginForm1;

implementation

{$R *.dfm}

uses
  uniGUIVars, uniGUIApplication, FP.MainModule;

function UniLoginForm1: TUniLoginForm1;
begin
  Result := TUniLoginForm1(UniMainModule.GetFormInstance(TUniLoginForm1));
end;

procedure rc_MoveAnimationForm(moveobj: TUniForm;
  leftfrom, leftto, topfrom, topto, duration, opacity: Integer;
  bClose: boolean);
var
  step: double;

  iDuration, iOpacity, i, f, moveareax, moveareay, value: Integer;
begin
  iDuration := duration;
  iOpacity := opacity;

  // mov. vertical
  if topfrom <> topto then
  begin
    UniSession.AddJS(moveobj.WebForm.JSName + '.animate({ ' + '    duration: ' +
      inttostr(iDuration) + ', ' + '    to: {  y:' + inttostr(topto) + ', ' +
      '    opacity: ' + inttostr(iOpacity) + ' } } );')
  end
  else
  begin
    // mov. horizontal
    UniSession.AddJS(moveobj.WebForm.JSName + '.animate({ ' + '    duration: ' +
      inttostr(iDuration) + ', ' + '    to: { x:' + inttostr(leftto) + ', ' +
      '    opacity: ' + inttostr(iOpacity) + ' } });');
  end;
end;

procedure TUniLoginForm1.btnLog_InClick(Sender: TObject);
var
  id: Integer;
  cadena, esquemas: string;
  vListaEsquemas: TStringList;
begin
  cadena := 'Error de autenticación';
  try
    esquemas := EmptyStr;
    id := UniMainModule.processLogin(edUserName.Text, edPassword.Text,
      esquemas);
  except
    on e: exception do
    begin
      id := -1;
      cadena := 'Hay un problema en la autenticación, inténtelo más tarde. ' +
        e.Message;
    end;
  end;
  if id = -1 then
  begin
    ModalResult := mrNone;
    showmessage(cadena);
  end
  else
  begin
    vListaEsquemas := TStringList.Create;
    try
      ExtractStrings([';'], [' '], PWideChar(esquemas), vListaEsquemas);

      cbEsquemas.Items := vListaEsquemas;

      if cbEsquemas.Visible then
        ModalResult := mrOk
      else
      begin

        if cbEsquemas.Items.Count > 1 then
        begin
          cbEsquemas.ItemIndex := 0;
          cbEsquemas.Text := cbEsquemas.Items[0];
          cbEsquemas.Visible := True;
        end
        else
        begin
          SCHEMA := cbEsquemas.Items[0];
          ModalResult := mrOk;
        end;
      end;
    finally
      vListaEsquemas.Free;
    end;
  end;
end;

procedure TUniLoginForm1.cbEsquemasSelect(Sender: TObject);
begin
  SCHEMA := cbEsquemas.Items[cbEsquemas.ItemIndex];
  ModalResult := mrOk;
end;

procedure TUniLoginForm1.UniLoginFormScreenResize(Sender: TObject;
  AWidth, AHeight: Integer);
begin

  Self.Left := (UniSession.UniApplication.ScreenWidth div 2) -
    (Self.Width div 2);
  Self.Top := 0;

  rc_MoveAnimationForm(Self, Self.Left, Self.Left, Self.Top,
    (UniSession.UniApplication.ScreenHeight div 2) - (Self.Height div 2), 400,
    1, false);

end;

initialization

RegisterAppFormClass(TUniLoginForm1);

end.
