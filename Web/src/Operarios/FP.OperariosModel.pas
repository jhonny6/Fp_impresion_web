unit FP.OperariosModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel, DBAccess,
  Uni, Data.DB, MemDS;

type
  TOperariosModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  OperariosModel: TOperariosModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TOperariosModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := '@PKG_MANTENIMIENTOS.FP0_Operarios';
end;

function TOperariosModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TOperariosModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TOperariosModel.TableName: string;
begin
  Result := 'fastence.gv_fp0_Operarios';
end;

end.
