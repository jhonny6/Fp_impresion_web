unit FP.OperariosController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TOperariosCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateOperariosCmd = class(TOperariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadOperariosCmd = class(TOperariosCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateOperariosCmd = class(TOperariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteOperariosCmd = class(TOperariosCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickOperariosCmd = class(TReadOperariosCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.OperariosBrowser,
  FP.OperariosModel,
  FP.OperariosView,
  FP.Main;

{ TProvinciasCommand }

function TOperariosCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TOperariosModel;
end;

function TOperariosCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TOperariosBrowser;
end;

function TOperariosCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TOperarios;
end;

{ TCreateProvinciasCmd }

procedure TCreateOperariosCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateOperariosCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteOperariosCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickOperariosCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateOperariosCmd, TReadOperariosCmd, TUpdateOperariosCmd, TDeleteOperariosCmd, TPickOperariosCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateOperariosCmd, TReadOperariosCmd, TUpdateOperariosCmd, TDeleteOperariosCmd, TPickOperariosCmd]);

end.
