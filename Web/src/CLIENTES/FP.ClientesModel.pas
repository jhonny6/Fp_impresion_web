unit FP.ClientesModel;

interface

uses
  System.SysUtils, System.Classes, CashBank.EntityModel,
  DBAccess, Uni, Data.DB, MemDS;

type
  TClientesModel = class(TEntityModel)
  protected
    function SQLFindAll_with_FK: string;
    function SQLFindAll: string; override;

  private
    { Private declarations }
  public
    class function TableName: string;override;
    class function PackageName: string;override;
  end;

var
  ClientesModel: TClientesModel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TProvinciasModel }

class function TClientesModel.PackageName: string;
begin
  //El IU y D, el framework lo pone solamente
  Result := 'FP0_Clientes_GEST';
end;

function TClientesModel.SQLFindAll: string;
begin
  Result := SQLFindAll_with_FK;
end;

function TClientesModel.SQLFindAll_with_FK: string;
begin
  Result := 'select * from '+TableName;
end;

class function TClientesModel.TableName: string;
begin
  Result := 'fastence.fp0_Clientes';
end;

end.
