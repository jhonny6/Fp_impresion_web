unit FP.ClientesController;

interface

uses CashBank.AppController,
  CashBank.EntityBrowser,
  CashBank.EntityView,
  CashBank.EntityModel, uniGUIClasses;

type

  TClientesCommand = class(TAppCommand)
  public
    function EntityModelClass: TEntityModelClass; override;
    function EntityBrowserClass: TEntityBrowserClass;override;
    function EntityViewClass: TEntityViewClass;override;
  end;

  TCreateClientesCmd = class(TClientesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TReadClientesCmd = class(TClientesCommand)
  protected
//    View: TEntityBrowser;
//    Model: TEntityModel;
  public
//    procedure Proccess(Args: TCommandArgs); overload;override;
//    procedure Proccess(Args: TUniControl); overload; override;
  end;

  TUpdateClientesCmd = class(TClientesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TDeleteClientesCmd = class(TClientesCommand)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

  TPickClientesCmd = class(TReadClientesCmd)
  public
    procedure Proccess(Args: TCommandArgs); override;
  end;

implementation

uses uniGUIApplication,
  FP.MainModule,
  FP.ClientesBrowser,
  FP.ClientesModel,
  FP.ClientesView,
  FP.Main;

{ TProvinciasCommand }

function TClientesCommand.EntityModelClass: TEntityModelClass;
begin
  Result := TClientesModel;
end;

function TClientesCommand.EntityBrowserClass: TEntityBrowserClass;
begin
  Result := TClientesBrowser;
end;

function TClientesCommand.EntityViewClass: TEntityViewClass;
begin
  Result := TClientes;
end;

{ TCreateProvinciasCmd }

procedure TCreateClientesCmd.Proccess(Args: TCommandArgs);
var
 View: TEntityView;
  Model: TEntityModel;
begin
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.NewEntity;
  View.ShowModal;
end;

{ TReadProvinciasCmd }

{
procedure TReadProvinciasCmd.Proccess(Args: TCommandArgs);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
}

(*
procedure TReadProvinciasCmd.Proccess(Args: TUniControl);
begin
  View := EntityBrowserClass.Create(uniSession.UniApplication);
  View.Parent:= Args;
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindAll;
  View.Show;
end;
*)
{ TUpdateProvinciasCmd }

procedure TUpdateClientesCmd.Proccess(Args: TCommandArgs);
var
  View: TEntityView;
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
  View := EntityViewClass.Create(uniSession.UniApplication);
  Model := EntityModelClass.Create(View);
  View.DataSource.DataSet := Model.FindByPK(PK);
  View.ShowModal;
end;

{ TDeleteProvinciasCmd }

procedure TDeleteClientesCmd.Proccess(Args: TCommandArgs);
var
  Model: TEntityModel;
  PK: variant;
begin
  PK := Args[0];
//  if not ConfirmDlg('Desea borrar este registro?') then
//    Exit;
  Model := EntityModelClass.Create(nil);
  try
    Model.FindByPK(PK).Delete;
  finally
    Model.Free;
  end;
end;

{ TPickProvinciasCmd }

procedure TPickClientesCmd.Proccess(Args: TCommandArgs);
begin
  inherited;
//  View.PickMode := True;
end;

initialization

TAppCommand.RegisterCmd([TCreateClientesCmd, TReadClientesCmd, TUpdateClientesCmd, TDeleteClientesCmd, TPickClientesCmd]);

finalization

TAppCommand.UnegisterCmd([TCreateClientesCmd, TReadClientesCmd, TUpdateClientesCmd, TDeleteClientesCmd, TPickClientesCmd]);

end.
