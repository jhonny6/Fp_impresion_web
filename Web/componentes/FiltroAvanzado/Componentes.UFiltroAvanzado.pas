unit Componentes.UFiltroAvanzado;

interface

uses
  System.SysUtils, System.Classes, System.Types, Generics.Collections, Data.DB,
  UniPanel, uniComboBox, uniEdit, uniButton, uniLabel,
  uniGUIClasses, uniScrollBox, Vcl.Controls;

// type
// TColumnCollectionHelper = class helper for TGridPanelLayout.TColumnCollection
// procedure AddRange(pCantidad: Integer);
// procedure PorcentajeAuto;
// end;

type
  TTipoConector = (tcNone, tcAnd, tcOr);
  TTipoOperacion = (toEsIgual, toNoEsIgual, toEsMayor, toEsMayorOIgual,
    toEsMenor, toEsMenorOIgual, toEsNulo, toNoEsNulo, toEmpiezaCon,
    toNoEmpiezaCon, toFinalizaCon, toNoFinalizaCon, toContiene, toNoContiene);

  TFiltroNivel = class(TUniPanel)
  private
    FcbColumna, FcbOperacion: TUniComboBox;
    // FpnlContenido: TGridPanelLayout;
    FpnlContenido: TUniPanel;
    FedValor: TUniEdit;
    FbtBotonAgrega, FbtBotonElimina: TUniButton;
    FOperacion: TTipoOperacion;
    FValor: String;
    FNombreColumnas: TStringList;
    FColumna: String;
    function CreaLabel(pTexto: String): TUniLabel;
    function CreaComboBox: TUniComboBox;
    function CreaEdit: TUniEdit;
    function CreaBoton: TUniButton;
    procedure AjustaControl(pControl: TUniControl);
    procedure SetOperacion(pOperacion: TTipoOperacion);

    // Eventos
    procedure OnClickBotonAgrega(Sender: TObject);
    procedure OnClickBotonElimina(Sender: TObject);
    procedure OnCambiaComboOperacion(Sender: TObject);
    procedure OnCambiaValor(Sender: TObject);
    procedure OnCambiaColumna(Sender: TObject);
    procedure SetColumna(const Value: String);
    procedure SetValor(const Value: String);
  public
    OnBotonAgrega, OnBotonElimina: TProc;

    property Columna: String read FColumna write SetColumna;
    property Operacion: TTipoOperacion read FOperacion write SetOperacion;
    property Valor: String read FValor write SetValor;

    procedure Redimensionar;
    procedure AgregaColumnas(pColumnas: TDictionary<String, String>);
    constructor Create(pOwner: TComponent); override;
  end;

  TConector = class(TUniComboBox)
  private
    FNivel: TFiltroNivel;
    FTipoConector: TTipoConector;
    procedure OnChangeConector(Sender: TObject);
    procedure SetTipoConector(pValue: TTipoConector);
  public
    property TipoConector: TTipoConector read FTipoConector
      write SetTipoConector;
    property Nivel: TFiltroNivel read FNivel write FNivel;

    constructor Create(pComponente: TComponent; pNivel: TFiltroNivel);
  end;

type
  TAplicaFiltroEvent = procedure(pFiltros: TList<TConector>) of object;

  TFiltroAvanzado = class(TUniPanel)
  private
    FCamposOmitidos: TStrings;
    FTitulo: String;
    FScroll: TUniScrollBox;
    FbtCerrar: TUniButton;
    FFiltros: TList<TConector>;
    FCampos: TDictionary<String, String>;
    FDataSet: TDataSet;
    FAplicaFiltroEvent: TAplicaFiltroEvent;
    FCondiciones: String;
    FOperadoresLogicos: TList<string>;
    FOnGuardaFiltro: TAplicaFiltroEvent;

    procedure SetCamposOmitidos(pValue: TStrings);
    procedure SetTitulo(pValue: String);
    procedure SetCampos(pValue: TDictionary<String, String>);
    procedure SetDataSet(pDataSet: TDataSet);

    procedure OnClickCerrar(Sender: TObject);
    procedure OnClickAplicaFiltro(Sender: TObject);
    procedure OnClickGuardaFiltro(Sender: TObject);
    procedure OnClickReiniciaFiltro(Sender: TObject);
    procedure OnCambiaCampo(Sender: TObject; const Item: String;
      Action: TCollectionNotification);
  protected
    FpnlPie: TUniPanel;
    FpnlTitulo: TUniPanel;
    FlbTitulo: TUniLabel;
  public
    btAplicaFiltro, btReiniciaFiltro, btGuardarFiltro: TUniButton;

    property Filtros: TList<TConector> read FFiltros write FFiltros;
    property Campos: TDictionary<String, String> read FCampos write SetCampos;
    property Condiciones: String read FCondiciones;

    procedure AgregaNivel;

    constructor Create(AOwner: TComponent); override;
  published
    property Titulo: String read FTitulo write SetTitulo;
    property DataSet: TDataSet read FDataSet write SetDataSet;
    property CamposOmitidos: TStrings read FCamposOmitidos
      write SetCamposOmitidos stored True;

    property OnAplicaFiltro: TAplicaFiltroEvent read FAplicaFiltroEvent
      write FAplicaFiltroEvent;
    property OnGuardaFiltro: TAplicaFiltroEvent read FOnGuardaFiltro
      write FOnGuardaFiltro;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Personalizados', [TFiltroAvanzado]);
end;

{ TODOFiltroAvanzado }

procedure TFiltroAvanzado.AgregaNivel;
var
  vFiltroNivel: TFiltroNivel;
  vConector: TConector;
begin
  vFiltroNivel := TFiltroNivel.Create(FScroll);
  vFiltroNivel.Parent := FScroll;

  // FScroll.AddObject(vFiltroNivel);
  // vFiltroNivel.Position.Y := (vFiltroNivel.Height * 3) * FFiltros.Count;
  // vFiltroNivel.Stored := False;

  vFiltroNivel.OnBotonAgrega := (
    procedure
    begin
      AgregaNivel;
    end);

  vConector := TConector.Create(Self, vFiltroNivel);
  if FFiltros.Count > 0 then
    vConector.TipoConector := tcAnd
  else
    vFiltroNivel.FbtBotonElimina.Visible := False;

  vConector.Parent := FScroll;
  // vConector.Position.Y := vFiltroNivel.Position.Y - vFiltroNivel.Height;
  // vConector.Margins := TBounds.Create(TRectF.Create(5, 5,
  // vConector.Width * 0.9, 5));

  FFiltros.Add(vConector);

  vFiltroNivel.OnBotonElimina := (
    procedure
    begin
      FFiltros.Remove(vConector);
      vConector.Free;
      vFiltroNivel.Free;
    end);

  vFiltroNivel.AgregaColumnas(FCampos);
  // FScroll.ViewportPosition := PointF(FScroll.ViewportPosition.X,
  // vFiltroNivel.Position.Y);
end;

constructor TFiltroAvanzado.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FCamposOmitidos := TStringList.Create;
  FFiltros := TList<TConector>.Create;
  FCampos := TDictionary<String, String>.Create;
  FCampos.OnKeyNotify := OnCambiaCampo;
  FOperadoresLogicos := TList<string>.Create;

  FOperadoresLogicos.AddRange(['=''%s''', '<>''%s''', '>''%s''', '>=''%s''',
    '<''%s''', '<=''%s''', '%s is null', '%s is not null', ' ilike ''%s%%''',
    ' not ilike ''%s%%''', ' ilike (''%%%s'')', ' not ilike (''%%%s'')',
    ' ilike ''%%%s%%''', ' not ilike ''%%%s%%''']);

  Height := 350;
  Width := 570;

  // if csDesigning in ComponentState then
  begin
    FpnlTitulo := TUniPanel.Create(Self);
    FpnlTitulo.Align := alTop;
    FpnlTitulo.Height := 40;
    FpnlTitulo.Parent := Self;
    // FpnlTitulo.Stored := False;

    FlbTitulo := TUniLabel.Create(FpnlTitulo);
    FlbTitulo.Align := alClient;
    FlbTitulo.Parent := FpnlTitulo;
    FlbTitulo.Text := 'B�SQUEDA AVANZADA';
    // FlbTitulo.Margins := TBounds.Create(TRectF.Create(5, 0, 0, 0));
    // FlbTitulo.Stored := False;

    FbtCerrar := TUniButton.Create(FpnlTitulo);
    FbtCerrar.Align := alRight;
    FbtCerrar.Width := 40;
    // FbtCerrar.Margins := TBounds.Create(TRectF.Create(2, 2, 2, 2));
    // FbtCerrar.StyleLookup := 'cleareditbutton';
    FbtCerrar.Parent := FpnlTitulo;
    FbtCerrar.OnClick := OnClickCerrar;
    // FbtCerrar.Stored := False;

    FScroll := TUniScrollBox.Create(Self);
    FScroll.Align := alClient;
    FScroll.Parent := Self;
    // FScroll.Stored := False;

    FpnlPie := TUniPanel.Create(Self);
    FpnlPie.Align := alBottom;
    FpnlPie.Height := 40;
    FpnlPie.Parent := Self;
    // FpnlPie.Stored := False;

    btReiniciaFiltro := TUniButton.Create(FpnlPie);
    btReiniciaFiltro.Align := alRight;
    btReiniciaFiltro.Width := 130;
    // btReiniciaFiltro.Margins := TBounds.Create(TRectF.Create(2, 5, 2, 2));
    btReiniciaFiltro.Caption := 'Reiniciar';
    // btReiniciaFiltro.StyledSettings := btReiniciaFiltro.StyledSettings -
    // [TStyledSetting.Size];
    // btReiniciaFiltro.TextSettings.Font.Size := 15;
    btReiniciaFiltro.Parent := FpnlPie;
    btReiniciaFiltro.OnClick := OnClickReiniciaFiltro;
    // btReiniciaFiltro.Stored := False;

    btAplicaFiltro := TUniButton.Create(FpnlPie);
    btAplicaFiltro.Align := alRight;
    btAplicaFiltro.Width := 130;
    // btAplicaFiltro.Margins := TBounds.Create(TRectF.Create(2, 5, 2, 2));
    btAplicaFiltro.Caption := 'Aplicar';
    // btAplicaFiltro.StyledSettings := btReiniciaFiltro.StyledSettings -
    // [TStyledSetting.Size];
    // btAplicaFiltro.TextSettings.Font.Size := 15;
    btAplicaFiltro.Parent := FpnlPie;
    btAplicaFiltro.OnClick := OnClickAplicaFiltro;
    // btAplicaFiltro.Stored := False;

    btGuardarFiltro := TUniButton.Create(FpnlPie);
    btGuardarFiltro.Align := alRight;
    btGuardarFiltro.Width := 130;
    // btGuardarFiltro.Margins := TBounds.Create(TRectF.Create(2, 5, 2, 2));
    btGuardarFiltro.Caption := 'Guardar';
    // btGuardarFiltro.StyledSettings := btGuardarFiltro.StyledSettings -
    // [TStyledSetting.Size];
    // btGuardarFiltro.TextSettings.Font.Size := 15;
    btGuardarFiltro.Parent := FpnlPie;
    btGuardarFiltro.OnClick := OnClickGuardaFiltro;
    // btGuardarFiltro.Stored := False;

    AgregaNivel;
  end;
end;

procedure TFiltroAvanzado.OnCambiaCampo(Sender: TObject; const Item: String;
Action: TCollectionNotification);
begin
  if Action = TCollectionNotification.cnAdded then
    SetCampos(FCampos);
end;

procedure TFiltroAvanzado.OnClickAplicaFiltro(Sender: TObject);
var
  vConector, vNombreCampo, vOperador: String;
  vFiltro: TConector;
begin
  FCondiciones := EmptyStr;
  for vFiltro in FFiltros do
  begin
    vNombreCampo := 'cast(' + vFiltro.Nivel.Columna + ' as text)';

    if vNombreCampo = EmptyStr then
    begin
      vFiltro.Nivel.FcbColumna.SetFocus;
      raise Exception.Create('Filtro no v�lido. Seleccione un campo');
    end;

    vConector := EmptyStr;
    case vFiltro.TipoConector of
      tcAnd:
        vConector := ' and ';
      tcOr:
        vConector := ' or ';
    end;

    vOperador := EmptyStr;
    vOperador := FOperadoresLogicos.Items[Integer(vFiltro.Nivel.FOperacion)];

    if vFiltro.Nivel.Operacion in [toEsNulo, toNoEsNulo] then
      FCondiciones := FCondiciones + vConector + '(' +
        Format(vOperador, [vFiltro.Nivel.Columna]) + ')'
    else
      FCondiciones := FCondiciones + vConector + '(' + vNombreCampo +
        Format(vOperador, [vFiltro.Nivel.Valor]) + ')';
  end;

  if Assigned(FAplicaFiltroEvent) then
    FAplicaFiltroEvent(FFiltros);
end;

procedure TFiltroAvanzado.OnClickGuardaFiltro(Sender: TObject);
var
  vConector, vNombreCampo, vOperador: String;
  vFiltro: TConector;
begin
  FCondiciones := EmptyStr;
  for vFiltro in FFiltros do
  begin
    vNombreCampo := vFiltro.Nivel.Columna;

    if vNombreCampo = EmptyStr then
    begin
      vFiltro.Nivel.FcbColumna.SetFocus;
      raise Exception.Create('Filtro no v�lido. Seleccione un campo');
    end;

    vConector := EmptyStr;
    case vFiltro.TipoConector of
      tcAnd:
        vConector := ' and ';
      tcOr:
        vConector := ' or ';
    end;

    vOperador := EmptyStr;
    vOperador := FOperadoresLogicos.Items[Integer(vFiltro.Nivel.FOperacion)];

    if vFiltro.Nivel.Operacion in [toEsNulo, toNoEsNulo] then
      FCondiciones := FCondiciones + vConector + '(' +
        Format(vOperador, [vFiltro.Nivel.Columna]) + ')'
    else
      FCondiciones := FCondiciones + vConector + '(' + vNombreCampo +
        Format(vOperador, [vFiltro.Nivel.Valor]) + ')';
  end;

  if Assigned(FOnGuardaFiltro) then
    FOnGuardaFiltro(FFiltros);
end;

procedure TFiltroAvanzado.OnClickCerrar(Sender: TObject);
begin
  Visible := False;
end;

procedure TFiltroAvanzado.OnClickReiniciaFiltro(Sender: TObject);
var
  vI: Integer;
begin
  for vI := Filtros.Count - 1 downto 1 do
  begin
    FFiltros.Items[vI].Nivel.Free;
    FFiltros.Items[vI].Free;
    FFiltros.Delete(vI);
  end;

  FFiltros.Items[0].Nivel.Columna := '';
  FFiltros.Items[0].Nivel.Valor := '';
end;

procedure TFiltroAvanzado.SetCampos(pValue: TDictionary<String, String>);
var
  vConector: TConector;
begin
  FCampos := pValue;

  for vConector in FFiltros do
    vConector.Nivel.AgregaColumnas(FCampos);
end;

procedure TFiltroAvanzado.SetCamposOmitidos(pValue: TStrings);
begin
  CamposOmitidos.Assign(pValue);
end;

procedure TFiltroAvanzado.SetDataSet(pDataSet: TDataSet);
var
  vI: Integer;
begin
  FDataSet := pDataSet;
  FCampos.Clear;

  if Assigned(pDataSet) then
    for vI := 0 to pDataSet.FieldCount - 1 do
      if ((pDataSet.Fields[vI].Visible) and
        (FCamposOmitidos.IndexOf(pDataSet.Fields[vI].FieldName) = -1)) then
        FCampos.Add(pDataSet.Fields[vI].FieldName,
          pDataSet.Fields[vI].DisplayName);

end;

procedure TFiltroAvanzado.SetTitulo(pValue: String);
begin
  FTitulo := pValue;
  FlbTitulo.Text := pValue;
end;

{ TODOFiltroNivel }

procedure TFiltroNivel.AgregaColumnas(pColumnas: TDictionary<String, String>);
var
  vClave: String;
begin
  if not Assigned(FNombreColumnas) then
    FNombreColumnas := TStringList.Create;

  FNombreColumnas.Clear;
  FcbColumna.Clear;

  for vClave in pColumnas.Keys do
  begin
    FNombreColumnas.Add(vClave);
    FcbColumna.Items.Add(pColumnas.Items[vClave]);
  end;

  // FcbColumna.DropDown;
end;

procedure TFiltroNivel.AjustaControl(pControl: TUniControl);
begin
  pControl.Align := alClient;
  // pControl.Margins := TBounds.Create(TRectF.Create(5, 8, 8, 8));
  pControl.Parent := FpnlContenido;
end;

function TFiltroNivel.CreaBoton: TUniButton;
begin
  Result := TUniButton.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaComboBox: TUniComboBox;
begin
  Result := TUniComboBox.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaEdit: TUniEdit;
begin
  Result := TUniEdit.Create(FpnlContenido);
  AjustaControl(Result);
end;

function TFiltroNivel.CreaLabel(pTexto: String): TUniLabel;
begin
  Result := TUniLabel.Create(FpnlContenido);
  Result.Text := pTexto;
  AjustaControl(Result);
end;

constructor TFiltroNivel.Create(pOwner: TComponent);
begin
  inherited Create(pOwner);

  Align := alTop;
  Height := 80;
  // Margins := TBounds.Create(TRectF.Create(5, 5, 5, 5));

  FpnlContenido := TUniPanel.Create(Self);
  FpnlContenido.Align := alClient;
  // FpnlContenido.ColumnCollection.Clear;
  // FpnlContenido.ColumnCollection.AddRange(5);
  FpnlContenido.Parent := Self;

  Redimensionar;
  Redimensionar;
  Redimensionar;

  CreaLabel('Columna');
  CreaLabel('Operaci�n');
  CreaLabel('Valor');
  CreaLabel(EmptyStr);
  CreaLabel(EmptyStr);

  FcbColumna := CreaComboBox;
  FcbColumna.OnChange := OnCambiaColumna;

  FcbOperacion := CreaComboBox;

  FcbOperacion.Items.AddStrings(['Es igual', 'No es igual', 'Es mayor',
    'Es mayor o igual', 'Es menor', 'Es menor o igual', 'Es nulo', 'No es nulo',
    'Empieza con', 'No empieza con', 'Finaliza con', 'No finaliza con',
    'Contiene', 'No contiene']);

  SetOperacion(toContiene);
  FcbOperacion.ItemIndex := 12;
  FcbOperacion.OnChange := OnCambiaComboOperacion;

  FedValor := CreaEdit;
  FedValor.OnChange := OnCambiaValor;

  FbtBotonAgrega := CreaBoton;
  // FbtBotonAgrega.StyleLookup := 'additembutton';
  FbtBotonAgrega.OnClick := OnClickBotonAgrega;

  FbtBotonElimina := CreaBoton;
  // FbtBotonElimina.StyleLookup := 'deleteitembutton';
  FbtBotonElimina.OnClick := OnClickBotonElimina;
end;

procedure TFiltroNivel.OnCambiaColumna(Sender: TObject);
begin
  FColumna := FNombreColumnas[FcbColumna.ItemIndex];

  // FcbOperacion.DropDown;
end;

procedure TFiltroNivel.OnCambiaValor(Sender: TObject);
begin
  FValor := FedValor.Text;
end;

procedure TFiltroNivel.OnClickBotonAgrega(Sender: TObject);
begin
  if Assigned(OnBotonAgrega) then
    OnBotonAgrega;
end;

procedure TFiltroNivel.OnClickBotonElimina(Sender: TObject);
begin
  if Assigned(OnBotonElimina) then
    OnBotonElimina;
end;

procedure TFiltroNivel.OnCambiaComboOperacion(Sender: TObject);
begin
  FOperacion := TTipoOperacion(FcbOperacion.ItemIndex);

  FedValor.Enabled := (not(FOperacion in [toEsNulo, toNoEsNulo]));

  if not FedValor.Enabled then
    FedValor.Text := EmptyStr;

  FedValor.SetFocus;
end;

procedure TFiltroNivel.Redimensionar;
begin
  // FpnlContenido.ColumnCollection.Items[0].Value := 30;
  // FpnlContenido.ColumnCollection.Items[1].Value := 30;
  // FpnlContenido.ColumnCollection.Items[2].Value := 30;
  // FpnlContenido.ColumnCollection.Items[3].Value := 5;
  // FpnlContenido.ColumnCollection.Items[4].Value := 5;
end;

procedure TFiltroNivel.SetColumna(const Value: String);
var
  vLista: TStrings;
begin
  FColumna := Value;

  vLista := TStringList.Create;
  try
    if FColumna = EmptyStr then
    begin
      vLista.Assign(FcbColumna.Items);
      FcbColumna.Clear;
      FcbColumna.Items.AddStrings(vLista);
    end
    else
      FcbColumna.ItemIndex := FcbColumna.Items.IndexOf(Value);
  finally
    vLista.Free;
  end;
end;

procedure TFiltroNivel.SetOperacion(pOperacion: TTipoOperacion);
begin
  if FOperacion <> pOperacion then
    FOperacion := pOperacion;

  if (FcbOperacion.ItemIndex <> Integer(pOperacion)) then
    FcbOperacion.ItemIndex := Integer(pOperacion);
end;

procedure TFiltroNivel.SetValor(const Value: String);
begin
  FValor := Value;

  // if FValor = EmptyStr then
  FedValor.Text := Value;
end;

{ TColumnCollectionHelper }

// procedure TColumnCollectionHelper.AddRange(pCantidad: Integer);
// var
// vI: Integer;
// begin
// for vI := 1 to pCantidad do
// Add;
// end;

// procedure TColumnCollectionHelper.PorcentajeAuto;
// var
// vI: Integer;
// vPorc: Double;
// begin
// if Count > 0 then
// vPorc := 100 / Count
// else
// vPorc := 100;
//
// for vI := 0 to Count - 1 do
// Items[vI].Value := vPorc;
// end;

{ TODOConector }

constructor TConector.Create(pComponente: TComponent; pNivel: TFiltroNivel);
begin
  inherited Create(pComponente);
  Align := alTop;
  FNivel := pNivel;

  Items.AddStrings(['Y', 'O']);
  SetTipoConector(tcNone);

  OnChange := OnChangeConector;
end;

procedure TConector.OnChangeConector(Sender: TObject);
begin
  FTipoConector := TTipoConector(ItemIndex + 1);
end;

procedure TConector.SetTipoConector(pValue: TTipoConector);
begin
  FTipoConector := pValue;

  ItemIndex := Integer(pValue) - 1;

  Visible := not(FTipoConector = tcNone);
end;

initialization

RegisterClasses([TFiltroNivel, TConector]);

end.
