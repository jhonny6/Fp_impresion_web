unit dataPpal;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.Oracle, FireDAC.Phys.OracleDef,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TDMPPal = class(TDataModule)
    SQLConn: TFDConnection;
    procedure DataModuleCreate(Sender: TObject);
  private

    { Private declarations }
  public
    function GetLogin(pIdUserTelegram: String): Boolean;
    function SetLogin(pIdUserTelegram: String): Boolean;
    function UnLogin(pIdUserTelegram: String): Boolean;
    function GetProducto(pCodigo: String): TFDQuery;
  end;

var
  DMPPal: TDMPPal;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TDMPPal }

function TDMPPal.SetLogin(pIdUserTelegram: String): Boolean;
var
  vQ: TFDQuery;
begin
  vQ := TFDQuery.Create(nil);
  try
    vQ.Connection := SQLConn;
    vQ.ExecSQL
      ('Insert into fastence.FP0_USER_BOT (USER_ID_TELEGRAM) Values(:USER_ID_TELEGRAM)',
      [pIdUserTelegram], [TFieldType.ftString]);

    Result := (vQ.RowsAffected > 0);
  finally
    FreeAndNil(vQ);
  end;
end;

function TDMPPal.GetLogin(pIdUserTelegram: String): Boolean;
var
  vQ: TFDQuery;
begin
  vQ := TFDQuery.Create(nil);
  try
    vQ.Connection := SQLConn;
    vQ.Open('Select count(*) from fastence.FP0_USER_BOT where USER_ID_TELEGRAM=:USER_ID_TELEGRAM',
      [pIdUserTelegram], [TFieldType.ftString]);

    Result := (vQ.Fields[0].AsInteger > 0);
  finally
    FreeAndNil(vQ);
  end;
end;

function TDMPPal.UnLogin(pIdUserTelegram: String): Boolean;
var
  vQ: TFDQuery;
begin
  vQ := TFDQuery.Create(nil);
  try
    vQ.Connection := SQLConn;
    vQ.ExecSQL
      ('delete from fastence.FP0_USER_BOT where USER_ID_TELEGRAM=:USER_ID_TELEGRAM',
      [pIdUserTelegram], [TFieldType.ftString]);

    Result := (vQ.RowsAffected > 0);
  finally
    FreeAndNil(vQ);
  end;
end;

function TDMPPal.GetProducto(pCodigo: String): TFDQuery;
begin
  Result := TFDQuery.Create(nil);
  // try
  Result.Connection := SQLConn;
  Result.Open
    ('select DESCRIPCION, ID_ZONA_PREPARACION, ROTACION, STOCK_MINIMO, FORMATO_ENVASADO from fastence.FP0_ARTICULOS where CODIGO=:CODIGO',
    [pCodigo], [TFieldType.ftString]);

  // Result := (vQ.RowsAffected > 0);
  // finally
  // FreeAndNil(vQ);
  // end;
end;

procedure TDMPPal.DataModuleCreate(Sender: TObject);
begin
  SQLConn.Connected := False;
end;

end.
