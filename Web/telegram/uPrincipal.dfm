object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ejemplo Telegram FP'
  ClientHeight = 543
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 7
    Width = 29
    Height = 13
    Caption = 'Token'
  end
  object Label2: TLabel
    Left = 415
    Top = 7
    Width = 102
    Height = 13
    Caption = 'ID de usuario destino'
  end
  object txtToken: TEdit
    Left = 8
    Top = 26
    Width = 401
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
    Text = '1838337814:AAG3fmBqeR6QHd_BwSOl_af3gt1mw-hCFMI'
    TextHint = 'Rellene este campo con el Token de Telegram'
  end
  object Button2: TButton
    Left = 580
    Top = 22
    Width = 75
    Height = 25
    Caption = 'Iniciar'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 661
    Top = 22
    Width = 75
    Height = 25
    Caption = 'Parar'
    TabOrder = 2
    OnClick = Button3Click
  end
  object memConsole: TMemo
    Left = 0
    Top = 401
    Width = 740
    Height = 142
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 3
    ExplicitTop = 403
  end
  object txtID: TEdit
    Left = 415
    Top = 26
    Width = 159
    Height = 21
    TabOrder = 4
    Text = '976865298'
    TextHint = 'Id de Usuario'
  end
  object cbDisableNotification: TCheckBox
    Left = 294
    Top = 49
    Width = 115
    Height = 17
    Caption = 'Disable Notification'
    TabOrder = 5
  end
  object cbProtectedContent: TCheckBox
    Left = 173
    Top = 49
    Width = 115
    Height = 17
    Caption = 'Protected Content'
    TabOrder = 6
  end
  object pcPpal: TPageControl
    Left = 0
    Top = 72
    Width = 740
    Height = 329
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 7
    object TabSheet1: TTabSheet
      Caption = 'Ejemplo FP'
      object Button6: TButton
        Left = 143
        Top = 3
        Width = 137
        Height = 25
        Caption = 'SetMyCommands'
        Default = True
        TabOrder = 0
        OnClick = btnCommandsClick
      end
      object Button7: TButton
        Left = 286
        Top = 3
        Width = 137
        Height = 25
        Caption = 'DeleteMyCommands'
        Default = True
        TabOrder = 1
        OnClick = btnDeleteCommandsClick
      end
      object Button8: TButton
        Left = 0
        Top = 3
        Width = 137
        Height = 25
        Caption = 'GetMyCommands'
        Default = True
        TabOrder = 2
        OnClick = btnGetCMDClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Ejemplos Varios'
      ImageIndex = 1
      object Label5: TLabel
        Left = 3
        Top = -3
        Width = 78
        Height = 13
        Caption = 'Token del Banco'
      end
      object Label4: TLabel
        Left = 410
        Top = -3
        Width = 84
        Height = 13
        Caption = 'Nombre del juego'
      end
      object Label3: TLabel
        Left = 3
        Top = 229
        Width = 53
        Height = 13
        Caption = 'Log del Bot'
      end
      object btnGetCMD: TButton
        Left = 3
        Top = 43
        Width = 137
        Height = 25
        Caption = 'GetMyCommands'
        Default = True
        TabOrder = 0
        OnClick = btnGetCMDClick
      end
      object btnDeleteCommands: TButton
        Left = 289
        Top = 43
        Width = 137
        Height = 25
        Caption = 'DeleteMyCommands'
        Default = True
        TabOrder = 1
        OnClick = btnDeleteCommandsClick
      end
      object btnCommands: TButton
        Left = 146
        Top = 43
        Width = 137
        Height = 25
        Caption = 'SetMyCommands'
        Default = True
        TabOrder = 2
        OnClick = btnCommandsClick
      end
      object Button5: TButton
        Left = 575
        Top = 74
        Width = 137
        Height = 25
        Caption = 'Enviar SlotMachine'
        Default = True
        TabOrder = 3
        OnClick = Button5Click
      end
      object Button4: TButton
        Left = 575
        Top = 202
        Width = 137
        Height = 25
        Caption = 'Enviar FootBal'
        Default = True
        TabOrder = 4
        OnClick = Button4Click
      end
      object Button1: TButton
        Left = 575
        Top = 171
        Width = 137
        Height = 25
        Caption = 'Enviar Basquete'
        Default = True
        TabOrder = 5
        OnClick = Button1Click
      end
      object txtTokenBanco: TEdit
        Left = 3
        Top = 16
        Width = 401
        Height = 21
        PasswordChar = '*'
        TabOrder = 6
        TextHint = 'Rellene este campo con el token bancario'
      end
      object btnADD: TButton
        Left = 115
        Top = 198
        Width = 25
        Height = 25
        Caption = '+'
        Default = True
        TabOrder = 7
        OnClick = btnADDClick
      end
      object btnComoSaberID: TButton
        Left = 575
        Top = 12
        Width = 137
        Height = 25
        Caption = 'Como Saber Mi ID?'
        Default = True
        TabOrder = 8
        OnClick = btnComoSaberIDClick
      end
      object btnSolicitarLocalizacao: TButton
        Left = 432
        Top = 198
        Width = 137
        Height = 25
        Caption = 'Solicitar Localiza'#231#227'o'
        Default = True
        TabOrder = 9
        OnClick = btnSolicitarLocalizacaoClick
      end
      object btnSolicitarCtt: TButton
        Left = 289
        Top = 198
        Width = 137
        Height = 25
        Caption = 'Solicitar Contato'
        Default = True
        TabOrder = 10
        OnClick = btnSolicitarCttClick
      end
      object txtNomeJogo: TEdit
        Left = 410
        Top = 16
        Width = 159
        Height = 21
        TabOrder = 11
        Text = 'Nombre del juego'
        TextHint = 'Nombre del juego'
      end
      object btnEnviarDardo: TButton
        Left = 575
        Top = 140
        Width = 137
        Height = 25
        Caption = 'Enviar um Dardo'
        Default = True
        TabOrder = 12
        OnClick = btnEnviarDardoClick
      end
      object btnEnviarAnimacao: TButton
        Left = 3
        Top = 167
        Width = 137
        Height = 25
        Caption = 'Enviar Anima'#231#227'o'
        Default = True
        TabOrder = 13
        OnClick = btnEnviarAnimacaoClick
      end
      object btnApagarBotoes: TButton
        Left = 432
        Top = 167
        Width = 137
        Height = 25
        Caption = 'Enviar txt e Apagar Botoes'
        Default = True
        TabOrder = 14
        OnClick = btnApagarBotoesClick
      end
      object btnEnviarTxtComBTInline: TButton
        Left = 289
        Top = 167
        Width = 137
        Height = 25
        Caption = 'Enviar txt c/ Botoes Inline'
        Default = True
        TabOrder = 15
        OnClick = btnEnviarTxtComBTInlineClick
      end
      object btnEnviarTxtComBt: TButton
        Left = 146
        Top = 167
        Width = 137
        Height = 25
        Caption = 'Enviar Texto c/ Botoes'
        Default = True
        TabOrder = 16
        OnClick = btnEnviarTxtComBtClick
      end
      object btnEnviarGrpMidias: TButton
        Left = 3
        Top = 198
        Width = 113
        Height = 25
        Caption = 'Enviar Grupo de Medios'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 17
        OnClick = btnEnviarGrpMidiasClick
      end
      object btnEnviarInvoice: TButton
        Left = 146
        Top = 198
        Width = 137
        Height = 25
        Caption = 'Enviar Invoice'
        Default = True
        TabOrder = 18
        OnClick = btnEnviarInvoiceClick
      end
      object btnEnviarAcao: TButton
        Left = 432
        Top = 136
        Width = 137
        Height = 25
        Caption = 'Enviar A'#231#227'o'
        Default = True
        TabOrder = 19
        OnClick = btnEnviarAcaoClick
      end
      object btnEnviarJogo: TButton
        Left = 432
        Top = 105
        Width = 137
        Height = 25
        Caption = 'Enviar Jogo'
        Default = True
        TabOrder = 20
        OnClick = btnEnviarJogoClick
      end
      object btnEnviarStiker: TButton
        Left = 3
        Top = 136
        Width = 137
        Height = 25
        Caption = 'Enviar Stiker'
        Default = True
        TabOrder = 21
        OnClick = btnEnviarStikerClick
      end
      object btnEnviarDado: TButton
        Left = 575
        Top = 109
        Width = 137
        Height = 25
        Caption = 'Enviar um Dado'
        Default = True
        TabOrder = 22
        OnClick = btnEnviarDadoClick
      end
      object btnEnviarPoll: TButton
        Left = 289
        Top = 136
        Width = 137
        Height = 25
        Caption = 'Enviar Enquete ou Quiz'
        Default = True
        TabOrder = 23
        OnClick = btnEnviarPollClick
      end
      object btnEnviarContato: TButton
        Left = 146
        Top = 136
        Width = 137
        Height = 25
        Caption = 'Enviar Contato'
        Default = True
        TabOrder = 24
        OnClick = btnEnviarContatoClick
      end
      object btnEnviarVenue: TButton
        Left = 432
        Top = 43
        Width = 137
        Height = 25
        Caption = 'Enviar Venue'
        Default = True
        TabOrder = 25
        OnClick = btnEnviarVenueClick
      end
      object btnEnviarLocalizacao: TButton
        Left = 575
        Top = 43
        Width = 137
        Height = 25
        Caption = 'Enviar Localiza'#231#227'o'
        Default = True
        TabOrder = 26
        OnClick = btnEnviarLocalizacaoClick
      end
      object btnEnviarNotaDeVideo: TButton
        Left = 146
        Top = 105
        Width = 137
        Height = 25
        Caption = 'Enviar Nota de Video'
        Default = True
        TabOrder = 27
        OnClick = btnEnviarNotaDeVideoClick
      end
      object btnEnviarVoz: TButton
        Left = 289
        Top = 105
        Width = 137
        Height = 25
        Caption = 'Enviar Voz'
        Default = True
        TabOrder = 28
        OnClick = btnEnviarVozClick
      end
      object btnEnviarVideo: TButton
        Left = 3
        Top = 105
        Width = 137
        Height = 25
        Caption = 'Enviar V'#237'deo'
        Default = True
        TabOrder = 29
        OnClick = btnEnviarVideoClick
      end
      object btnEnviarDocumento: TButton
        Left = 432
        Top = 74
        Width = 137
        Height = 25
        Caption = 'Enviar Documento'
        Default = True
        TabOrder = 30
        OnClick = btnEnviarDocumentoClick
      end
      object btnEnviaAudio: TButton
        Left = 289
        Top = 74
        Width = 137
        Height = 25
        Caption = 'Enviar Audio'
        Default = True
        TabOrder = 31
        OnClick = btnEnviaAudioClick
      end
      object btnEnviaFoto: TButton
        Left = 146
        Top = 74
        Width = 137
        Height = 25
        Caption = 'Enviar Foto'
        Default = True
        TabOrder = 32
        OnClick = btnEnviaFotoClick
      end
      object btnEnviaTexto: TButton
        Left = 3
        Top = 74
        Width = 137
        Height = 25
        Caption = 'Enviar Texto'
        Default = True
        TabOrder = 33
        OnClick = btnEnviaTextoClick
      end
    end
  end
  object InjectTelegramExceptionManagerUI1: TInjectTelegramExceptionManagerUI
    OnLog = InjectTelegramExceptionManagerUI1Log
    Left = 200
    Top = 302
  end
  object cuHttpClientSysNet1: TcuHttpClientSysNet
    Left = 520
    Top = 306
  end
  object AbrirArquivo: TOpenDialog
    Filter = 'Imagens|*.jpeg;*.jpg;*.gif;*.png;*.bmp'
    FilterIndex = 0
    Left = 632
    Top = 306
  end
  object InjectTelegramBot1: TInjectTelegramBot
    Logger = InjectTelegramExceptionManagerUI1
    HttpCore = cuHttpClientSysNet1
    OnReceiveRawData = InjectTelegramBot1ReceiveRawData
    OnSendData = InjectTelegramBot1SendData
    OnDisconect = InjectTelegramBot1Disconect
    Left = 56
    Top = 302
  end
  object BotManager1: TInjectTelegramBotManager
    Bot = InjectTelegramBot1
    SenhaADM = '1234'
    Simultaneos = 3
    TempoInatividade = 2
    OnStart = ServiceStart
    OnStop = ServiceStop
    OnMessage = ReceiverMessage
    OnChosenInlineResult = ChosenInlineResult
    OnCallbackQuery = ReceiverCallbackQuery
    Left = 368
    Top = 304
  end
  object tmrEscucha: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmrEscuchaTimer
    Left = 624
    Top = 416
  end
end
