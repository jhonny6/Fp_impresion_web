program ExemplosTInjectTelegram;
uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {Form1},
  uResourceString in 'uResourceString.pas',
  dataPpal in 'dataPpal.pas' {DMPPal: TDataModule};

{$R *.res}
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMPPal, DMPpal);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
